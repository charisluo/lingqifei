<!--#include file="Admin_createcommon.asp"-->
<%
Dim PW_Content
Set PW_Content = New Product
PW_Content.Init
tmpPageTitle = strPageTitle    '保存页面标题到临时变量中，以做为栏目及内容页循环生成时初始值
tmpNavPath = strNavPath
ProductID = Trim(Request("ProductID"))
Select Case Action
Case "CreateProduct"
    Call CreateProduct
Case "CreateClass"
    Call CreateClass
Case "CreateIndex"
    Call CreateIndex
Case "CreateProduct2"
    If AutoCreateType > 0 Then
        IsAutoCreate = True
        Call CreateProduct
        If ClassID > 0 Then
            ClassID = ParentPath & "," & ClassID
            Call CreateClass
        End If
        '在生成首页前，要将栏目ID和专题ID置为0
        ClassID = 0
        arrChildID = 0
        Call CreateIndex
        Call CreateSiteIndex     '生成网站首页
    End If
Case Else
    FoundErr = True
    ErrMsg = ErrMsg & "<li>参数错误！</li>"
End Select

If FoundErr = True Then
    Call WriteErrMsg(ErrMsg, ComeUrl)
ELse
	Call ShowProcess
End If

Response.Write "</body></html>"
Set PW_Content = Nothing
Call CloseConn


Sub CreateProduct()
    'On Error Resume Next
    Dim sql, strFields, ProductPath
    
    If IsAutoCreate = False Then
        Response.Write "<b>正在生成" & ChannelShortName & "页面……请稍候！<font color='red'>在此过程中请勿刷新此页面！！！</font></b><br>"
        Response.Flush
    End If
    sql = "select * from PW_Product where Deleted=" & PE_False &""

    Select Case CreateType
    Case 1 '选定的产品
        If IsValidID(ProductID) = False Then
            FoundErr = True
            ErrMsg = ErrMsg & "<li>请正确指定要生成的" & ChannelShortName & "ID</li>"
            Exit Sub
        End If
        If InStr(ProductID, ",") > 0 Then
            sql = sql & " and ProductID in (" & ProductID & ")"
        Else
            sql = sql & " and ProductID=" & ProductID
        End If
        strUrlParameter = "&ProductID=" & ProductID
    Case 2 '选定的栏目
        ClassID = PE_CLng(GetValue("ClassID"))
        If ClassID = 0 Then
            FoundErr = True
            ErrMsg = ErrMsg & "<li>请指定要生成的栏目ID</li>"
            Exit Sub
        End If
        Call GetClass
        If InStr(arrChildID, ",") > 0 Then
            sql = sql & " and ClassID in (" & arrChildID & ")"
        Else
            sql = sql & " and ClassID=" & ClassID
        End If
    Case 3 '所有产品
        
    Case 4 '最新的商品
        Dim TopNew
        TopNew = PE_CLng(GetValue("TopNew"))
        If TopNew <= 0 Then
            FoundErr = True
            ErrMsg = ErrMsg & "<li>请指定有效的数目！"
            Exit Sub
        End If
        sql = "select top " & TopNew & " * from PW_Product where Deleted=" & PE_False & " and InfoPurview='0'"
        strUrlParameter = "&TopNew=" & TopNew
    Case 5 '指定上市时间
        Dim BeginDate, EndDate
        BeginDate = GetValue("BeginDate")
        EndDate = GetValue("EndDate")
        If Not (IsDate(BeginDate) And IsDate(EndDate)) Then
            FoundErr = True
            ErrMsg = ErrMsg & "<li>请输入有效的日期！</li>"
            Exit Sub
        End If
        sql = sql & " and UpdateTime between #" & BeginDate & "# and #" & EndDate & "#"
        strUrlParameter = "&BeginDate=" & BeginDate & "&EndDate=" & EndDate
    Case 6 '指定ID范围
        Dim BeginID, EndID
        BeginID = GetValue("BeginID")
        EndID = GetValue("EndID")
        If Not (IsNumeric(BeginID) And IsNumeric(EndID)) Then
            FoundErr = True
            ErrMsg = ErrMsg & "<li>请输入数字！</li>"
            Exit Sub
        End If
        sql = sql & " and ProductID between " & BeginID & " and " & EndID
        strUrlParameter = "&BeginID=" & BeginID & "&EndID=" & EndID
    Case 9 '所有未生成的商品
        sql = "select top " & MaxPerPage_Create & " * from PW_Product where Deleted=" & PE_False
		sql = sql & " And InfoPurview='0' and (CreateTime is null or CreateTime<=UpdateTime)"
    Case Else
        Response.Write "参数错误！"
        Exit Sub
    End Select
    Set rsProduct = Server.CreateObject("ADODB.Recordset")
    rsProduct.Open sql, Conn, 1, 1
    If rsProduct.Bof And rsProduct.EOF Then
        TotalCreate = 0
		iTotalPage = 0
        rsProduct.Close
        Set rsProduct = Nothing
        Exit Sub
    Else
        If CreateType = 9 Then
			TotalCreate = PE_Clng(Conn.Execute("select count(*) from PW_Product where Deleted=" & PE_False & " and (CreateTime is null or CreateTime<=UpdateTime)")(0))
		Else
			TotalCreate = rsProduct.RecordCount
		End If
    End If
    strFileName = ChannelUrl_ASPFile & "/detial.asp"
	
    Call MoveRecord(rsProduct)
    Call ShowTotalCreate(ChannelItemUnit & ChannelShortName)
    Do While Not rsProduct.EOF
        FoundErr = False
        strPageTitle = tmpPageTitle
        strNavPath = tmpNavPath
        ProductID = rsProduct("ProductID")
        ClassID = rsProduct("ClassID")
        Call GetClass
		If temparticle <> article_template And article_template<>"" then
			strTemplate = GetTemplate(article_template) '分类的内容页
		Else
			strTemplate = GetTemplate(temparticle)'频道的内容页
		end If
        iCount = iCount + 1
		CurrentPage = 1
		ProductPath = ChannelUrl&"/"
		If CreateMultiFolder(ProductPath) = False Then
			Response.Write "请检查服务器。系统不能创建生成文件所需要的文件夹，"
			Exit Sub
		End If
		tmpFileName = ProductPath & GetItemFileName(rsProduct("UpdateTime"), ProductID)
		
		ProductName = Replace(Replace(Replace(Replace(rsProduct("ProductName") & "", "&nbsp;", " "), "&quot;", Chr(34)), "&gt;", ">"), "&lt;", "<")
		strHtml = strTemplate
		Call PW_Content.GetHtml_Product
		Call GetModelFront_Html()
		Call WriteToFile(tmpFileName, strHTML)
		Response.Write "<li>成功生成第 <font color='red'><b>" & iCount & " </b></font> " & ChannelItemUnit & ChannelShortName & "。&nbsp;&nbsp;ID：" & ProductID & " &nbsp;&nbsp;标题：" & rsProduct("ProductName") & " &nbsp;&nbsp;地址：<a href='" & tmpFileName & "' target='_blank'>" & tmpFileName & "</a></li>" & vbCrLf
		Response.Flush
		Conn.Execute ("update PW_Product set CreateTime=" & PE_Now & " where ProductID=" & ProductID)
        If Response.IsClientConnected = False Then Exit Do
        If iCount Mod MaxPerPage_Create = 0 Then Exit Do
        rsProduct.MoveNext
    Loop
    rsProduct.Close
    Set rsProduct = Nothing
End Sub

%>

		
		
		
		
		