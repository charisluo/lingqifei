<%
Const EnableStopInjection = False        '是否启用防SQL注入功能，True为启用，False为禁用

Const EnableSiteManageCode = False       '是否启用后台管理认证码 是： True  否： False
Const SiteManageCode = "MyWeb"  '后台管理认证码，您可以修改成您的管理员认证码：×××××××××

Const objName_FSO="Scripting.FileSystemObject"  '''fso的名称
'Const TemplateDir="Default"  ''模板的目录
Const TemplateDir="hh"  ''模板的目录

Const MaxPerPage_Create = 20   '生成HTML时，每页生成的数量，建议不要超过100，否则可能会导致页面超时
Const SleepTime = 3            '每页生成完毕后，暂停时间，单位为秒。如果为0，则不暂停，生成当前页面后马上跳转到下一页继续生成。建议设置为3－10
%>
