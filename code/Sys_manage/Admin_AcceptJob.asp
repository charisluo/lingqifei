<!--#include file="Admin_Common.asp"-->
<%

Const NeedCheckComeUrl = True   '是否需要检查外部访问

Const PurviewLevel_Channel = 0   '0--不检查,1检查
Const PurviewLevel_Others = "Job"   '其他权限
Dim ID
ID=GetValue("ID")
If IsValidID(ID) = False Then
    ID = ""
End If
FileName = "Admin_AcceptJob.asp"

Response.Write "<html><head><title>人才应聘管理</title>" & vbCrLf
Response.Write "<meta http-equiv='Content-Type' content='text/html; charset=gb2312'>" & vbCrLf
Response.Write "<link href='Admin_Style.css' rel='stylesheet' type='text/css'>" & vbCrLf
Response.Write "</head>" & vbCrLf
Response.Write "<body leftmargin='2' topmargin='0' marginwidth='0' marginheight='0'>" & vbCrLf
Response.Write "<table width='100%' border='0' align='center' cellpadding='2' cellspacing='1' class='border'>" & vbCrLf
call ShowPageTitle("人 才 应 聘 管 理")

Response.Write "<table width='100%' border='0' align='center' cellpadding='2' cellspacing='1' class='border'>" & vbCrLf
Response.Write "  <tr class='tdbg'>" & vbCrLf
Response.Write "    <td width='70' height='30'><strong>管理导航：</strong></td>" & vbCrLf
Response.Write "    <td>"
Response.Write "    <a href='Admin_Job.asp'>人才招聘管理首页</a>&nbsp;|&nbsp;"
Response.Write "    <a href='Admin_Job.asp?Action=Add'>添加招聘信息</a>&nbsp;|&nbsp;"
Response.Write "    <a href='Admin_AcceptJob.asp?Action=Add'>查看应聘信息</a>&nbsp;"
Response.Write "    </td>" & vbCrLf
Response.Write "  </tr>" & vbCrLf
Response.Write "  </table>" & vbCrLf

Select Case Action
	Case "Show"
		Call Show
	Case "Del"
		Call Del
	Case Else
		Call main
End Select

If FoundErr = True Then
    Call WriteErrMsg(ErrMsg, ComeUrl)
End If
Response.Write "</body></html>"
Call CloseConn

Sub main
	Dim Rs, sql
	Call ShowJS_Main("应聘")
    Response.Write "<br><table width='100%'><tr><td align='left'>您现在的位置：<a href='Admin_AcceptJob.asp'>人才应聘</a>&nbsp;&gt;&gt;&nbsp;应聘列表</td></tr></table>"& vbCrLf
	Response.Write "<table width='100%' border='0' cellpadding='0' cellspacing='0'><tr>"
    Response.Write "  <form name='myform' method='Post' action='Admin_AcceptJob.asp' onsubmit='return ConfirmDel();'>"&vbcrlf
    Response.Write "  <td><table class='border' border='0' cellspacing='1' width='100%' cellpadding='0'>"& vbCrLf
    Response.Write "  <tr class='title'>"& vbCrLf
    Response.Write "    <td width='30' height='22' align='center'><strong>选中</strong></td>"& vbCrLf
    Response.Write "    <td width='30' align='center'><strong>ID</strong></td>"& vbCrLf
    Response.Write "    <td align='center'><strong>姓名</strong></td>"& vbCrLf
    Response.Write "    <td align='center'><strong>应聘职位</strong></td>"& vbCrLf
    Response.Write "    <td align='center'><strong>性别</strong></td>"& vbCrLf
    Response.Write "    <td align='center'><strong>婚姻状况</strong></td>"& vbCrLf
    Response.Write "    <td align='center'><strong>学历</strong></td>"& vbCrLf
    Response.Write "    <td width='120' align='center'><strong>提交时间</strong></td>"& vbCrLf
    Response.Write "    <td width='80' height='22' align='center'><strong>操作</strong></td>"& vbCrLf
 	Response.Write "  </tr>"& vbCrLf
	Sql="Select * from PW_AcceptJob order by ID desc"
	Set Rs=server.CreateObject("Adodb.recordset")
	Rs.open Sql,conn,1,3
	If Rs.eof And Rs.bof Then
		Response.Write "<tr class='tdbg'><td colspan='20' align='center'><br>没有任何应聘信息！<br><br></td></tr>"& vbCrLf
	Else
       totalPut = Rs.RecordCount
        If CurrentPage < 1 Then
            CurrentPage = 1
        End If
        If (CurrentPage - 1) * MaxPerPage > totalPut Then
            If (totalPut Mod MaxPerPage) = 0 Then
                CurrentPage = totalPut \ MaxPerPage
            Else
                CurrentPage = totalPut \ MaxPerPage + 1
            End If
        End If
        If CurrentPage > 1 Then
            If (CurrentPage - 1) * MaxPerPage < totalPut Then
                Rs.Move (CurrentPage - 1) * MaxPerPage
            Else
                CurrentPage = 1
            End If
        End If
        Dim PersonNum
        PersonNum = 0
        Do While Not Rs.EOF
            Response.Write "        <tr class='tdbg' onmouseout=""this.className='tdbg'"" onmouseover=""this.className='tdbgmouseover'"">"& vbCrLf
            Response.Write "          <td align='center'>"& vbCrLf
            Response.Write "            <input name='ID' type='checkbox' onclick='unselectall()' id='ID' value='" & Rs("ID") & "'>"
            Response.Write "          </td>"& vbCrLf
			Response.Write "          <td align='center'>" & Rs("ID") & "</td>"& vbCrLf
			Response.write "		  <td>"& Rs("Name") &"</td>"
			Response.write "		  <td>"& Rs("JobName") &"</td>"
			Response.write "		  <td>"& Rs("Sex") &"</td>"
			Response.write "		  <td>"
			if Rs("Marryed") = true then
				Response.write "已婚"
			Else
				Response.write "未婚"
			End if
			Response.write "			</td>"
			Response.write "		  <td>"& Rs("Studydegree") &"</td>"
			Response.write "		  <td>"& Rs("Adddate") &"</td>"
			Response.write "		  <td width='80' align='center'>"
            Response.Write "			<a href='Admin_AcceptJob.asp?Action=Show&ID=" & Rs("ID") & "'>查看</a>&nbsp;"
            Response.Write "			<a href='Admin_AcceptJob.asp?Action=Del&ID=" & Rs("ID") & "' onClick=""return confirm('确定要删除此应聘吗？');"">删除</a>&nbsp;"
			Response.write "		  </td>"
            Response.Write "		</tr>"& vbCrLf
			Rs.movenext
            PersonNum = PersonNum + 1
            If PersonNum >= MaxPerPage Then Exit Do
		loop	
	End If
	Rs.close
	Set Rs=Nothing
    Response.Write "</table>"& vbCrLf
    Response.Write "<table width='100%' border='0' cellpadding='0' cellspacing='0'>"& vbCrLf
    Response.Write "  <tr>"& vbCrLf
    Response.Write "    <td width='130' height='30'><input name='chkAll' type='checkbox' id='chkAll' onclick='CheckAll(this.form)' value='checkbox'>选中所有的应聘</td><td>"
    Response.Write "<input type='submit' value='删除选定的应聘' name='submit' onClick=""document.myform.Action.value='Del'"" class='button'>&nbsp;&nbsp;"
    Response.Write "  </td></tr>"& vbCrLf
    Response.Write "</table>"& vbCrLf
    Response.Write "</td>"& vbCrLf
    Response.Write "</form></tr></table>"& vbCrLf
    If totalPut > 0 Then
        Response.Write ShowPage(FileName, totalPut, MaxPerPage, CurrentPage, True, True, "个应聘", True)
    End If
End Sub


Sub Show()
	Dim Rs
    If ID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请选择要查看的信息！</li>"
        Exit Sub
    Else
        ID = PE_CLng(ID)
    End If
	Set Rs=Conn.execute("Select * from PW_AcceptJob where ID="&ID)
    If Rs.BOF And Rs.EOF Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>找不到相对应的信息！</li>"
        Rs.Close
        Set Rs = Nothing
        Exit Sub
    End If
    Response.Write "<br><table width='100%'><tr><td align='left'>您现在的位置：<a href='Admin_AcceptJob.asp'>人才应聘</a>&nbsp;&gt;&gt;&nbsp;查看应聘资料</td></tr></table>"& vbCrLf
    Response.Write "  <table border='0' cellpadding='2' cellspacing='1' align='center' width='100%' class='border'>"& vbCrLf
    Response.Write "    <tr class='title'>"& vbCrLf
    Response.Write "      <td height='22' colspan='2' align='center'><strong>查看应聘信息</strong></td>"& vbCrLf
    Response.Write "    </tr>"& vbCrLf
    Response.Write "    <tr class='tdbg'>"& vbCrLf
    Response.Write "      <td colspan='4' align='left'>应聘职位："& Rs("JobName") &"</td>"& vbCrLf
    Response.Write "    </tr>"& vbCrLf	
    Response.Write "    <tr class='tdbg'>"& vbCrLf
    Response.Write "      <td width='10%' align='center'>姓名：</td>"& vbCrLf
	Response.write "	  <td width='40%' align='left'>&nbsp;"& Rs("Name") &"</td>"
    Response.Write "      <td width='10%' align='center'>性别：</td>"& vbCrLf
	Response.write "	  <td width='40%' align='left'>&nbsp;"& Rs("Sex") &"</td>"
    Response.Write "    </tr>"& vbCrLf	
    Response.Write "    <tr class='tdbg'>"& vbCrLf
    Response.Write "      <td width='10%' align='center'>出生日期：</td>"& vbCrLf
	Response.write "	  <td width='40%' align='left'>&nbsp;"& Rs("Birthday") &"</td>"
    Response.Write "      <td width='10%' align='center'>婚姻状况：</td>"& vbCrLf
	Response.write "		  <td width='40%' align='left'>"
	if Rs("Marryed") = true then
		Response.write "已婚"
	Else
		Response.write "未婚"
	End if
	Response.write "			</td>"
    Response.Write "    </tr>"& vbCrLf	
    Response.Write "    <tr class='tdbg'>"& vbCrLf
    Response.Write "      <td width='10%' align='center'>身高：</td>"& vbCrLf
	Response.write "	  <td width='40%' align='left'>&nbsp;"& Rs("Stature") &"</td>"
    Response.Write "      <td width='10%' align='center'>学历：</td>"& vbCrLf
	Response.write "	  <td width='40%' align='left'>&nbsp;"& Rs("Studydegree") &"</td>"
    Response.Write "    </tr>"& vbCrLf	
    Response.Write "    <tr class='tdbg'>"& vbCrLf
    Response.Write "      <td width='10%' align='center'>专业：</td>"& vbCrLf
	Response.write "	  <td width='40%' align='left'>&nbsp;"& Rs("Specialty") &"</td>"
    Response.Write "      <td width='10%' align='center'>毕业时间：</td>"& vbCrLf
	Response.write "	  <td width='40%' align='left'>&nbsp;"& Rs("GradTime") &"</td>"
    Response.Write "    </tr>"& vbCrLf	
    Response.Write "    <tr class='tdbg'>"& vbCrLf
    Response.Write "      <td align='center'>毕业院校：</td>"& vbCrLf
    Response.write "	  <td colspan='3' align='left'>&nbsp;"& Rs("Specialty") &"</td>"
    Response.Write "    </tr>"& vbCrLf
    Response.Write "    <tr class='tdbg'>"& vbCrLf
    Response.Write "      <td align='center'>现居地址：</td>"& vbCrLf
    Response.write "	  <td colspan='3' align='left'>&nbsp;"& Rs("Address") &"</td>"
    Response.Write "    </tr>"& vbCrLf	
    Response.Write "    <tr class='tdbg'>"& vbCrLf
    Response.Write "      <td width='10%' align='center'>联系电话：</td>"& vbCrLf
	Response.write "	  <td width='40%' align='left'>&nbsp;"& Rs("Phone") &"</td>"
    Response.Write "      <td width='10%' align='center'>Email：</td>"& vbCrLf
	Response.write "	  <td width='40%' align='left'>&nbsp;"& Rs("Email") &"</td>"
    Response.Write "    </tr>"& vbCrLf	
    Response.Write "    <tr class='tdbg'>"& vbCrLf
    Response.Write "      <td align='center'>教育经历：</td>"& vbCrLf
    Response.write "	  <td colspan='3' align='left'>&nbsp;"& Rs("Edulevel") &"</td>"
    Response.Write "    </tr>"& vbCrLf
    Response.Write "    <tr class='tdbg'>"& vbCrLf
    Response.Write "      <td align='center'>工作经历：</td>"& vbCrLf
    Response.write "	  <td colspan='3' align='left'>&nbsp;"& Rs("Experience") &"</td>"
    Response.Write "    </tr>"& vbCrLf
    Response.Write "  </table>"& vbCrLf	
    Response.Write "  <p align='center'>"& vbCrLf
    Response.Write "<form method='post' name='myform' action='Admin_AcceptJob.asp'>"& vbCrLf
    Response.Write "   <input name='ID' type='hidden' id='ID' value='" & ID & "'>"
    Response.Write "   <input name='Action' type='hidden' id='Action' value='Del'>"
    Response.Write "        <input name='Submit' type='submit' id='Submit' value=' 删 除 ' class='submit'>"
    Response.Write "        &nbsp;"
    Response.Write "        <input name='Cancel' type='button' id='Cancel' value=' 返 回 ' onClick=""window.location.href='Admin_AcceptJob.asp';"" style='cursor:hand;' class='Submit'>"
    Response.Write "   </p><br>"& vbCrLf
	Response.Write "</form>"& vbCrLf

End Sub


Sub Del()
	Dim SqlJob
    If ID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请选择要删除的应聘</li>"
    End If
	If FoundErr = True Then
		Exit sub
	End if
    If InStr(ID, ",") > 0 Then
        SqlJob = "Delete * from PW_AcceptJob where ID in (" & ID & ")"
    Else
        SqlJob = "Delete * from PW_AcceptJob where ID=" & ID
    End If
	Conn.execute(SqlJob)
    Call CloseConn
	Response.Redirect(ComeUrl)	
End Sub
%>