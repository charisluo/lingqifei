<!--#include file="../Sys_Start.asp"-->
<!--#include file="../include/PW_FSO.asp"-->
<!--#include file="../include/PW_Channel.asp"-->
<!--#include file="../include/PW_Class.asp"-->
<!--#include file="../include/PW_Common_Front.asp"-->
<!--#include file="../include/PW_models_front.asp"-->
<%
Dim ID
Dim strParameter,strList,strTemp
ID=PE_Clng(GetValue("ID"))
If IsValidID(ID) = False Then
	ID = ""
End If
strFileName="Job.asp"
Select Case Action
	Case "Show"
		Call showjob()
	case "Save"
		Call SaveAcc
	Case "Accept"
		Call Accept
	Case Else
		Call main()
End select
If FoundErr = True Then
	Call ShowErrMsg(ErrMsg, ComeUrl)
	Response.End()
End If

Response.Write "</body></html>"
Call CloseConn

Sub Main()
	strHtml = ReadFileContent(Template_Dir &"plus/job.html")
	Call ReplaceCommonLabel
	strHtml = PE_Replace(strHtml, "{$pagetitle}", "人才招聘_"&sitename)
	strHtml = PE_Replace(strHtml, "{$location}",strNavPath & "&nbsp;" & strNavLink & "&nbsp;人才招聘")
	strHtml = PE_Replace(strHtml, "{$showpage}",ShowPage(strFileName, totalPut, MaxPerPage, CurrentPage, True, True, "条记录",False))
	Call GetModelFront_Html
	Response.Write strHtml
End Sub

Sub showjob()
	Dim oRs
	if ID="" then
		FoundErr = True
		ErrMsg="请指定要查看招聘的ID！"
		exit sub
	End if
	strHtml = ReadFileContent(Template_Dir &"plus/job_show.html")
	Call ReplaceCommonLabel
	Call GetModelFront_Html
	strHtml = PE_Replace(strHtml, "{$pagetitle}", "人才招聘_"&sitename)
	strHtml = PE_Replace(strHtml,"{$location}",strNavPath & "&nbsp;" & strNavLink & "&nbsp;人才招聘")
	Set oRs=conn.execute("Select * from PW_job where JobID="&ID)
	if oRs.eof and oRs.bof then
		FoundErr = True
		ErrMsg="你要查看的信息不存在或已经被删除！"
		exit sub
	else
		strHtml = PE_Replace(strHtml,"{$jobname}",oRs("JobName"))
		strHtml = PE_Replace(strHtml,"{$jobsex}",oRs("JobSex"))
		strHtml = PE_Replace(strHtml,"{$jobage}",oRs("JobAge"))
		strHtml = PE_Replace(strHtml,"{$jobsalary}",oRs("JobSalary"))
		strHtml = PE_Replace(strHtml,"{$jobnum}",oRs("JobRequireNum"))
		strHtml = PE_Replace(strHtml,"{$jobaddress}",oRs("JobAddress"))
		strHtml = PE_Replace(strHtml,"{$jobtime}",oRs("JobDate"))
		strHtml = PE_Replace(strHtml,"{$jobdetial}",oRs("JobDetial"))
		strHtml = PE_Replace(strHtml,"{$accepturl}",InstallDir&"plus/job.asp?Action=Accept&ID="&oRs("JobID"))
	End if
	oRs.close
	set oRs=Nothing
	Response.Write strHtml
End Sub

Sub Accept
	Dim oRs
	if ID="" then
		FoundErr = True
		ErrMsg="参数错误，请返回重新查看！"
		exit sub
	End if
	strHtml = ReadFileContent(Template_Dir &"plus/accept_job.html")
	Call ReplaceCommonLabel
	Call GetModelFront_Html
	strHtml = PE_Replace(strHtml, "{$pagetitle}", "人才招聘_"&sitename)
	strHtml = PE_Replace(strHtml,"{$location}",strNavPath & "&nbsp;" & strNavLink & "&nbsp;人才招聘")
	Set oRs=conn.execute("Select * from PW_job where JobID="&ID)
	if oRs.eof and oRs.bof then
		FoundErr = True
		ErrMsg="没找到对应的职位！"
		exit sub
	else
		strHtml = PE_Replace(strHtml,"{$jobname}",oRs("JobName"))
	End if
	oRs.close
	set oRs=Nothing
	Response.Write strHtml
End Sub

Sub SaveAcc
	Dim Jobname,Name,Sex,Marryed,Birthday,Stature,School,Studydegree,Specialty,GradTime,Address,Edulevel,Experience,Phone,Email,CheckCode
	Dim oRs,Sql
	CheckCode = ReplaceBadChar(GetForm("CheckCode"))
    If CheckCode = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<br><li>验证码不能为空！</li>"
    End If
	If CheckCode <> Session("CheckCode") then
        FoundErr = True
        ErrMsg = ErrMsg & "<br><li>您输入的验证码和系统产生的不一致，请重新输入。</li>"		
	End If
	If Action <>"SaveAcc" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<br><li>本页面禁止从外部提交数据！</li>"	
	End If
    If FoundErr = True Then
        Exit Sub
    End If
	
	Jobname = ReplaceBadChar(GetForm("Jobname"))
	Name = ReplaceBadChar(GetForm("Name"))
	Sex = ReplaceBadChar(GetForm("Sex"))
	Marryed = ReplaceBadChar(GetForm("Marryed"))
	Birthday = ReplaceBadChar(GetForm("Birthday"))
	Stature = ReplaceBadChar(GetForm("Stature"))
	School = ReplaceBadChar(GetForm("School"))
	Studydegree = ReplaceBadChar(GetForm("Studydegree"))
	Specialty = ReplaceBadChar(GetForm("Specialty"))
	GradTime = ReplaceBadChar(GetForm("GradTime"))
	Address = ReplaceBadChar(GetForm("Address"))
	Edulevel = PE_HTMLEncode(GetForm("Edulevel"))
	Experience = PE_HTMLEncode(GetForm("Experience"))
	Phone = ReplaceBadChar(GetForm("Phone"))
	Email = ReplaceBadChar(GetForm("Email"))
	If Jobname="" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>职位名称不能为空！</li>"		
	End if
	If Name="" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>姓名不能为空！</li>"		
	End if	
	If Sex="" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请选择性别！</li>"		
	End if	
	If Marryed="" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请选择婚姻状况！</li>"		
	End if	
	If Birthday="" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请输入出生日期！</li>"		
	End if	
	If Stature="" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请输入身高！</li>"		
	End if	
	If Studydegree="" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<br><li>学历不能为空！</li>"		
	End if		
	If Experience="" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<br><li>工作经验不能为空！</li>"		
	End if	
	If Phone="" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<br><li>联系电话不能为空！</li>"		
	End if	
    If FoundErr = True Then
        Exit Sub
    End If
	Set oRs=Server.CreateObject("Adodb.recordset")
	Sql="Select * from PW_AcceptJob where 1=0 order by ID desc"
	oRs.open Sql,conn,1,3
	oRs.addnew
	oRs("JobName")=Jobname
	oRs("Name")=Name
	oRs("Sex")=Sex
	oRs("Birthday")=Birthday
	oRs("Stature")=Stature
	oRs("Marryed")=Marryed
	oRs("School")=School
	oRs("Studydegree")=Studydegree
	oRs("Specialty")=Specialty
	oRs("GradTime")=GradTime
	oRs("Address")=Address
	oRs("Edulevel")=Edulevel
	oRs("Experience")=Experience	
	oRs("Phone")=Phone
	oRs("Email")=Email	
	oRs("Adddate")=Date()
	oRs.update
	oRs.close
	Set oRs=Nothing
	Response.write "<script language='javascript'>alert('谢谢你对本站的支持！\n你的简历我们已经收到,我们会尽快和你取得联系！');location.href='job.asp'</script>"
End Sub

%>