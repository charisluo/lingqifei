<!--#include file="../Sys_Start.asp"-->
<!--#include file="../include/PW_FSO.asp"-->
<!--#include file="ChannelCofig.asp"-->
<!--#include file="../include/PW_Channel.asp"-->
<!--#include file="../include/PW_Common_Front.asp"-->
<!--#include file="../Include/PW_OrderFlow.asp"-->

<%
If UserID > 0 then
	GetUser(UserID)
End If
CartID = ReplaceBadChar(Trim(Request.Cookies("Cart" & Site_Sn)("CartID")))
ProductList = SelectCart(CartID)


If ProductList = "" Then       '如果购物车为空，转入提示界面
    ShowTips_CartIsEmpty = "<br><br>您好！目前您的购物车中没有任何商品，是不是<a href='index.asp'><font color='#FF0000'>继续购物</font></a>？<br><br>"
Else
    ShowTips_CartIsEmpty = ""
End If

strHtml = ReadFileContent(Template_Dir &"plus/payment.html")
Call ReplaceCommonLabel
strHtml = PE_Replace(strHtml, "{$pagetitle}", strPageTitle)
strHtml = PE_Replace(strHtml, "{$showtips_cartisempty}", ShowTips_CartIsEmpty)
strHtml = PE_Replace(strHtml, "{$truename}", TrueName)
strHtml = PE_Replace(strHtml, "{$u_address}", U_Address)
strHtml = PE_Replace(strHtml, "{$u_phone}", U_Phone)
strHtml = PE_Replace(strHtml, "{$u_fax}", U_Fax)
strHtml = PE_Replace(strHtml, "{$u_zipCode}", U_ZipCode)
strHtml = PE_Replace(strHtml, "{$u_email}", U_Email)


strHtml = PE_Replace(strHtml, "{$showcart}", ShowCart2())

Response.Write strHtml
Call CloseConn

%>