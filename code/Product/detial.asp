<!--#include file="CommonCode.asp"-->
<%
ProductID = PE_CLng(GetValue("ProductID"))
If ProductID = 0 Then
    FoundErr = True
    ErrMsg = ErrMsg & "<li>请指定ProductID！</li>"
End If
If FoundErr = True Then
    Call WriteErrMsg(ErrMsg, ComeUrl)
    Response.End
End If

Dim sql

Conn.Execute ("update PW_Product set Hits=Hits+1 where ProductID=" & ProductID)

sql = "select * from PW_Product where Deleted=" & PE_False & " and ProductID=" & ProductID & " and ChannelID=" & ChannelID & ""
Set rsProduct = Conn.Execute(sql)
If rsProduct.BOF And rsProduct.EOF Then
    FoundErr = True
    ErrMsg = ErrMsg & "<li>你要找的" & ChannelShortName & "不存在，或者已经被管理员删除！</li>"
Else
    ClassID = rsProduct("ClassID")
    If ClassID > 0 Then
        Call GetClass
	else
		article_template = temparticle
    End If
End If
If FoundErr = True Then
    Call WriteErrMsg(ErrMsg, ComeUrl)
    rsProduct.Close
    Set rsProduct = Nothing
    Response.End
End If

ProductName = Replace(Replace(Replace(Replace(rsProduct("ProductName") & "", "&nbsp;", " "), "&quot;", Chr(34)), "&gt;", ">"), "&lt;", "<")
strHtml = GetTemplate(article_template)
Call PW_Content.GetHtml_Product
Call GetModelFront_Html()
Response.Write strHtml
rsProduct.Close
Set rsProduct = Nothing
Set PW_Content = Nothing
Call CloseConn

%>