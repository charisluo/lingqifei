<!--#include file="CommonCode.asp"-->
<%
'**************************************************************

PhotoID = PE_CLng(GetValue("PhotoID"))
If PhotoID = 0 Then
    FoundErr = True
    ErrMsg = ErrMsg & "<li>请指定PhotoID！</li>"
End If
If FoundErr = True Then
    Call WriteErrMsg(ErrMsg, ComeUrl)
    Response.End
End If

Dim sql
ItemID = PhotoID
    
sql = "select * from PW_Photo where Deleted=" & PE_False & " and PhotoID=" & PhotoID & " and ChannelID=" & ChannelID & ""
Set rsPhoto = Conn.Execute(sql)
If rsPhoto.BOF And rsPhoto.EOF Then
    FoundErr = True
    ErrMsg = ErrMsg & "<li>你要找的" & ChannelShortName & "不存在，或者已经被管理员删除！</li>"
Else
    ClassID = rsPhoto("ClassID")
    If ClassID > 0 Then
        Call GetClass
	else
		article_template = temparticle
    End If
End If
If FoundErr = True Then
    Call WriteErrMsg(ErrMsg, ComeUrl)
    rsPhoto.Close
    Set rsPhoto = Nothing
    Response.End
End If
    
Dim sqlHits, LastHitTime
sqlHits = "update PW_Photo set Hits=Hits+1"
sqlHits = sqlHits & " where PhotoID=" & PhotoID
Conn.Execute (sqlHits)

PhotoName = Replace(Replace(Replace(Replace(rsPhoto("PhotoName") & "", "&nbsp;", " "), "&quot;", Chr(34)), "&gt;", ">"), "&lt;", "<")

strHtml = GetTemplate(article_template)
Call PW_Content.GetHtml_Photo
Call PW_Content.ReplaceViewPhoto
Call GetModelFront_Html()
Response.Write strHtml

rsPhoto.Close
Set rsPhoto = Nothing
Set PW_Content = Nothing
Call CloseConn
%>
