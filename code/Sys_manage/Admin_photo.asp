<!--#include file="Admin_Common.asp"-->
<!--#include file="../Include/PW_FSO.asp"-->
<!--#include file="../PW_Editor/fckeditor.asp"-->
<%
Const NeedCheckComeUrl = True   '是否需要检查外部访问
Const PurviewLevel_Channel = 1   '0--不检查，1 检查
Const PurviewLevel_Others = ""   '其他权限


Dim ManageType
Dim tClass, ClassName, RootID, ParentID, Depth, ParentPath, Child, arrChildID
Dim PhotoID
Dim ClassID, OnTop, IsElite, IsHot


If ChannelID = 0 Then
    Response.Write "频道参数丢失！"
    FoundErr = True
    Response.End
End If


If ModuleType <> 4 Then
    FoundErr = True
    Response.Write "<li>指定的频道ID不对！</li>"
    Response.End
End If



ManageType = GetValue("ManageType")
OnTop = GetValue("OnTop")
IsElite = GetValue("IsElite")
IsHot = GetValue("IsHot")
ClassID = PE_CLng(GetValue("ClassID"))
PhotoID = GetValue("PhotoID")

If Action = "" Then
    Action = "Manage"
End If
If IsValidID(PhotoID) = False Then
    PhotoID = ""
End If
FileName = "Admin_Photo.asp?ChannelID=" & ChannelID & "&Action=" & Action & "&ManageType=" & ManageType
strFileName = FileName & "&ClassID=" & ClassID & "&Field=" & strField & "&keyword=" & Keyword

Response.Write "<html><head><title>" & ChannelShortName & "管理</title>" & vbCrLf
Response.Write "<meta http-equiv='Content-Type' content='text/html; charset=gb2312'>" & vbCrLf
Response.Write "<link href='Admin_Style.css' rel='stylesheet' type='text/css'>" & vbCrLf
Response.Write "</head>" & vbCrLf
Response.Write "<body leftmargin='2' topmargin='0' marginwidth='0' marginheight='0'>" & vbCrLf
Response.Write "<table width='100%' border='0' align='center' cellpadding='2' cellspacing='1' class='border'>"
Dim strTitle
strTitle = ChannelName & "管理----"
Select Case Action
Case "Add"
    strTitle = strTitle & "添加" & ChannelShortName
Case "Modify"
    strTitle = strTitle & "修改" & ChannelShortName
Case "SaveAdd", "SaveModify"
    strTitle = strTitle & "保存" & ChannelShortName
Case "Preview", "Show"
    strTitle = strTitle & "预览" & ChannelShortName
Case "Manage"
    Select Case ManageType
    Case "Recyclebin"
        strTitle = strTitle & ChannelShortName & "回收站管理"
    Case Else
        strTitle = strTitle & ChannelShortName & "管理首页"
    End Select
End Select
Call ShowPageTitle(strTitle)

Response.Write "  <tr class='tdbg'>"
Response.Write "    <td width='70' height='30' ><strong>管理导航：</strong></td><td colspan='5'>"
Response.Write "<a href='Admin_Photo.asp?ChannelID=" & ChannelID & "&Status=9'>" & ChannelShortName & "管理首页</a>"
if Purview_Add=True then
	Response.Write "&nbsp;|&nbsp;<a href='Admin_Photo.asp?ChannelID=" & ChannelID & "&Action=Add&AddType=1&ClassID=" & ClassID & "'>添加" & ChannelShortName & "</a>"
end if
if Purview_Del = True then
    Response.Write "&nbsp;|&nbsp;<a href='Admin_Photo.asp?ChannelID=" & ChannelID & "&ManageType=Recyclebin'>" & ChannelShortName & "回收站管理</a>"
end if

if FoundInArr(FieldShow, "ClassID", ",") and Purview_Class=True then
	Response.Write "&nbsp;|&nbsp;<a href='Admin_Class.asp?ChannelID=" & ChannelID & "'>" & ChannelShortName & "分类管理</a>"
end if
Response.Write "</td></tr>" & vbCrLf
If Action = "Manage" Then
	if FoundInArr(FieldShow, "Property", ",") then
		Response.Write "<form name='form3' method='Post' action='" & strFileName & "'><tr class='tdbg'>"
		Response.Write "  <td width='70' height='30' ><strong>" & ChannelShortName & "选项：</strong></td>"
		Response.Write "<td>"
		Response.Write "<input name='OnTop' type='checkbox' onclick='submit()' value=""True"" " & IsRadioChecked(OnTop, "True") & "> 固顶" & ChannelShortName & "&nbsp;&nbsp;&nbsp;&nbsp;"
		Response.Write "<input name='IsElite' type='checkbox' onclick='submit()' value=""True"" " & IsRadioChecked(IsElite, "True") & "> 推荐" & ChannelShortName & "&nbsp;&nbsp;&nbsp;&nbsp;"
		Response.Write "<input name='IsHot' type='checkbox' onclick='submit()' value=""True"" " & IsRadioChecked(IsHot, "True") & "> 热门" & ChannelShortName
		Response.Write "</td></tr></form>" & vbCrLf
    End If
End If
Response.Write "</table>" & vbCrLf
strFileName = strFileName & "&OnTop=" & OnTop & "&IsElite=" & IsElite & "&IsHot=" & IsHot


Select Case Action
Case "Add"
    Call Add
Case "Modify"
    Call Modify
Case "SaveAdd", "SaveModify"
    Call SavePhoto
Case "SetOnTop", "CancelOnTop", "SetElite", "CancelElite"
    Call SetProperty
Case "Show"
    Call Show
Case "Del"
    Call Del
Case "ConfirmDel"
    Call ConfirmDel
Case "ClearRecyclebin"
    Call ClearRecyclebin
Case "Restore"
    Call Restore
Case "RestoreAll"
    Call RestoreAll
Case "Manage"
    Call main
End Select


If FoundErr = True Then
    Call WriteErrMsg(ErrMsg, ComeUrl)
End If
Response.Write "</body></html>"
Call CloseConn


Sub main()
    Dim rsPhotoList, sql, Querysql
    If ClassID > 0 Then
        Set tClass = Conn.Execute("select ClassName,RootID,ParentID,Depth,ParentPath,Child,arrChildID from PW_Class where ClassID=" & ClassID)
        If tClass.BOF And tClass.EOF Then
            FoundErr = True
            ErrMsg = ErrMsg & "<li>找不到指定的栏目</li>"
        Else
            ClassName = tClass(0)
            RootID = tClass(1)
            ParentID = tClass(2)
            Depth = tClass(3)
            ParentPath = tClass(4)
            Child = tClass(5)
            arrChildID = tClass(6)
        End If
        Set tClass = Nothing
    End If

    If FoundErr = True Then Exit Sub
	
    Call ShowJS_Main(ChannelShortName)
	if FoundInArr(FieldShow, "ClassID", ",") then
		Response.Write "<br><table width='100%' border='0' align='center' cellpadding='2' cellspacing='1' class='border'>"
		Response.Write "  <tr class='title'>"
		Response.Write "    <td height='22'>" & GetRootClass() & "</td>"
		Response.Write "  </tr>" & GetChild_Root() & ""
		Response.Write "</table>"
	End if
	Response.write "<br>"
    Select Case ManageType
    Case "Recyclebin"
        Call ShowContentManagePath(ChannelShortName & "回收站管理")
    Case Else
        Call ShowContentManagePath(ChannelShortName & "管理")
    End Select
    Response.Write "<table width='100%' border='0' cellpadding='0' cellspacing='2' class='border'>"
    Response.Write "<form name='myform' method='Post' action='Admin_Photo.asp' onsubmit='return ConfirmDel();'><tr>"
	If ClassID = -1 Or (ClassID > 0 And Child = 0) Then
		sql = "select top " & MaxPerPage & " P.ClassID,P.PhotoID,P.PhotoName,P.Keyword,P.Author,P.UpdateTime,P.PhotoThumb,P.Hits,P.OnTop,P.Elite from PW_Photo P "
	Else
		sql = "select top " & MaxPerPage & " P.ClassID,P.PhotoID,C.ClassName,P.PhotoName,P.Keyword,P.Author,P.UpdateTime,P.PhotoThumb,P.Hits,P.OnTop,P.Elite from PW_Photo P left join PW_Class C on P.ClassID=C.ClassID "
	End If
    Querysql = " where P.ChannelID=" & ChannelID
    If ManageType = "Recyclebin" Then
        Querysql = Querysql & " and P.Deleted=" & PE_True & ""
    Else
        Querysql = Querysql & " and P.Deleted=" & PE_False & ""
    End If
	If OnTop = "True" Then
		Querysql = Querysql & " and P.OnTop=" & PE_True & ""
	End If
	If IsElite = "True" Then
		Querysql = Querysql & " and P.Elite=" & PE_True & ""
	End If
	If IsHot = "True" Then
		Querysql = Querysql & " and P.Hits>=" & HitsOfHot & ""
	End If
	
    If ClassID <> 0 Then
        If Child > 0 Then
            Querysql = Querysql & " and P.ClassID in (" & arrChildID & ")"
        Else
            Querysql = Querysql & " and P.ClassID=" & ClassID
        End If
    End If
    If Keyword <> "" Then
    	Querysql = Querysql & " and P.PhotoName like '%" & Keyword & "%' "
    End If
	totalPut = PE_CLng(Conn.Execute("select Count(*) from PW_Photo P " & Querysql)(0))
    If CurrentPage < 1 Then
        CurrentPage = 1
    End If
    If (CurrentPage - 1) * MaxPerPage > totalPut Then
        If (totalPut Mod MaxPerPage) = 0 Then
            CurrentPage = totalPut \ MaxPerPage
        Else
            CurrentPage = totalPut \ MaxPerPage + 1
        End If
    End If
    If CurrentPage > 1 Then
    	Querysql = Querysql & " and P.PhotoID < (select min(PhotoID) from (select top " & ((CurrentPage - 1) * MaxPerPage) & " P.PhotoID from PW_Photo P " & Querysql & " order by P.PhotoID desc) as QueryPhoto)"
    End If
	sql = sql & Querysql & " order by P.PhotoID desc"
    Set rsPhotoList = Server.CreateObject("ADODB.Recordset")
    rsPhotoList.Open sql, Conn, 1, 1
    If rsPhotoList.BOF And rsPhotoList.EOF Then
	totalPut = 0
	Response.Write "<tr class='tdbg'><td colspan='20' align='center'><br>"& vbCrLf

	If ClassID > 0 Then
		Response.Write "此栏目及其子栏目中没有任何"
	Else
		Response.Write "没有任何"
	End If
	Response.Write ChannelShortName & "！"
    Response.Write "<br><br></td></tr>"& vbCrLf
    Else
        Dim PhotoNum
        PhotoNum = 0
        Do While Not rsPhotoList.EOF
            Response.Write "<td class='tdbg'><table width='100%'  cellpadding='0' cellspacing='0' onmouseout=""this.className='tdbg'"" onmouseover=""this.className='tdbgmouseover'"">"& vbCrLf
            Response.Write "<tr><td colspan='2' align='center'><a href='Admin_Photo.asp?ChannelID=" & ChannelID & "&Action=Show&PhotoID=" & rsPhotoList("PhotoID") & "'><img src='" & GetPhotoUrl(rsPhotoList("PhotoThumb")) & "' width='130' height='90' border='0'></a></td></tr>"
			If rsPhotoList("ClassID") <> ClassID And ClassID <> -1 Then
				Response.Write "<tr><td align='right'>栏目名称：</td><td><a href='" & FileName & "&ClassID=" & rsPhotoList("ClassID") & "'>["
				If rsPhotoList("ClassName") <> "" Then
					Response.Write rsPhotoList("ClassName")
				Else
					Response.Write "<font color='gray'>不属于任何栏目</font>"
				End If
				Response.Write "]</a></td></tr>"& vbCrLf
			End If
            Response.Write "<tr><td align='right'>" & ChannelShortName & "名称：</td><td>"
            Response.Write "<a href='Admin_Photo.asp?ChannelID=" & ChannelID & "&Action=Show&PhotoID=" & rsPhotoList("PhotoID") & "'"
            Response.Write " title='名&nbsp;&nbsp;&nbsp;&nbsp;称：" & rsPhotoList("PhotoName") & vbCrLf 
			if FoundInArr(FieldShow, "Author", ",") then
				Response.write "作&nbsp;&nbsp;&nbsp;&nbsp;者：" & rsPhotoList("Author") & vbCrLf
			end if
			if FoundInArr(FieldShow, "UpdateTime", ",") then
				Response.write "更新时间：" & rsPhotoList("UpdateTime") & vbCrLf
			end if
			if FoundInArr(FieldShow, "Hits", ",") then
				Response.write "点 击 数：" & rsPhotoList("Hits") & vbCrLf
			end if
			if FoundInArr(FieldShow, "Keyword", ",") then
				Response.write "关 键 字：" & Mid(rsPhotoList("Keyword"), 2, Len(rsPhotoList("Keyword")) - 2)
			end if
            Response.Write "'>" & rsPhotoList("PhotoName") & "</a></td></tr>"& vbCrLf
			if FoundInArr(FieldShow, "Hits", ",") then
				Response.Write "<tr><td align='right'>点 击 数：</td><td>" & rsPhotoList("Hits") & "</td></tr>"& vbCrLf
			end if
			if FoundInArr(FieldShow, "Property", ",") then
				Response.Write "<tr><td align='right'>" & ChannelShortName & "属性：</td><td>"& vbCrLf
				If rsPhotoList("OnTop") = True Then
					Response.Write "<font color=blue>顶</font>&nbsp;"
				Else
					Response.Write "&nbsp;&nbsp;&nbsp;"
				End If
				If rsPhotoList("Hits") >= HitsOfHot Then
					Response.Write "<font color=red>热</a>&nbsp;"
				Else
					Response.Write "&nbsp;&nbsp;&nbsp;"
				End If
				If rsPhotoList("Elite") = True Then
					Response.Write "<font color=green>荐</a>"
				Else
					Response.Write "&nbsp;&nbsp;"
				End If
				Response.Write "</td></tr>"& vbCrLf
 			end if
            Response.Write "<tr><td align='right'>操作选项：</td><td>"& vbCrLf
                Response.Write "<input name='PhotoID' type='checkbox' onclick='CheckItem(this,""TABLE"")' id='PhotoID' value='" & rsPhotoList("PhotoID") & "'>"
            Response.Write "</td></tr>"& vbCrLf
            Response.Write "<tr><td colspan='2' align='center'>"& vbCrLf
			If ManageType = "Recyclebin" Then
				If Purview_Del Then
						Response.Write "<a href='Admin_Photo.asp?ChannelID=" & ChannelID & "&Action=ConfirmDel&PhotoID=" & rsPhotoList("PhotoID") & "' onclick=""return confirm('确定要彻底删除此" & ChannelShortName & "吗？彻底删除后将无法还原！');"">彻底删除</a> "
						Response.Write "<a href='Admin_Photo.asp?ChannelID=" & ChannelID & "&Action=Restore&PhotoID=" & rsPhotoList("PhotoID") & "'>还原</a>"
				end if
            Else
				if Purview_Modify then
                    Response.Write "<a href='Admin_Photo.asp?ChannelID=" & ChannelID & "&Action=Modify&PhotoID=" & rsPhotoList("PhotoID") & "'>修改</a>&nbsp;"
				end if
				if Purview_Del then
                    Response.Write "<a href='Admin_Photo.asp?ChannelID=" & ChannelID & "&Action=Del&PhotoID=" & rsPhotoList("PhotoID") & "' onclick=""return confirm('确定要删除此" & ChannelShortName & "吗？删除后你还可以从回收站中还原。');"">删除</a>&nbsp;"
				end if
				if FoundInArr(FieldShow, "Property", ",") then
                    If rsPhotoList("OnTop") = False Then
                        Response.Write "<a href='Admin_Photo.asp?ChannelID=" & ChannelID & "&Action=SetOnTop&PhotoID=" & rsPhotoList("PhotoID") & "'>固顶</a>&nbsp;"
                    Else
                        Response.Write "<a href='Admin_Photo.asp?ChannelID=" & ChannelID & "&Action=CancelOnTop&PhotoID=" & rsPhotoList("PhotoID") & "'>解固</a>&nbsp;"
                    End If
                    If rsPhotoList("Elite") = False Then
                        Response.Write "<a href='Admin_Photo.asp?ChannelID=" & ChannelID & "&Action=SetElite&PhotoID=" & rsPhotoList("PhotoID") & "'>设为推荐</a>"
                    Else
                        Response.Write "<a href='Admin_Photo.asp?ChannelID=" & ChannelID & "&Action=CancelElite&PhotoID=" & rsPhotoList("PhotoID") & "'>取消推荐</a>"
                    End If
				end if
			end if
            Response.Write "</td></tr>"& vbCrLf
            Response.Write "</table></td>"& vbCrLf

            PhotoNum = PhotoNum + 1
            If PhotoNum Mod 4 = 0 Then
                Response.Write "</tr><tr>"
            End If
            If PhotoNum >= MaxPerPage Then Exit Do
            rsPhotoList.MoveNext
        Loop
    End If
    rsPhotoList.Close
    Set rsPhotoList = Nothing
    Response.Write "</table>"
    Response.Write "<table width='100%' border='0' cellpadding='0' cellspacing='0'>"& vbCrLf
    Response.Write "  <tr>"& vbCrLf
    Response.Write "    <td width='200' height='30'><input name='chkAll' type='checkbox' id='chkAll' onclick='CheckAll(this.form)' value='checkbox'>选中本页显示的所有" & ChannelShortName & "</td><td>"& vbCrLf
    Select Case ManageType
    Case "Recyclebin"
		If Purview_Del Then
            Response.Write "<input name='submit1' type='submit' id='submit1' onClick=""document.myform.Action.value='ConfirmDel'"" value=' 彻底删除 '>&nbsp;"
            Response.Write "<input name='Submit2' type='submit' id='Submit2' onClick=""document.myform.Action.value='ClearRecyclebin'"" value='清空回收站'>&nbsp;&nbsp;&nbsp;&nbsp;"
            Response.Write "<input name='Submit3' type='submit' id='Submit3' onClick=""document.myform.Action.value='Restore'"" value='还原选定的" & ChannelShortName & "'>&nbsp;"
            Response.Write "<input name='Submit4' type='submit' id='Submit4' onClick=""document.myform.Action.value='RestoreAll'"" value='还原所有" & ChannelShortName & "'>"
		end if
    Case Else
		If Purview_Del Then
            Response.Write "<input name='submit1' type='submit' value=' 批量删除 ' onClick=""document.myform.Action.value='Del'""> "
		end if
    End Select
    
    Response.Write "<input name='Action' type='hidden' id='Action' value=''>"
    Response.Write "<input name='ChannelID' type='hidden' id='ChannelID' value='" & ChannelID & "'>"
    Response.Write "  </td></tr>"& vbCrLf
    Response.Write "</table>"& vbCrLf
    Response.Write "</td>"& vbCrLf
    Response.Write "</form></tr></table>"& vbCrLf
    If totalPut > 0 Then
        Response.Write ShowPage(strFileName, totalPut, MaxPerPage, CurrentPage, True, True, ChannelItemUnit & ChannelShortName & "", True)
    End If
    Response.Write "<form method='Get' name='SearchForm' action='" & FileName & "'>"& vbCrLf
    Response.Write "<table width='100%' border='0' cellpadding='0' cellspacing='0' class='border'>"& vbCrLf
    Response.Write "  <tr class='tdbg'>"& vbCrLf
    Response.Write "   <td width='80' align='right'><strong>" & ChannelShortName & "搜索：</strong></td>"& vbCrLf
    Response.Write "   <td>"& vbCrLf
    Response.Write "<select name='Field' size='1'>"& vbCrLf
    Response.Write "<option value='PhotoName' selected>" & ChannelShortName & "名称</option>"& vbCrLf
    Response.Write "</select>"& vbCrLf
	if FoundInArr(FieldShow, "ClassID", ",") then
		Response.Write "<select name='ClassID'><option value=''>所有栏目</option>" & GetClass_Option(1,ClassID) & "</select>"& vbCrLf
    end if
    Response.Write "<input type='text' name='keyword'  size='20' value='关键字' maxlength='50' onFocus='this.select();'>"& vbCrLf
    Response.Write "<input type='submit' name='Submit'  value='搜索'>"& vbCrLf
    Response.Write "<input name='ManageType' type='hidden' id='ManageType' value='" & ManageType & "'>"
    Response.Write "<input name='ChannelID' type='hidden' id='ChannelID' value='" & ChannelID & "'>"
    Response.Write "</td></tr></table></form>"& vbCrLf
	if FoundInArr(FieldShow, "Property", ",") then
		Response.Write "<br><b>说明：</b><br>&nbsp;&nbsp;&nbsp;&nbsp;" & ChannelShortName & "属性中的各项含义：<font color=blue>顶</font>----固顶" & ChannelShortName & "，<font color=red>热</font>----热门" & ChannelShortName & "，<font color=green>荐</font>----推荐" & ChannelShortName & "<br><br>"
	end if
End Sub


Sub ShowJS_Photo()
    Response.Write "<script language = 'JavaScript'>" & vbCrLf
    Response.Write "function SelectPhoto(iType){" & vbCrLf
    Response.Write "  var arr=showModalDialog('Admin_SelectFile.asp?ChannelID=" & ChannelID & "&DialogType=photo', '', 'dialogWidth:820px; dialogHeight:600px; help: no; scroll: yes; status: no');" & vbCrLf
    Response.Write "  if(arr!=null){" & vbCrLf
    Response.Write "    var ss=arr.split('|');" & vbCrLf
    Response.Write "    var strPhotoUrl=ss[0];" & vbCrLf
    Response.Write "    if(iType==0){document.myform.PhotoThumb.value=ss[0];}" & vbCrLf
    Response.Write "    else{" & vbCrLf
    Response.Write "    var url='" & ChannelShortName & "地址'+(document.myform.PhotoUrl.length+1)+'|'+strPhotoUrl;" & vbCrLf
    Response.Write "    document.myform.PhotoUrl.options[document.myform.PhotoUrl.length]=new Option(url,url);}" & vbCrLf
    Response.Write "  }" & vbCrLf
    Response.Write "}" & vbCrLf
    Response.Write "function AddUrl(){" & vbCrLf
    Response.Write "  var thisurl='" & ChannelShortName & "地址'+(document.myform.PhotoUrl.length+1)+'|http://'; " & vbCrLf
    Response.Write "  var url=prompt('请输入" & ChannelShortName & "地址名称和链接，中间用“|”隔开：',thisurl);" & vbCrLf
    Response.Write "  if(url!=null&&url!=''){document.myform.PhotoUrl.options[document.myform.PhotoUrl.length]=new Option(url,url);}" & vbCrLf
    Response.Write "}" & vbCrLf
    Response.Write "function ModifyUrl(){" & vbCrLf
    Response.Write "  if(document.myform.PhotoUrl.length==0) return false;" & vbCrLf
    Response.Write "  var thisurl=document.myform.PhotoUrl.value; " & vbCrLf
    Response.Write "  if (thisurl=='') {alert('请先选择一个" & ChannelShortName & "地址，再点修改按钮！');return false;}" & vbCrLf
    Response.Write "  var url=prompt('请输入" & ChannelShortName & "地址名称和链接，中间用“|”隔开：',thisurl);" & vbCrLf
    Response.Write "  if(url!=thisurl&&url!=null&&url!=''){document.myform.PhotoUrl.options[document.myform.PhotoUrl.selectedIndex]=new Option(url,url);}" & vbCrLf
    Response.Write "}" & vbCrLf
    Response.Write "function DelUrl(){" & vbCrLf
    Response.Write "  if(document.myform.PhotoUrl.length==0) return false;" & vbCrLf
    Response.Write "  var thisurl=document.myform.PhotoUrl.value; " & vbCrLf
    Response.Write "  if (thisurl=='') {alert('请先选择一个" & ChannelShortName & "地址，再点删除按钮！');return false;}" & vbCrLf
    Response.Write "  document.myform.PhotoUrl.options[document.myform.PhotoUrl.selectedIndex]=null;" & vbCrLf
    Response.Write "}" & vbCrLf
    Response.Write "function CheckForm(){" & vbCrLf
	if FoundInArr(FieldShow, "Title", ",") then
		Response.Write "  if (document.myform.PhotoName.value==''){" & vbCrLf
		Response.Write "    alert('" & ChannelShortName & "名称不能为空！');" & vbCrLf
		Response.Write "    document.myform.PhotoName.focus();" & vbCrLf
		Response.Write "    return false;" & vbCrLf
		Response.Write "  }" & vbCrLf
	end if
	if FoundInArr(FieldShow, "Keyword", ",") then
		Response.Write "  if (document.myform.Keyword.value==''){" & vbCrLf
		Response.Write "    alert('关键字不能为空！');" & vbCrLf
		Response.Write "    document.myform.Keyword.focus();" & vbCrLf
		Response.Write "    return false;" & vbCrLf
		Response.Write "  }" & vbCrLf
	end if
	if FoundInArr(FieldShow, "PhotoThumb", ",") then
    Response.Write "  if (document.myform.PhotoThumb.value==''){" & vbCrLf
    Response.Write "    alert('缩略图地址不能为空！');" & vbCrLf
    Response.Write "    document.myform.PhotoThumb.focus();" & vbCrLf
    Response.Write "    return false;" & vbCrLf
    Response.Write "  }" & vbCrLf
	end if
	if FoundInArr(FieldShow, "PhotoUrl", ",") then
        Response.Write "  if(document.myform.PhotoUrl.length==0){" & vbCrLf
        Response.Write "    ShowTabs(0);" & vbCrLf
        Response.Write "    alert('" & ChannelShortName & "地址不能为空！');" & vbCrLf
        Response.Write "    document.myform.PhotoUrl.focus();" & vbCrLf
        Response.Write "    return false;" & vbCrLf
        Response.Write "  }" & vbCrLf
        Response.Write "  document.myform.PhotoUrls.value='';" & vbCrLf
        Response.Write "  if (document.myform.Action.value!='Preview'){" & vbCrLf
        Response.Write "    for(var i=0;i<document.myform.PhotoUrl.length;i++){" & vbCrLf
        Response.Write "      if (document.myform.PhotoUrls.value=='') document.myform.PhotoUrls.value=document.myform.PhotoUrl.options[i].value;" & vbCrLf
        Response.Write "      else document.myform.PhotoUrls.value+='$$$'+document.myform.PhotoUrl.options[i].value;" & vbCrLf
        Response.Write "    }" & vbCrLf
        Response.Write "  }" & vbCrLf
	end if
	if FoundInArr(FieldShow, "ClassID", ",") then
		Response.Write "  var obj=document.myform.ClassID;" & vbCrLf
		Response.Write "  var iCount=0;" & vbCrLf
		Response.Write "  for(var i=0;i<obj.length;i++){" & vbCrLf
		Response.Write "    if(obj.options[i].selected==true){" & vbCrLf
		Response.Write "      iCount=iCount+1;" & vbCrLf
		Response.Write "    }" & vbCrLf
		Response.Write "  }" & vbCrLf
		Response.Write "  if (iCount==0){" & vbCrLf
		Response.Write "    alert('请选择所属栏目！');" & vbCrLf
		Response.Write "    document.myform.ClassID.focus();" & vbCrLf
		Response.Write "    return false;" & vbCrLf
		Response.Write "  }" & vbCrLf
	End if
    Dim rsField
    Set rsField = Conn.Execute("select * from PW_Field where ChannelID=" & ChannelID & "")
    Do While Not rsField.EOF
        If rsField("ShowOnForm") = True And rsField("EnableNull") = False Then
			Response.Write "  if (document.myform."&rsField("FieldName")&".value==''){" & vbCrLf
			Response.Write "    alert('"&rsField("Title")&"不能为空！');" & vbCrLf
			Response.Write "    document.myform."&rsField("FieldName")&".focus();" & vbCrLf
			Response.Write "    return false;" & vbCrLf
			Response.Write "  }" & vbCrLf
        End If
        rsField.MoveNext
    Loop
    Response.Write "}" & vbCrLf
    Response.Write "</script>" & vbCrLf
End Sub


Sub Add()
    Call ShowJS_Photo
    
    Response.Write "<br><table width='100%'><tr><td align='left'>您现在的位置：<a href='Admin_Photo.asp?ChannelID=" & ChannelID & "'>" & ChannelName & "管理</a>&nbsp;&gt;&gt;&nbsp;添加" & ChannelShortName & "</td></tr></table>"
    Response.Write "<form method='POST' name='myform' onSubmit='return CheckForm();' action='Admin_Photo.asp' target='_self'>"

    Response.Write "<table width='100%' border='0' align='center' cellpadding='5' cellspacing='0' class='border'>"
    Response.Write "  <tr align='center'>"
    Response.Write "    <td class='tdbg' height='200' valign='top'>"
    Response.Write "      <table width='98%' border='0' cellpadding='2' cellspacing='1' bgcolor='#FFFFFF'>"
	if FoundInArr(FieldShow, "Title", ",") then
		Response.Write "          <tr class='tdbg'>"
		Response.Write "            <td width='120' align='right' class='tdbg5'>" & ChannelShortName & "名称：</td>"
		Response.Write "            <td><input name='PhotoName' type='text' value='' autocomplete='off' size='50' maxlength='255'> <font color='#FF0000'>*</font><input type='button' name='checksame' value='检查是否存在相同的" & ChannelShortName & "名' onclick=""showModalDialog('Admin_CheckSameTitle.asp?ModuleType=" & ModuleType & "&Title='+document.myform.PhotoName.value,'checksame','dialogWidth:350px; dialogHeight:250px; help: no; scroll: no; status: no');"">"
		Response.Write "</td>"
		Response.Write "          </tr>"
	end if
	if FoundInArr(FieldShow, "ClassID", ",") then
		Response.Write "          <tr class='tdbg'>" &vbcrlf
		Response.Write "            <td width='120' align='right' class='tdbg5'>所属栏目：</td>" &vbcrlf
		Response.Write "            <td>" &vbcrlf
		Response.Write "              <select name='ClassID'>" & GetClass_Option(1,ClassID) & "</select>" &vbcrlf
		Response.Write "            </td>" &vbcrlf
		Response.Write "          </tr>" &vbcrlf
	end if
	if FoundInArr(FieldShow, "Keyword", ",") then
		Response.Write "          <tr class='tdbg'>" &vbcrlf
		Response.Write "            <td width='120' align='right' class='tdbg5'>关键字：</td>" &vbcrlf
		Response.Write "            <td>" &vbcrlf
		Response.Write "              <input name='Keyword' type='text' style=""clear:both"" id='Keyword' value='" & Trim(Session("Keyword")) & "' size='50' maxlength='255'> <font color='#FF0000'>*</font> "
		Response.Write "              <font color='#0000FF'>用来查找相关" & ChannelShortName & "，可输入多个关键字，中间用<font color='#FF0000'>“|”</font>隔开。不能出现&quot;'&?;:()等字符。</font>"
		Response.Write "            </td>" &vbcrlf
		Response.Write "          </tr>" &vbcrlf
	end if
	if FoundInArr(FieldShow, "Author", ",") then
		Response.Write "          <tr class='tdbg'>" &vbcrlf
		Response.Write "            <td width='120' align='right' class='tdbg5'>" & ChannelShortName & "作者：</td>" &vbcrlf
		Response.Write "            <td>" &vbcrlf
		Response.Write "              <input name='Author' type='text' id='Author' value='" & Trim(Session("Author")) & "' size='50' maxlength='100'>" & GetAuthorList(AdminName)
		Response.Write "            </td>" &vbcrlf
		Response.Write "          </tr>" &vbcrlf
	end if
	if FoundInArr(FieldShow, "CopyFrom", ",") then
		Response.Write "          <tr class='tdbg'>" & vbCrLf
		Response.Write "            <td width='120' align='right' class='tdbg5'>" & ChannelShortName & "来源：</td>" & vbCrLf
		Response.Write "            <td>" & vbCrLf
		Response.Write "              <input name='CopyFrom' type='text' id='CopyFrom' value='" & Trim(Session("CopyFrom")) & "' size='50' maxlength='100'>" & GetCopyFromList()
		Response.Write "            </td>" & vbCrLf
		Response.Write "          </tr>" & vbCrLf
	end if
	Call ShowTabs_MyField_Add()
	if FoundInArr(FieldShow, "Intro", ",") then
		Response.Write "          <tr class='tdbg'>" & vbCrLf
		Response.Write "            <td width='120' align='right' class='tdbg5'>" & ChannelShortName & "简介：</td>" & vbCrLf
		Response.Write "            <td>" & vbCrLf
		Response.Write "              <textarea cols=""70"" rows=""3"" name='PhotoIntro'></textarea>"
		Response.Write "            </td>" & vbCrLf
		Response.Write "          </tr>" & vbCrLf
	end if
	if FoundInArr(FieldShow, "PhotoDetial", ",") then
		Response.Write "          <tr class='tdbg'>"
		Response.Write "            <td width='120' align='right' class='tdbg5'>" & ChannelShortName & "简介：</td>"
		Response.Write "            <td>"
		Call ShowFckEditor("Soft","","PhotoDetial","100%","400")
		Response.Write "          </tr>"
	end if
	if FoundInArr(FieldShow, "PhotoUrl", ",") then
        Response.Write "          <tr class='tdbg'>"
        Response.Write "            <td width='120' align='right' class='tdbg5'>缩略图：</td>"
        Response.Write "            <td>"
        Response.Write "              <input name='PhotoThumb' type='text' id='PhotoThumb' size='60' maxlength='200'>"
        Response.Write "              <input type='button' name='Button2' value='从已上传缩略图中选择' onclick='SelectPhoto(0)'>"
        Response.Write "            </td>"
        Response.Write "          </tr>"
		Response.Write "          <tr class='tdbg'>" & vbCrLf
		Response.Write "            <td width='120' align='right' class='tdbg5'></td>" & vbCrLf
		Response.Write "            <td><table><tr><td>上传缩略图：</td><td><iframe style='top:2px' id='UploadFiles' src='upload.asp?ChannelID=" & ChannelID & "&dialogtype=Photopic' frameborder=0 scrolling=no width='450' height='25'></iframe></td></tr></table></td>" & vbCrLf
		Response.Write "          </tr>" & vbCrLf
        Response.Write "          <tr class='tdbg'>"
        Response.Write "            <td width='120' align='right' class='tdbg5'>" & ChannelShortName & "地址：</td>"
        Response.Write "            <td>"
        Response.Write "              <table width='100%' border='0' cellpadding='0' cellspacing='0'>"
        Response.Write "                <tr>"
        Response.Write "                  <td width='410'>"
        Response.Write "                    <input type='hidden' name='PhotoUrls' value=''>"
        Response.Write "                    <select name='PhotoUrl' style='width:400;height:100' size='2' ondblclick='return ModifyUrl();'></select>"
        Response.Write "                  </td>"
        Response.Write "                  <td>"
        Response.Write "                    <input type='button' name='photoselect' value='从已上传" & ChannelShortName & "中选择' onclick='SelectPhoto(1)'><br><br>"
        Response.Write "                    <input type='button' name='addurl' value='添加外部地址' onclick='AddUrl();'><br>"
        Response.Write "                    <input type='button' name='modifyurl' value='修改当前地址' onclick='return ModifyUrl();'><br>"
        Response.Write "                    <input type='button' name='delurl' value='删除当前地址' onclick='DelUrl();'>"
        Response.Write "                  </td>"
        Response.Write "                </tr>"
        Response.Write "              </table>"
        Response.Write "            </td>"
        Response.Write "          </tr>"
        Response.Write "          <tr class='tdbg'>"
        Response.Write "            <td width='120' align='right' class='tdbg5'>上传" & ChannelShortName & "：</td>"
        Response.Write "            <td><iframe style='top:2px' id='UploadFiles' src='upload.asp?ChannelID=" & ChannelID & "&dialogtype=photos' frameborder=0 scrolling=no width='650' height='150'></iframe></td>"
        Response.Write "          </tr>"
	end if

	if FoundInArr(FieldShow, "Property", ",") then
		Response.Write "          <tr class='tdbg'>" &vbcrlf
		Response.Write "            <td width='120' align='right' class='tdbg5'>" & ChannelShortName & "属性：</td>" &vbcrlf
		Response.Write "            <td>" &vbcrlf
		Response.Write "              <input name='OnTop' type='checkbox' id='OnTop' value='yes'> 固顶" & ChannelShortName 
		Response.Write "              &nbsp;&nbsp;&nbsp;<input name='Hot' type='checkbox' id='Hot' value='yes' onclick=""javascript:document.myform.Hits.value='" & HitsOfHot & "'""> 热门" & ChannelShortName 
		Response.Write "              &nbsp;&nbsp;&nbsp;<input name='Elite' type='checkbox' id='Elite' value='yes'> 推荐" & ChannelShortName 
		Response.Write "            </td>" &vbcrlf
		Response.Write "          </tr>" &vbcrlf
	end if
	if FoundInArr(FieldShow, "Hits", ",") then
        Response.Write "          <tr class='tdbg'>" &vbcrlf
        Response.Write "            <td width='120' align='right' class='tdbg5'>点击数初始值：</td>" &vbcrlf
        Response.Write "            <td>" &vbcrlf
        Response.Write "              <input name='Hits' type='text' id='Hits' value='0' size='10' maxlength='10' style='text-align:center'>&nbsp;&nbsp; <font color='#0000FF'>这功能是提供给管理员作弊用的。不过尽量不要用呀！^_^</font>"
        Response.Write "            </td>" &vbcrlf
        Response.Write "          </tr>" &vbcrlf
	end if
	if FoundInArr(FieldShow, "UpdateTime", ",") then
		Response.Write "          <tr class='tdbg'>" &vbcrlf
		Response.Write "            <td width='120' align='right' class='tdbg5'>录入时间：</td>" &vbcrlf
		Response.Write "            <td>" &vbcrlf
		Response.Write "              <input name='UpdateTime' type='text' id='UpdateTime' value='" & Now() & "' maxlength='50'> 时间格式为“年-月-日 时:分:秒”，如：<font color='#0000FF'>2003-5-12 12:32:47</font>"
		Response.Write "            </td>" &vbcrlf
		Response.Write "          </tr>" &vbcrlf
	end if
    Response.Write "        </table>" &vbcrlf
    Response.Write "      </td>" &vbcrlf
    Response.Write "    </tr>" &vbcrlf
    Response.Write "  </table>" &vbcrlf
    Response.Write "  <p align='center'>"
    Response.Write "   <input name='Action' type='hidden' id='Action' value='SaveAdd'>"
    Response.Write "   <input name='ChannelID' type='hidden' id='ChannelID' value='" & ChannelID & "'>"
    Response.Write "   <input name='add' type='submit'  id='Add' value=' 添 加 ' onClick=""document.myform.Action.value='SaveAdd';document.myform.target='_self';"" style='cursor:hand;'>&nbsp; "
    Response.Write "   <input name='Cancel' type='button' id='Cancel' value=' 取 消 ' onClick=""window.location.href='Admin_Article.asp?ChannelID=" & ChannelID & "&Action=Manage';"" style='cursor:hand;'>"
    Response.Write "  </p><br>" &vbcrlf
    Response.Write "</form>" &vbcrlf
End Sub


Sub Modify()
    Dim rsPhoto, sql, tmpAuthor, tmpCopyFrom
    
    If PhotoID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请指定要修改的" & ChannelShortName & "ID</li>"
        Exit Sub
    Else
        PhotoID = PE_CLng(PhotoID)
    End If
    sql = "select * from PW_Photo where PhotoID=" & PhotoID & ""
    Set rsPhoto = Conn.Execute(sql)
    If rsPhoto.BOF And rsPhoto.EOF Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>找不到" & ChannelShortName & "</li>"
        rsPhoto.Close
        Set rsPhoto = Nothing
        Exit Sub
    End If

    ClassID = rsPhoto("ClassID")
    tmpAuthor = rsPhoto("Author")
    tmpCopyFrom = rsPhoto("CopyFrom")
    Call ShowJS_Photo
    Response.Write "<br><table width='100%'><tr><td align='left'>您现在的位置：<a href='Admin_Photo.asp?ChannelID=" & ChannelID & "'>" & ChannelName & "管理</a>&nbsp;&gt;&gt;&nbsp;修改" & ChannelShortName & "</td></tr></table>"
    Response.Write "<form method='POST' name='myform' onSubmit='return CheckForm();' action='Admin_Photo.asp' target='_self'>"
    Response.Write "<table width='100%' border='0' align='center' cellpadding='5' cellspacing='0' class='border'>"
    Response.Write "  <tr align='center'>"
    Response.Write "    <td class='tdbg' height='200' valign='top'>"
    Response.Write "      <table width='98%' border='0' cellpadding='2' cellspacing='1' bgcolor='#FFFFFF'>"
	if FoundInArr(FieldShow, "Title", ",") then
		Response.Write "          <tr class='tdbg'>"
		Response.Write "            <td width='120' align='right' class='tdbg5'>" & ChannelShortName & "名称：</td>"
		Response.Write "            <td><input name='PhotoName' type='text' value='" & rsPhoto("PhotoName") & "' autocomplete='off' size='50' maxlength='255'> <font color='#FF0000'>*</font><input type='button' name='checksame' value='检查是否存在相同的" & ChannelShortName & "名' onclick=""showModalDialog('Admin_CheckSameTitle.asp?ModuleType=" & ModuleType & "&Title='+document.myform.PhotoName.value,'checksame','dialogWidth:350px; dialogHeight:250px; help: no; scroll: no; status: no');"">"
		Response.Write "</td>"
		Response.Write "          </tr>"
	end if
	if FoundInArr(FieldShow, "ClassID", ",") then
		Response.Write "          <tr class='tdbg'>" &vbcrlf
		Response.Write "            <td width='120' align='right' class='tdbg5'>所属栏目：</td>" &vbcrlf
		Response.Write "            <td>" &vbcrlf
		Response.Write "              <select name='ClassID'>" & GetClass_Option(1,ClassID) & "</select>" &vbcrlf
		Response.Write "            </td>" &vbcrlf
		Response.Write "          </tr>" &vbcrlf
	end if
	if FoundInArr(FieldShow, "Keyword", ",") then
		Response.Write "          <tr class='tdbg'>" &vbcrlf
		Response.Write "            <td width='120' align='right' class='tdbg5'>关键字：</td>" &vbcrlf
		Response.Write "            <td>" &vbcrlf
		Response.Write "              <input name='Keyword' type='text' style=""clear:both"" id='Keyword' value='" & Mid(rsPhoto("Keyword"), 2, Len(rsPhoto("Keyword")) - 2) & "' size='50' maxlength='255'> <font color='#FF0000'>*</font> "
		Response.Write "              <font color='#0000FF'>用来查找相关" & ChannelShortName & "，可输入多个关键字，中间用<font color='#FF0000'>“|”</font>隔开。不能出现&quot;'&?;:()等字符。</font>"
		Response.Write "            </td>" &vbcrlf
		Response.Write "          </tr>" &vbcrlf
	end if
	if FoundInArr(FieldShow, "Author", ",") then
		Response.Write "          <tr class='tdbg'>" &vbcrlf
		Response.Write "            <td width='120' align='right' class='tdbg5'>" & ChannelShortName & "作者：</td>" &vbcrlf
		Response.Write "            <td>" &vbcrlf
		Response.Write "              <input name='Author' type='text' id='Author' value='" & tmpAuthor & "' size='50' maxlength='100'>" & GetAuthorList(AdminName)
		Response.Write "            </td>" &vbcrlf
		Response.Write "          </tr>" &vbcrlf
	end if
	if FoundInArr(FieldShow, "CopyFrom", ",") then
		Response.Write "          <tr class='tdbg'>" & vbCrLf
		Response.Write "            <td width='120' align='right' class='tdbg5'>" & ChannelShortName & "来源：</td>" & vbCrLf
		Response.Write "            <td>" & vbCrLf
		Response.Write "              <input name='CopyFrom' type='text' id='CopyFrom' value='" & tmpCopyFrom & "' size='50' maxlength='100'>" & GetCopyFromList()
		Response.Write "            </td>" & vbCrLf
		Response.Write "          </tr>" & vbCrLf
	end if
	Call ShowTabs_MyField_Modify(rsPhoto)
	if FoundInArr(FieldShow, "Intro", ",") then
		Response.Write "          <tr class='tdbg'>" & vbCrLf
		Response.Write "            <td width='120' align='right' class='tdbg5'>" & ChannelShortName & "来源：</td>" & vbCrLf
		Response.Write "            <td>" & vbCrLf
		Response.Write "              <textarea cols=""70"" rows=""3"" name=""PhotoIntro"">"& rsPhoto("PhotoIntro") &"</textarea>"
		Response.Write "            </td>" & vbCrLf
		Response.Write "          </tr>" & vbCrLf
	end if
	if FoundInArr(FieldShow, "PhotoDetial", ",") then
		Response.Write "          <tr class='tdbg'>"
		Response.Write "            <td width='120' align='right' class='tdbg5'>" & ChannelShortName & "简介：</td>"
		Response.Write "            <td>"
		Call ShowFckEditor("Soft",rsPhoto("PhotoDetial"),"PhotoDetial","100%","200")
		Response.Write "          </tr>"
	end if
	if FoundInArr(FieldShow, "PhotoUrl", ",") then
        Response.Write "          <tr class='tdbg'>"
        Response.Write "            <td width='120' align='right' class='tdbg5'>缩略图：</td>"
        Response.Write "            <td>"
        Response.Write "              <input name='PhotoThumb' type='text' id='PhotoThumb' size='60' maxlength='200' value='" & rsPhoto("PhotoThumb") & "'>"
        Response.Write "              <input type='button' name='Button2' value='从已上传缩略图中选择' onclick='SelectPhoto(0)'>"
        Response.Write "            </td>"
        Response.Write "          </tr>"
		Response.Write "          <tr class='tdbg'>" & vbCrLf
		Response.Write "            <td width='120' align='right' class='tdbg5'></td>" & vbCrLf
		Response.Write "            <td><table><tr><td>上传缩略图：</td><td><iframe style='top:2px' id='UploadFiles' src='upload.asp?ChannelID=" & ChannelID & "&dialogtype=Photopic' frameborder=0 scrolling=no width='450' height='25'></iframe></td></tr></table></td>" & vbCrLf
		Response.Write "          </tr>" & vbCrLf
        Response.Write "          <tr class='tdbg'>"
        Response.Write "            <td width='120' align='right' class='tdbg5'>" & ChannelShortName & "地址：</td>"
        Response.Write "            <td>"
        Response.Write "              <table width='100%' border='0' cellpadding='0' cellspacing='0'>"
        Response.Write "                <tr>"
        Response.Write "                  <td width='410'>"
        Response.Write "                    <input type='hidden' name='PhotoUrls' value=''>"
    Response.Write "                    <select name='PhotoUrl' style='width:400;height:100' size='2' ondblclick='return ModifyUrl();'>"
    Dim PhotoUrls, arrPhotoUrls, iTemp
    PhotoUrls = rsPhoto("PhotoUrl")
    If InStr(PhotoUrls, "$$$") > 1 Then
        arrPhotoUrls = Split(PhotoUrls, "$$$")
        For iTemp = 0 To UBound(arrPhotoUrls)
            Response.Write "<option value='" & arrPhotoUrls(iTemp) & "'>" & arrPhotoUrls(iTemp) & "</option>"
        Next
    Else
        Response.Write "<option value='" & PhotoUrls & "'>" & PhotoUrls & "</option>"
    End If
    Response.Write "                    </select>"
        Response.Write "                  </td>"
        Response.Write "                  <td>"
        Response.Write "                    <input type='button' name='photoselect' value='从已上传" & ChannelShortName & "中选择' onclick='SelectPhoto(1)'><br><br>"
        Response.Write "                    <input type='button' name='addurl' value='添加外部地址' onclick='AddUrl();'><br>"
        Response.Write "                    <input type='button' name='modifyurl' value='修改当前地址' onclick='return ModifyUrl();'><br>"
        Response.Write "                    <input type='button' name='delurl' value='删除当前地址' onclick='DelUrl();'>"
        Response.Write "                  </td>"
        Response.Write "                </tr>"
        Response.Write "              </table>"
        Response.Write "            </td>"
        Response.Write "          </tr>"
        Response.Write "          <tr class='tdbg'>"
        Response.Write "            <td width='120' align='right' class='tdbg5'>上传" & ChannelShortName & "：</td>"
        Response.Write "            <td><iframe style='top:2px' id='UploadFiles' src='upload.asp?ChannelID=" & ChannelID & "&dialogtype=photos' frameborder=0 scrolling=no width='650' height='150'></iframe></td>"
        Response.Write "          </tr>"
	end if

	if FoundInArr(FieldShow, "Property", ",") then
		Response.Write "          <tr class='tdbg'>" &vbcrlf
		Response.Write "            <td width='120' align='right' class='tdbg5'>" & ChannelShortName & "属性：</td>" &vbcrlf
		Response.Write "            <td>" &vbcrlf
		Response.Write "              <input name='OnTop' type='checkbox' id='OnTop' value='yes'"
		If rsPhoto("OnTop") = True Then Response.Write " checked"
		Response.Write "> 固顶" & ChannelShortName & "&nbsp;&nbsp;&nbsp;&nbsp;"
		Response.Write "              <input name='Hot' type='checkbox' id='Hot' value='yes' disabled> 热门" & ChannelShortName & "&nbsp;&nbsp;&nbsp;&nbsp;"
		Response.Write "              <input name='Elite' type='checkbox' id='Elite' value='yes'"
		If rsPhoto("Elite") = True Then Response.Write " checked"
		Response.Write "> 推荐" & ChannelShortName & "&nbsp;&nbsp;&nbsp;&nbsp;"
		Response.Write "            </td>" &vbcrlf
		Response.Write "          </tr>" &vbcrlf
	end if
	if FoundInArr(FieldShow, "Hits", ",") then
        Response.Write "          <tr class='tdbg'>" &vbcrlf
        Response.Write "            <td width='120' align='right' class='tdbg5'>点击数初始值：</td>" &vbcrlf
        Response.Write "            <td>" &vbcrlf
        Response.Write "              <input name='Hits' type='text' id='Hits' value='0' size='10' maxlength='10' style='text-align:center'>&nbsp;&nbsp; <font color='#0000FF'>这功能是提供给管理员作弊用的。不过尽量不要用呀！^_^</font>"
        Response.Write "            </td>" &vbcrlf
        Response.Write "          </tr>" &vbcrlf
	end if
	if FoundInArr(FieldShow, "UpdateTime", ",") then
		Response.Write "          <tr class='tdbg'>" &vbcrlf
		Response.Write "            <td width='120' align='right' class='tdbg5'>录入时间：</td>" &vbcrlf
		Response.Write "            <td>" &vbcrlf
		Response.Write "              <input name='UpdateTime' type='text' id='UpdateTime' value='" & Now() & "' maxlength='50'> 时间格式为“年-月-日 时:分:秒”，如：<font color='#0000FF'>2003-5-12 12:32:47</font>"
		Response.Write "            </td>" &vbcrlf
		Response.Write "          </tr>" &vbcrlf
	end if
    Response.Write "        </table>" &vbcrlf
    Response.Write "      </td>" &vbcrlf
    Response.Write "    </tr>" &vbcrlf
    Response.Write "  </table>" &vbcrlf
    Response.Write "  <p align='center'>"
    Response.Write "   <input name='Action' type='hidden' id='Action' value='SaveModify'>"
    Response.Write "   <input name='PhotoID' type='hidden' id='PhotoID' value='" & rsPhoto("PhotoID") & "'>"
    Response.Write "   <input name='ChannelID' type='hidden' id='ChannelID' value='" & ChannelID & "'>"
    Response.Write "   <input name='Save' type='submit' value='保存修改结果' onClick=""document.myform.Action.value='SaveModify';document.myform.target='_self';"" style='cursor:hand;'>&nbsp;"
    Response.Write "   <input name='Cancel' type='button' id='Cancel' value=' 取 消 ' onClick=""window.location.href='Admin_Photo.asp?ChannelID=" & ChannelID & "&Action=Manage';"" style='cursor:hand;'>"
    Response.Write "  </p><br>"
    Response.Write "</form>"
    rsPhoto.Close
    Set rsPhoto = Nothing
End Sub


Sub SavePhoto()
    Dim rsPhoto, sql, trs, i
    Dim PhotoID, ClassID, PhotoName, Keyword, Author, tAuthor, CopyFrom
    Dim PhotoIntro, PhotoDetial, PhotoThumb, PhotoUrl, UpdateTime

    PhotoID = GetForm("PhotoID")
    ClassID = GetForm("ClassID")
    PhotoName = GetForm("PhotoName")
    Keyword = GetForm("Keyword")
    Author = GetForm("Author")
    CopyFrom = GetForm("CopyFrom")
    PhotoIntro = GetForm("PhotoIntro")
	PhotoDetial = GetForm("PhotoDetial")
    PhotoThumb = GetForm("PhotoThumb")
    PhotoUrl = GetForm("PhotoUrls")
    UpdateTime = PE_CDate(GetForm("UpdateTime"))
	if FoundInArr(FieldShow, "ClassID", ",") then
		If ClassID = "" Or IsNull(ClassID) Or Not IsNumeric(ClassID) Then
			FoundErr = True
			ErrMsg = ErrMsg & "<li>未指定所属栏目！</li>"
		Else
			ClassID = PE_CLng(ClassID)
			Select Case ClassID
			Case -1
                ClassName = "不指定任何栏目"
                Depth = -1
                ParentPath = ""
			Case Else
				Set tClass = Conn.Execute("select ClassName,ClassType,Depth,ParentID,ParentPath,Child from PW_Class where ClassID=" & ClassID)
				If tClass.BOF And tClass.EOF Then
					FoundErr = True
					ErrMsg = ErrMsg & "<li>找不到指定的栏目！</li>"
				Else
					ClassName = tClass("ClassName")
					Depth = tClass("Depth")
					ParentPath = tClass("ParentPath")
					ParentID = tClass("ParentID")
					Child = tClass("Child")
				End If
            Set tClass = Nothing
			End Select
		end if
	else
		ClassID = -1
		Depth = -1
		ParentPath = ""
	end if
    If FoundErr = True Then Exit Sub
	if FoundInArr(FieldShow, "Title", ",") then
		If PhotoName = "" Then
			FoundErr = True
			ErrMsg = ErrMsg & "<li>" & ChannelShortName & "名称不能为空</li>"
		End If
	end if
	if FoundInArr(FieldShow, "Keyword", ",") then
		Keyword = ReplaceBadChar(Keyword)
		If Keyword = "" Then
			FoundErr = True
			ErrMsg = ErrMsg & "<li>请输入" & ChannelShortName & "关键字</li>"
		End If
	end if
	if FoundInArr(FieldShow, "PhotoThumb", ",") then
		If PhotoThumb = "" Then
			FoundErr = True
			ErrMsg = ErrMsg & "<li>缩略图地址不能为空</li>"
		End If
    End If
	if FoundInArr(FieldShow, "PhotoUrl", ",") then
		If PhotoUrl = "" Then
			FoundErr = True
			ErrMsg = ErrMsg & "<li>" & ChannelShortName & "地址不能为空</li>"
		End If
    End If
    Dim rsField
    Set rsField = Conn.Execute("select * from PW_Field where ChannelID=" & ChannelID & "")
    Do While Not rsField.EOF
        If rsField("EnableNull") = False  and rsField("ShowOnForm") = True Then
            If Trim(Request(rsField("FieldName"))) = "" Then
                FoundErr = True
                ErrMsg = ErrMsg & "<li>请输入" & rsField("Title") & "！</li>"
            End If
        End If
        rsField.MoveNext
    Loop
    
    If FoundErr = True Then
        Exit Sub
    End If

    PhotoName = PE_HTMLEncode(PhotoName)
    Keyword = Replace("|" & Keyword & "|","||","|")
    If Author = "" Then Author ="佚名"
    If CopyFrom = "" Then CopyFrom = "本站原创"
    Set rsPhoto = Server.CreateObject("adodb.recordset")
    If Action = "SaveAdd" Then
		PhotoID = GetNewID("PW_Photo", "PhotoID")
		sql = "select top 1 * from PW_Photo"
		rsPhoto.Open sql, Conn, 1, 3
			rsPhoto.addnew
			rsPhoto("PhotoID") = PhotoID
			rsPhoto("ChannelID") = ChannelID
    ElseIf Action = "SaveModify" Then
        If PhotoID = "" Then
            FoundErr = True
            ErrMsg = ErrMsg & "<li>不能确定PhotoID的值</li>"
        Else
            PhotoID = PE_CLng(PhotoID)
            sql = "select * from PW_Photo where PhotoID=" & PhotoID
            rsPhoto.Open sql, Conn, 1, 3
            If rsPhoto.BOF And rsPhoto.EOF Then
                FoundErr = True
                ErrMsg = ErrMsg & "<li>找不到此" & ChannelShortName & "，可能已经被其他人删除。</li>"
            End If
        End If
    End If
	rsPhoto("ClassID") = ClassID
	rsPhoto("PhotoName") = PhotoName
	rsPhoto("Keyword") = Keyword
	rsPhoto("Author") = Author
	rsPhoto("CopyFrom") = CopyFrom
	rsPhoto("PhotoIntro") = PhotoIntro
	rsPhoto("PhotoDetial") = PhotoDetial
	rsPhoto("PhotoThumb") = SetPhotoUrl(PhotoThumb)
	rsPhoto("PhotoUrl") = SetPhotoUrl(PhotoUrl)
	rsPhoto("Hits") = PE_CLng(GetForm("Hits"))
	rsPhoto("UpdateTime") = UpdateTime
	rsPhoto("OnTop") = PE_CBool(GetForm("OnTop"))
	rsPhoto("Elite") = PE_CBool(GetForm("Elite"))
	rsPhoto("Deleted") = False

    If Not (rsField.BOF And rsField.EOF) Then
        rsField.MoveFirst
        Do While Not rsField.EOF
			If FoundInArr(FieldShow, rsField("FieldName"), ",") then
				If GetForm(rsField("FieldName")) <> "" or rsField("EnableNull")=True  Then
					rsPhoto(Trim(rsField("FieldName"))) = GetForm(rsField("FieldName"))
				End If
			End if
            rsField.MoveNext
        Loop
    End If
	rsField.Close
    Set rsField = Nothing
	rsPhoto.Update
	rsPhoto.Close
    Set rsPhoto = Nothing

    Response.Write "<br><br>"
    Response.Write "<table class='border' align=center width='500' border='0' cellpadding='2' cellspacing='1'>"
    Response.Write "  <tr align=center>"
    Response.Write "    <td  height='22' colspan='3' align='center' class='title'> "
    If Action = "SaveAdd" Then
        Response.Write "<b>添加" & ChannelShortName & "成功</b>"
    Else
        Response.Write "<b>修改" & ChannelShortName & "成功</b>"
    End If
    Response.Write "    </td>"
    Response.Write "  </tr>"
    Response.Write "  <tr class='tdbg'> "
    Response.Write "    <td width='100' align='right' class='tdbg5'><strong>" & ChannelShortName & "名称：</td>"
    Response.Write "    <td width='250'>" & PE_HTMLEncode(PhotoName) & "</td>"
    Response.Write "  </tr>"
	if FoundInArr(FieldShow, "ClassID", ",") then
		Response.Write "        <tr class='tdbg'>"
		Response.Write "          <td width='100' align='right' class='tdbg5'><strong>所属栏目：</strong></td>"
		Response.Write "          <td width='400'>" & ShowClassPath() & "</td>"
		Response.Write "        </tr>"
	end if
	if FoundInArr(FieldShow, "Author", ",") then
		Response.Write "  <tr class='tdbg'> "
		Response.Write "    <td width='100' align='right' class='tdbg5'><strong>" & ChannelShortName & "作者：</td>"
		Response.Write "    <td width='250'>" & PE_HTMLEncode(Author) & "</td>"
		Response.Write "  </tr>"
	end if
	if FoundInArr(FieldShow, "Keyword", ",") then
		Response.Write "        <tr class='tdbg'>"
		Response.Write "          <td width='100' align='right' class='tdbg5'><strong>关 键 字：</strong></td>"
		Response.Write "          <td width='400'>" & Mid(Keyword, 2, Len(Keyword) - 2) & "</td>"
		Response.Write "        </tr>"
	end if
    Response.Write "  <tr class='tdbg'>"
    Response.Write "    <td height='40' colspan='4' align='center'>"
    Response.Write "【<a href='Admin_Photo.asp?ChannelID=" & ChannelID & "&Action=Modify&PhotoID=" & PhotoID & "'>修改此" & ChannelShortName & "</a>】&nbsp;"
    Response.Write "【<a href='Admin_Photo.asp?ChannelID=" & ChannelID & "&Action=Add&ClassID=" & ClassID & "'>继续添加" & ChannelShortName & "</a>】&nbsp;"
    Response.Write "【<a href='Admin_Photo.asp?ChannelID=" & ChannelID & "&Action=Manage&ClassID=" & ClassID & "'>" & ChannelShortName & "管理</a>】&nbsp;"
    Response.Write "【<a href='Admin_Photo.asp?ChannelID=" & ChannelID & "&Action=Show&PhotoID=" & PhotoID & "'>预览" & ChannelShortName & "内容</a>】"
    Response.Write "    </td>"
    Response.Write "  </tr>"
    Response.Write "</table>" & vbCrLf

    Session("Keyword") = Trim(Request("Keyword"))
    Session("Author") = Author
    Session("CopyFrom") = CopyFrom
	If UseCreateHTML > 0 And ObjInstalled_FSO = True And AutoCreateType = 1 Then
       Response.Write "<br><iframe id='CreatePhoto' width='100%' height='210' frameborder='0' src='Admin_CreatePhoto.asp?ChannelID=" & ChannelID & "&Action=CreatePhoto2&ClassID=" & ClassID & "&PhotoID=" & PhotoID & "&ShowBack=No'></iframe>"
    End If
End Sub



Sub SetProperty()
    If PhotoID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请先选定" & ChannelShortName & "！</li>"
        Exit Sub
    End If
    If Action = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>参数不足！</li>"
        Exit Sub
    End If
    
    Dim sqlProperty, rsProperty
    If InStr(PhotoID, ",") > 0 Then
        sqlProperty = "select * from PW_Photo where PhotoID in (" & PhotoID & ")"
    Else
        sqlProperty = "select * from PW_Photo where PhotoID=" & PhotoID
    End If
    Set rsProperty = Server.CreateObject("ADODB.Recordset")
    rsProperty.Open sqlProperty, Conn, 1, 3
    Do While Not rsProperty.EOF
		Select Case Action
		Case "SetOnTop"
			rsProperty("OnTop") = True
		Case "CancelOnTop"
			rsProperty("OnTop") = False
		Case "SetElite"
			rsProperty("Elite") = True
		Case "CancelElite"
			rsProperty("Elite") = False
		End Select
            rsProperty.Update
        rsProperty.MoveNext
    Loop
    rsProperty.Close
    Set rsProperty = Nothing
    Call WriteSuccessMsg("操作成功！", "Admin_Photo.asp?ChannelID=" & ChannelID)
	Call Refresh("Admin_Photo.asp?ChannelID=" & ChannelID,1)
End Sub

'******************************************************************************************
'以下为删除、清空、还原等操作使用的函数，各模块实现过程类似，修改时注意同时修改各模块内容。
'******************************************************************************************

Sub Del()
    If PhotoID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请先选定" & ChannelShortName & "！</li>"
        Exit Sub
    End If
    
    Dim sqlDel, rsDel
    sqlDel = "select PhotoID,Deleted from PW_Photo"
    If InStr(PhotoID, ",") > 0 Then
        sqlDel = sqlDel & " where PhotoID in (" & PhotoID & ") order by PhotoID"
    Else
        sqlDel = sqlDel & " where PhotoID=" & PhotoID
    End If
    Set rsDel = Server.CreateObject("ADODB.Recordset")
    rsDel.Open sqlDel, Conn, 1, 3
    Do While Not rsDel.EOF
		rsDel("Deleted") = True
		rsDel.Update
        rsDel.MoveNext
    Loop
    rsDel.Close
    Set rsDel = Nothing

    Call WriteSuccessMsg("操作成功！", "Admin_Photo.asp?ChannelID=" & ChannelID)
	Call Refresh("Admin_Photo.asp?ChannelID=" & ChannelID,1)
End Sub



Sub ConfirmDel()
    If PhotoID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请先选定" & ChannelShortName & "！</li>"
        Exit Sub
    End If
    
    Dim sqlDel, rsDel
    sqlDel = "select PhotoThumb,PhotoUrl from PW_Photo where PhotoID in (" & PhotoID & ")"
    Set rsDel = Conn.Execute(sqlDel)
    Do While Not rsDel.EOF
        Call DelUploadFiles(GetUploadFiles(GetPhotoUrl(rsDel("PhotoUrl")), GetPhotoUrl(rsDel("PhotoThumb"))))
        rsDel.MoveNext
    Loop
    rsDel.Close
    Set rsDel = Nothing
    Conn.Execute ("delete from PW_Photo where PhotoID in (" & PhotoID & ")")
    Conn.Execute ("delete from PW_Comment where ModuleType=" & ModuleType & " and InfoID in (" & PhotoID & ")")
    Call CloseConn
    Response.Redirect ComeUrl
End Sub


Sub ClearRecyclebin()
    
    Dim sqlDel, rsDel
    PhotoID = ""
    sqlDel = "select PhotoID,PhotoThumb,PhotoUrl from PW_Photo where Deleted=" & PE_True & " and ChannelID=" & ChannelID
    Set rsDel = Conn.Execute(sqlDel)
    Do While Not rsDel.EOF
        If PhotoID = "" Then
            PhotoID = rsDel(0)
        Else
            PhotoID = PhotoID & "," & rsDel(0)
        End If
        Call DelUploadFiles(GetUploadFiles(GetPhotoUrl(rsDel("PhotoUrl")), GetPhotoUrl(rsDel("PhotoThumb"))))
        rsDel.MoveNext
    Loop
    rsDel.Close
    Set rsDel = Nothing
    If PhotoID <> "" Then
        Conn.Execute ("delete from PW_Photo where Deleted=" & PE_True & " and ChannelID=" & ChannelID & "")
        Conn.Execute ("delete from PW_Comment where ModuleType=" & ModuleType & " and InfoID in (" & PhotoID & ")")
    End If
    Call CloseConn
    Response.Redirect ComeUrl
End Sub

Sub Restore()
    If PhotoID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请先选定" & ChannelShortName & "！</li>"
        Exit Sub
    End If
    
    Dim sqlDel, rsDel
    If InStr(PhotoID, ",") > 0 Then
        sqlDel = "select * from PW_Photo where PhotoID in (" & PhotoID & ")"
    Else
        sqlDel = "select * from PW_Photo where PhotoID=" & PhotoID
    End If
    Set rsDel = Server.CreateObject("ADODB.Recordset")
    rsDel.Open sqlDel, Conn, 1, 3
    Do While Not rsDel.EOF
        rsDel("Deleted") = False
        rsDel.Update
        rsDel.MoveNext
    Loop
    rsDel.Close
    Set rsDel = Nothing

    Call WriteSuccessMsg("操作成功！", "Admin_Photo.asp?ChannelID=" & ChannelID)
    Call Refresh("Admin_Photo.asp?ChannelID=" & ChannelID,1)
End Sub

Sub RestoreAll()
    Dim sqlDel, rsDel
    sqlDel = "select * from PW_Photo where Deleted=" & PE_True & " and ChannelID=" & ChannelID
    Set rsDel = Server.CreateObject("ADODB.Recordset")
    rsDel.Open sqlDel, Conn, 1, 3
    Do While Not rsDel.EOF
        rsDel("Deleted") = False
        rsDel.Update
        rsDel.MoveNext
    Loop
    rsDel.Close
    Set rsDel = Nothing
    Call WriteSuccessMsg("操作成功！", "Admin_Photo.asp?ChannelID=" & ChannelID)
    Call Refresh("Admin_Photo.asp?ChannelID=" & ChannelID,1)
End Sub



Function GetUploadFiles(PhotoUrls, PhotoThumb)
    Dim arrPhotoUrls, arrUrls, iTemp, strUrls
    strUrls = ""
    strUrls = strUrls & PhotoThumb
    arrPhotoUrls = Split(PhotoUrls, "$$$")
    For iTemp = 0 To UBound(arrPhotoUrls)
        arrUrls = Split(arrPhotoUrls(iTemp), "|")
        If UBound(arrUrls) = 1 Then
        	 strUrls = strUrls & "|" & arrUrls(1)
        End If
    Next
    GetUploadFiles = strUrls
End Function






%>