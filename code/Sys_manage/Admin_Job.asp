<!--#include file="Admin_Common.asp"-->
<!--#include file="../PW_Editor/fckeditor.asp"-->
<%

Const NeedCheckComeUrl = True   '是否需要检查外部访问

Const PurviewLevel_Channel = 0   '0--不检查,1检查
Const PurviewLevel_Others = "Job"   '其他权限
Dim JobID
JobID=GetValue("JobID")
If IsValidID(JobID) = False Then
    JobID = ""
End If
FileName = "Admin_Job.asp"

Response.Write "<html><head><title>人才招聘管理</title>" & vbCrLf
Response.Write "<meta http-equiv='Content-Type' content='text/html; charset=gb2312'>" & vbCrLf
Response.Write "<link href='Admin_Style.css' rel='stylesheet' type='text/css'>" & vbCrLf
Response.Write "</head>" & vbCrLf
Response.Write "<body leftmargin='2' topmargin='0' marginwidth='0' marginheight='0'>" & vbCrLf
Response.Write "<table width='100%' border='0' align='center' cellpadding='2' cellspacing='1' class='border'>" & vbCrLf
call ShowPageTitle("人 才 招 聘 管 理")

Response.Write "<table width='100%' border='0' align='center' cellpadding='2' cellspacing='1' class='border'>" & vbCrLf
Response.Write "  <tr class='tdbg'>" & vbCrLf
Response.Write "    <td width='70' height='30'><strong>管理导航：</strong></td>" & vbCrLf
Response.Write "    <td>"
Response.Write "    <a href='Admin_Job.asp'>人才招聘管理首页</a>&nbsp;|&nbsp;"
Response.Write "    <a href='Admin_Job.asp?Action=Add'>添加招聘信息</a>&nbsp;|&nbsp;"
Response.Write "    <a href='Admin_AcceptJob.asp?Action=Add'>查看应聘信息</a>&nbsp;"
Response.Write "    </td>" & vbCrLf
Response.Write "  </tr>" & vbCrLf
Response.Write "  </table>" & vbCrLf

Select Case Action
Case "Add"
    Call Add
Case "Modify"
    Call Modify
Case "SaveAdd", "SaveModify"
    Call SaveJob
Case "Del"
    Call Del
Case Else
    Call main
End Select
If FoundErr = True Then
    Call WriteErrMsg(ErrMsg, ComeUrl)
End If
Response.Write "</body></html>"
Call CloseConn

Sub main
	Call ShowJS_Main("招聘")
    Response.Write "<br><table width='100%'><tr><td align='left'>您现在的位置：<a href='Admin_Job.asp'>人才招聘</a>&nbsp;&gt;&gt;&nbsp;招聘列表</td></tr></table>"& vbCrLf
	Response.Write "<table width='100%' border='0' cellpadding='0' cellspacing='0'><tr>"
    Response.Write "  <form name='myform' method='Post' action='Admin_Job.asp' onsubmit='return ConfirmDel();'>"&vbcrlf
    Response.Write "  <td><table class='border' border='0' cellspacing='1' width='100%' cellpadding='0'>"& vbCrLf
    Response.Write "  <tr class='title'>"& vbCrLf
    Response.Write "    <td width='30' height='22' align='center'><strong>选中</strong></td>"& vbCrLf
    Response.Write "    <td width='30' align='center'><strong>ID</strong></td>"& vbCrLf
    Response.Write "    <td align='center'><strong>招聘职位</strong></td>"& vbCrLf
    Response.Write "    <td align='center'><strong>招聘人数</strong></td>"& vbCrLf
    Response.Write "    <td align='center'><strong>薪资待遇</strong></td>"& vbCrLf
    Response.Write "    <td width='60' align='center'><strong>状态</strong></td>"& vbCrLf
    Response.Write "    <td width='120' align='center'><strong>发布时间</strong></td>"& vbCrLf
    Response.Write "    <td width='80' height='22' align='center'><strong>操作</strong></td>"& vbCrLf
 	Response.Write "  </tr>"& vbCrLf
	Dim sqlJob,rsJob
    sqlJob = "select * from PW_Job order by JobID desc"
	Set rsJob=server.CreateObject("Adodb.recordset")
	rsJob.open sqlJob,conn,1,3
	If rsJob.BOF And rsJob.EOF Then
        Response.Write "<tr class='tdbg'><td colspan='20' align='center'><br>没有任何招聘信息！<br><br></td></tr>"& vbCrLf
    Else
       totalPut = rsJob.RecordCount
        If CurrentPage < 1 Then
            CurrentPage = 1
        End If
        If (CurrentPage - 1) * MaxPerPage > totalPut Then
            If (totalPut Mod MaxPerPage) = 0 Then
                CurrentPage = totalPut \ MaxPerPage
            Else
                CurrentPage = totalPut \ MaxPerPage + 1
            End If
        End If
        If CurrentPage > 1 Then
            If (CurrentPage - 1) * MaxPerPage < totalPut Then
                rsJob.Move (CurrentPage - 1) * MaxPerPage
            Else
                CurrentPage = 1
            End If
        End If
        Dim JobNum
        JobNum = 0
        Do While Not rsJob.EOF
            Response.Write "        <tr class='tdbg' onmouseout=""this.className='tdbg'"" onmouseover=""this.className='tdbgmouseover'"">"& vbCrLf
            Response.Write "          <td align='center'>"& vbCrLf
            Response.Write "            <input name='ID' type='checkbox' onclick='unselectall()' id='ID' value='" & rsJob("JobID") & "'>"
            Response.Write "          </td>"& vbCrLf
			Response.Write "      <td align='center'>" & rsJob("JobID") & "</td>"& vbCrLf
			Response.Write "      <td>&nbsp;<a href='Admin_Job.asp?Action=Modify&JobID=" & rsJob("JobID") & "'>" & rsJob("JobName") & "</a></td>"& vbCrLf
			Response.Write "      <td align='center'>" & rsJob("JobRequireNum") & "</td>"& vbCrLf
			Response.Write "      <td align='center'>" & rsJob("JobSalary") & "</td>"& vbCrLf
			Response.Write "      <td align='center'>"& vbCrLf
			If rsJob("JobShow")=True Then
				Response.write "<font color='Green'>激活</font>"
			Else
				Response.write "<font color='red'>未激活</font>"
			End if
			Response.Write "	  </td>"& vbCrLf
			Response.Write "      <td align='center'>" & rsJob("JobDate") & "</td>"& vbCrLf
			Response.Write "      <td width='80' align='center'>"& vbCrLf
            Response.Write "      <a href='Admin_Job.asp?Action=Modify&JobID=" & rsJob("JobID") & "'>修改</a>&nbsp;"
            Response.Write "      <a href='Admin_Job.asp?Action=Del&JobID=" & rsJob("JobID") & "' onClick=""return confirm('确定要删除此招聘吗？');"">删除</a>&nbsp;"
            Response.Write "      </td>"& vbCrLf
            Response.Write "    </tr>"& vbCrLf
			rsJob.movenext
            JobNum = JobNum + 1
            If JobNum >= MaxPerPage Then Exit Do
		loop
	End if
   	rsJob.close
	Set rsJob=nothing
    Response.Write "</table>"& vbCrLf
    Response.Write "<table width='100%' border='0' cellpadding='0' cellspacing='0'>"& vbCrLf
    Response.Write "  <tr>"& vbCrLf
    Response.Write "    <td width='130' height='30'><input name='chkAll' type='checkbox' id='chkAll' onclick='CheckAll(this.form)' value='checkbox'>选中所有的招聘</td><td>"
    Response.Write "<input type='submit' value='删除选定的招聘' name='submit' onClick=""document.myform.Action.value='Del'"" class='button'>&nbsp;&nbsp;"
    Response.Write "  </td></tr>"& vbCrLf
    Response.Write "</table>"& vbCrLf
    Response.Write "</td>"& vbCrLf
    Response.Write "</form></tr></table>"& vbCrLf
    If totalPut > 0 Then
        Response.Write ShowPage(FileName, totalPut, MaxPerPage, CurrentPage, True, True, "条招聘", True)
    End If

    Response.Write "<br><b>说明：</b><br>&nbsp;&nbsp;&nbsp;&nbsp;只有将招聘激活后才会在前台显示"& vbCrLf
    Response.Write "<br><br>"& vbCrLf
End Sub

Sub ShowJS_AddModify()
    Response.Write "<script language = 'JavaScript'>" & vbCrLf
    Response.Write "function CheckForm(){" & vbCrLf
    Response.Write "  if (document.myform.JobName.value==''){" & vbCrLf
    Response.Write "     alert('招聘职位不能为空！');" & vbCrLf
    Response.Write "     document.myform.JobName.focus();" & vbCrLf
    Response.Write "     return false;" & vbCrLf
    Response.Write "  }" & vbCrLf
    Response.Write "  if (document.myform.JobRequireNum.value==''){" & vbCrLf
    Response.Write "     alert('招聘人数不能为空！');" & vbCrLf
    Response.Write "     document.myform.JobRequireNum.focus();" & vbCrLf
    Response.Write "     return false;" & vbCrLf
    Response.Write "  }" & vbCrLf
    Response.Write "  if (document.myform.JobAge.value==''){" & vbCrLf
    Response.Write "     alert('年龄要求不能为空！');" & vbCrLf
    Response.Write "     document.myform.JobAge.focus();" & vbCrLf
    Response.Write "     return false;" & vbCrLf
    Response.Write "  }" & vbCrLf
    Response.Write "  if (document.myform.JobSalary.value==''){" & vbCrLf
    Response.Write "     alert('工作薪资不能为空！');" & vbCrLf
    Response.Write "     document.myform.JobSalary.focus();" & vbCrLf
    Response.Write "     return false;" & vbCrLf
    Response.Write "  }" & vbCrLf
    Response.Write "  if (document.myform.JobAddress.value==''){" & vbCrLf
    Response.Write "     alert('工作地点不能为空！');" & vbCrLf
    Response.Write "     document.myform.JobAddress.focus();" & vbCrLf
    Response.Write "     return false;" & vbCrLf
    Response.Write "  }" & vbCrLf
    Response.Write "  if (eWebEditor1.getHTML()==''){" & vbCrLf
    Response.Write "     alert('职位要求不能为空！');" & vbCrLf
    Response.Write "     return false;" & vbCrLf
    Response.Write "  }" & vbCrLf
    Response.Write "  return true;  " & vbCrLf
    Response.Write "}" & vbCrLf
    Response.Write "</script>" & vbCrLf
End Sub


Sub Add
	ShowJS_AddModify()
    Response.Write "<br><table width='100%'><tr><td align='left'>您现在的位置：<a href='Admin_Job.asp'>人才招聘</a>&nbsp;&gt;&gt;&nbsp;添加招聘</td></tr></table>"& vbCrLf
    Response.Write "<table width='100%' border='0' cellpadding='0' cellspacing='0'>" & vbCrLf
    Response.Write "  <tr>" & vbCrLf
    Response.Write "<form method='post' name='myform' onsubmit='return CheckForm()' action='Admin_Job.asp'>"& vbCrLf
    Response.Write "    <td>" & vbCrLf
    Response.Write "  <table border='0' cellpadding='2' cellspacing='1' align='center' width='100%' class='border'>"& vbCrLf
    Response.Write "    <tr class='title'>"& vbCrLf
    Response.Write "      <td height='22' colspan='2' align='center'><strong>添加招聘</strong></td>"& vbCrLf
    Response.Write "    </tr>"& vbCrLf
    Response.Write "    <tr class='tdbg'>"& vbCrLf
    Response.Write "      <td width='150' align='right'>招聘职位：</td>"& vbCrLf
    Response.Write "      <td>"& vbCrLf
    Response.Write "        <input type='text' name='JobName' id='JobName' size='30' maxlength='50' value=''> <font color='#FF0000'> *</font>"& vbCrLf
    Response.Write "      </td>"& vbCrLf
    Response.Write "    </tr>"& vbCrLf
    Response.Write "    <tr class='tdbg'>"& vbCrLf
    Response.Write "      <td width='150' align='right'>招聘人数：</td>"& vbCrLf
    Response.Write "      <td>"& vbCrLf
    Response.Write "        <input type='text' name='JobRequireNum' id='JobRequireNum' size='20' maxlength='10' value=''> <font color='#FF0000'> *</font>"& vbCrLf
    Response.Write "      </td>"& vbCrLf
    Response.Write "    </tr>"& vbCrLf
    Response.Write "    <tr class='tdbg'>"& vbCrLf
    Response.Write "      <td width='150' align='right'>性别要求：</td>"& vbCrLf
    Response.Write "      <td>"& vbCrLf
    Response.Write "        <input type='radio' value='0' name='JobSex' /> 男 <input type='radio' value='1' name='JobSex' /> 女 <input type='radio' value='2' checked name='JobSex' /> 男女不限"
    Response.Write "      </td>"& vbCrLf
    Response.Write "    </tr>"& vbCrLf
    Response.Write "    <tr class='tdbg'>"& vbCrLf
    Response.Write "      <td width='150' align='right'>年龄要求：</td>"& vbCrLf
    Response.Write "      <td>"& vbCrLf
    Response.Write "        <input type='text' name='JobAge' id='JobAge' size='30' maxlength='20' value=''> <font color='#FF0000'> *</font>"& vbCrLf
    Response.Write "      </td>"& vbCrLf
    Response.Write "    </tr>"& vbCrLf
    Response.Write "    <tr class='tdbg'>"& vbCrLf
    Response.Write "      <td width='150' align='right'>工作薪资：</td>"& vbCrLf
    Response.Write "      <td>"& vbCrLf
    Response.Write "        <input type='text' name='JobSalary' id='JobSalary' size='30' maxlength='20' value=''> <font color='#FF0000'> *</font>"& vbCrLf
    Response.Write "      </td>"& vbCrLf
    Response.Write "    </tr>"& vbCrLf
    Response.Write "    <tr class='tdbg'>"& vbCrLf
    Response.Write "      <td width='150' align='right'>工作地点：</td>"& vbCrLf
    Response.Write "      <td>"& vbCrLf
    Response.Write "        <input type='text' name='JobAddress' id='JobAddress' size='50' maxlength='200' value=''> <font color='#FF0000'> *</font>"& vbCrLf
    Response.Write "      </td>"& vbCrLf
    Response.Write "    </tr>"& vbCrLf
    Response.Write "    <tr class='tdbg'>"& vbCrLf
    Response.Write "      <td width='150' align='right'>职位要求：</td>"& vbCrLf
    Response.Write "      <td>"& vbCrLf
	Dim oFCKeditor
	Set oFCKeditor = New FCKeditor
	oFCKeditor.BasePath = InStallDir&"PW_Editor/" 
	oFCKeditor.ToolbarSet = "Default"
	oFCKeditor.Width = "100%"
	oFCKeditor.Height = "400"
	oFCKeditor.Value = "" 
	oFCKeditor.Create "JobDetial"
    Response.Write "      </td>"& vbCrLf
    Response.Write "    </tr>"& vbCrLf
    Response.Write "    <tr class='tdbg'>"& vbCrLf
    Response.Write "      <td width='150' align='right'>是否启用：</td>"& vbCrLf
    Response.Write "      <td>"& vbCrLf
    Response.Write "        <input type='radio' name='JobShow' value='1' checked /> 启用 <input type='radio' name='JobShow' value='0' /> 休眠"& vbCrLf
    Response.Write "      </td>"& vbCrLf
    Response.Write "    </tr>"& vbCrLf
    Response.Write "  </table>"& vbCrLf

    Response.Write "  <p align='center'>"& vbCrLf
    Response.Write "   <input name='Action' type='hidden' id='Action' value='SaveAdd'>"& vbCrLf
    Response.Write "        <input name='Submit' type='submit' id='Submit' value=' 添 加 ' class='submit'>"
    Response.Write "        &nbsp;"
    Response.Write "        <input name='Cancel' type='button' id='Cancel' value=' 取 消 ' onClick=""window.location.href='Admin_Job.asp';"" style='cursor:hand;' class='Submit'>"
    Response.Write "   </p><br>"& vbCrLf
	Response.Write "</form>"& vbCrLf
    Response.Write "    </td>" & vbCrLf
    Response.Write "  </tr>" & vbCrLf
    Response.Write "</table>" & vbCrLf
End Sub

Sub Modify
	Dim Rs
    If JobID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请指定要修改的ID！</li>"
        Exit Sub
    Else
        JobID = PE_CLng(JobID)
    End If
	Set Rs=Conn.execute("Select * from PW_Job where JobID="&JobID)
    If Rs.BOF And Rs.EOF Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>找不到相对应的招聘！</li>"
        Rs.Close
        Set Rs = Nothing
        Exit Sub
    End If
	ShowJS_AddModify()
    Response.Write "<br><table width='100%'><tr><td align='left'>您现在的位置：<a href='Admin_Job.asp'>人才招聘</a>&nbsp;&gt;&gt;&nbsp;修改招聘</td></tr></table>"& vbCrLf
    Response.Write "<form method='post' name='myform' onsubmit='return CheckForm()' action='Admin_Job.asp'>"& vbCrLf
    Response.Write "  <table border='0' cellpadding='2' cellspacing='1' align='center' width='100%' class='border'>"& vbCrLf
    Response.Write "    <tr class='title'>"& vbCrLf
    Response.Write "      <td height='22' colspan='2' align='center'><strong>添加招聘</strong></td>"& vbCrLf
    Response.Write "    </tr>"& vbCrLf
    Response.Write "    <tr class='tdbg'>"& vbCrLf
    Response.Write "      <td width='150' align='right'>招聘职位：</td>"& vbCrLf
    Response.Write "      <td>"& vbCrLf
    Response.Write "        <input type='text' name='JobName' id='JobName' size='30' maxlength='50' value='"& Rs("JobName") &"'> <font color='#FF0000'> *</font>"
    Response.Write "      </td>"& vbCrLf
    Response.Write "    </tr>"& vbCrLf
    Response.Write "    <tr class='tdbg'>"& vbCrLf
    Response.Write "      <td width='150' align='right'>招聘人数：</td>"& vbCrLf
    Response.Write "      <td>"& vbCrLf
    Response.Write "        <input type='text' name='JobRequireNum' id='JobRequireNum' size='20' maxlength='10' value='"& Rs("JobRequireNum") &"'> <font color='#FF0000'> *</font>"& vbCrLf
    Response.Write "      </td>"& vbCrLf
    Response.Write "    </tr>"& vbCrLf
    Response.Write "    <tr class='tdbg'>"& vbCrLf
    Response.Write "      <td width='150' align='right'>性别要求：</td>"& vbCrLf
    Response.Write "      <td>"& vbCrLf
    Response.Write "        <input type='radio' value='0' name='JobSex' "& IsRadioChecked(Rs("JobSex"),0) &" /> 男 <input type='radio' value='1' name='JobSex' "& IsRadioChecked(Rs("JobSex"),1) &" /> 女 <input type='radio' value='2' name='JobSex' "& IsRadioChecked(Rs("JobSex"),2) &" /> 男女不限"
    Response.Write "      </td>"& vbCrLf
    Response.Write "    </tr>"& vbCrLf
    Response.Write "    <tr class='tdbg'>"& vbCrLf
    Response.Write "      <td width='150' align='right'>年龄要求：</td>"& vbCrLf
    Response.Write "      <td>"& vbCrLf
    Response.Write "        <input type='text' name='JobAge' id='JobAge' size='30' maxlength='20' value='"& Rs("JobAge") &"'> <font color='#FF0000'> *</font>"& vbCrLf
    Response.Write "      </td>"& vbCrLf
    Response.Write "    </tr>"& vbCrLf
    Response.Write "    <tr class='tdbg'>"& vbCrLf
    Response.Write "      <td width='150' align='right'>工作薪资：</td>"& vbCrLf
    Response.Write "      <td>"& vbCrLf
    Response.Write "        <input type='text' name='JobSalary' id='JobSalary' size='30' maxlength='20' value='"& Rs("JobSalary") &"'> <font color='#FF0000'> *</font>"& vbCrLf
    Response.Write "      </td>"& vbCrLf
    Response.Write "    </tr>"& vbCrLf
    Response.Write "    <tr class='tdbg'>"& vbCrLf
    Response.Write "      <td width='150' align='right'>工作地点：</td>"& vbCrLf
    Response.Write "      <td>"& vbCrLf
    Response.Write "        <input type='text' name='JobAddress' id='JobAddress' size='50' maxlength='200' value='"& Rs("JobAddress") &"'> <font color='#FF0000'> *</font>"& vbCrLf
    Response.Write "      </td>"& vbCrLf
    Response.Write "    </tr>"& vbCrLf
    Response.Write "    <tr class='tdbg'>"& vbCrLf
    Response.Write "      <td width='150' align='right'>职位要求：</td>"& vbCrLf
    Response.Write "      <td>"& vbCrLf
	Dim oFCKeditor
	Set oFCKeditor = New FCKeditor
	oFCKeditor.BasePath = InStallDir&"PW_Editor/" 
	oFCKeditor.ToolbarSet = "Default"
	oFCKeditor.Width = "100%"
	oFCKeditor.Height = "400"
	oFCKeditor.Value = Rs("JobDetial")
	oFCKeditor.Create "JobDetial"
    Response.Write "      </td>"& vbCrLf
    Response.Write "    </tr>"& vbCrLf
    Response.Write "    <tr class='tdbg'>"& vbCrLf
    Response.Write "      <td width='150' align='right'>是否启用：</td>"& vbCrLf
    Response.Write "      <td>"& vbCrLf
    Response.Write "        <input type='radio' name='JobShow' value='1' "& IsRadioChecked(Rs("JobShow"),True) &" /> 启用 <input type='radio' name='JobShow' value='0' "& IsRadioChecked(Rs("JobShow"),False) &" /> 休眠"
    Response.Write "      </td>"& vbCrLf
    Response.Write "    </tr>"& vbCrLf
    Response.Write "  </table>"& vbCrLf

    Response.Write "  <p align='center'>"& vbCrLf
    Response.Write "   <input name='JobID' type='hidden' id='JobID' value='" & JobID & "'>"
    Response.Write "   <input name='Action' type='hidden' id='Action' value='SaveModify'>"
    Response.Write "        <input name='Submit' type='submit' id='Submit' value=' 修 改 ' class='submit'>"
    Response.Write "        &nbsp;"
    Response.Write "        <input name='Cancel' type='button' id='Cancel' value=' 取 消 ' onClick=""window.location.href='Admin_Job.asp';"" style='cursor:hand;' class='Submit'>"
    Response.Write "   </p><br>"& vbCrLf
	Response.Write "</form>"& vbCrLf
End Sub

Sub SaveJob
	Dim JobName,JobRequireNum,JobSex,JobAge,JobSalary,JobAddress,JobDetial,JobShow
	Dim oRs,Sql
    JobName = ReplaceBadChar(GetForm("JobName"))
	JobRequireNum = ReplaceBadChar(GetForm("JobRequireNum"))
	JobSex = PE_Clng(GetForm("JobSex"))
	JobAge = ReplaceBadChar(GetForm("JobAge"))
	JobSalary = ReplaceBadChar(GetForm("JobSalary"))
	JobAddress = ReplaceBadChar(GetForm("JobAddress"))
	JobDetial = GetForm("JobDetial")
	JobShow = PE_CBool(GetForm("JobShow"))
	If JobName="" Then
		FoundErr = True
		ErrMsg = ErrMsg & "<li>招聘职位不能为空！</li>"		
	End if
	If JobRequireNum="" Then
		FoundErr = True
		ErrMsg = ErrMsg & "<li>招聘人数不能为空！</li>"		
	End if
	If JobAge="" Then
		FoundErr = True
		ErrMsg = ErrMsg & "<li>年龄要求不能为空！</li>"		
	End if
	If JobSalary="" Then
		FoundErr = True
		ErrMsg = ErrMsg & "<li>工作薪资不能为空！</li>"		
	End if
	If JobAddress="" Then
		FoundErr = True
		ErrMsg = ErrMsg & "<li>工作地址不能为空！</li>"		
	End if
	If JobDetial="" Then
		FoundErr = True
		ErrMsg = ErrMsg & "<li>职位要求不能为空！</li>"		
	End if
    Set oRs = Server.CreateObject("adodb.recordset")
    If Action = "SaveAdd" Then
        sql = "select top 1 * from PW_Job"
        oRs.Open sql, Conn, 1, 3
        oRs.addnew
    ElseIf Action = "SaveModify" Then
        If JobID = "" Then
            FoundErr = True
            ErrMsg = ErrMsg & "<li>不能确定招聘ID</li>"
        Else
            sql = "select * from PW_Job where JobID=" & PE_CLng(JobID)
            Set oRs = Server.CreateObject("adodb.recordset")
            oRs.Open sql, Conn, 1, 3
            If oRs.BOF And oRs.EOF Then
                FoundErr = True
                ErrMsg = ErrMsg & "<li>找不到指定的招聘！</li>"
            End If
        End If
    End If
    If FoundErr = True Then
        Exit Sub
    End If
	oRs("JobName")=JobName
	oRs("JobRequireNum")=JobRequireNum
	oRs("JobSex")=JobSex
	oRs("JobAge")=JobAge
	oRs("JobSalary")=JobSalary
	oRs("JobAddress")=JobAddress
	oRs("JobDetial")=JobDetial
	oRs("JobShow")=JobShow
	oRs("JobDate")=Date()
	oRs.update
    oRs.Close
    Set oRs = Nothing
    Call CloseConn
	Response.redirect "Admin_Job.asp"
End Sub


Sub Del()
	Dim SqlJob

    If JobID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请选择要删除的招聘</li>"
    End If
	If FoundErr = True Then
		Exit sub
	End if
    If InStr(JobID, ",") > 0 Then
        SqlJob = "Delete * from PW_Job where JobID in (" & JobID & ")"
    Else
        SqlJob = "Delete * from PW_Job where JobID=" & JobID
    End If
	Conn.execute(SqlJob)
    Call CloseConn
	Response.Redirect(ComeUrl)	
End Sub
%>