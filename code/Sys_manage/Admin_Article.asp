<!--#include file="Admin_Common.asp"-->
<!--#include file="../Include/PW_FSO.asp"-->
<!--#include file="../PW_Editor/fckeditor.asp"-->
<!--#include file="../Include/PW_XmlHttp.asp"-->
<%
Const NeedCheckComeUrl = True   '是否需要检查外部访问
Const PurviewLevel_Channel = 1   '0--不检查，1 检查
Const PurviewLevel_Others = ""   '其他权限


Dim ArticleID
Dim ManageType,ClassID,OnTop, IsElite, IsHot
Dim tClass, ClassName, RootID, ParentID, Depth, ParentPath, Child, arrChildID
Dim DefaultPicUrl, IncludePic,UploadFiles
Dim ArticlePro1, ArticlePro2, ArticlePro3, ArticlePro4

If ChannelID = 0 Then
    Response.Write "频道参数丢失！"
    Call CloseConn
    Response.End
End If


If ModuleType <> 1 Then
    Response.Write "<li>指定的频道ID不对！</li>"
    Call CloseConn
    Response.End
End If


ManageType = GetValue("ManageType")
OnTop = GetValue("OnTop")
IsElite = GetValue("IsElite")
IsHot = GetValue("IsHot")
ClassID = PE_CLng(GetValue("ClassID"))
ArticleID = GetValue("ArticleID")


If Action = "" Then
    Action = "Manage"
End If


If IsValidID(ArticleID) = False Then
    ArticleID = ""
End If


FileName = "Admin_Article.asp?ChannelID=" & ChannelID & "&Action=" & Action & "&ManageType=" & ManageType
strFileName = FileName & "&ClassID=" & ClassID & "&Field=" & strField & "&keyword=" & Keyword
ArticlePro1 = "[图文]"
ArticlePro2 = "[组图]"
ArticlePro3 = "[推荐]"
ArticlePro4 = "[注意]"

Response.Write "<html><head><title>" & ChannelShortName & "管理</title>" & vbCrLf
Response.Write "<meta http-equiv='Content-Type' content='text/html; charset=gb2312'>" & vbCrLf
Response.Write "<link href='Admin_Style.css' rel='stylesheet' type='text/css'>" & vbCrLf
Response.Write "</head>" & vbCrLf
Response.Write "<body leftmargin='2' topmargin='0' marginwidth='0' marginheight='0'>" & vbCrLf
Response.Write "<table width='100%' border='0' align='center' cellpadding='2' cellspacing='1' class='border'>"
Dim strTitle
strTitle = ChannelName & "管理----"
Select Case Action
Case "Add"
    strTitle = strTitle & "添加" & ChannelShortName
Case "Modify"
    strTitle = strTitle & "修改" & ChannelShortName
Case "SaveAdd", "SaveModify"
    strTitle = strTitle & "保存" & ChannelShortName
Case "Show"
    strTitle = strTitle & "预览" & ChannelShortName
Case "Manage"
    Select Case ManageType
    Case "Recyclebin"
        strTitle = strTitle & ChannelShortName & "回收站管理"
    Case Else
        strTitle = strTitle & ChannelShortName & "管理首页"
    End Select
End Select
Call ShowPageTitle(strTitle)
Response.Write "  <tr class='tdbg'>"
Response.Write "    <td width='70' height='30' ><strong>管理导航：</strong></td><td colspan='5'>"
if Purview_View=True then
	Response.Write "<a href='Admin_Article.asp?ChannelID=" & ChannelID & "&Status=9'>" & ChannelShortName & "管理首页</a>"
end if
if Purview_Add=True then
	Response.Write "&nbsp;|&nbsp;<a href='Admin_Article.asp?ChannelID=" & ChannelID & "&Action=Add&ClassID=" & ClassID & "'>添加" & ChannelShortName & "</a>"
End if
if Purview_Del = True then
	Response.Write "&nbsp;|&nbsp;<a href='Admin_Article.asp?ChannelID=" & ChannelID & "&ManageType=Recyclebin'>" & ChannelShortName & "回收站管理</a>"
end if
if FoundInArr(FieldShow, "ClassID", ",") and Purview_Class=True then
	Response.Write "&nbsp;|&nbsp;<a href='Admin_Class.asp?ChannelID=" & ChannelID & "'>" & ChannelShortName & "分类管理</a>"
end if
Response.Write "</td></tr>" & vbCrLf
If Action = "Manage" Then
	if FoundInArr(FieldShow, "Property", ",") then
		Response.Write "<form name='form3' method='Post' action='" & strFileName & "'><tr class='tdbg'>"
		Response.Write "  <td width='70' height='30' ><strong>" & ChannelShortName & "选项：</strong></td>"
		Response.Write "<td>"
		Response.Write "<input name='OnTop' type='checkbox' onclick='submit()' value=""True"" " & IsRadioChecked(OnTop, "True") & "> 固顶" & ChannelShortName & "&nbsp;&nbsp;&nbsp;&nbsp;"
		Response.Write "<input name='IsElite' type='checkbox' onclick='submit()' value=""True"" " & IsRadioChecked(IsElite, "True") & "> 推荐" & ChannelShortName & "&nbsp;&nbsp;&nbsp;&nbsp;"
		Response.Write "<input name='IsHot' type='checkbox' onclick='submit()' value=""True"" " & IsRadioChecked(IsHot, "True") & "> 热门" & ChannelShortName
		Response.Write "</td></tr></form>" & vbCrLf
		strFileName = strFileName & "&OnTop=" & OnTop & "&IsElite=" & IsElite & "&IsHot=" & IsHot
	end if
end if
Response.Write "</table>" & vbCrLf


Select Case Action
Case "Add"
    Call Add
Case "Modify"
    Call Modify
Case "SaveAdd", "SaveModify"
    Call SaveArticle
Case "SetOnTop", "CancelOnTop", "SetElite", "CancelElite"
    Call SetProperty
Case "Show"
    Call Show
Case "Del"
    Call Del
Case "ConfirmDel"
    Call ConfirmDel
Case "ClearRecyclebin"
    Call ClearRecyclebin
Case "Restore"
    Call Restore
Case "RestoreAll"
    Call RestoreAll
Case "Manage"
    Call main
End Select


If FoundErr = True Then
    Call WriteErrMsg(ErrMsg, ComeUrl)
End If
Response.Write "</body></html>"
Call CloseConn


Sub main
    Dim rsArticleList, sql, Querysql
    If ClassID > 0 Then
        Set tClass = Conn.Execute("select ClassName,RootID,ParentID,Depth,ParentPath,Child,arrChildID from PW_Class where ClassID=" & ClassID)
        If tClass.BOF And tClass.EOF Then
            FoundErr = True
            ErrMsg = ErrMsg & "<li>找不到指定的栏目</li>"
        Else
            ClassName = tClass(0)
            RootID = tClass(1)
            ParentID = tClass(2)
            Depth = tClass(3)
            ParentPath = tClass(4)
            Child = tClass(5)
            arrChildID = tClass(6)
        End If
        Set tClass = Nothing
    End If

    If FoundErr = True Then Exit Sub
	
    Call ShowJS_Main(ChannelShortName)
	if FoundInArr(FieldShow, "ClassID", ",") then
		Response.Write "<br><table width='100%' border='0' align='center' cellpadding='2' cellspacing='1' class='border'>"
		Response.Write "  <tr class='title'>"
		Response.Write "    <td height='22'>" & GetRootClass() & "</td>"
		Response.Write "  </tr>" & GetChild_Root() & ""
		Response.Write "</table>"
	End if
	Response.write "<br>"
    Select Case ManageType
    Case "Recyclebin"
        Call ShowContentManagePath(ChannelShortName & "回收站管理")
    Case Else
        Call ShowContentManagePath(ChannelShortName & "管理")
    End Select
    Response.Write "<table width='100%' border='0' cellpadding='0' cellspacing='0'><tr>"
    Response.Write "    <form name='myform' method='Post' action='Admin_Article.asp' onsubmit='return ConfirmDel();'>"
    Response.Write "     <td><table class='border' border='0' cellspacing='1' width='100%' cellpadding='0'>"
    Response.Write "          <tr class='title' height='22'> "
    Response.Write "            <td height='22' width='30' align='center'><strong>选中</strong></td>"
    Response.Write "            <td width='25' align='center'><strong>ID</strong></td>"
    Response.Write "            <td align='center' ><strong>" & ChannelShortName & "标题</strong></td>"
	if FoundInArr(FieldShow, "Hits", ",") then
		Response.Write "            <td width='40' align='center' ><strong>点击数</strong></td>"
	end if
	if FoundInArr(FieldShow, "Property", ",") then
		Response.Write "            <td width='80' align='center' ><strong>" & ChannelShortName & "属性</strong></td>"
	end if
	If ManageType = "Recyclebin" Then
        Response.Write "            <td width='100' align='center' ><strong>回收站操作</strong></td>"
    Else
        Response.Write "            <td width='150' align='center' ><strong>常规管理操作</strong></td>"
    End If
    Response.Write "          </tr>"

	If ClassID = -1 Or (ClassID > 0 And Child = 0) Then
		sql = "select top " & MaxPerPage & " A.ClassID,A.ArticleID,A.Title,A.Keyword,A.Author,A.UpdateTime,A.CopyFrom,A.IncludePic,A.DefaultPicUrl,A.Hits,A.OnTop,A.Elite from PW_Article A "
	Else
		sql = "select top " & MaxPerPage & " A.ClassID,A.ArticleID,C.ClassName,A.Title,A.Keyword,A.Author,A.UpdateTime,A.CopyFrom,A.IncludePic,A.DefaultPicUrl,A.Hits,A.OnTop,A.Elite from PW_Article A left join PW_Class C on A.ClassID=C.ClassID "
	End If
    Querysql = " where A.ChannelID=" & ChannelID
    If ManageType = "Recyclebin" Then
        Querysql = Querysql & " and A.Deleted=" & PE_True & ""
    Else
        Querysql = Querysql & " and A.Deleted=" & PE_False & ""
    End If
	If OnTop = "True" Then
		Querysql = Querysql & " and A.OnTop=" & PE_True & ""
	End If
	If IsElite = "True" Then
		Querysql = Querysql & " and A.Elite=" & PE_True & ""
	End If
	If IsHot = "True" Then
		Querysql = Querysql & " and A.Hits>=" & HitsOfHot & ""
	End If
    If ClassID <> 0 Then
        If Child > 0 Then
            Querysql = Querysql & " and A.ClassID in (" & arrChildID & ")"
        Else
            Querysql = Querysql & " and A.ClassID=" & ClassID
        End If
    End If
    If Keyword <> "" Then
            Querysql = Querysql & " and A.Title like '%" & Keyword & "%' "
    End If
	totalPut = PE_CLng(Conn.Execute("select Count(*) from PW_Article A " & Querysql)(0))
    If CurrentPage < 1 Then
        CurrentPage = 1
    End If
    If (CurrentPage - 1) * MaxPerPage > totalPut Then
        If (totalPut Mod MaxPerPage) = 0 Then
            CurrentPage = totalPut \ MaxPerPage
        Else
            CurrentPage = totalPut \ MaxPerPage + 1
        End If
    End If
    If CurrentPage > 1 Then
    	Querysql = Querysql & " and A.ArticleID < (select min(ArticleID) from (select top " & ((CurrentPage - 1) * MaxPerPage) & " A.ArticleID from PW_Article A " & Querysql & " order by A.ArticleID desc) as QueryArticle)"
    End If
	sql = sql & Querysql & " order by A.ArticleID desc"
    Set rsArticleList = Server.CreateObject("ADODB.Recordset")
    rsArticleList.Open sql, Conn, 1, 1
    If rsArticleList.BOF And rsArticleList.EOF Then
        totalPut = 0
        Response.Write "<tr class='tdbg'><td colspan='20' align='center'><br>"
        If ClassID > 0 Then
            Response.Write "此栏目及其子栏目中没有任何"
        Else
            Response.Write "没有任何"
        End If
		Response.Write ChannelShortName & "！"
        Response.Write "<br><br></td></tr>"
    Else
	Dim ArticleNum
        ArticleNum = 0
        Do While Not rsArticleList.EOF
            Response.Write "      <tr class='tdbg' onmouseout=""this.className='tdbg'"" onmouseover=""this.className='tdbgmouseover'"">"
			Response.Write "        <td width='30' align='center'><input name='ArticleID' type='checkbox' onclick='CheckItem(this)' id='ArticleID' value='" & rsArticleList("ArticleID") & "'></td>"
			Response.Write "        <td width='25' align='center'>" & rsArticleList("ArticleID") & "</td>"
            Response.Write "        <td>"
                If rsArticleList("ClassID") <> ClassID And ClassID <> -1 Then
                    Response.Write "<a href='" & FileName & "&ClassID=" & rsArticleList("ClassID") & "'>["
                    If rsArticleList("ClassName") <> "" Then
                        Response.Write rsArticleList("ClassName")
                    Else
                        Response.Write "<font color='gray'>不属于任何栏目</font>"
                    End If
                    Response.Write "]</a>&nbsp;"
                End If
            Select Case rsArticleList("IncludePic")
                Case 1
                    Response.Write "<font color=blue>" & ArticlePro1 & "</font>"
                Case 2
                    Response.Write "<font color=blue>" & ArticlePro2 & "</font>"
                Case 3
                    Response.Write "<font color=blue>" & ArticlePro3 & "</font>"
                Case 4
                    Response.Write "<font color=blue>" & ArticlePro4 & "</font>"
            End Select
            Response.Write "<a href='Admin_Article.asp?ChannelID=" & ChannelID & "&Action=Show&ArticleID=" & rsArticleList("ArticleID") & "'"
			Response.Write " title='标&nbsp;&nbsp;&nbsp;&nbsp;题：" & rsArticleList("Title") & vbCrLf
			if FoundInArr(FieldShow, "Author", ",") then
				Response.write "作&nbsp;&nbsp;&nbsp;&nbsp;者：" & rsArticleList("Author") & vbCrLf
			end if
			if FoundInArr(FieldShow, "CopyFrom", ",") then
				Response.write "转 贴 自：" & rsArticleList("CopyFrom") & vbCrLf
			end if
			if FoundInArr(FieldShow, "UpdateTime", ",") then
				Response.write "更新时间：" & rsArticleList("UpdateTime") & vbCrLf
			end if
			if FoundInArr(FieldShow, "Hits", ",") then
				Response.write "点 击 数：" & rsArticleList("Hits") & vbCrLf
			end if
			if FoundInArr(FieldShow, "Keyword", ",") then
				Response.write "关 键 字：" & Mid(rsArticleList("Keyword"), 2, Len(rsArticleList("Keyword")) - 2)
			end if
            Response.Write "'>" & rsArticleList("title") & "</a>"
            Response.Write "</td>"
			if FoundInArr(FieldShow, "Hits", ",") then
				Response.Write "      <td align='center'>" & rsArticleList("Hits") & "</td>"
			end if
			if FoundInArr(FieldShow, "Property", ",") then
				Response.Write "      <td width='80' align='center'>"
				If rsArticleList("OnTop") = True Then
					Response.Write "<font color=blue>顶</font> "
				Else
					Response.Write "&nbsp;&nbsp;&nbsp;"
				End If
				If rsArticleList("Hits") >= HitsOfHot Then
					Response.Write "<font color=red>热</font> "
				Else
					Response.Write "&nbsp;&nbsp;&nbsp;"
				End If
				If rsArticleList("Elite") = True Then
					Response.Write "<font color=green>荐</font> "
				Else
					Response.Write "&nbsp;&nbsp;&nbsp;"
				End If
				If Trim(rsArticleList("DefaultPicUrl")) <> "" Then
					Response.Write "<font color=blue>图</font>"
				Else
					Response.Write "&nbsp;&nbsp;"
				End If
				Response.Write "    </td>"
			end if
			Select Case ManageType
            Case "Recyclebin"
				if Purview_Del then
					Response.Write "<td width='100' align='center'>"
					Response.Write "<a href='Admin_Article.asp?ChannelID=" & ChannelID & "&Action=ConfirmDel&ArticleID=" & rsArticleList("ArticleID") & "' onclick=""return confirm('确定要彻底删除此" & ChannelShortName & "吗？彻底删除后将无法还原！');"">彻底删除</a> "
					Response.Write "<a href='Admin_Article.asp?ChannelID=" & ChannelID & "&Action=Restore&ArticleID=" & rsArticleList("ArticleID") & "'>还原</a>"
					Response.Write "</td>"
				end if
            Case Else
                Response.Write "    <td width='150' align='left'>&nbsp;"
				if Purview_Modify then
					Response.Write "<a href='Admin_Article.asp?ChannelID=" & ChannelID & "&Action=Modify&ArticleID=" & rsArticleList("ArticleID") & "'>修改</a>&nbsp;"
				end if
				if Purview_Del then
					Response.Write "<a href='Admin_Article.asp?ChannelID=" & ChannelID & "&Action=Del&ArticleID=" & rsArticleList("ArticleID") & "' onclick=""return confirm('确定要删除此" & ChannelShortName & "吗？删除后你还可以从回收站中还原。');"">删除</a>&nbsp;"
				end if
				if FoundInArr(FieldShow, "Property", ",") then
                    If rsArticleList("OnTop") = False Then
                        Response.Write "<a href='Admin_Article.asp?ChannelID=" & ChannelID & "&Action=SetOnTop&ArticleID=" & rsArticleList("ArticleID") & "'>固顶</a>&nbsp;"
                    Else
                        Response.Write "<a href='Admin_Article.asp?ChannelID=" & ChannelID & "&Action=CancelOnTop&ArticleID=" & rsArticleList("ArticleID") & "'>解固</a>&nbsp;"
                    End If
                    If rsArticleList("Elite") = False Then
                        Response.Write "<a href='Admin_Article.asp?ChannelID=" & ChannelID & "&Action=SetElite&ArticleID=" & rsArticleList("ArticleID") & "'>设为推荐</a>"
                    Else
                        Response.Write "<a href='Admin_Article.asp?ChannelID=" & ChannelID & "&Action=CancelElite&ArticleID=" & rsArticleList("ArticleID") & "'>取消推荐</a>"
                    End If
				end if
                Response.Write "</td>"
            End Select
            Response.Write "</tr>"

            ArticleNum = ArticleNum + 1
            If ArticleNum >= MaxPerPage Then Exit Do
            rsArticleList.MoveNext
        Loop
    End If
    rsArticleList.Close
    Set rsArticleList = Nothing
    Response.Write "</table>"
    Response.Write "<table width='100%' border='0' cellpadding='0' cellspacing='0'>"
    Response.Write "  <tr>"
    Response.Write "    <td width='200' height='30'><input name='chkAll' type='checkbox' id='chkAll' onclick='CheckAll(this.form)' value='checkbox'>选中本页显示的所有" & ChannelShortName & "</td><td>"
    Select Case ManageType
    Case "Recyclebin"
        If Purview_Del Then
            Response.Write "<input name='submit1' type='submit' id='submit1' onClick=""document.myform.Action.value='ConfirmDel'"" value=' 彻底删除 '>&nbsp;"
            Response.Write "<input name='Submit2' type='submit' id='Submit2' onClick=""document.myform.Action.value='ClearRecyclebin'"" value='清空回收站'>&nbsp;&nbsp;&nbsp;&nbsp;"
            Response.Write "<input name='Submit3' type='submit' id='Submit3' onClick=""document.myform.Action.value='Restore'"" value='还原选定的" & ChannelShortName & "'>&nbsp;"
            Response.Write "<input name='Submit4' type='submit' id='Submit4' onClick=""document.myform.Action.value='RestoreAll'"" value='还原所有" & ChannelShortName & "'>"
        End If
    Case Else
		If Purview_Del Then
			Response.Write "<input name='submit1' type='submit' value=' 批量删除 ' onClick=""document.myform.Action.value='Del'""> "
		end if
    End Select
    
    Response.Write "<input name='Action' type='hidden' id='Action' value=''>"
    Response.Write "<input name='ChannelID' type='hidden' id='ChannelID' value='" & ChannelID & "'>"
    Response.Write "  </td></tr>"
    Response.Write "</table>"
    Response.Write "</td>"
    Response.Write "</form></tr></table>"
    If totalPut > 0 Then
        Response.Write ShowPage(strFileName, totalPut, MaxPerPage, CurrentPage, True, True, ChannelItemUnit & ChannelShortName & "", True)
    End If
    Response.Write "<form method='Get' name='SearchForm' action='" & FileName & "'>"
    Response.Write "<table width='100%' border='0' cellpadding='0' cellspacing='0' class='border'>"
    Response.Write "  <tr class='tdbg'>"
    Response.Write "   <td width='80' align='right'><strong>" & ChannelShortName & "搜索：</strong></td>"
    Response.Write "   <td>"
    Response.Write "<select name='Field' size='1'>"
    Response.Write "<option value='Title' selected>" & ChannelShortName & "标题</option>"
    Response.Write "</select>"
	if FoundInArr(FieldShow, "ClassID", ",") then
		Response.Write "<select name='ClassID'><option value=''>所有栏目</option>" & GetClass_Option(1,ClassID) & "</select>"
    end if
	Response.Write "<input type='text' name='keyword'  size='20' value='关键字' maxlength='50' onFocus='this.select();'>"
    Response.Write "<input type='submit' name='Submit'  value='搜索'>"
    Response.Write "<input name='ManageType' type='hidden' id='ManageType' value='" & ManageType & "'>"
    Response.Write "<input name='ChannelID' type='hidden' id='ChannelID' value='" & ChannelID & "'>"
    Response.Write "</td></tr></table></form>"
	if FoundInArr(FieldShow, "Property", ",") then
		Response.Write "<br><b>说明：</b><br>&nbsp;&nbsp;&nbsp;&nbsp;" & ChannelShortName & "属性中的各项含义：<font color=blue>顶</font>----固顶" & ChannelShortName & "，<font color=red>热</font>----热门" & ChannelShortName & "，<font color=green>荐</font>----推荐" & ChannelShortName & "，<font color=blue>图</font>----首页图片" & ChannelShortName & "<br>"
	end if
End Sub






Sub ShowJS_Article()
    Response.Write "<script language = 'JavaScript'>" & vbCrLf
	Response.Write "function SelectPic(formstr){" & vbCrLf
	Response.Write "  var Picform = document.all(formstr);" & vbCrLf
	Response.Write "  var arr=showModalDialog('Admin_SelectFile.asp?dialogtype=photo&ChannelID="& ChannelID &"', '', 'dialogWidth:820px; dialogHeight:600px; help: no; scroll: yes; status: no');" & vbCrLf
	Response.Write "  if(arr!=null){" & vbCrLf
	Response.Write "    var ss=arr.split('|');" & vbCrLf
	Response.Write "    Picform.value=ss[0];" & vbCrLf
	Response.Write "  }" & vbCrLf
	Response.Write "}" & vbCrLf
    Response.Write "function rUseLinkUrl(){" & vbCrLf
    Response.Write "  if(document.myform.UseLinkUrl.checked==true){" & vbCrLf
    Response.Write "    document.myform.LinkUrl.disabled=false;" & vbCrLf
    Response.Write "     ArticleContent.style.display='none';" & vbCrLf
    Response.Write "  }" & vbCrLf
    Response.Write "  else{" & vbCrLf
    Response.Write "    document.myform.LinkUrl.disabled=true;" & vbCrLf
    Response.Write "    ArticleContent.style.display='';" & vbCrLf
    Response.Write "  }" & vbCrLf
    Response.Write "}" & vbCrLf
    Response.Write "function CheckForm(){" & vbCrLf
	if FoundInArr(FieldShow, "Title", ",") then
		Response.Write "  if (document.myform.Title.value==''){" & vbCrLf
		Response.Write "    alert('" & ChannelShortName & "标题不能为空！');" & vbCrLf
		Response.Write "    document.myform.Title.focus();" & vbCrLf
		Response.Write "    return false;" & vbCrLf
		Response.Write "  }" & vbCrLf
	end if
	if FoundInArr(FieldShow, "Keyword", ",") then
		Response.Write "  if (document.myform.Keyword.value==''){" & vbCrLf
		Response.Write "    alert('关键字不能为空！');" & vbCrLf
		Response.Write "    document.myform.Keyword.focus();" & vbCrLf
		Response.Write "    return false;" & vbCrLf
		Response.Write "  }" & vbCrLf
	end if
	if FoundInArr(FieldShow, "LinkUrl", ",")  and FoundInArr(FieldShow, "Content", ",") then
		Response.Write "  if(document.myform.UseLinkUrl.checked==true){" & vbCrLf
		Response.Write "    if (document.myform.LinkUrl.value==''||document.myform.LinkUrl.value=='http://'){" & vbCrLf
		Response.Write "      alert('请输入转向链接的地址！');" & vbCrLf
		Response.Write "      document.myform.LinkUrl.focus();" & vbCrLf
		Response.Write "      return false;" & vbCrLf
		Response.Write "    }" & vbCrLf
		Response.Write "  }" & vbCrLf
		Response.Write "  else{" & vbCrLf
		Response.Write "  var oEditor  =FCKeditorAPI.GetInstance(""Content"");" & vbCrLf
		Response.Write "  var Content =oEditor.GetXHTML();" & vbCrLf
		Response.Write "  if (Content==null || Content==''){" & vbCrLf
		Response.Write "     alert('" & ChannelShortName & "内容不能为空！');" & vbCrLf
		Response.Write "     oEditor.Focus();" & vbCrLf
		Response.Write "     return(false);" & vbCrLf
		Response.Write "  }" & vbCrLf
		Response.Write "  }" & vbCrLf
	Elseif FoundInArr(FieldShow, "Content", ",") then
		Response.Write "  var oEditor  =FCKeditorAPI.GetInstance(""Content"");" & vbCrLf
		Response.Write "  var Content =oEditor.GetXHTML();" & vbCrLf
		Response.Write "  if (Content==null || Content==''){" & vbCrLf
		Response.Write "     alert('" & ChannelShortName & "内容不能为空！');" & vbCrLf
		Response.Write "     oEditor.Focus();" & vbCrLf
		Response.Write "     return(false);" & vbCrLf
		Response.Write "  }" & vbCrLf
	end if
	if FoundInArr(FieldShow, "ClassID", ",") then
		Response.Write "  var obj=document.myform.ClassID;" & vbCrLf
		Response.Write "  var iCount=0;" & vbCrLf
		Response.Write "  for(var i=0;i<obj.length;i++){" & vbCrLf
		Response.Write "    if(obj.options[i].selected==true){" & vbCrLf
		Response.Write "      iCount=iCount+1;" & vbCrLf
		Response.Write "    }" & vbCrLf
		Response.Write "  }" & vbCrLf
		Response.Write "  if (iCount==0){" & vbCrLf
		Response.Write "    alert('请选择所属栏目！');" & vbCrLf
		Response.Write "    document.myform.ClassID.focus();" & vbCrLf
		Response.Write "    return false;" & vbCrLf
		Response.Write "  }" & vbCrLf
	End if
    Dim rsField
    Set rsField = Conn.Execute("select * from PW_Field where ChannelID=" & ChannelID & "")
    Do While Not rsField.EOF
        If rsField("ShowOnForm") = True And rsField("EnableNull") = False Then
			Response.Write "  if (document.myform."&rsField("FieldName")&".value==''){" & vbCrLf
			Response.Write "    alert('"&rsField("Title")&"不能为空！');" & vbCrLf
			Response.Write "    document.myform."&rsField("FieldName")&".focus();" & vbCrLf
			Response.Write "    return false;" & vbCrLf
			Response.Write "  }" & vbCrLf
        End If
        rsField.MoveNext
    Loop
	
    Response.Write "  return true;  " & vbCrLf
    Response.Write "}" & vbCrLf
    Response.Write "</script>" & vbCrLf
End Sub


Sub Add()
    Call ShowJS_Article
    
    Response.Write "<br><table width='100%'><tr><td align='left'>您现在的位置：<a href='Admin_Article.asp?ChannelID=" & ChannelID & "'>" & ChannelName & "管理</a>&nbsp;&gt;&gt;&nbsp;添加" & ChannelShortName & "</td></tr></table>" &vbcrlf
    Response.Write "<form method='POST' name='myform' onSubmit='return CheckForm();' action='Admin_Article.asp' target='_self'>" &vbcrlf

    Response.Write "<table width='100%' border='0' align='center' cellpadding='5' cellspacing='0' class='border'>" &vbcrlf
    Response.Write "  <tr align='center'>" &vbcrlf
    Response.Write "    <td class='tdbg' height='200' valign='top'>" &vbcrlf
    Response.Write "      <table width='98%' border='0' cellpadding='2' cellspacing='1' bgcolor='#FFFFFF'>" &vbcrlf

	if FoundInArr(FieldShow, "Title", ",") then
		Response.Write "          <tr class='tdbg'>" &vbcrlf
		Response.Write "            <td width='120' align='right' class='tdbg5'>" & ChannelShortName & "标题：</td>" &vbcrlf
		Response.Write "            <td>" &vbcrlf
		Response.Write "              <table width='100%' border='0' cellpadding='0' cellspacing='2'>" &vbcrlf
		Response.Write "                <tr>" &vbcrlf
		Response.Write "                  <td>" &vbcrlf
		
		Response.Write "                    <select name='IncludePic'>" &vbcrlf
		Response.Write "                      <option  value='0' selected> </option>" &vbcrlf
		Response.Write "                      <option value='1'>" & ArticlePro1 & "</option>" &vbcrlf
		Response.Write "                      <option value='2'>" & ArticlePro2 & "</option>" &vbcrlf
		Response.Write "                      <option value='3'>" & ArticlePro3 & "</option>" &vbcrlf
		Response.Write "                      <option value='4'>" & ArticlePro4 & "</option>" &vbcrlf
		Response.Write "                    </select>" &vbcrlf
		Response.Write "                    <input name='Title' type='text' id='Title' value='' size='56' autocomplete='off' maxlength='255' class='bginput'>" &vbcrlf
		Response.Write "                    <select name='TitleFontColor' id='TitleFontColor'>" &vbcrlf
		Response.Write "                      <option value='' selected>颜色</option>" &vbcrlf
		Response.Write "                      <option value=''>默认</option>" &vbcrlf
		Response.Write "                      <option value='#000000' style='background-color:#000000'></option>" &vbcrlf
		Response.Write "                      <option value='#FFFFFF' style='background-color:#FFFFFF'></option>" &vbcrlf
		Response.Write "                      <option value='#008000' style='background-color:#008000'></option>" &vbcrlf
		Response.Write "                      <option value='#800000' style='background-color:#800000'></option>" &vbcrlf
		Response.Write "                      <option value='#808000' style='background-color:#808000'></option>" &vbcrlf
		Response.Write "                      <option value='#000080' style='background-color:#000080'></option>" &vbcrlf
		Response.Write "                      <option value='#800080' style='background-color:#800080'></option>" &vbcrlf
		Response.Write "                      <option value='#808080' style='background-color:#808080'></option>" &vbcrlf
		Response.Write "                      <option value='#FFFF00' style='background-color:#FFFF00'></option>" &vbcrlf
		Response.Write "                      <option value='#00FF00' style='background-color:#00FF00'></option>" &vbcrlf
		Response.Write "                      <option value='#00FFFF' style='background-color:#00FFFF'></option>" &vbcrlf
		Response.Write "                      <option value='#FF00FF' style='background-color:#FF00FF'></option>" &vbcrlf
		Response.Write "                      <option value='#FF0000' style='background-color:#FF0000'></option>" &vbcrlf
		Response.Write "                      <option value='#0000FF' style='background-color:#0000FF'></option>" &vbcrlf
		Response.Write "                      <option value='#008080' style='background-color:#008080'></option>" &vbcrlf
		Response.Write "                    </select>" &vbcrlf
		Response.Write "                    <select name='TitleFontType' id='TitleFontType'>" &vbcrlf
		Response.Write "                      <option value='0' selected>字形</option>" &vbcrlf
		Response.Write "                      <option value='1'>粗体</option>" &vbcrlf
		Response.Write "                      <option value='2'>斜体</option>" &vbcrlf
		Response.Write "                      <option value='3'>粗+斜</option>" &vbcrlf
		Response.Write "                      <option value='0'>规则</option>" &vbcrlf
		Response.Write "                    </select>" &vbcrlf
		Response.Write "                  </td>" &vbcrlf
		Response.Write "                </tr>" &vbcrlf
		Response.Write "              </table>" &vbcrlf
		Response.Write "            </td>" &vbcrlf
		Response.Write "          </tr>" &vbcrlf
	end if
	if FoundInArr(FieldShow, "ClassID", ",") then
		Response.Write "          <tr class='tdbg'>" &vbcrlf
		Response.Write "            <td width='120' align='right' class='tdbg5'>所属栏目：</td>" &vbcrlf
		Response.Write "            <td>" &vbcrlf
		Response.Write "              <select name='ClassID'>" & GetClass_Option(1,ClassID) & "</select>" &vbcrlf
		Response.Write "            </td>" &vbcrlf
		Response.Write "          </tr>" &vbcrlf
	end if

	if FoundInArr(FieldShow, "Keyword", ",") then
		Response.Write "          <tr class='tdbg'>" &vbcrlf
		Response.Write "            <td width='120' align='right' class='tdbg5'>关键字：</td>" &vbcrlf
		Response.Write "            <td>" &vbcrlf
		Response.Write "              <input name='Keyword' type='text' style=""clear:both"" id='Keyword' value='" & Trim(Session("Keyword")) & "' size='50' maxlength='255'> <font color='#FF0000'>*</font> "
		Response.Write "              <font color='#0000FF'>用来查找相关" & ChannelShortName & "，可输入多个关键字，中间用<font color='#FF0000'>“|”</font>隔开。不能出现&quot;'&?;:()等字符。</font>"
		Response.Write "            </td>" &vbcrlf
		Response.Write "          </tr>" &vbcrlf
	end if
	if FoundInArr(FieldShow, "Author", ",") then
		Response.Write "          <tr class='tdbg'>" &vbcrlf
		Response.Write "            <td width='120' align='right' class='tdbg5'>作者：</td>" &vbcrlf
		Response.Write "            <td>" &vbcrlf
		Response.Write "              <input name='Author' type='text' id='Author' value='" & Trim(Session("Author")) & "' size='50' maxlength='100'>" & GetAuthorList(AdminName)
		Response.Write "            </td>" &vbcrlf
		Response.Write "          </tr>" &vbcrlf
	end if
	if FoundInArr(FieldShow, "CopyFrom", ",") then
		Response.Write "          <tr class='tdbg'>" &vbcrlf
		Response.Write "            <td width='120' align='right' class='tdbg5'>" & ChannelShortName & "来源：</td>" &vbcrlf
		Response.Write "            <td>" &vbcrlf
		Response.Write "              <input name='CopyFrom' type='text' id='CopyFrom' value='" & Trim(Session("CopyFrom")) & "' autocomplete='off' size='50' maxlength='100'>" & GetCopyFromList()
		Response.Write "            </td>" &vbcrlf
		Response.Write "          </tr>" &vbcrlf
	end if
	Call ShowTabs_MyField_Add()
	if FoundInArr(FieldShow, "LinkUrl", ",") then
		Response.Write "          <tr class='tdbg'>" &vbcrlf
		Response.Write "            <td width='120' align='right' class='tdbg5'><font color='#FF0000'>转向链接：</font></td>" &vbcrlf
		Response.Write "            <td>" &vbcrlf
		Response.Write "              <input name='LinkUrl' type='text' id='LinkUrl' value='http://' size='50' maxlength='255' disabled>" &vbcrlf
		Response.Write "              <input name='UseLinkUrl' type='checkbox' id='UseLinkUrl' value='Yes'"
		if FoundInArr(FieldShow, "Content", ",") then
			Response.write " onClick='rUseLinkUrl();'"
		end if
		Response.write "> <font color='#FF0000'>使用转向链接</font>"
		Response.Write "            </td>" &vbcrlf
		Response.Write "          </tr>" &vbcrlf
	end if
	if FoundInArr(FieldShow, "Intro", ",") then
		Response.Write "          <tr class='tdbg'>" &vbcrlf
		Response.Write "            <td width='120' align='right' class='tdbg5'>" & ChannelShortName & "简介：</td>" &vbcrlf
		Response.Write "            <td><textarea name='Intro' cols='80' rows='4'></textarea></td>" &vbcrlf
		Response.Write "          </tr>" &vbcrlf
	end if
	if FoundInArr(FieldShow, "Content", ",") then
		Response.Write "          <tr class='tdbg' id='ArticleContent' style=""display:''"">" &vbcrlf
		Response.Write "            <td width='120' align='right' valign='bottom' class='tdbg5'><p>" & ChannelShortName & "内容：</p>"
		If IsObjInstalled("Microsoft.XMLHTTP") = True Then
			Response.Write "<table><tr><td><input type='checkbox' name='SaveRemotePic' value='Yes' checked></td><td>自动下载" & ChannelShortName & "内容里的图片</td>"
			Response.Write "</tr></table>"
			Response.Write "<div align='left'><font color='#006600'>&nbsp;&nbsp;&nbsp;&nbsp;启用此功能后，如果从其它网站上复制内容到右边的编辑器中，并且内容中包含有图片，本系统会在保存" & ChannelShortName & "时自动把相关图片复制到本站服务器上。"
			Response.Write "<br>&nbsp;&nbsp;&nbsp;&nbsp;系统会因所下载图片的大小而影响速度，建议图片较多时不要使用此功能。</font>"
		End If
		Response.Write "<br><br><font color='red'>换行请按Shift+Enter<br><br>另起一段请按Enter</font></div><br><br><br><br><iframe id='frmPreview' width='120' height='150' frameborder='1' src='Admin_imgPreview.asp'></iframe>"
		Response.Write "            </td>" &vbcrlf
		Response.Write "            <td  valign='top'>" &vbcrlf
		Call ShowFckEditor("Default","","Content","100%","500")
		Response.Write "            </td>" &vbcrlf
		Response.Write "          </tr>" &vbcrlf
	end if
	if FoundInArr(FieldShow, "DefaultPicUrl", ",") then
		Response.Write "          <tr class='tdbg'>" &vbcrlf
		Response.Write "            <td width='120' align='right' class='tdbg5'><font color='#FF0000'>缩略图：</font></td>" &vbcrlf
		Response.Write "            <td>" &vbcrlf
		Response.Write "              <input name='DefaultPicUrl' type='text' id='DefaultPicUrl' size='56' maxlength='200' onPropertyChange=""frmPreview.img.src=((this.value == '') ? '../images/nopic.gif' : this.value);"" >"
		Response.write "&nbsp;<input type='button' name='Button2' class='Button' value='站内选择' onclick=""SelectPic('DefaultPicUrl')""> 默认将抓取内容中的第一张图片"
		Response.Write "              <input name='UploadFiles' type='hidden' id='UploadFiles'>"
		Response.write "<div><iframe style='top:2px' src='upload.asp?ChannelID=" & ChannelID & "&dialogtype=Articlepic' frameborder=0 scrolling=no width='450' height='25'></iframe></div>"
		Response.Write "            </td>" &vbcrlf
		Response.Write "          </tr>" &vbcrlf
	end if
	if FoundInArr(FieldShow, "Property", ",") then
		Response.Write "          <tr class='tdbg'>" &vbcrlf
		Response.Write "            <td width='120' align='right' class='tdbg5'>" & ChannelShortName & "属性：</td>" &vbcrlf
		Response.Write "            <td>" &vbcrlf
		Response.Write "              <input name='OnTop' type='checkbox' id='OnTop' value='yes'> 固顶" & ChannelShortName 
		Response.Write "              &nbsp;&nbsp;&nbsp;<input name='Hot' type='checkbox' id='Hot' value='yes' onclick=""javascript:document.myform.Hits.value='" & HitsOfHot & "'""> 热门" & ChannelShortName 
		Response.Write "              &nbsp;&nbsp;&nbsp;<input name='Elite' type='checkbox' id='Elite' value='yes'> 推荐" & ChannelShortName 
		Response.Write "            </td>" &vbcrlf
		Response.Write "          </tr>" &vbcrlf
	end if
	Response.write ShowInfoPurview("0")
	if FoundInArr(FieldShow, "Hits", ",") then
        Response.Write "          <tr class='tdbg'>" &vbcrlf
        Response.Write "            <td width='120' align='right' class='tdbg5'>点击数初始值：</td>" &vbcrlf
        Response.Write "            <td>" &vbcrlf
        Response.Write "              <input name='Hits' type='text' id='Hits' value='0' size='10' maxlength='10' style='text-align:center'>&nbsp;&nbsp; <font color='#0000FF'>这功能是提供给管理员作弊用的。不过尽量不要用呀！^_^</font>"
        Response.Write "            </td>" &vbcrlf
        Response.Write "          </tr>" &vbcrlf
	end if
	if FoundInArr(FieldShow, "UpdateTime", ",") then
		Response.Write "          <tr class='tdbg'>" &vbcrlf
		Response.Write "            <td width='120' align='right' class='tdbg5'>录入时间：</td>" &vbcrlf
		Response.Write "            <td>" &vbcrlf
		Response.Write "              <input name='UpdateTime' type='text' id='UpdateTime' value='" & Now() & "' maxlength='50'> 时间格式为“年-月-日 时:分:秒”，如：<font color='#0000FF'>2003-5-12 12:32:47</font>"
		Response.Write "            </td>" &vbcrlf
		Response.Write "          </tr>" &vbcrlf
	end if
    Response.Write "        </table>" &vbcrlf
    Response.Write "      </td>" &vbcrlf
    Response.Write "    </tr>" &vbcrlf
    Response.Write "  </table>" &vbcrlf
    Response.Write "  <p align='center'>"
    Response.Write "   <input name='Action' type='hidden' id='Action' value='SaveAdd'>"
    Response.Write "   <input name='ChannelID' type='hidden' id='ChannelID' value='" & ChannelID & "'>"
    Response.Write "   <input name='add' type='submit'  id='Add' value=' 添 加 ' onClick=""document.myform.Action.value='SaveAdd';document.myform.target='_self';"" style='cursor:hand;'>&nbsp; "
    Response.Write "   <input name='Cancel' type='button' id='Cancel' value=' 取 消 ' onClick=""window.location.href='Admin_Article.asp?ChannelID=" & ChannelID & "&Action=Manage';"" style='cursor:hand;'>"
    Response.Write "  </p><br>" &vbcrlf
    Response.Write "</form>" &vbcrlf
End Sub



Sub Modify()
    Dim rsArticle, sql, tmpAuthor, tmpCopyFrom

    If ArticleID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请指定要修改的" & ChannelShortName & "ID</li>"
        Exit Sub
    Else
        ArticleID = PE_CLng(ArticleID)
    End If
    sql = "select * from PW_Article where ArticleID=" & ArticleID & ""
    Set rsArticle = Conn.Execute(sql)
    If rsArticle.BOF And rsArticle.EOF Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>找不到" & ChannelShortName & "</li>"
        rsArticle.Close
        Set rsArticle = Nothing
        Exit Sub
    End If
    ClassID = rsArticle("ClassID")
    If FoundErr = True Then
        rsArticle.Close
        Set rsArticle = Nothing
        Exit Sub
    End If
    tmpAuthor = rsArticle("Author")
    tmpCopyFrom = rsArticle("CopyFrom")
    Call ShowJS_Article
    
    Response.Write "<br><table width='100%'><tr><td align='left'>您现在的位置：<a href='Admin_Article.asp?ChannelID=" & ChannelID & "'>" & ChannelName & "管理</a>&nbsp;&gt;&gt;&nbsp;修改" & ChannelShortName & "</td></tr></table>"
    Response.Write "<form method='POST' name='myform' onSubmit='return CheckForm();' action='Admin_Article.asp' target='_self'>" &vbcrlf

    Response.Write "<table width='100%' border='0' align='center' cellpadding='5' cellspacing='0' class='border'>" &vbcrlf
    Response.Write "  <tr align='center'>" &vbcrlf
    Response.Write "    <td class='tdbg' height='200' valign='top'>" &vbcrlf
    Response.Write "      <table width='98%' border='0' cellpadding='2' cellspacing='1' bgcolor='#FFFFFF'>" &vbcrlf

	if FoundInArr(FieldShow, "Title", ",") then
		Response.Write "          <tr class='tdbg'>" &vbcrlf
		Response.Write "            <td width='120' align='right' class='tdbg5'>" & ChannelShortName & "标题：</td>" &vbcrlf
		Response.Write "            <td>" &vbcrlf
		Response.Write "              <table width='100%' border='0' cellpadding='0' cellspacing='2'>" &vbcrlf
		Response.Write "                <tr>" &vbcrlf
		Response.Write "                  <td>" &vbcrlf
		
		Response.Write "                    <select name='IncludePic'>"
		Response.Write "                      <option  value='0'"
		If rsArticle("IncludePic") = 0 Then Response.Write " selected"
		Response.Write "> </option>"
		Response.Write "                      <option value='1'"
		If rsArticle("IncludePic") = 1 Then Response.Write " selected"
		Response.Write ">" & ArticlePro1 & "</option>"
		Response.Write "                      <option value='2'"
		If rsArticle("IncludePic") = 2 Then Response.Write " selected"
		Response.Write ">" & ArticlePro2 & "</option>"
		Response.Write "                      <option value='3'"
		If rsArticle("IncludePic") = 3 Then Response.Write " selected"
		Response.Write ">" & ArticlePro3 & "</option>"
		Response.Write "                      <option value='4'"
		If rsArticle("IncludePic") = 4 Then Response.Write " selected"
		Response.Write ">" & ArticlePro4 & "</option>"
		Response.Write "                    </select>"
		
		Response.Write "                    <input name='Title' type='text' id='Title' value='" & rsArticle("Title") & "' size='56' autocomplete='off' maxlength='255' class='bginput'>" &vbcrlf
		Response.Write "                    <select name='TitleFontColor' id='TitleFontColor'>"
		Response.Write "                      <option value=''"
		If rsArticle("TitleFontColor") = "" Then Response.Write " selected"
		Response.Write ">颜色</option>"
		Response.Write "                      <option value=''>默认</option>"
		Response.Write "                      <option value='#000000' style='background-color:#000000'"
		If rsArticle("TitleFontColor") = "#000000" Then Response.Write " selected"
		Response.Write "></option>"
		Response.Write "                      <option value='#FFFFFF' style='background-color:#FFFFFF'"
		If rsArticle("TitleFontColor") = "#FFFFFF" Then Response.Write " selected"
		Response.Write "></option>"
		Response.Write "                      <option value='#008000' style='background-color:#008000'"
		If rsArticle("TitleFontColor") = "#008000" Then Response.Write " selected"
		Response.Write "></option>"
		Response.Write "                      <option value='#800000' style='background-color:#800000'"
		If rsArticle("TitleFontColor") = "#800000" Then Response.Write " selected"
		Response.Write "></option>"
		Response.Write "                      <option value='#808000' style='background-color:#808000'"
		If rsArticle("TitleFontColor") = "#808000" Then Response.Write " selected"
		Response.Write "></option>"
		Response.Write "                      <option value='#000080' style='background-color:#000080'"
		If rsArticle("TitleFontColor") = "#000080" Then Response.Write " selected"
		Response.Write "></option>"
		Response.Write "                      <option value='#800080' style='background-color:#800080'"
		If rsArticle("TitleFontColor") = "#800080" Then Response.Write " selected"
		Response.Write "></option>"
		Response.Write "                      <option value='#808080' style='background-color:#808080'"
		If rsArticle("TitleFontColor") = "#808080" Then Response.Write " selected"
		Response.Write "></option>"
		Response.Write "                      <option value='#FFFF00' style='background-color:#FFFF00'"
		If rsArticle("TitleFontColor") = "#FFFF00" Then Response.Write " selected"
		Response.Write "></option>"
		Response.Write "                      <option value='#00FF00' style='background-color:#00FF00'"
		If rsArticle("TitleFontColor") = "#00FF00" Then Response.Write " selected"
		Response.Write "></option>"
		Response.Write "                      <option value='#00FFFF' style='background-color:#00FFFF'"
		If rsArticle("TitleFontColor") = "#00FFFF" Then Response.Write " selected"
		Response.Write "></option>"
		Response.Write "                      <option value='#FF00FF' style='background-color:#FF00FF'"
		If rsArticle("TitleFontColor") = "#FF00FF" Then Response.Write " selected"
		Response.Write "></option>"
		Response.Write "                      <option value='#FF0000' style='background-color:#FF0000'"
		If rsArticle("TitleFontColor") = "#FF0000" Then Response.Write " selected"
		Response.Write "></option>"
		Response.Write "                      <option value='#0000FF' style='background-color:#0000FF'"
		If rsArticle("TitleFontColor") = "#0000FF" Then Response.Write " selected"
		Response.Write "></option>"
		Response.Write "                      <option value='#008080' style='background-color:#008080'"
		If rsArticle("TitleFontColor") = "#008080" Then Response.Write " selected"
		Response.Write "></option>"
		Response.Write "                    </select>"
		Response.Write "                    <select name='TitleFontType' id='TitleFontType'>"
		Response.Write "                      <option value='0'"
		If rsArticle("TitleFontType") = 0 Then Response.Write " selected"
		Response.Write ">字形</option>"
		Response.Write "                      <option value='1'"
		If rsArticle("TitleFontType") = 1 Then Response.Write " selected"
		Response.Write ">粗体</option>"
		Response.Write "                      <option value='2'"
		If rsArticle("TitleFontType") = 2 Then Response.Write " selected"
		Response.Write ">斜体</option>"
		Response.Write "                      <option value='3'"
		If rsArticle("TitleFontType") = 3 Then Response.Write " selected"
		Response.Write ">粗+斜</option>"
		Response.Write "                      <option value='0'"
		If rsArticle("TitleFontType") = 4 Then Response.Write " selected"
		Response.Write ">规则</option>"
		Response.Write "                    </select>"
		Response.Write "                  </td>" &vbcrlf
		Response.Write "                </tr>" &vbcrlf
		Response.Write "              </table>" &vbcrlf
		Response.Write "            </td>" &vbcrlf
		Response.Write "          </tr>" &vbcrlf
	end if
	if FoundInArr(FieldShow, "ClassID", ",") then
		Response.Write "          <tr class='tdbg'>" &vbcrlf
		Response.Write "            <td width='120' align='right' class='tdbg5'>所属栏目：</td>" &vbcrlf
		Response.Write "            <td>" &vbcrlf
		Response.Write "              <select name='ClassID'>" & GetClass_Option(1,ClassID) & "</select>" &vbcrlf
		Response.Write "            </td>" &vbcrlf
		Response.Write "          </tr>" &vbcrlf
	end if

	if FoundInArr(FieldShow, "Keyword", ",") then
		Response.Write "          <tr class='tdbg'>" &vbcrlf
		Response.Write "            <td width='120' align='right' class='tdbg5'>关键字：</td>" &vbcrlf
		Response.Write "            <td>" &vbcrlf
		Response.Write "              <input name='Keyword' type='text' style=""clear:both"" id='Keyword' value='" & Mid(rsArticle("Keyword"), 2, Len(rsArticle("Keyword")) - 2) & "' size='50' maxlength='255'> <font color='#FF0000'>*</font> "
		Response.Write "              <font color='#0000FF'>用来查找相关" & ChannelShortName & "，可输入多个关键字，中间用<font color='#FF0000'>“|”</font>隔开。不能出现&quot;'&?;:()等字符。</font>"
		Response.Write "            </td>" &vbcrlf
		Response.Write "          </tr>" &vbcrlf
	end if
	if FoundInArr(FieldShow, "Author", ",") then
		Response.Write "          <tr class='tdbg'>" &vbcrlf
		Response.Write "            <td width='120' align='right' class='tdbg5'>作者：</td>" &vbcrlf
		Response.Write "            <td>" &vbcrlf
		Response.Write "              <input name='Author' type='text' id='Author' value='" & tmpAuthor & "' size='50' maxlength='100'>" & GetAuthorList(AdminName)
		Response.Write "            </td>" &vbcrlf
		Response.Write "          </tr>" &vbcrlf
	end if
	if FoundInArr(FieldShow, "CopyFrom", ",") then
		Response.Write "          <tr class='tdbg'>" &vbcrlf
		Response.Write "            <td width='120' align='right' class='tdbg5'>" & ChannelShortName & "来源：</td>" &vbcrlf
		Response.Write "            <td>" &vbcrlf
		Response.Write "              <input name='CopyFrom' type='text' id='CopyFrom' value='" & tmpCopyFrom & "' autocomplete='off' size='50' maxlength='100'>" & GetCopyFromList()
		Response.Write "            </td>" &vbcrlf
		Response.Write "          </tr>" &vbcrlf
	end if
	Call ShowTabs_MyField_Modify(rsArticle)
	if FoundInArr(FieldShow, "LinkUrl", ",") then
		Response.Write "          <tr class='tdbg'>" &vbcrlf
		Response.Write "            <td width='120' align='right' class='tdbg5'><font color='#FF0000'>转向链接：</font></td>" &vbcrlf
		Response.Write "            <td>" &vbcrlf
		Response.Write "              <input name='LinkUrl' type='text' id='LinkUrl' value='" & rsArticle("LinkUrl") & "' size='50' maxlength='255'"
		If rsArticle("LinkUrl") = "" Or rsArticle("LinkUrl") = "http://" Then Response.Write " disabled"
		Response.Write "> <input name='UseLinkUrl' type='checkbox' id='UseLinkUrl' value='Yes' "
		if FoundInArr(FieldShow, "Content", ",") then
			Response.write " onClick='rUseLinkUrl();'"
		end if
		If rsArticle("LinkUrl") <> "" And rsArticle("LinkUrl") <> "http://" Then Response.Write " checked"
		Response.Write "><font color='#FF0000'>使用转向链接</font>"
	
		Response.Write "            </td>" &vbcrlf
		Response.Write "          </tr>" &vbcrlf
	end if
	if FoundInArr(FieldShow, "Intro", ",") then
		Response.Write "          <tr class='tdbg'>" &vbcrlf
		Response.Write "            <td width='120' align='right' class='tdbg5'>" & ChannelShortName & "简介：</td>" &vbcrlf
		Response.Write "            <td><textarea name='Intro' cols='80' rows='4'>" & rsArticle("Intro") & "</textarea></td>" &vbcrlf
		Response.Write "          </tr>" &vbcrlf
	end if
	if FoundInArr(FieldShow, "Content", ",") then
		Response.Write "          <tr class='tdbg' id='ArticleContent' style=""display:'"
		If rsArticle("LinkUrl") <> "" And rsArticle("LinkUrl") <> "http://" Then Response.Write "none"
		Response.Write "'"">"
		Response.Write "            <td width='120' align='right' valign='bottom' class='tdbg5'><p>" & ChannelShortName & "内容：</p>"
		If IsObjInstalled("Microsoft.XMLHTTP") = True Then
			Response.Write "<table><tr><td><input type='checkbox' name='SaveRemotePic' value='Yes' checked></td><td>自动下载" & ChannelShortName & "内容里的图片</td>"
			Response.Write "</tr></table>"
			Response.Write "<div align='left'><font color='#006600'>&nbsp;&nbsp;&nbsp;&nbsp;启用此功能后，如果从其它网站上复制内容到右边的编辑器中，并且内容中包含有图片，本系统会在保存" & ChannelShortName & "时自动把相关图片复制到本站服务器上。"
			Response.Write "<br>&nbsp;&nbsp;&nbsp;&nbsp;系统会因所下载图片的大小而影响速度，建议图片较多时不要使用此功能。</font>"
		End If
		Response.Write "<br><br><font color='red'>换行请按Shift+Enter<br><br>另起一段请按Enter</font></div><br><br><br><br><iframe id='frmPreview' width='120' height='150' frameborder='1' src='Admin_imgPreview.asp'></iframe>"
		Response.Write "            </td>" &vbcrlf
		Response.Write "            <td  valign='top'>" &vbcrlf
		Call ShowFckEditor("Default",GetPhotoUrl(rsArticle("Content")),"Content","100%","500")
		Response.Write "            </td>" &vbcrlf
		Response.Write "          </tr>" &vbcrlf
	end if
	if FoundInArr(FieldShow, "DefaultPicUrl", ",") then
		Response.Write "          <tr class='tdbg'>" &vbcrlf
		Response.Write "            <td width='120' align='right' class='tdbg5'><font color='#FF0000'>缩 略 图：</font></td>" &vbcrlf
		Response.Write "            <td>" &vbcrlf
		Response.Write "              <input name='DefaultPicUrl' type='text' id='DefaultPicUrl' size='56' maxlength='200' onPropertyChange=""frmPreview.img.src=((this.value == '') ? '../images/nopic.gif' : this.value);"" value='" & GetPhotoUrl(rsArticle("DefaultPicUrl")) & "'>"
		Response.Write "              <select name='DefaultPicList' id='DefaultPicList' onChange=""DefaultPicUrl.value=this.value;frmPreview.img.src=((this.value == '') ? '../images/nopic.gif' : this.value);"">"
		Response.Write "                <option value=''"
		If GetPhotoUrl(rsArticle("DefaultPicUrl")) = "" Then Response.Write "selected"
		Response.Write ">不指定缩略图</option>"
		Dim UploadFiles
		UploadFiles = GetPhotoUrl(rsArticle("UploadFiles"))
		If UploadFiles <> "" Then
			Dim IsOtherUrl
			IsOtherUrl = True
			If InStr(UploadFiles, "|") > 1 Then
				Dim arrUploadFiles, intTemp
				arrUploadFiles = Split(UploadFiles, "|")
				For intTemp = 0 To UBound(arrUploadFiles)
					If GetPhotoUrl(rsArticle("DefaultPicUrl")) = arrUploadFiles(intTemp) Then
						Response.Write "<option value='" & arrUploadFiles(intTemp) & "' selected>" & arrUploadFiles(intTemp) & "</option>"
						IsOtherUrl = False
					Else
						Response.Write "<option value='" & arrUploadFiles(intTemp) & "'>" & arrUploadFiles(intTemp) & "</option>"
					End If
				Next
			Else
				If UploadFiles = GetPhotoUrl(rsArticle("DefaultPicUrl")) Then
					Response.Write "<option value='" & UploadFiles & "' selected>" & UploadFiles & "</option>"
					IsOtherUrl = False
				Else
					Response.Write "<option value='" & UploadFiles & "'>" & UploadFiles & "</option>"
				End If
			End If
			If IsOtherUrl = True And GetPhotoUrl(rsArticle("DefaultPicUrl")) <> "" Then
				Response.Write "<option value='" & GetPhotoUrl(rsArticle("DefaultPicUrl")) & "' selected>" & GetPhotoUrl(rsArticle("DefaultPicUrl")) & "</option>"
			End If
		End If
		Response.Write "              </select>"
		Response.Write "              <input name='UploadFiles' type='hidden' id='UploadFiles' value='" & rsArticle("UploadFiles") & "'> "
		Response.write "&nbsp;<input type='button' name='Button2' class='Button' value='站内选择' onclick=""SelectPic('DefaultPicUrl')""> 默认将抓取内容中的第一张图片"
		Response.write "<div><iframe style='top:2px' src='upload.asp?ChannelID=" & ChannelID & "&dialogtype=Articlepic' frameborder=0 scrolling=no width='450' height='25'></iframe></div>"

		Response.Write "            </td>" &vbcrlf
		Response.Write "          </tr>" &vbcrlf
	end if
	if FoundInArr(FieldShow, "Property", ",") then
		Response.Write "          <tr class='tdbg'>" &vbcrlf
		Response.Write "            <td width='120' align='right' class='tdbg5'>" & ChannelShortName & "属性：</td>" &vbcrlf
		Response.Write "            <td>" &vbcrlf
		Response.Write "              <input name='OnTop' type='checkbox' id='OnTop' value='yes'"
		If rsArticle("OnTop") = True Then Response.Write " checked"
		Response.Write "> 固顶" & ChannelShortName & "&nbsp;&nbsp;&nbsp;&nbsp;"
		Response.Write "              <input name='Hot' type='checkbox' id='Hot' value='yes'> 热门" & ChannelShortName & "&nbsp;&nbsp;&nbsp;&nbsp;"
		Response.Write "              <input name='Elite' type='checkbox' id='Elite' value='yes'"
		If rsArticle("Elite") = True Then Response.Write " checked"
		Response.Write "> 推荐" & ChannelShortName & "&nbsp;&nbsp;&nbsp;&nbsp;"
		Response.Write "            </td>" &vbcrlf
		Response.Write "          </tr>" &vbcrlf
	end if
	Response.write ShowInfoPurview(rsArticle("InfoPurview"))
	if FoundInArr(FieldShow, "Hits", ",") then
        Response.Write "          <tr class='tdbg'>" &vbcrlf
        Response.Write "            <td width='120' align='right' class='tdbg5'>点击数初始值：</td>" &vbcrlf
        Response.Write "            <td>" &vbcrlf
        Response.Write "              <input name='Hits' type='text' id='Hits' value='" & rsArticle("Hits") & "' size='10' maxlength='10' style='text-align:center'>&nbsp;&nbsp; <font color='#0000FF'>这功能是提供给管理员作弊用的。不过尽量不要用呀！^_^</font>"
        Response.Write "            </td>" &vbcrlf
        Response.Write "          </tr>" &vbcrlf
	end if
	if FoundInArr(FieldShow, "UpdateTime", ",") then
		Response.Write "          <tr class='tdbg'>" &vbcrlf
		Response.Write "            <td width='120' align='right' class='tdbg5'>录入时间：</td>" &vbcrlf
		Response.Write "            <td>" &vbcrlf
		Response.Write "              <input name='UpdateTime' type='text' id='UpdateTime' value='" & rsArticle("UpdateTime") & "' maxlength='50'> 时间格式为“年-月-日 时:分:秒”，如：<font color='#0000FF'>2003-5-12 12:32:47</font>"
		Response.Write "            </td>" &vbcrlf
		Response.Write "          </tr>" &vbcrlf
	end if
    Response.Write "        </table>" &vbcrlf
    Response.Write "      </td>" &vbcrlf
    Response.Write "    </tr>" &vbcrlf
    Response.Write "  </table>" &vbcrlf
    Response.Write "  <p align='center'>"
    Response.Write "   <input name='Action' type='hidden' id='Action' value='SaveModify'>"
    Response.Write "   <input name='ArticleID' type='hidden' id='ArticleID' value='" & rsArticle("ArticleID") & "'>"
    Response.Write "   <input name='ChannelID' type='hidden' id='ChannelID' value='" & ChannelID & "'>"
    Response.Write "   <input name='Save' type='submit' value='保存修改结果' onClick=""document.myform.Action.value='SaveModify';document.myform.target='_self';"" style='cursor:hand;'>&nbsp;"
    Response.Write "   <input name='Cancel' type='button' id='Cancel' value=' 取 消 ' onClick=""window.location.href='Admin_Article.asp?ChannelID=" & ChannelID & "&Action=Manage';"" style='cursor:hand;'><br>"
    Response.Write "  </p><br>" & vbCrLf
    Response.Write "</form>" &vbcrlf
    Response.Write "<script language='javascript'>setTimeout('setpic()',1000);" & vbCrLf
    Response.Write "function setpic(){" & vbCrLf
    If rsArticle("DefaultPicUrl") <> "" Then
    	Response.Write "frmPreview.img.src='" & GetPhotoUrl(rsArticle("DefaultPicUrl")) & "';"
    End If
    Response.Write "}" & vbCrLf
    Response.Write "</script>"
    
    rsArticle.Close
    Set rsArticle = Nothing
End Sub

Sub SaveArticle()
    Dim rsArticle, sql, trs, i
	Dim ArticleID, ClassID, Title, Intro, Content
	Dim Keyword, Author, tAuthor, CopyFrom, UpdateTime,InfoPurview
	Dim LinkUrl, UseLinkUrl
	
    ArticleID = GetForm("ArticleID")
    ClassID = GetForm("ClassID")
	
    Title = GetForm("Title")
    Keyword = GetForm("Keyword")
    UseLinkUrl = GetForm("UseLinkUrl")
    LinkUrl = GetForm("LinkUrl")
	Intro = GetForm("Intro")
    For i = 1 To Request.Form("Content").Count
        Content = Content & Request.Form("Content")(i)
    Next
    Author = GetForm("Author")
    CopyFrom = GetForm("CopyFrom")
    DefaultPicUrl = GetForm("DefaultPicUrl")
    UpdateTime = PE_CDate(GetForm("UpdateTime"))
    IncludePic = PE_CLng(GetForm("IncludePic"))
	InfoPurview = GetForm("GroupID")
	if InfoPurview="" then
		InfoPurview = "0"
	End if
	if FoundInArr(FieldShow, "Title", ",") then
		If Title = "" Then
			FoundErr = True
			ErrMsg = ErrMsg & "<li>" & ChannelShortName & "标题不能为空</li>"
		End If
	end if
	if FoundInArr(FieldShow, "ClassID", ",") then
		If ClassID = "" Or IsNull(ClassID) Or Not IsNumeric(ClassID) Then
			FoundErr = True
			ErrMsg = ErrMsg & "<li>未指定所属栏目！</li>"
		Else
			ClassID = PE_CLng(ClassID)
			Select Case ClassID
			Case -1
                ClassName = "不指定任何栏目"
                Depth = -1
                ParentPath = ""
			Case Else
				Set tClass = Conn.Execute("select ClassName,ClassType,Depth,ParentID,ParentPath,Child from PW_Class where ClassID=" & ClassID)
				If tClass.BOF And tClass.EOF Then
					FoundErr = True
					ErrMsg = ErrMsg & "<li>找不到指定的栏目！</li>"
				Else
					ClassName = tClass("ClassName")
					Depth = tClass("Depth")
					ParentPath = tClass("ParentPath")
					ParentID = tClass("ParentID")
					Child = tClass("Child")
				End If
            Set tClass = Nothing
			End Select
		end if
	else
		ClassID = -1
		Depth = -1
		ParentPath = ""
	end if
	if FoundInArr(FieldShow, "Keyword", ",") then
		Keyword = ReplaceBadChar(Keyword)
		If Keyword = "" Then
			FoundErr = True
			ErrMsg = ErrMsg & "<li>请输入" & ChannelShortName & "关键字</li>"
		End If
	end if
    If UseLinkUrl = "Yes" Then
        If LinkUrl = "" Or LCase(LinkUrl) = "http://" Then
            FoundErr = True
            ErrMsg = ErrMsg & "<li>链接地址不能为空</li>"
        Else
            If InStr(LinkUrl, "://") <= 0 And Left(LinkUrl, 1) <> "/" Then
                FoundErr = True
                ErrMsg = ErrMsg & "<li>本站地址请以 / 开头。</li>"
            End If
        End If
    Else
		if FoundInArr(FieldShow, "Content", ",") then
			If Content = "" Then
				FoundErr = True
				ErrMsg = ErrMsg & "<li>" & ChannelShortName & "内容不能为空</li>"
			End If
		end if
    End If
	
    Dim rsField
    Set rsField = Conn.Execute("select * from PW_Field where ChannelID=" & ChannelID & "")
    Do While Not rsField.EOF
		If FoundInArr(FieldShow, rsField("FieldName"), ",") then
			If rsField("EnableNull") = False and rsField("ShowOnForm") Then
				If GetForm(rsField("FieldName")) = "" Then
					FoundErr = True
					ErrMsg = ErrMsg & "<li>请输入" & rsField("Title") & "！</li>"
				End If
			End If
		End If
        rsField.MoveNext
    Loop
    
    If FoundErr = True Then
        Exit Sub
    End If
	
	if FoundInArr(FieldShow, "Intro", ",") and FoundInArr(FieldShow, "Content", ",") then 
		if Intro="" then
			Intro = GetSubStr(Nohtml(FilterJS(Content)),200,True)
		End if
	End if
	
    Title = PE_HTMLEncode(Title)
    Keyword = Replace("|" & Keyword & "|","||","|")

    '将绝对地址转化为相对地址
	Content = SetPhotoUrl(Content) '替换地址为{PW_Photourl}
    If GetForm("SaveRemotePic") = "Yes" Then
        Content = ReplaceRemoteUrl(Content)
    End If
    If Author = "" Then Author ="佚名"
    If CopyFrom = "" Then CopyFrom = "本站原创"
    Set rsArticle = Server.CreateObject("adodb.recordset")
    If Action = "SaveAdd" Then
    	ArticleID = GetNewID("PW_Article", "ArticleID")
		sql = "select top 1 * from PW_Article where 1=0"
		rsArticle.Open sql, Conn, 1, 3
		rsArticle.addnew
		rsArticle("ArticleID") = ArticleID
		rsArticle("ChannelID") = ChannelID
    ElseIf Action = "SaveModify" Then
        If ArticleID = "" Then
            FoundErr = True
            ErrMsg = ErrMsg & "<li>不能确定ArticleID的值</li>"
        Else
            ArticleID = PE_CLng(ArticleID)
            sql = "select * from PW_Article where ArticleID=" & ArticleID
            rsArticle.Open sql, Conn, 1, 3
            If rsArticle.BOF And rsArticle.EOF Then
                FoundErr = True
                ErrMsg = ErrMsg & "<li>找不到此" & ChannelShortName & "，可能已经被其他人删除。</li>"
            End If
        End If
    End If
    rsArticle("ClassID") = ClassID
    rsArticle("Title") = Title
    rsArticle("TitleFontColor") = GetForm("TitleFontColor")
    rsArticle("TitleFontType") = PE_CLng(GetForm("TitleFontType"))
    rsArticle("Intro") = Intro
    rsArticle("Content") = Content
    rsArticle("Keyword") = Keyword
    rsArticle("Author") = Author
    rsArticle("CopyFrom") = CopyFrom
    rsArticle("LinkUrl") = LinkUrl
    rsArticle("IncludePic") = IncludePic
    rsArticle("OnTop") = PE_CBool(GetForm("OnTop"))
    rsArticle("Elite") = PE_CBool(GetForm("Elite"))
	rsArticle("Hits") = PE_CLng(GetForm("Hits"))
    rsArticle("UpdateTime") = UpdateTime
    rsArticle("DefaultPicUrl") = SetPhotoUrl(DefaultPicUrl)
    rsArticle("UploadFiles") = SetPhotoUrl(UploadFiles)
    rsArticle("Deleted") = False
    rsArticle("InfoPurview") = InfoPurview
    If Not (rsField.BOF And rsField.EOF) Then
        rsField.MoveFirst
        Do While Not rsField.EOF
			If FoundInArr(FieldShow, rsField("FieldName"), ",") then
				If GetForm(rsField("FieldName")) <> "" or rsField("EnableNull")=True Then
					rsArticle(Trim(rsField("FieldName"))) = GetForm(rsField("FieldName"))
				End If
			End if
            rsField.MoveNext
        Loop
    End If
	rsField.Close
    Set rsField = Nothing
    rsArticle.Update
    rsArticle.Close
    Set rsArticle = Nothing
    Response.Write "<br><br>"
    Response.Write "<table class='border' align='center' border='0' cellpadding='2' cellspacing='1'>"
    Response.Write "  <tr class='title'> "
    Response.Write "    <td  height='22' align='center' colspan='2'> "
    If Action = "SaveAdd" Then
        Response.Write "<b>添加" & ChannelShortName & "成功</b>"
    Else
        Response.Write "<b>修改" & ChannelShortName & "成功</b>"
    End If
    Response.Write "    </td>"
    Response.Write "  </tr>"
    Response.Write "        <tr class='tdbg'>"
    Response.Write "          <td width='100' align='right' class='tdbg5'><strong>" & ChannelShortName & "标题：</strong></td>"
    Response.Write "          <td width='400'>" & Title & "</td>"
    Response.Write "        </tr>"
	if FoundInArr(FieldShow, "ClassID", ",") then
		Response.Write "        <tr class='tdbg'>"
		Response.Write "          <td width='100' align='right' class='tdbg5'><strong>所属栏目：</strong></td>"
		Response.Write "          <td width='400'>" & ShowClassPath() & "</td>"
		Response.Write "        </tr>"
	end if
	if FoundInArr(FieldShow, "Author", ",") then
		Response.Write "        <tr class='tdbg'>"
		Response.Write "          <td width='100' align='right' class='tdbg5'><strong>作&nbsp;&nbsp;&nbsp;&nbsp;者：</strong></td>"
		Response.Write "          <td width='400'>" & Author & "</td>"
		Response.Write "        </tr>"
	end if
	if FoundInArr(FieldShow, "CopyFrom", ",") then
		Response.Write "        <tr class='tdbg'>"
		Response.Write "          <td width='100' align='right' class='tdbg5'><strong>" & ChannelShortName & "来源：</strong></td>"
		Response.Write "          <td width='400'>" & CopyFrom & "</td>"
		Response.Write "        </tr>"
	end if
	if FoundInArr(FieldShow, "Keyword", ",") then
		Response.Write "        <tr class='tdbg'>"
		Response.Write "          <td width='100' align='right' class='tdbg5'><strong>关 键 字：</strong></td>"
		Response.Write "          <td width='400'>" & Mid(Keyword, 2, Len(Keyword) - 2) & "</td>"
		Response.Write "        </tr>"
	end if
    Response.Write "  <tr class='tdbg' align='center'>"
    Response.Write "    <td height='30' colspan='2'>"
    Response.Write "【<a href='Admin_Article.asp?ChannelID=" & ChannelID & "&Action=Modify&ArticleID=" & ArticleID & "'>修改本文</a>】&nbsp;"
    Response.Write "【<a href='Admin_Article.asp?ChannelID=" & ChannelID & "&Action=Add&ClassID=" & ClassID & "'>继续添加" & ChannelShortName & "</a>】&nbsp;"
    Response.Write "【<a href='Admin_Article.asp?ChannelID=" & ChannelID & "&Action=Manage&ClassID=" & ClassID & "'>" & ChannelShortName & "管理</a>】&nbsp;"
    Response.Write "【<a href='Admin_Article.asp?ChannelID=" & ChannelID & "&Action=Show&ArticleID=" & ArticleID & "'>查看" & ChannelShortName & "内容</a>】"
    Response.Write "    </td>"
    Response.Write "  </tr>"
    Response.Write "</table>" & vbCrLf
    Session("Keyword") = Trim(Request("Keyword"))
    Session("Author") = Author
    Session("CopyFrom") = CopyFrom
	If UseCreateHTML > 0 And InfoPurview="0" And ObjInstalled_FSO = True And AutoCreateType = 1 Then
        Response.Write "<br><iframe id='CreateArticle' width='100%' height='210' frameborder='0' src='Admin_CreateArticle.asp?ChannelID=" & ChannelID & "&Action=CreateArticle2&ClassID=" & ClassID & "&ArticleID=" & ArticleID & "'></iframe>"
    End If
End Sub


Sub SetProperty()
    If ArticleID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请先选定" & ChannelShortName & "！</li>"
        Exit Sub
    End If
    If Action = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>参数不足！</li>"
        Exit Sub
    End If
    
    Dim sqlProperty, rsProperty
    If InStr(ArticleID, ",") > 0 Then
        sqlProperty = "select * from PW_Article where ArticleID in (" & ArticleID & ")"
    Else
        sqlProperty = "select * from PW_Article where ArticleID=" & ArticleID
    End If
    Set rsProperty = Server.CreateObject("ADODB.Recordset")
    rsProperty.Open sqlProperty, Conn, 1, 3
    Do While Not rsProperty.EOF
		Select Case Action
		Case "SetOnTop"
			rsProperty("OnTop") = True
		Case "CancelOnTop"
			rsProperty("OnTop") = False
		Case "SetElite"
			rsProperty("Elite") = True
		Case "CancelElite"
			rsProperty("Elite") = False
		End Select
            rsProperty.Update
        rsProperty.MoveNext
    Loop
    rsProperty.Close
    Set rsProperty = Nothing
    Call WriteSuccessMsg("操作成功！", "Admin_Article.asp?ChannelID=" & ChannelID)
	Call Refresh("Admin_Article.asp?ChannelID=" & ChannelID,1)
End Sub

'******************************************************************************************
'以下为删除、清空、还原等操作使用的函数，各模块实现过程类似，修改时注意同时修改各模块内容。
'******************************************************************************************

Sub Del()
    If ArticleID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请先选定" & ChannelShortName & "！</li>"
        Exit Sub
    End If
    
    Dim sqlDel, rsDel
    sqlDel = "select A.ArticleID,A.Title,A.UpdateTime,A.Deleted,A.ClassID from PW_Article A "
    If InStr(ArticleID, ",") > 0 Then
        sqlDel = sqlDel & " where A.ArticleID in (" & ArticleID & ") order by A.ArticleID"
    Else
        sqlDel = sqlDel & " where A.ArticleID=" & ArticleID
    End If
    Set rsDel = Server.CreateObject("ADODB.Recordset")
    rsDel.Open sqlDel, Conn, 1, 3
    Do While Not rsDel.EOF
		rsDel("Deleted") = True
		rsDel.Update
        rsDel.MoveNext
    Loop
    rsDel.Close
    Set rsDel = Nothing

    Call WriteSuccessMsg("操作成功！", "Admin_Article.asp?ChannelID=" & ChannelID)
	Call Refresh("Admin_Article.asp?ChannelID=" & ChannelID,1)
End Sub



Sub ConfirmDel()
    If ArticleID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请先选定" & ChannelShortName & "！</li>"
        Exit Sub
    End If
    
    Dim sqlDel, rsDel
    sqlDel = "select UploadFiles from PW_Article where ArticleID in (" & ArticleID & ")"
    Set rsDel = Conn.Execute(sqlDel)
    Do While Not rsDel.EOF
        Call DelFiles(GetPhotoUrl(rsDel("UploadFiles")))
        rsDel.MoveNext
    Loop
    rsDel.Close
    Set rsDel = Nothing
    Conn.Execute ("delete from PW_Article where ArticleID in (" & ArticleID & ")")
    Conn.Execute ("delete from PW_Comment where ModuleType=" & ModuleType & " and InfoID in (" & ArticleID & ")")
    Call CloseConn
    Response.Redirect ComeUrl
End Sub


Sub ClearRecyclebin()
    
    Dim sqlDel, rsDel
    ArticleID = ""
    sqlDel = "select ArticleID,UploadFiles from PW_Article where Deleted=" & PE_True & " and ChannelID=" & ChannelID
    Set rsDel = Conn.Execute(sqlDel)
    Do While Not rsDel.EOF
        If ArticleID = "" Then
            ArticleID = rsDel(0)
        Else
            ArticleID = ArticleID & "," & rsDel(0)
        End If
        Call DelFiles(GetPhotoUrl(rsDel("UploadFiles")))
        rsDel.MoveNext
    Loop
    rsDel.Close
    Set rsDel = Nothing
    If ArticleID <> "" Then
        Conn.Execute ("delete from PW_Article where Deleted=" & PE_True & " and ChannelID=" & ChannelID & "")
        Conn.Execute ("delete from PW_Comment where ModuleType=" & ModuleType & " and InfoID in (" & ArticleID & ")")
    End If
    Call CloseConn
    Response.Redirect ComeUrl
End Sub

Sub Restore()
    If ArticleID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请先选定" & ChannelShortName & "！</li>"
        Exit Sub
    End If
    
    Dim sqlDel, rsDel
    If InStr(ArticleID, ",") > 0 Then
        sqlDel = "select * from PW_Article where ArticleID in (" & ArticleID & ")"
    Else
        sqlDel = "select * from PW_Article where ArticleID=" & ArticleID
    End If
    Set rsDel = Server.CreateObject("ADODB.Recordset")
    rsDel.Open sqlDel, Conn, 1, 3
    Do While Not rsDel.EOF
        rsDel("Deleted") = False
        rsDel.Update
        rsDel.MoveNext
    Loop
    rsDel.Close
    Set rsDel = Nothing

    Call WriteSuccessMsg("操作成功！", "Admin_Article.asp?ChannelID=" & ChannelID)
    Call Refresh("Admin_Article.asp?ChannelID=" & ChannelID,1)
End Sub

Sub RestoreAll()
    Dim sqlDel, rsDel
    sqlDel = "select * from PW_Article where Deleted=" & PE_True & " and ChannelID=" & ChannelID
    Set rsDel = Server.CreateObject("ADODB.Recordset")
    rsDel.Open sqlDel, Conn, 1, 3
    Do While Not rsDel.EOF
        rsDel("Deleted") = False
        rsDel.Update
        rsDel.MoveNext
    Loop
    rsDel.Close
    Set rsDel = Nothing
    Call WriteSuccessMsg("操作成功！", "Admin_Article.asp?ChannelID=" & ChannelID)
    Call Refresh("Admin_Article.asp?ChannelID=" & ChannelID,1)
End Sub


Sub Show()
    If ArticleID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请指定" & ChannelShortName & "ID！</li>"
        Exit Sub
    End If
    Dim rsArticle
    Set rsArticle = Conn.Execute("select * from PW_Article where ArticleID=" & ArticleID & "")
    If rsArticle.BOF And rsArticle.EOF Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>找不到" & ChannelShortName & "！</li>"
        rsArticle.Close
        Set rsArticle = Nothing
        Exit Sub
    End If
    ClassID = rsArticle("ClassID")
	Select Case ClassID
        Case 0
            FoundErr = True
            ErrMsg = ErrMsg & "<li>指定的栏目不允许此操作！</li>"
		Case -1
			ClassName = "不指定任何栏目"
		Case Else
            Set tClass = Conn.Execute("select ClassName,ClassType,Depth,ParentID,ParentPath,Child from PW_Class where ClassID=" & ClassID)
            If tClass.BOF And tClass.EOF Then
                FoundErr = True
                ErrMsg = ErrMsg & "<li>找不到指定的栏目！</li>"
            Else
                ClassName = tClass("ClassName")
                Depth = tClass("Depth")
                ParentPath = tClass("ParentPath")
                ParentID = tClass("ParentID")
                Child = tClass("Child")
            End If
            Set tClass = Nothing
	end select
    If FoundErr = True Then
        rsArticle.Close
        Set rsArticle = Nothing
        Exit Sub
    End If

    Response.Write "<br>您现在的位置：&nbsp;<a href='Admin_Article.asp?ChannelID=" & ChannelID & "'>" & ChannelShortName & "管理</a>&nbsp;&gt;&gt;&nbsp;"
    If ParentID > 0 Then
        Dim sqlPath, rsPath
        sqlPath = "select ClassID,ClassName from PW_Class where ClassID in (" & ParentPath & ") order by Depth"
        Set rsPath = Conn.Execute(sqlPath)
        Do While Not rsPath.EOF
            Response.Write "<a href='Admin_Article.asp?ChannelID=" & ChannelID & "&ClassID=" & rsPath(0) & "'>" & rsPath(1) & "</a>&nbsp;&gt;&gt;&nbsp;"
            rsPath.MoveNext
        Loop
        rsPath.Close
        Set rsPath = Nothing
    End If
    Response.Write "<a href='Admin_Article.asp?ChannelID=" & ChannelID & "&ClassID=" & ClassID & "'>" & ClassName & "</a>&nbsp;&gt;&gt;&nbsp;查看" & ChannelShortName & "内容："
    Select Case rsArticle("IncludePic")
        Case 1
            Response.Write "<font color=blue>" & ArticlePro1 & "</font>"
        Case 2
            Response.Write "<font color=blue>" & ArticlePro2 & "</font>"
        Case 3
            Response.Write "<font color=blue>" & ArticlePro3 & "</font>"
        Case 4
            Response.Write "<font color=blue>" & ArticlePro4 & "</font>"
    End Select
    
    Response.Write rsArticle("Title") & "<br><br>"

    Response.Write "<table width='100%' border='0' align='center' cellpadding='5' cellspacing='0' class='border'>"
    Response.Write "  <tr align='center'>"
    Response.Write "    <td class='tdbg' height='200' valign='top'>"
    Response.Write "      <table width='98%' border='0' cellpadding='2' cellspacing='1' bgcolor='#FFFFFF'>"
    Response.Write "  <tr align='center' class='tdbg'>"
    Response.Write "    <td height='40' colspan='2'>"
    Response.Write "<font size='4'><b>" & rsArticle("Title") & "</b></font>"
    Response.Write "</td>"
    Response.Write "  <tr align='center' class='tdbg'>"
    Response.Write "    <td colspan='2'>"
    Dim Author, CopyFrom
    Author = rsArticle("Author")
    CopyFrom = rsArticle("CopyFrom")
	if FoundInArr(FieldShow, "Author", ",") then
		Response.Write "作者：" & Author & "&nbsp;&nbsp;&nbsp;&nbsp;"
	end if
	if FoundInArr(FieldShow, "CopyFrom", ",") then
		Response.write ChannelShortName &"来源： "& CopyFrom &"&nbsp;&nbsp;&nbsp;&nbsp;"
	end if
	if FoundInArr(FieldShow, "Hits", ",") then
		Response.Write "点击数：" & rsArticle("Hits") & "&nbsp;&nbsp;&nbsp;&nbsp;"
	end if
	if FoundInArr(FieldShow, "UpdateTime", ",") then
		Response.write "更新时间：" & FormatDateTime(rsArticle("UpdateTime"), 2)
	end if
	if FoundInArr(FieldShow, "CopyFrom", ",") then
		Response.write ChannelShortName & "属性："
		If rsArticle("OnTop") = True Then
			Response.Write "<font color=blue>顶</font>&nbsp;"
		Else
			Response.Write "&nbsp;&nbsp;&nbsp;"
		End If
		If rsArticle("Hits") >= HitsOfHot Then
			Response.Write "<font color=red>热</font>&nbsp;"
		Else
			Response.Write "&nbsp;&nbsp;&nbsp;"
		End If
		If rsArticle("Elite") = True Then
			Response.Write "<font color=green>荐</font>"
		Else
			Response.Write "&nbsp;&nbsp;"
		End If
	end if
    Response.Write "    </td>"
    Response.Write "  </tr>"
    Response.Write "  <tr class='tdbg'>"
    Response.Write "    <td colspan='2'><table width='100%' height='100%' border='0' cellpadding='0' cellspacing='5'>"
    Response.Write "        <tr>"
    Response.Write "          <td height='200' valign='top'>"
    If Trim(rsArticle("LinkUrl")) <> "" Then
        Response.Write "<p align='center'><br><br><br><font color=red>本" & ChannelShortName & "是链接外部" & ChannelShortName & "内容。链接地址为：<a href='" & rsArticle("LinkUrl") & "' target='_blank'>" & rsArticle("LinkUrl") & "</a></font></p>"
    Else
        Response.Write "<p>" & GetPhotoUrl(rsArticle("Content")) & "</p>"
    End If
    Response.Write "       </td>"
    Response.Write "        </tr>"
    Response.Write "      </table>"
    Response.Write "    </td>"
    Response.Write "  </tr>"
    Response.Write "      </table>"
    Response.Write "    </td>"
    Response.Write "  </tr>"
    Response.Write "</table>" & vbCrLf
    Response.Write "<form name='formA' method='get' action='Admin_Article.asp'><p align='center'>"
    Response.Write "<input type='hidden' name='ChannelID' value='" & ChannelID & "'>"
    Response.Write "<input type='hidden' name='ArticleID' value='" & ArticleID & "'>"
    Response.Write "<input type='hidden' name='Action' value=''>" & vbCrLf

    If rsArticle("Deleted") = False Then
		if Purview_Del then
            Response.Write "<input type='submit' name='submit' value=' 删 除 ' onclick=""document.formA.Action.value='Del'"">&nbsp;&nbsp;"
		end if
		if FoundInArr(FieldShow, "Property", ",") then
            If rsArticle("OnTop") = False Then
                Response.Write "<input type='submit' name='submit' value='设为固顶' onclick=""document.formA.Action.value='SetOnTop'"">&nbsp;&nbsp;"
            Else
                Response.Write "<input type='submit' name='submit' value='取消固顶' onclick=""document.formA.Action.value='CancelOnTop'"">&nbsp;&nbsp;"
            End If
            If rsArticle("Elite") = False Then
                Response.Write "<input type='submit' name='submit' value='设为推荐' onclick=""document.formA.Action.value='SetElite'"">"
            Else
                Response.Write "<input type='submit' name='submit' value='取消推荐' onclick=""document.formA.Action.value='CancelElite'"">"
            End If
		end if
    Else
		if Purview_Del then
            Response.Write "<input type='submit' name='submit' value='彻底删除' onclick=""if(confirm('确定要彻底删除此" & ChannelShortName & "吗？彻底删除后将无法还原！')==true){document.formA.Action.value='ConfirmDel';}"">&nbsp;&nbsp;"
            Response.Write "<input type='submit' name='submit' value=' 还 原 ' onclick=""document.formA.Action.value='Restore'"">"
		end if
    End If
    Response.Write "&nbsp;&nbsp;<input type='submit' name='submit' value=' 返 回 ' onclick=""window.history.go(-1)"">"
    Response.Write "</p></form>"

    rsArticle.Close
    Set rsArticle = Nothing
End Sub
%>