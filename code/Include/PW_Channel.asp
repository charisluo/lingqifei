<%
'定义频道设置相关的变量
Dim ChannelID, ChannelName, ChannelShortName, ChannelItemUnit, MaxPerPage_Channel,FieldShow,ChannelDir, ChannelUrl, ChannelUrl_ASPFile
Dim ModuleType, ModuleName, SheetName
Dim tempindex, templist, temparticle



Sub GetChannel(tChannelID)
    Dim sqlChannel, rsChannel
    ChannelItemUnit = ""
    MaxPerPage_Channel = 20
    ModuleType = 0
	tempindex = ""
	templist = ""
	temparticle = ""
	ChannelUrl = ""
    If tChannelID > 0 Then
        sqlChannel = "select * from PW_Channel where ChannelID=" & tChannelID
        Set rsChannel = Conn.Execute(sqlChannel)
        If rsChannel.BOF And rsChannel.EOF Then
            FoundErr = True
            ErrMsg = ErrMsg & "找不到指定的频道"
        Else
            If rsChannel("Disabled") = True Then
                FoundErr = True
                ErrMsg = ErrMsg & "<li>此频道已经被管理员禁用！</li>"
            End If
            ChannelName = rsChannel("ChannelName")
            ChannelShortName = rsChannel("ChannelShortName")
            ChannelItemUnit = rsChannel("ChannelItemUnit")
			ChannelDir = rsChannel("ChannelDir")
            MaxPerPage_Channel = rsChannel("MaxPerPage_Channel")
            ModuleType = rsChannel("ModuleType")
			tempindex = rsChannel("Tempindex")
			templist = rsChannel("Templist")
			temparticle = rsChannel("Temparticle")
			FieldShow = rsChannel("FieldShow")
			ChannelUrl = strInstallDir & HTMLDIR & "/" & ChannelDir
			ChannelUrl_ASPFile = strInstallDir & ChannelDir
            Select Case ModuleType
            Case 1
                ModuleName = "Article"
                SheetName = "PW_Article"
            Case 2
                ModuleName = "Soft"
                SheetName = "PW_Soft"
            Case 3
                ModuleName = "Product"
                SheetName = "PW_Product"
            Case 4
                ModuleName = "Photo"
                SheetName = "PW_Photo"
            End Select
        End If
        rsChannel.Close
        Set rsChannel = Nothing
    End If
End Sub
%>