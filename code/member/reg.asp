<!--#include file="../Sys_Start.asp"-->
<!--#include file="../Include/PW_MD5.asp"-->
<!--#include file="../include/PW_FSO.asp"-->
<!--#include file="../include/PW_Channel.asp"-->
<!--#include file="../include/PW_Class.asp"-->
<!--#include file="../include/PW_Common_Front.asp"-->
<!--#include file="../include/PW_models_front.asp"-->
<%
If Action = "SaveAddUser" then
 	Call SaveUser
	If FoundErr = True Then
		Call ShowErrMsg(ErrMsg, ComeUrl)
		Response.End()
	End If
	Call ShowSuccessMsg("注册会员成功！", ComeUrl)
Else
	Dim strParameter,strList,strTemp
	UserID=PE_Clng(GetValue("UserID"))
	strFileName="reg.asp"
	
	if UserID>0 then
		strFileName=strFileName&"?UserID="&UserID
	End if
	
	strHtml = ReadFileContent(Template_Dir &"member/reg.html")
	
	Call ReplaceCommonLabel
	strHtml = PE_Replace(strHtml, "{$GroupOption}", "用户注册_" &  GetUserGroup_Option(1))
	strHtml = PE_Replace(strHtml, "{$pagetitle}", "用户注册_" & sitename)
	strHtml = PE_Replace(strHtml,"{$location}",strNavPath & "&nbsp;" & strNavLink & "&nbsp;在线留言")
	Call GetModelFront_Html
	Response.Write strHtml
	
	Call CloseConn
end If



Sub SaveUser()
	Dim UserID,sqlUser
    Dim rsUser,Question,Answer,UserName,Email,TrueName,Sex,Address,Phone,Fax,ZipCode
    UserID = PE_CLng(GetForm("UserID"))
    GroupID = PE_CLng(GetForm("GroupID"))
    UserName = ReplaceBadChar(GetForm("UserName"))
    UserPassword = ReplaceBadChar(GetForm("UserPassword"))
	TrueName = ReplaceBadChar(GetForm("TrueName"))
	Sex = PE_CLng(GetForm("Sex"))
	Address = ReplaceBadChar(GetForm("Address"))
	Phone = ReplaceBadChar(GetForm("Phone"))
	Fax = ReplaceBadChar(GetForm("Fax"))
	ZipCode = ReplaceBadChar(GetForm("ZipCode"))
    Email = ReplaceBadChar(GetForm("Email"))
    Question = ReplaceBadChar(GetForm("Question"))
    Answer = ReplaceBadChar(GetForm("Answer"))
    'If Action = "SaveAdd" Then
        If UserName = "" Then
            FoundErr = True
            ErrMsg = "用户名不能为空！"
        End If
        If UserPassword = "" Then
            FoundErr = True
            ErrMsg = "密码不能为空！"
        End If
'        If Answer = "" Then
'            FoundErr = True
'            ErrMsg = "提示答案不能为空！"
'        End If
   ' End If
'    If Question = "" Then
'        FoundErr = True
'        ErrMsg = "提示问题不能为空！"
'    End If
'    If TrueName = "" Then
'        FoundErr = True
'        ErrMsg = "真实姓名不能为空！"
'    End If
'    If Address = "" Then
'        FoundErr = True
'        ErrMsg = "联系地址不能为空！"
'    End If
'    If Phone = "" Then
'        FoundErr = True
'        ErrMsg = "联系电话不能为空！"
'    End If
    If FoundErr Then
        Exit Sub
    End If
    If Action = "SaveAddUser" Then
        sqlUser = "SELECT * FROM PW_User Where UserName='" & UserName & "'"
        Set rsUser = Server.CreateObject("adodb.recordset")
        rsUser.Open sqlUser, Conn, 1, 3
        If rsUser.BOF And rsUser.EOF Then
            UserID = GetNewID("PW_User", "UserID")
            rsUser.addnew
            rsUser("UserID") = UserID
            rsUser("UserName") = UserName
            rsUser("LoginTimes") = 0
            rsUser("RegTime") = Now()
        Else
            FoundErr = True
            ErrMsg = "该用户名已被他人占用，请输入不同的用户名！"
        End If
    Else
        sqlUser = "SELECT * FROM PW_User Where UserID=" & UserID & ""
        Set rsUser = Server.CreateObject("adodb.recordset")
        rsUser.Open sqlUser, Conn, 1, 3
        If rsUser.BOF And rsUser.EOF Then
            FoundErr = True
            ErrMsg = "找不到指定的会员！"
        End If
    End If
    If FoundErr Then
        rsUser.Close
        Set rsUser = Nothing
        Exit Sub
    End If
    If UserPassword <> "" Then
        rsUser("UserPassword") = MD5(UserPassword, 16)
    End If
    rsUser("Question") = Question
    If Answer <> "" Then
        rsUser("Answer") = MD5(Answer, 16)
    End If
    rsUser("GroupID") = GroupID
    rsUser("Email") = Email
    rsUser("TrueName") = TrueName
    rsUser("Sex") = Sex
    rsUser("Address") = Address
    rsUser("Phone") = Phone
    rsUser("Fax") = Fax
    rsUser("ZipCode") = ZipCode
    rsUser("Email") = Email
    rsUser.Update
    rsUser.Close
    Set rsUser = Nothing
End Sub



Function GetUserGroup_Option(CurrentGroupID)
    Dim strGroup, rsGroup
    Set rsGroup = Conn.Execute("select GroupID,GroupName from PW_UserGroup order by GroupID asc")
    Do While Not rsGroup.EOF
        strGroup = strGroup & "<option value='" & rsGroup(0) & "'"
        If rsGroup(0) = CurrentGroupID Then
            strGroup = strGroup & " selected"
        End If
        strGroup = strGroup & ">" & rsGroup(1) & "</option>"
        rsGroup.MoveNext
    Loop
    rsGroup.Close
    Set rsGroup = Nothing
    
    GetUserGroup_Option = strGroup
End Function

%>