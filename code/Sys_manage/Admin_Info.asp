<!--#include file="Admin_info_common.asp"-->
<%
Const NeedCheckComeUrl = True   '是否需要检查外部访问
Const PurviewLevel_Channel = 0   '0--不检查，1--频道管理员，2--栏目总编，3--栏目管理员
Const PurviewLevel_Others = "Info"   '其他权限
Dim pagelike, ID

pagelike = GetUrl("pagelike")
strFileName="Admin_Info.asp?pagelike="&pagelike
ID = GetValue("ID")
If IsValidID(ID) = False Then
    ID = ""
End If

Response.Write "<html><head><title>单页文档管理</title>" & vbCrLf
Response.Write "<meta http-equiv='Content-Type' content='text/html; charset=gb2312'>" & vbCrLf
Response.Write "<link href='Admin_Style.css' rel='stylesheet' type='text/css'>" & vbCrLf
Response.Write "</head>" & vbCrLf
Response.Write "<body leftmargin='2' topmargin='0' marginwidth='0' marginheight='0'>" & vbCrLf
Response.Write "<table width='100%' border='0' align='center' cellpadding='2' cellspacing='1' class='border'>" & vbCrLf
call ShowPageTitle( "单页文档管理")
Response.Write "  <tr class='tdbg'>" & vbCrLf
Response.Write "    <td width='70' height='30'><strong>管理导航：</strong></td>" & vbCrLf
Response.Write "    <td height='30'>" & vbCrLf
if FoundInArr(AdminPurview_Info,"info_Add", ",") or AdminPurview=1 then
	Response.Write "	<a href='Admin_Info.asp?Action=Add'>添加文档</a>&nbsp;|&nbsp;"
end if
Response.Write "	<a href='Admin_Info.asp'>所有文档</a>&nbsp;"
Response.Write "    </td>" & vbCrLf
Response.Write "  </tr>" & vbCrLf
Response.Write "</table>" & vbCrLf

Select Case Action
	Case "Add"
		Call Add()
	Case "SaveAdd","SaveModify"
		Call SaveInfo()
	Case "Modify"
		Call Modify()
	Case "Del"
		Call DeleteInfo()
	Case "Create","CreateAll"
		Call CreateHtml()
	case else
		Call main()
end select


If FoundErr = True Then
    Call WriteErrMsg(ErrMsg, ComeUrl)
End If
Response.Write "</body></html>"
Call CloseConn


Sub main()
	Dim RsInfoList, Sql, InfoNum ,Querysql
    Call ShowJS_Main("文档")
    Response.Write "<br><table width='100%' border='0' align='center' cellpadding='2' cellspacing='1' class='border'>"& vbCrLf
	Response.Write "  <tr class='title'>"& vbCrLf
	Response.Write "    <td height='22'>" & GetRootPagelike() & "</td>"& vbCrLf
	Response.Write "  </tr>"
    Response.Write "</table><br>"& vbCrLf
    Response.Write "<table width='100%' border='0' cellpadding='0' cellspacing='0'><tr>"& vbCrLf
    Response.Write "    <form name='myform' method='Post' action='Admin_Info.asp' onsubmit='return ConfirmDel();'>"& vbCrLf
    Response.Write "     <td><table class='border' border='0' cellspacing='1' width='100%' cellpadding='0'>"& vbCrLf
    Response.Write "          <tr class='title' height='22'> "& vbCrLf
    Response.Write "            <td height='22' width='50' align='center'><strong>选中</strong></td>"& vbCrLf
    Response.Write "            <td width='25' align='center'><strong>ID</strong></td>"& vbCrLf
    Response.Write "            <td align='center' ><strong>文档标题</strong></td>"& vbCrLf
    Response.Write "            <td width='100' align='center'><strong>标识</strong></td>"& vbCrLf
    Response.Write "            <td width='150' align='center' ><strong>常规管理操作</strong></td>"& vbCrLf
    Response.Write "          </tr>"& vbCrLf
	Response.write ""
	if pagelike<>"" then
		Querysql=" and Pagelike = '"& Pagelike &"'"
	end if
	Sql="Select * from PW_Info where 1=1 "& Querysql &" order by ID desc"
	Set RsInfoList=Server.CreateObject("ADODB.Recordset")
    RsInfoList.Open sql, Conn, 1, 1
    If RsInfoList.BOF And RsInfoList.EOF Then
        totalPut = 0
		Response.write "<tr><td colspan='10' height='30' align='center'>没有任何文档</td></tr>"
	else
        InfoNum = 0
		totalPut = RsInfoList.RecordCount
        If CurrentPage < 1 Then
            CurrentPage = 1
        End If
        If (CurrentPage - 1) * MaxPerPage > totalPut Then
            If (totalPut Mod MaxPerPage) = 0 Then
                CurrentPage = totalPut \ MaxPerPage
            Else
                CurrentPage = totalPut \ MaxPerPage + 1
            End If
        End If
        If CurrentPage > 1 Then
            If (CurrentPage - 1) * MaxPerPage < totalPut Then
                RsInfoList.Move (CurrentPage - 1) * MaxPerPage
            Else
                CurrentPage = 1
            End If
        End If
        Do While Not RsInfoList.EOF
            Response.Write "      <tr class='tdbg' onmouseout=""this.className='tdbg'"" onmouseover=""this.className='tdbgmouseover'"">"& vbCrLf
			Response.Write "        <td align='center'><input name='ID' type='checkbox' id='ID' value='" & RsInfoList("ID") & "'></td>"& vbCrLf
			Response.Write "        <td align='center'>" & RsInfoList("ID") & "</td>"& vbCrLf
			Response.Write "        <td align='left'>"
            Response.Write "        &nbsp;<a href='Admin_Info.asp?Action=Modify&ID="& RsInfoList("ID") &"'>"& RsInfoList("Title") &"</a>"& vbCrLf
			Response.Write "        </td>"
            Response.Write "        <td align='center'>"& RsInfoList("Pagelike") &"</td>"& vbCrLf
			Response.write "		<td align='center'>"
			if FoundInArr(AdminPurview_Info,"info_Modify", ",") or AdminPurview=1 then
				Response.write "<a href='Admin_Info.asp?Action=Modify&ID="& RsInfoList("ID") &"'>修改</a>&nbsp;"
			end if
			if  FoundInArr(AdminPurview_Info,"info_Del", ",") or AdminPurview=1 then
				Response.write "<a href='Admin_Info.asp?Action=Del&ID="& RsInfoList("ID") &"' onclick=""return confirm('确定要删除此版面吗？');"">删除</a>&nbsp;"
			end if
			Response.write "<a href='Admin_Info.asp?Action=Create&ID="& RsInfoList("ID") &"'>更新文档</a>&nbsp;"
		RsInfoList.movenext
		InfoNum=InfoNum+1
		loop
	end if
	Response.write " 		</table>"& vbCrLf
	if AdminPurview=1 then
		Response.Write "<table width='100%' border='0' cellpadding='0' cellspacing='0'>"& vbCrLf
		Response.Write "  <tr>"& vbCrLf
		Response.Write "    <td width='180' height='30'><input name='chkAll' type='checkbox' id='chkAll' onclick='CheckAll(this.form)' value='checkbox'>选中本页显示的所有文档</td>"& vbCrLf
		Response.Write "    <td><input name='submit1' type='submit' id='submit1' onClick=""document.myform.Action.value='Del'"" value=' 彻底删除 ' Class='Submit'>&nbsp;<input name='submit1' type='submit' onClick=""document.myform.Action.value='Create'"" value=' 更新选中文档 ' Class='Button'>&nbsp;<input name='submit1' type='submit' onClick=""document.myform.Action.value='CreateAll'"" value=' 更新全部文档 ' Class='Button'>"
		Response.Write "<input name='Action' type='hidden' id='Action' value=''>"
		Response.Write "  </td></tr>"& vbCrLf
		Response.Write "</table>"& vbCrLf
	end if
	Response.write " 	</td>"& vbCrLf
	Response.write " 	</form>"& vbCrLf
	Response.write " </tr>"& vbCrLf
	Response.write " </table>"& vbCrLf

    If totalPut > 0 Then
        Response.Write ShowPage(strFileName, totalPut, MaxPerPage, CurrentPage, false, True, "篇文档", false)
    End If
End Sub


function Show_InfoJs()
	Response.Write "<script language='JavaScript'>" & vbCrLf
    Response.Write "function CheckForm(){" & vbCrLf
    Response.Write "  if (document.form1.Title.value==''){" & vbCrLf
    Response.Write "    alert('文档标题不能为空！');" & vbCrLf
    Response.Write "    document.form1.Title.focus();" & vbCrLf
    Response.Write "    return false;" & vbCrLf
    Response.Write "  }" & vbCrLf
    Response.Write "  if (document.form1.OrderID.value==''){" & vbCrLf
    Response.Write "    alert('排序号不能为空！');" & vbCrLf
    Response.Write "    document.form1.OrderID.focus();" & vbCrLf
    Response.Write "    return false;" & vbCrLf
    Response.Write "  }" & vbCrLf
    Response.Write "  if (document.form1.Pagelike.value==''){" & vbCrLf
    Response.Write "    alert('关联标识不能为空！');" & vbCrLf
    Response.Write "    document.form1.Pagelike.focus();" & vbCrLf
    Response.Write "    return false;" & vbCrLf
    Response.Write "  }" & vbCrLf
	Response.Write "  if (document.form1.filename.value==''){" & vbCrLf
	Response.Write "    alert('文件名不能为空！');" & vbCrLf
	Response.Write "    document.form1.filename.focus();" & vbCrLf
	Response.Write "    return false;" & vbCrLf
	Response.Write "  }" & vbCrLf
	Response.Write "  if (document.form1.templateInfo.value==''){" & vbCrLf
	Response.Write "    alert('模板不能为空！');" & vbCrLf
	Response.Write "    document.form1.templateInfo.focus();" & vbCrLf
	Response.Write "    return false;" & vbCrLf
	Response.Write "  }" & vbCrLf
    Response.Write "  var oEditor  =FCKeditorAPI.GetInstance(""Content"");" & vbCrLf
    Response.Write "  var Content =oEditor.GetXHTML();" & vbCrLf
    Response.Write "  if (Content==null || Content==''){" & vbCrLf
    Response.Write "     alert('内容不能为空！');" & vbCrLf
    Response.Write "     oEditor.Focus();" & vbCrLf
    Response.Write "     return(false);" & vbCrLf
    Response.Write "  }" & vbCrLf
    Response.Write "}" & vbCrLf
	Response.Write "function SelectTemplate(formstr){" & vbCrLf
	Response.Write "  var Templateform = document.getElementById(formstr);" & vbCrLf
	Response.Write "  var arr=showModalDialog('Admin_SelectFile.asp?dialogtype=Template', '', 'dialogWidth:820px; dialogHeight:600px; help: no; scroll: yes; status: no');" & vbCrLf
	Response.Write "  if(arr!=null){" & vbCrLf
	Response.Write "    var ss=arr.split('|');" & vbCrLf
	Response.write "    var regS = new RegExp('"& Template_Dir &"',""gi"");"
	Response.write "    str=ss[0].replace(regS,'{@TemplateDir}'); "
	Response.Write "    Templateform.value=str;" & vbCrLf
	Response.Write "  }" & vbCrLf
	Response.Write "}" & vbCrLf
	Response.Write "</script>" & vbCrLf
end function


Sub Add()
	Call Show_InfoJs()
    Response.Write "<br><table width='100%'><tr><td align='left'>您现在的位置：<a href='Admin_Info.asp'>单页文档管理</a>&nbsp;&gt;&gt;&nbsp;添加文档</td></tr></table>"
    Response.Write "<form name='form1' method='post' action='Admin_Info.asp' onsubmit='return CheckForm()'>" & vbCrLf
    Response.Write "<table width='100%' border='0' align='center' cellpadding='5' cellspacing='1' class='border'><tr class='tdbg'><td height='100' valign='top'>" & vbCrLf
    Response.Write "<table width='98%' align='center' cellpadding='2' cellspacing='1' bgcolor='#FFFFFF'>" & vbCrLf
    Response.Write "  <tbody>" & vbCrLf
    Response.Write "    <tr class='tdbg'>"
    Response.Write "      <td class='tdbg5' align='center'>文档标题：</td>"
    Response.Write "      <td><input name='Title' type='text' size='30' maxlength='50'> (50字以内)<font color=red>*</font></td>"
    Response.Write "    </tr>" & vbCrLf
    Response.Write "    <tr class='tdbg'>" & vbCrLf
    Response.Write "      <td class='tdbg5' align='center'>关键字：</td>" & vbCrLf
    Response.Write "      <td><input name='Meta_Keywords' type='text' size='40' maxlength='250'> 多个关键词请用,号分隔</td>" & vbCrLf
    Response.Write "    </tr>" & vbCrLf
    Response.Write "    <tr class='tdbg'>" & vbCrLf
    Response.Write "      <td class='tdbg5' align='center'>摘要信息：</td>" & vbCrLf
    Response.Write "      <td><textarea name='Meta_Description' cols='60' rows='2' id='Meta_Description'></textarea>针对搜索引擎设置的网页描述</td>" & vbCrLf
    Response.Write "    </tr>" & vbCrLf
    Response.Write "    <tr class='tdbg'>"
    Response.Write "      <td class='tdbg5' align='center'>排序号：</td>"
    Response.Write "      <td><input name='OrderID' type='text' size='5'> (只能是数字)<font color=red> *</font></td>"
    Response.Write "    </tr>" & vbCrLf
    Response.Write "    <tr class='tdbg'>"
    Response.Write "      <td class='tdbg5' align='center'>关联标识：</td>"
    Response.Write "      <td><input name='Pagelike' id='Pagelike' type='text' size='10'> "& Pagelike_List("") &" <font color=red>*</font> 通过这个标识来识别类同页面,标识名称为前台栏目名称</td>"
    Response.Write "    </tr>" & vbCrLf 
	Response.Write "    <tr class='tdbg'>"
	Response.Write "      <td class='tdbg5' align='center'>文件名：</td>"
	Response.Write "      <td><input name='filename' type='text' size='30' value="""& InstallDir & HTMLDIR &"/info_"& GetNewID("PW_Info", "ID") &".html""> <font color='#FF0000'>*</font> 相对于CMS安装目录</td>"
	Response.Write "    </tr>" & vbCrLf 
	if AdminPurview=1 then
		Response.Write "    <tr class='tdbg'>"
		Response.Write "      <td class='tdbg5' align='center'>模板选择：</td>"
		Response.Write "      <td><input name='templateInfo' id='templateInfo' type='text' size='30'>  <input type='button' name='Button2' class='Button' value='选择模板... ' onclick=""SelectTemplate('templateInfo')""> <font color='#FF0000'>*</font></td>"
		Response.Write "    </tr>" & vbCrLf
	End if
    Response.Write "    <tr class='tdbg'>"
    Response.Write "      <td class='tdbg5' align='center'>文档内容：</td>"
    Response.Write "      <td>"& vbCrLf
    Call ShowFckEditor("Default","","Content","100%","500")
	Response.write "      </td>"
    Response.Write "    </tr>" & vbCrLf
    Response.Write "  </table>"& vbCrLf
	Response.Write "</td></table>"
    Response.Write "  <p align='center'>"& vbCrLf
    Response.Write "   <input name='Action' type='hidden' id='Action' value='SaveAdd'>"
    Response.Write "   <input name='ChannelID' type='hidden' id='ChannelID' value='" & ChannelID & "'>"
    Response.Write "   <input name='add' type='submit'  id='Add' value=' 添 加 ' onClick=""document.myform.Action.value='SaveAdd';document.myform.target='_self';"" style='cursor:hand;' class='Submit'>&nbsp; "
    Response.Write "   <input name='Cancel' type='button' id='Cancel' value=' 取 消 ' onClick=""window.location.href='Admin_Info.asp';"" style='cursor:hand;' class='Submit'>"
    Response.Write "  </p>"
    Response.Write "</form>"& vbCrLf
End Sub


Sub Modify()
	Dim Rs, Sql
    If ID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请指定要修改的文档ID</li>"
        Exit Sub
    Else
        ID = PE_CLng(ID)
    End If
	
    Sql = "select * from PW_Info where ID=" & ID & ""
    Set Rs = Conn.Execute(Sql)
    If Rs.BOF And Rs.EOF Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>找不到该文档</li>"
        Rs.Close
        Set Rs = Nothing
        Exit Sub
    End If
    If FoundErr = True Then
        Rs.Close
        Set Rs = Nothing
        Exit Sub
    End If
	Call Show_InfoJs()
    Response.Write "<br><table width='100%'><tr><td align='left'>您现在的位置：<a href='Admin_Info.asp'>单页文档管理</a>&nbsp;&gt;&gt;&nbsp;修改文档</td></tr></table>"
    Response.Write "<form name='form1' method='post' action='Admin_Info.asp' onsubmit='return CheckForm()'>" & vbCrLf
    Response.Write "<table width='100%' border='0' align='center' cellpadding='5' cellspacing='1' class='border'><tr class='tdbg'><td height='100' valign='top'>" & vbCrLf
    Response.Write "<table width='95%' align='center' cellpadding='2' cellspacing='1' bgcolor='#FFFFFF'>" & vbCrLf
    Response.Write "  <tbody>" & vbCrLf
    Response.Write "    <tr class='tdbg'>"& vbCrLf
    Response.Write "      <td class='tdbg5' align='center'>文档标题：</td>"& vbCrLf
    Response.Write "      <td><input name='Title' type='text' size='30' maxlength='50' value='"& Rs("Title") &"'> (50字以内)<font color=red>*</font></td>"& vbCrLf
    Response.Write "    <tr class='tdbg'>" & vbCrLf
    Response.Write "      <td class='tdbg5' align='center'>关键字：</td>" & vbCrLf
    Response.Write "      <td><input name='Meta_Keywords' type='text' value='"& Rs("Meta_Keywords") &"' size='40' maxlength='250'> 多个关键词请用,号分隔</td>" & vbCrLf
    Response.Write "    </tr>" & vbCrLf
    Response.Write "    <tr class='tdbg'>" & vbCrLf
    Response.Write "      <td class='tdbg5' align='center'>摘要信息：</td>" & vbCrLf
    Response.Write "      <td><textarea name='Meta_Description' cols='60' rows='2' id='Meta_Description'>"& Rs("Meta_Description") &"</textarea>针对搜索引擎设置的网页描述</td>" & vbCrLf
    Response.Write "    </tr>" & vbCrLf
    Response.Write "    <tr class='tdbg'>"& vbCrLf
    Response.Write "      <td class='tdbg5' align='center'>排序号：</td>"& vbCrLf
    Response.Write "      <td><input name='OrderID' type='text' size='5' value='"& Rs("OrderID") &"'> (只能是数字)<font color=red>*</font></td>"& vbCrLf
    Response.Write "    </tr>" & vbCrLf
    Response.Write "    <tr class='tdbg'>"& vbCrLf
    Response.Write "      <td class='tdbg5' align='center'>关联标识：</td>"& vbCrLf
    Response.Write "      <td><input name='Pagelike' id='Pagelike' type='text' size='10' value='"& Rs("Pagelike") &"'> "& Pagelike_List(Rs("Pagelike")) &"<font color=red>*</font>通过这个标识来识别类同页面,标识名称为前台栏目名称</td>"& vbCrLf
    Response.Write "    </tr>" & vbCrLf 
	Response.Write "    <tr class='tdbg'>"
	Response.Write "      <td class='tdbg5' align='center'>文件名：</td>"
	Response.Write "      <td><input name='filename' value='"& Rs("filename") &"' type='text' size='30'> <font color='#FF0000'>*</font> 相对于CMS安装目录</td>"
	Response.Write "    </tr>" & vbCrLf 
	if AdminPurview=1 then
		Response.Write "    <tr class='tdbg'>"
		Response.Write "      <td class='tdbg5' align='center'>模板选择：</td>"
		Response.Write "      <td><input name='templateInfo' id='templateInfo' type='text' value='"& Rs("templateInfo") &"' size='30'>  <input type='button' name='Button2' class='Button' value='选择模板... ' onclick=""SelectTemplate('templateInfo')""> <font color='#FF0000'>*</font></td>"
		Response.Write "    </tr>" & vbCrLf 
	end if
    Response.Write "    <tr class='tdbg'>"& vbCrLf
    Response.Write "      <td class='tdbg5' align='center'>文档内容：</td>"& vbCrLf
    Response.Write "      <td>"& vbCrLf
	Call ShowFckEditor("Default",GetPhotoUrl(Rs("Content")),"Content","100%","500")
	Response.write "      </td>"& vbCrLf
    Response.Write "    </tr>" & vbCrLf
    Response.Write "  </table>"& vbCrLf
	
    Response.Write "  <p align='center'>"& vbCrLf
    Response.Write "   <input name='Action' type='hidden' id='Action' value='SaveModify'>"
    Response.Write "   <input name='ID' type='hidden' id='ID' value='" & ID & "'>"
    Response.Write "   <input name='Modify' type='submit'  id='Add' value=' 修 改 ' onClick=""document.myform.target='_self';"" style='cursor:hand;' class='Submit'>&nbsp; "
    Response.Write "   <input name='Cancel' type='button' id='Cancel' value=' 取 消 ' onClick=""window.location.href='Admin_Info.asp';"" style='cursor:hand;' class='Submit'>"
    Response.Write "  </p>"
    Response.Write "</form>"& vbCrLf
	Rs.close
	set Rs=nothing
End Sub



Sub SaveInfo()
	Dim Rs,Sql,i,fid
	Dim Title,Meta_Keywords,Meta_Description,OrderID,Pagelike,Pagelike_List,Content,templateInfo,filename
	Title = GetForm("Title")
	Meta_Keywords = GetForm("Meta_Keywords")
	Meta_Description = GetForm("Meta_Description")
	OrderID = PE_CLng(GetForm("OrderID"))
	Pagelike = GetForm("Pagelike")
	Pagelike_List = GetForm("Pagelike_List")
	Content = GetForm("Content")
	filename = GetForm("filename")
	templateInfo = GetForm("templateInfo")
    If Title = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>文档标题不能为空</li>"
	End if
	if OrderID < 0 then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>排序号必须为数字</li>"
	End if
    If Pagelike = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请选择关联标识</li>"
	End if
    If Content = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>文档内容不能为空</li>"
	End if
	if filename="" then
		FoundErr = True
		ErrMsg = ErrMsg & "<li>文件名不能为空！</li>"
	end if
	
    If FoundErr = True Then
        Exit Sub
    End If
	
	
	if templateInfo="" then
		set Rs=conn.execute("Select templateInfo from PW_Info where Pagelike='"& Pagelike &"' order by OrderID asc")
		if Rs.eof then
			FoundErr = True
			ErrMsg = ErrMsg & "<li>找不到对应的模版,请在已有的关联标识中选择！</li>"
		Else
			templateInfo = Rs("templateInfo")
		end if 
		Rs.close
		Set Rs=Nothing
	End if
	if Action = "SaveAdd" Then
		set Rs=conn.execute("Select filename from PW_Info where filename='"& filename &"'")
		if not Rs.eof then
			FoundErr = True
			ErrMsg = ErrMsg & "<li>已经存在相同的文件名，请更改为其它文件名！</li>"
		end if 
		Rs.close
		Set Rs=Nothing
	end if
    If FoundErr = True Then
        Exit Sub
    End If
	
    Set Rs = Server.CreateObject("adodb.recordset")
	if Action = "SaveAdd" Then
            Sql = "select top 1 * from PW_Info"
            Rs.Open sql, Conn, 1, 3
            Rs.addnew
			Rs("ID") = GetNewID("PW_Info", "ID")
    ElseIf Action = "SaveModify" Then
        If ID = "" Then
            FoundErr = True
            ErrMsg = ErrMsg & "<li>不能确定文档ID的值</li>"
        Else
            ID = PE_CLng(ID)
            Sql = "select * from PW_Info where ID=" & ID
            Rs.Open sql, Conn, 1, 3
            If Rs.BOF And Rs.EOF Then
                FoundErr = True
                ErrMsg = ErrMsg & "<li>找不到此文档，可能已经被其他人删除。</li>"
			end if
		end if
	end if
    If FoundErr = True Then
        Exit Sub
    End If
	
	Rs("Title") = Title
	Rs("Meta_Keywords") = Meta_Keywords
	Rs("Meta_Description") = Meta_Description
	Rs("OrderID") = OrderID
	Rs("Pagelike") = Pagelike
	Rs("Content") = SetPhotoUrl(Content)
	Rs("filename") = filename
	Rs("templateInfo") = templateInfo
    Rs.Update
	fid = Rs("ID")
    Rs.Close
    Set Rs = Nothing
    Response.Write "<br><table class='border' align='center' border='0' cellpadding='2' cellspacing='1' width='100%'>"& vbCrLf
	Response.write "<Tr class='title' height='22' align='center'><td><strong>提示信息</strong></td></tr>"
    Response.Write "  <tr'> "& vbCrLf
    Response.Write "    <td align='center'> "& vbCrLf
    If Action = "SaveAdd"  Then
        Response.Write "添加文档成功"
    Else
        Response.Write "修改文档成功"
    End If
	Response.write "&nbsp;&nbsp;"
	Call CreateInfohtml(fid)
	Response.write "<br /><a href='Admin_Info.asp'>如果你的浏览器没有跳转，请点这里</a></td></tr>"
    Response.Write "</table>" & vbCrLf
	
	Call Refresh("Admin_Info.asp",2)
	
End Sub



Sub DeleteInfo()
	Dim sqlDel,rsDel
	Dim furl
	if ID = "" then
		Response.Redirect "Admin_Info.asp"
		exit sub
	End if
    sqlDel = "select * from PW_Info where ID in("&ID&")"
    Set rsDel = Server.CreateObject("ADODB.Recordset")
    rsDel.Open sqlDel, Conn, 1, 3
	if not rsDel.eof then
		do while not rsDel.eof
			furl = InStallDir&rsDel("filename")
			furl = Replace(furl,"//","/")
			DelSerialFiles Server.MapPath(furl)
			rsDel.delete
			rsDel.update
			rsDel.movenext
			loop
	End if
	rsDel.close
	set rsDel = Nothing
    Response.Write "<table class='border' align='center' border='0' cellpadding='2' cellspacing='1' width='100%'>"& vbCrLf
	Response.write "<Tr class='title' height='22'><td>提示信息</td></tr>"
    Response.Write "  <tr'> "& vbCrLf
    Response.Write "    <td align='center'> "& vbCrLf
    Response.Write "<b>删除文档成功</b>"
	Response.write "<br /><a href='Admin_Info.asp'>如果你的浏览器没有跳转，请点这里</a></td></tr>"
    Response.Write "</table>" & vbCrLf
	Call Refresh("Admin_Info.asp",2)
end Sub

Sub CreateHtml
	Dim sql, oRs
	if Action = "CreateAll" then
		sql = "select * from PW_Info"
	Else
		If ID = "" Then
			FoundErr = True
			ErrMsg = ErrMsg & "<li>请先选定你要更新的文档！</li>"
			Exit Sub
		End If
		sql = "select * from PW_Info where ID in (" & ID & ")"
	End if
	Set oRs = Server.CreateObject("ADODB.Recordset")
	oRs.open sql,conn,1,3
    Set oRs = Conn.Execute(sql)
    Do While Not oRs.EOF
        CreateInfohtml(oRs("ID"))
        oRs.MoveNext
    Loop
    oRs.Close
    Set oRs = Nothing
    Response.Write "<table class='border' align='center' border='0' cellpadding='2' cellspacing='1' width='100%'>"& vbCrLf
	Response.write "<Tr class='title' height='22'><td>提示信息</td></tr>"
    Response.Write "  <tr'> "& vbCrLf
    Response.Write "    <td align='center'> "& vbCrLf
    Response.Write "<b>成功更新文档！</b>"
	Response.write "<br /><a href='Admin_Info.asp'>如果你的浏览器没有跳转，请点这里</a></td></tr>"
    Response.Write "</table>" & vbCrLf
	Call Refresh("Admin_Info.asp",2)
End Sub

function GetRootPagelike()
	dim rs,sql,temstr
	temstr="&nbsp;"
    sql = "select distinct Pagelike from PW_Info"
    Set rs = Conn.Execute(sql)
    If rs.BOF And rs.EOF Then
		temstr = temstr& "暂无标识！"
	else
		do while not rs.eof
			temstr = temstr &"<a href='Admin_Info.asp?pagelike="& rs(0) &"'>"& rs(0) &"</a>&nbsp;&nbsp;"&vbcrlf
		rs.movenext
		loop
	end if
	rs.close
	set rs=nothing	
	GetRootPagelike = temstr
end function

function Pagelike_List(clike)
	dim rs,sql,temstr
	temstr = ""&vbcrlf
    sql = "select distinct Pagelike from PW_Info"
    Set rs = Conn.Execute(sql)
    If Not( rs.BOF And rs.EOF) Then
		temstr = temstr & "<select name='Pagelike_List' id='Pagelike_List' onchange=""document.form1.Pagelike.value=this.value;"">"
		temstr = temstr & "<option value=''>请选择</option>"
		do while not rs.eof
			temstr = temstr & "<option value='"& rs(0) &"'"
			if clike=rs(0) then temstr = temstr & " selected "
			temstr = temstr & ">"& rs(0) &"</option>"&vbcrlf
		rs.movenext
		loop
		temstr = temstr & "</select>"
	end if
	rs.close
	set rs=nothing	
	Pagelike_List = temstr
end Function

Sub CreateInfohtml(fid)
	Dim rsInfo,Sql
	Dim tempPath,infoPath,infoFilename
	Dim strTemp,arrTemp,LoopTemp
	fid = PE_Clng(fid)
	Sql = "Select * from PW_Info where ID="&fid
	Set rsInfo = Conn.execute(Sql)
	If not rsInfo.eof then
		strHtml = GetTemplate(rsInfo("templateInfo"))
		infoPath = InStallDir&rsInfo("filename")
		infoPath = Replace(infoPath,"//","/")
		tempPath = left(infoPath,InstrRev(infoPath,"/"))
		If CreateMultiFolder(tempPath) = False Then
			Response.Write "<font color='red'>更新页面失败，请检查服务器。系统不能创建生成文件所需要的文件夹。</font>"
		End If
		''替换内容
		strHtml = PE_Replace(strHtml,"{$pagetitle}",rsInfo("Title")&"_"& SiteName)
		strHtml = PE_Replace(strHtml,"{$location}",strNavPath & "&nbsp;" & strNavLink & "&nbsp;" & rsInfo("Title"))
		strHtml = PE_Replace(strHtml,"{$meta_keywords}",rsInfo("Meta_Keywords"))
		strHtml = PE_Replace(strHtml,"{$meta_description}",rsInfo("Meta_Description"))
		strHtml = PE_Replace(strHtml,"{$field(content)}",GetContent(rsInfo("Content")))
		strHtml = PE_Replace(strHtml,"{$field(title)}",rsInfo("title"))
		strHtml = PE_Replace(strHtml,"{$field(location)}",infoPath)
		regEx.Pattern = "\{\$field\((.*?)\)\}"
		Set Matches = regEx.Execute(strHtml)
		For Each Match In Matches
			strHtml = Replace(strHtml,Match.value,rsInfo(Match.SubMatches(0)))
		next
		'替换相关文档
		regEx.Pattern = "\【likepage\((.*?)\)\】([\s\S]*?)\【\/likepage\】"
		Set Matches = regEx.Execute(strHtml)
		For Each Match In Matches
			arrTemp = Match.SubMatches(0)
			strHtml = PE_Replace(strHtml, Match.Value, Showlikepage(arrTemp, Match.SubMatches(1)))
		Next
		Call ReplaceCommonLabel
		Call GetModelFront_Html
		Call WriteToFile(infoPath, strHtml)
		Response.Write "<font color='blue'>文档更新成功！</font>"
	End if
	rsInfo.close
	set rsInfo = Nothing
End Sub

Function Showlikepage(likestr,loopstr)
	Dim oRs,Sql,tmpStr,furl
	if likestr = "" then
		Showlikepage = ""
		exit function
	end if
	Sql="Select ID,Title,filename from PW_Info where Pagelike='"& likestr &"' order by OrderID asc"
	Set oRs=conn.execute(Sql)
	if not oRs.eof then
		do while not oRs.eof
			tmpStr = tmpStr & loopstr
			furl = InStallDir&oRs("filename")
			furl = Replace(furl,"//","/")
			tmpStr = PE_Replace(tmpStr, "{$infourl}", furl)
			tmpStr = PE_Replace(tmpStr, "{$infotitle}", oRs("Title"))
			oRs.movenext
		loop
	else
		Showlikepage =	""
	end if
	oRs.close
	set oRs=Nothing
	Showlikepage = tmpStr
End Function
%>