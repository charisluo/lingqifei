<!--#include file="Admin_Common.asp"-->

<%
Const NeedCheckComeUrl = True   '是否需要检查外部访问
Const PurviewLevel_Others = ""   '其他权限
Const PurviewLevel_Channel = 0   '0--不检查

%>
<html>
<head>
<title><%=SiteName & "--后台管理首页"%></title>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
<link rel="stylesheet" href="admin_Style.css">
</head>
<body topmargin="0" marginheight="0">
<table width="100%" height="101"  border="0" align="center" cellpadding="0" cellspacing="0" background="images/banner_bg.gif">
  <tr>
    <td width="50%" valign="top"><img src="images/banner.jpg" width="480" height="100"></td>
    <td width="50%"><div align="right"><img src="images/banner_right.gif" width="62" height="101"></div></td>
  </tr>
</table>
<br />
<br />

<table cellpadding="2" cellspacing="1" border="0" width="100%" class="border" align=center>
  <tr align="center">
    <td height=25 colspan=2 class="topbg"><span class="Glow">服 务 器 信 息</span>
  </tr>
  <tr class="tdbg" height=23>
    <td width="50%">服务器类型：      <%=Request.ServerVariables("OS")%>(IP:<%=Request.ServerVariables("LOCAL_ADDR")%>)</td>
    <td width="50%">脚本解释引擎：
    <%
    response.write ScriptEngine & "/"& ScriptEngineMajorVersion &"."&ScriptEngineMinorVersion&"."& ScriptEngineBuildVersion
    If CSng(ScriptEngineMajorVersion & "." & ScriptEngineMinorVersion) < 5.6 Then
        response.write "&nbsp;&nbsp;<a href='http://www.microsoft.com/downloads/release.asp?ReleaseID=33136' target='_blank'><font color='green'>版本过低，请点此更新</font></a>"
    End If
    %>
    </td>
  </tr>
  <tr class="tdbg" height=23>
    <td width="50%">站点物理路径：      <%=request.ServerVariables("APPL_PHYSICAL_PATH")%></td>
    <td width="50%">数据库使用：<%ShowObjectInstalled("adodb.connection")%></td>
  </tr>
  <tr class="tdbg" height=23>
    <td width="50%">FSO文本读写：<%ShowObjectInstalled(objName_FSO)%></td>
    <td width="50%">数据流读写：<%ShowObjectInstalled("Adodb.Stream")%></td>
  </tr>
  <tr class="tdbg" height=23>
    <td width="50%">XMLHTTP组件支持：<%ShowObjectInstalled("Microsoft.XMLHTTP")%></td>
    <td width="50%">XMLDOM组件支持：<%ShowObjectInstalled("Microsoft.XMLDOM")%></td>
  </tr>
  <tr class="tdbg" height=23>
    <td width="50%">XML组件支持：<%ShowObjectInstalled("MSXML2.XMLHTTP")%></td>
    <td width="50%">AspJpeg组件支持：<%ShowObjectInstalled("Persits.Jpeg")%></td>
  </tr>
  
  <tr class="tdbg" height=23>
    <td width="50%">Jmail组件支持：<%ShowObjectInstalled("JMail.SMTPMail")%></td>
    <td width="50%">CDONTS组件支持：<%ShowObjectInstalled("CDONTS.NewMail")%></td>
  </tr>
  <tr class="tdbg" height=23>
    <td width="50%">ASPEMAIL组件支持：<%ShowObjectInstalled("Persits.MailSender")%></td>
    <td width="50%">WebEasyMail组件支持：<%ShowObjectInstalled("easymail.MailSend")%></td>
  </tr>
  <tr class="tdbg" height=23>
    <td width="50%"> </td>
    <td width="50%" align="right"><a href="Admin_ServerInfo.asp">点此查看更详细的服务器信息&gt;&gt;&gt;</a></td>
  </tr>
</table>
<br>
<table cellpadding="2" cellspacing="1" border="0" width="100%" class="border" align=center>
  <tr align="center">
    <td height=25 class="topbg"><span class="Glow">Copyright 2008 &copy; <%=SiteName%> All Rights Reserved.</span>
  </tr>
</table>
<%
Sub ShowObjectInstalled(strObjName)
    If IsObjInstalled(strObjName) Then
        response.write "<b>√</b>"
    Else
        response.write "<font color='red'><b>×</b></font>"
    End If
End Sub
%>