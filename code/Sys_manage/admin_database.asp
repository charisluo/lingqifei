<!--#include file="Admin_Common.asp"-->
<!--#include file="../Include/PW_FSO.asp"-->
<%
Const NeedCheckComeUrl = True   '是否需要检查外部访问
Const PurviewLevel_Channel = 0   '0--不检查，1--频道管理员，2--栏目总编，3--栏目管理员
Const PurviewLevel_Others = "DataBase"   '其他权限


Dim dbpath, Barwidth

dbpath = Server.MapPath(InStallDir&DBFileName)
Barwidth = 500
Response.Write "<html><head><title>数据库管理</title>" & vbCrLf
Response.Write "<meta http-equiv='Content-Type' content='text/html; charset=gb2312'>" & vbCrLf
Response.Write "<link href='Admin_Style.css' rel='stylesheet' type='text/css'>" & vbCrLf
Response.Write "</head>" & vbCrLf
Response.Write "<body leftmargin='2' topmargin='0' marginwidth='0' marginheight='0'>" & vbCrLf
Response.Write "<table width='100%' border='0' align='center' cellpadding='2' cellspacing='1' class='border'>" & vbCrLf
Call ShowPageTitle("数 据 库 管 理")
Response.Write "  <tr class='tdbg'>" & vbCrLf
Response.Write "    <td width='70' height='30' ><strong>管理导航：</strong></td><td>"
Response.Write "<a href='Admin_Database.asp?Action=Backup'>备份数据库</a>&nbsp;|&nbsp;"
Response.Write "<a href='Admin_Database.asp?Action=Restore'>恢复数据库</a>&nbsp;|&nbsp;"
Response.Write "<a href='Admin_Database.asp?Action=Compact'>压缩数据库</a>&nbsp;|&nbsp;"
Response.Write "<a href='Admin_Database.asp?Action=Init'>系统初始化</a>&nbsp;|&nbsp;"
Response.Write "<a href='Admin_Database.asp?Action=SpaceSize'>系统空间占用情况</a>"
Response.Write "    </td>" & vbCrLf
Response.Write "  </tr>" & vbCrLf
Response.Write "</table>" & vbCrLf



Select Case Action
Case "Backup"
    Call ShowBackup
Case "BackupData"
    Call BackupData
Case "Compact"
    Call ShowCompact
Case "CompactData"
    Call CompactData
Case "Restore"
    Call ShowRestore
Case "RestoreData"
    Call RestoreData
Case "SpaceSize"
    Call SpaceSize
Case Else
    FoundErr = True
    ErrMsg = ErrMsg & "<li>错误参数！</li>"
End Select
If FoundErr = True Then
    Call WriteErrMsg(ErrMsg, ComeUrl)
End If
Response.Write "</body></html>"
Call CloseConn

Sub ShowBackup()
    Response.Write "<form method='post' action='Admin_Database.asp?action=BackupData'>"
    Response.Write "<table width='100%' border='0' align='center' cellpadding='0' cellspacing='0' class='border'>"
    Response.Write "  <tr class='title'>"
    Response.Write "      <td align='center' height='22' valign='middle'><b>备 份 数 据 库</b></td>"
    Response.Write "  </tr>"
    Response.Write "  <tr class='tdbg'>"
    Response.Write "    <td height='150' align='center' valign='middle'>"
    Response.Write "<table cellpadding='3' cellspacing='1' border='0' width='100%'>"
    Response.Write "  <tr>"
    Response.Write " <td width='200' height='33' align='right'>备份目录：</td>"
    Response.Write " <td><input type=text size=20 name=bkfolder value=Databackup></td>"
    Response.Write " <td>相对路径目录，如目录不存在，将自动创建</td>"
    Response.Write "  </tr>"
    Response.Write "  <tr>"
    Response.Write " <td width='200' height='34' align='right'>备份名称：</td>"
    Response.Write " <td height='34'><input type=text size=20 name=bkDBname value='backup'></td>"
    Response.Write " <td height='34'>不用输入文件名后缀（默认为“.asa”）。如有同名文件，将覆盖</td>"
    Response.Write "  </tr>"
    Response.Write "  <tr align='center'>"
    Response.Write " <td height='40' colspan='3'><input name='submit' type=submit value=' 开始备份 '"
    If SystemDatabaseType = "SQL" Or ObjInstalled_FSO = False Then
        Response.Write " disabled"
    End If
    Response.Write "></td>"
    Response.Write "  </tr>"
    Response.Write "</table>"
    If ObjInstalled_FSO = False Then
        Response.Write "<b><font color=red>你的服务器不支持 FSO(Scripting.FileSystemObject)! 不能使用本功能</font></b>"
    End If
    Response.Write "    </td>"
    Response.Write "  </tr>"
    Response.Write "</table>"
    Response.Write "</form>"
    If SystemDatabaseType = "SQL" Then
        Response.Write "<br><b>说明：</b><br>&nbsp;&nbsp;&nbsp;&nbsp;您使用的是SQL版，请直接使用SQL2000提供的数据库备份功能进行备份！<br><br>"
    End If
End Sub

Sub ShowCompact()
    Response.Write "<form method='post' action='Admin_Database.asp?action=CompactData'>"
    Response.Write "<table class='border' width='100%' border='0' align='center' cellpadding='0' cellspacing='0'>"
    Response.Write " <tr class='title'>"
    Response.Write "     <td align='center' height='22' valign='middle'><b>数据库在线压缩</b></td>"
    Response.Write " </tr>"
    Response.Write " <tr class='tdbg'>"
    Response.Write "     <td align='center' height='150' valign='middle'>"
    Response.Write "      <br>"
    Response.Write "      <br>"
    Response.Write "      压缩前，建议先备份数据库，以免发生意外错误。 <br>"
    Response.Write "      <br>"
    Response.Write "      <br>"
    Response.Write " <input name='submit' type=submit value=' 压缩数据库 '"
    If SystemDatabaseType = "SQL" Then
        Response.Write " disabled"
    End If
    Response.Write "><br><br>"
    If ObjInstalled_FSO = False Or ObjInstalled_FSO = False Then
        Response.Write "<b><font color=red>你的服务器不支持 FSO(Scripting.FileSystemObject)! 不能使用本功能</font></b>"
    End If
    Response.Write "    </td>"
    Response.Write "  </tr>"
    Response.Write "</table>"
    Response.Write "</form>"
    If SystemDatabaseType = "SQL" Then
        Response.Write "<br><b>说明：</b><br>&nbsp;&nbsp;&nbsp;&nbsp;您使用的是SQL版，无需进行压缩操作！<br><br>"
    End If
End Sub


Sub ShowRestore()
    Response.Write "<form method='post' action='Admin_Database.asp?action=RestoreData'>"
    Response.Write "<table width='100%' class='border' border='0' align='center' cellpadding='0' cellspacing='0'>"
    Response.Write "  <tr class='title'>"
    Response.Write "    <td align='center' height='22' valign='middle'><b>数据库恢复</b></td>"
    Response.Write "  </tr>"
    Response.Write "  <tr class='tdbg'>"
    Response.Write "    <td align='center' height='150' valign='middle'>"
    Response.Write "      <table width='100%' border='0' cellspacing='0' cellpadding='0'>"
    Response.Write "        <tr>"
    Response.Write "          <td width='200' height='30' align='right'>原备份数据库路径（相对）：</td>"
    Response.Write "          <td height='30'><input name=backpath type=text id='backpath' value='Databackup\sitedata.asp' size=50 maxlength='200'></td>"
    Response.Write "        </tr>"
    Response.Write "        <tr align='center'>"
    Response.Write "          <td height='40' colspan='2'><input name='submit' type=submit value=' 恢复数据 '"
    If SystemDatabaseType = "SQL" Or ObjInstalled_FSO = False Then
        Response.Write " disabled"
    End If
    Response.Write ">"
    Response.Write "          </td>"
    Response.Write "        </tr>"
    Response.Write "      </table>"
    If ObjInstalled_FSO = False Then
        Response.Write "<b><font color=red>你的服务器不支持 FSO(Scripting.FileSystemObject)! 不能使用本功能</font></b>"
    End If
    Response.Write "    </td>"
    Response.Write "  </tr>"
    Response.Write "</table>"
    Response.Write "</form>"
    If SystemDatabaseType = "SQL" Then
        Response.Write "<br><b>说明：</b><br>&nbsp;&nbsp;&nbsp;&nbsp;您使用的是SQL版，请直接使用SQL2000提供的数据库恢复功能进行恢复！<br><br>"
    Else
        Response.Write "<br><b>说明：</b><br>&nbsp;&nbsp;&nbsp;&nbsp;原备份数据库的扩展名必须为：asa或者asp<br><br>"
    End If
End Sub


Sub SpaceSize()
    'On Error Resume Next
    Response.Write "<br><table class='border' width='100%' border='0' align='center' cellpadding='0' cellspacing='0'>"
    Response.Write "  <tr class='title'>"
    Response.Write "    <td align='center' height='22' valign='middle'><b>系统空间占用情况</b></td>"
    Response.Write "  </tr>"
    Response.Write "  <tr class='tdbg'>"
    Response.Write "    <td width='100%' height='150' valign='middle'>"
    Response.Write "    <blockquote>"
    Response.Write "      <br><b>系统文件占用空间情况：</b><br>"
    Response.Write "      基本系统占用空间：" & ShowSpace("Inc|Include|JS|member|PW_Editor|Sys_manage")
    Response.Write "      <br>"
    Response.Write "      系统图片占用空间：" & ShowSpace("Images|Flash")
    Response.Write "      <br>"
    Response.Write "      模板占用空间：" & ShowSpace("template")
    Response.Write "      <br>"
    Response.Write "      数 据 库占用空间：" & ShowSpace("Data")
    Response.Write "      <br>"
    Response.Write "      上传图片占用空间：" & ShowSpace("uploadfiles")
    Response.Write "      <br><br>"
    Response.Write "      <b>频道文件占用空间情况：</b><br>"
    Dim rsChannel, sqlChannel
    sqlChannel = "select * from PW_Channel where Disabled=false order by OrderID"
    Set rsChannel = Conn.Execute(sqlChannel)
    Do While Not rsChannel.EOF
        Response.Write "      <font color='#0000ff'>" & rsChannel("ChannelName") & "</font>占用空间：" & ShowSpace(rsChannel("ChannelDir"))
        Response.Write "      <br>"
        rsChannel.MoveNext
    Loop
    rsChannel.Close
    Set rsChannel = Nothing
    Response.Write "      <br>未知目录占用空间：" & ShowSpace(GetOtherFolder())
    Response.Write "      <br>网站占用空间总计：" & ShowSpace(" ")
    Response.Write "    </blockquote>"
    Response.Write "    </td>"
    Response.Write "  </tr>"
    Response.Write "</table>"
End Sub


Sub BackupData()
    Dim bkfolder, bkdbname
    bkfolder = GetForm("bkfolder")
    bkdbname = GetForm("bkdbname")
    If bkfolder = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请指定备份目录！</li>"
    End If
    If bkdbname = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请指定备份文件名</li>"
    End If
    If FoundErr = True Then Exit Sub
    bkfolder = Server.MapPath(bkfolder)
    If fso.FileExists(dbpath) Then
        If fso.FolderExists(bkfolder) = False Then
            fso.CreateFolder (bkfolder)
        End If
        fso.copyfile dbpath, bkfolder & "\" & bkdbname & ".asa"
        Call WriteSuccessMsg("备份数据库成功，备份的数据库为：<br>" & bkfolder & "\" & bkdbname & ".asa", ComeUrl)
    Else
        FoundErr = True
        ErrMsg = ErrMsg & "<li>找不到源数据库文件，请检查Conn.asp中的配置。</li>"
    End If
End Sub


Sub CompactData()
    On Error Resume Next

    Dim Engine, strDBPath
    Call CloseConn

    strDBPath = Left(dbpath, InStrRev(dbpath, "\"))
    If fso.FileExists(dbpath) Then
        Set Engine = Server.CreateObject("JRO.JetEngine")
        Engine.CompactDatabase "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & dbpath, " Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & strDBPath & "temp.mdb"
        fso.copyfile strDBPath & "temp.mdb", dbpath
        fso.DeleteFile (strDBPath & "temp.mdb")
        Set Engine = Nothing
        Call WriteSuccessMsg("数据库压缩成功!", ComeUrl)
    Else
        FoundErr = True
        ErrMsg = ErrMsg & "<li>数据库没有找到!</li>"
    End If
    If Err.Number <> 0 Then
        FoundErr = True
        ErrMsg = ErrMsg & Err.Description
        Err.Clear
        Exit Sub
    End If
End Sub


Sub RestoreData()
    Dim backpath
    backpath = GetForm("backpath")
    If backpath = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请指定原备份的数据库文件名！</li>"
        Exit Sub
    End If
    If GetFileExt(backpath) <> "asa" And GetFileExt(backpath) <> "asp" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>原备份数据库文件的扩展名必须为asa或asp！</li>"
        Exit Sub
    End If
    backpath = Server.MapPath(backpath)
    If fso.FileExists(backpath) Then
        fso.copyfile backpath, dbpath
        Call WriteSuccessMsg("成功恢复数据！", ComeUrl)
    Else
        FoundErr = True
        ErrMsg = ErrMsg & "<li>找不到指定的备份文件！</li>"
    End If
End Sub

Function ShowSpace(FolderPath)
    Dim ft, fd, fs, TotalSize, SpaceSize, FolderBarWidth, arrPath, strSize, i
    Set ft = fso.GetFolder(Server.MapPath(InstallDir))
    TotalSize = ft.size
    If TotalSize = 0 Then TotalSize = 1

    SpaceSize = 0
    arrPath = Split(FolderPath, "|")
    For i = 0 To UBound(arrPath)
		  If fso.FolderExists(Server.MapPath(InstallDir & arrPath(i))) Then
			  Set fd = fso.GetFolder(Server.MapPath(InstallDir & arrPath(i)))
			  SpaceSize = SpaceSize + fd.size
		  End If
    Next
    FolderBarWidth = PE_CLng((SpaceSize / TotalSize) * Barwidth)

    strSize = SpaceSize & "&nbsp;Byte"
    If SpaceSize > 1024 Then
       SpaceSize = (SpaceSize / 1024)
       strSize = FormatNumber(SpaceSize, 2, vbTrue, vbFalse, vbTrue) & "&nbsp;KB"
    End If
    If SpaceSize > 1024 Then
       SpaceSize = (SpaceSize / 1024)
       strSize = FormatNumber(SpaceSize, 2, vbTrue, vbFalse, vbTrue) & "&nbsp;MB"
    End If
    If SpaceSize > 1024 Then
       SpaceSize = (SpaceSize / 1024)
       strSize = FormatNumber(SpaceSize, 2, vbTrue, vbFalse, vbTrue) & "&nbsp;GB"
    End If
    strSize = "<font face=verdana>" & strSize & "</font>"
    ShowSpace = "&nbsp;<img src='../images/bar.gif' width='" & FolderBarWidth & "' height='10' title='" & FolderPath & "'>&nbsp;" & strSize
End Function

Function GetOtherFolder()
    Dim ft, fd, strOther, strSystem, arrPath
    strSystem = "Data|member|guestbook|Flash|Images|Inc|Include|JS|PW_Editor|Sys_manage|template|uploadfiles"
    Dim rsChannel, sqlChannel
    sqlChannel = "select * from PW_Channel order by OrderID"
    Set rsChannel = Conn.Execute(sqlChannel)
    Do While Not rsChannel.EOF
        strSystem = strSystem & "|" & rsChannel("ChannelDir")
        rsChannel.MoveNext
    Loop
    rsChannel.Close
    Set rsChannel = Nothing

    Set ft = fso.GetFolder(Server.MapPath(InstallDir))
    For Each fd In ft.SubFolders
        If InStr("|" & strSystem & "|", "|" & fd.name & "|") = 0 Then
            If strOther = "" Then
                strOther = fd.name
            Else
                strOther = strOther & "|" & fd.name
            End If
        End If
    Next
	
    GetOtherFolder = strOther
End Function


%>