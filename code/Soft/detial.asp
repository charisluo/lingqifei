<!--#include file="CommonCode.asp"-->
<%
SoftID = PE_CLng(GetUrl("SoftID"))
If SoftID = 0 Then
    FoundErr = True
    ErrMsg = ErrMsg & "<li>请指定SoftID！</li>"
End If
If FoundErr = True Then
    Call WriteErrMsg(ErrMsg, ComeUrl)
    Response.End
End If

Dim sql
sql = "select * from PW_Soft where ChannelID=" & ChannelID & " and Deleted=" & PE_False & " and SoftID=" & SoftID & ""
Set rsSoft = Conn.Execute(sql)
If rsSoft.BOF And rsSoft.EOF Then
    FoundErr = True
    ErrMsg = ErrMsg & "<li>你要找的" & ChannelShortName & "不存在，或者已经被管理员删除！</li>"
Else
    ClassID = rsSoft("ClassID")
    If ClassID > 0 Then
        Call GetClass
	else
		article_template = temparticle	
    End If
End If

If FoundErr = True Then
    Call WriteErrMsg(ErrMsg, ComeUrl)
    rsSoft.Close
    Set rsSoft = Nothing
    Response.End
End If
SoftName = Replace(Replace(Replace(Replace(rsSoft("SoftName") & "", "&nbsp;", " "), "&quot;", Chr(34)), "&gt;", ">"), "&lt;", "<")

strHtml = GetTemplate(article_template)
Call PW_Content.GetHtml_Soft
Call GetModelFront_Html()
Response.Write strHtml
rsSoft.Close
Set rsSoft = Nothing
Set PW_Content = Nothing
Call CloseConn
%>
