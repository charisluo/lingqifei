<!--#include file="Admin_Common.asp"-->
<%
Const NeedCheckComeUrl = True   '是否需要检查外部访问
Const PurviewLevel_Channel = 0   '频道检查
Const PurviewLevel_Others = "FriendSite"   '其他权限


Dim KindID, KindName
KindID = GetValue("KindID")

If IsValidID(KindID) = False Then
    KindID = ""
End If

FileName = "Admin_FriendSite.asp?Action=" & Action
strFileName = FileName & "&KindID=" & KindID & ""

'页面头部HTML代码
Response.Write "<html><head><title>友情链接管理</title>" & vbCrLf
Response.Write "<meta http-equiv='Content-Type' content='text/html; charset=gb2312'>" & vbCrLf
Response.Write "<link href='Admin_Style.css' rel='stylesheet' type='text/css'>" & vbCrLf
Response.Write "</head>" & vbCrLf
Response.Write "<body leftmargin='2' topmargin='0' marginwidth='0' marginheight='0'>" & vbCrLf
Response.Write "<table width='100%' border='0' align='center' cellpadding='2' cellspacing='1' class='border'>" & vbCrLf
Call ShowPageTitle("友 情 链 接 管 理")
Response.Write "  <tr class='tdbg'>" & vbCrLf
Response.Write "    <td width='70' height='30'><strong>管理导航：</strong></td>" & vbCrLf
Response.Write "    <td>"
Response.Write "    <a href='Admin_FriendSite.asp'>友情链接管理首页</a>&nbsp;|&nbsp;"
Response.Write "    <a href='Admin_FriendSite.asp?Action=Add'>添加友情链接</a>&nbsp;|&nbsp;"
Response.Write "    <a href='Admin_FriendSite.asp?Action=FsKind'>链接类别管理</a>&nbsp;|&nbsp;"
Response.Write "    <a href='Admin_FriendSite.asp?Action=AddFsKind'>添加链接类别</a>&nbsp;|&nbsp;"
Response.Write "    <a href='Admin_FriendSite.asp?Action=Order'>友情链接排序</a>"
Response.Write "    </td>" & vbCrLf
Response.Write "  </tr>" & vbCrLf
Response.Write "</table>" & vbCrLf



'执行的操作
Select Case Action
Case "Add"
    Call Add
Case "Modify"
    Call Modify
Case "SaveAdd", "SaveModify"
    Call SaveFriendSite
Case "SetElite", "CancelElite", "Del"
    Call SetProperty
Case "FsKind"
    Call FsKind
Case "AddFsKind"
    Call AddFsKind
Case "ModifyFsKind"
    Call ModifyFsKind
Case "DelFsKind"
    Call DelFsKind
Case "ClearFsKind"
    Call ClearFsKind
Case "SaveAddFsKind", "SaveModifyFsKind"
    Call SaveFsKind
Case "Order"
    Call Order
Case "UpOrder"
    Call UpOrder
Case "DownOrder"
    Call DownOrder
Case Else
    Call main
End Select
If FoundErr = True Then
    Call WriteErrMsg(ErrMsg, ComeUrl)
End If
Response.Write "</body></html>"
Call CloseConn



Sub main()
    Dim rsFriendSite, sqlFriendSite
    If KindID <> "" Then
        Dim tKind
        Set tKind = Conn.Execute("select * from PW_FsKind where KindID=" & KindID)
        If tKind.BOF And tKind.EOF Then
            FoundErr = True
            ErrMsg = ErrMsg & "<li>找不到指定的类别</li>"
            Exit Sub
        Else
            KindName = tKind("KindName")
        End If
    End If

    Call ShowJS_Main("友情链接")
    Response.Write "<br>"
    Response.Write "<table width='100%' border='0' align='center' cellpadding='2' cellspacing='1' class='border'>" & vbCrLf
    Response.Write "  <tr class='title'>" & vbCrLf
    Response.Write "    <td height='22'>" & GetFsKindList() & "</td>" & vbCrLf
    Response.Write "  </tr>" & vbCrLf
    Response.Write "</table>" & vbCrLf
    Response.Write "<br>" & vbCrLf
    Response.Write "<table width='100%' border='0' align='center' cellpadding='0' cellspacing='0'>" & vbCrLf
    Response.Write "  <tr>" & vbCrLf
    Response.Write "    <td height='22'>" & GetFriendSitePath() & "</td>" & vbCrLf
    Response.Write "  </tr>" & vbCrLf
    Response.Write "</table>" & vbCrLf
    Response.Write "<table width='100%' border='0' cellpadding='0' cellspacing='0'>" & vbCrLf
    Response.Write "  <tr>" & vbCrLf
    Response.Write "  <form name='myform' method='Post' action='Admin_FriendSite.asp' onsubmit='return ConfirmDel();'>" & vbCrLf
    Response.Write "    <td>" & vbCrLf
    Response.Write "      <table class='border' border='0' cellspacing='1' width='100%' cellpadding='0'>" & vbCrLf
    Response.Write "        <tr class='title' height='22'> " & vbCrLf
    Response.Write "          <td width='30' align='center'><strong>选中</strong></td>" & vbCrLf
    Response.Write "          <td width='80' align='center'><strong>链接类别</strong></td>" & vbCrLf
    Response.Write "          <td width='60' align='center'><strong>链接类型</strong></td>" & vbCrLf
    Response.Write "          <td align='center'><strong>网站名称</strong></td>" & vbCrLf
    Response.Write "          <td width='100' align='center'><strong>网站LOGO</strong></td>" & vbCrLf
    Response.Write "          <td width='40' align='center'><strong>状态</strong></td>" & vbCrLf
    Response.Write "          <td width='150' align='center'><strong>操作</strong></td>" & vbCrLf
    Response.Write "        </tr>" & vbCrLf
    sqlFriendSite = "select ID,KindID,LinkType,SiteName,SiteUrl,SiteIntro,LogoUrl,Elite,UpdateTime from PW_FriendSite where  1=1 "
    If KindID <> "" Then
       sqlFriendSite = sqlFriendSite & " and KindID=" & KindID
    End If
    sqlFriendSite = sqlFriendSite & " order by ID desc"
    Set rsFriendSite = Server.CreateObject("ADODB.Recordset")
    rsFriendSite.Open sqlFriendSite, Conn, 1, 1
    If rsFriendSite.BOF And rsFriendSite.EOF Then
        totalPut = 0
		Response.Write "<tr class='tdbg'><td colspan='20' align='center'><br>没有任何友情链接！<br><br></td></tr>"
    Else
        totalPut = rsFriendSite.RecordCount
        If CurrentPage < 1 Then
            CurrentPage = 1
        End If
        If (CurrentPage - 1) * MaxPerPage > totalPut Then
            If (totalPut Mod MaxPerPage) = 0 Then
                CurrentPage = totalPut \ MaxPerPage
            Else
                CurrentPage = totalPut \ MaxPerPage + 1
            End If
        End If
        If CurrentPage > 1 Then
            If (CurrentPage - 1) * MaxPerPage < totalPut Then
                rsFriendSite.Move (CurrentPage - 1) * MaxPerPage
            Else
                CurrentPage = 1
            End If
        End If

        Dim FriendSiteNum
        FriendSiteNum = 0
        Do While Not rsFriendSite.EOF
            Response.Write "        <tr class='tdbg' onmouseout=""this.className='tdbg'"" onmouseover=""this.className='tdbgmouseover'"">" & vbCrLf
            Response.Write "          <td align='center'>"
            Response.Write "            <input name='ID' type='checkbox' onclick='unselectall()' id='ID' value='" & rsFriendSite("ID") & "'>"
            Response.Write "          </td>" & vbCrLf
            Response.Write "          <td align='center'>"
            Response.Write GetKindName(rsFriendSite("KindID"))
            Response.Write "          </td>" & vbCrLf
            Response.Write "          <td align='center'>"
            If rsFriendSite("LinkType") = 1 Then
                Response.Write "           LOGO链接"
            Else
                Response.Write "            文字链接"
            End If
            Response.Write "          </td>" & vbCrLf
            Response.Write "          <td>&nbsp;<a href='" & rsFriendSite("SiteUrl") & "' target='blank'>"& rsFriendSite("SiteName") & "</a></td>"
            Response.Write "          <td align='center'>"
            If rsFriendSite("LogoUrl") <> "" And rsFriendSite("LogoUrl") <> "http://" Then
                If LCase(Right(GetPhotoUrl(rsFriendSite("LogoUrl")), 3)) = "swf" Then
                    Response.Write "<object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000' codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#versFriendSiteion=5,0,0,0' width='88' height='31'><param name='movie' value='" & GetPhotoUrl(rsFriendSite("LogoUrl")) & "'><param name='quality' value='high'><embed src='" & GetPhotoUrl(rsFriendSite("LogoUrl")) & "' pluginspage='http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash' type='application/x-shockwave-flash' width='88' height='31'></embed></object>"
                Else
                    Response.Write "<a href='" & rsFriendSite("SiteUrl") & "' target='_blank' title='" & rsFriendSite("LogoUrl") & "'><img src='" & GetPhotoUrl(rsFriendSite("LogoUrl")) & "' width='88' height='31' border='0'></a>"
                End If
            Else
                Response.Write "&nbsp;"
            End If
            Response.Write "          </td>" & vbCrLf
            Response.Write "          <td width='40' align='center'>"
            If rsFriendSite("Elite") = True Then
                Response.Write "<font color=green>推荐</font> "
            Else
                Response.Write "&nbsp;"
            End If
            Response.Write "          </td>" & vbCrLf
            Response.Write "          <td align='center'>"
            Response.Write "            <a href='Admin_FriendSite.asp?Action=Modify&ID=" & rsFriendSite("ID") & "'>修改</a>"
            If rsFriendSite("Elite") = False Then
                Response.Write "            <a href='Admin_FriendSite.asp?Action=SetElite&ID=" & rsFriendSite("ID") & "'>设为推荐</a>&nbsp;"
            Else
                Response.Write "            <a href='Admin_FriendSite.asp?Action=CancelElite&ID=" & rsFriendSite("ID") & "'>取消推荐</a>&nbsp;"
            End If
            Response.Write "            <a href='Admin_FriendSite.asp?Action=Del&ID=" & rsFriendSite("ID") & "' onclick=""return confirm('确定要删除此友情链接站点吗？');"">删除</a>"
            Response.Write "          </td>" & vbCrLf
            Response.Write "        </tr>" & vbCrLf
            FriendSiteNum = FriendSiteNum + 1
            If FriendSiteNum >= MaxPerPage Then Exit Do
            rsFriendSite.MoveNext
        Loop
    End If
    rsFriendSite.Close
    Set rsFriendSite = Nothing
    Response.Write "      </table>" & vbCrLf
    Response.Write "      <table width='100%' border='0' cellpadding='0' cellspacing='0'>" & vbCrLf
    Response.Write "        <tr>" & vbCrLf
    Response.Write "          <td width='160' height='30'>"
    Response.Write "            <input name='chkAll' type='checkbox' id='chkAll' onclick='CheckAll(this.form)' value='checkbox'>选中本页所有友情链接"
    Response.Write "          </td>" & vbCrLf
    Response.Write "          <td>"
    Response.Write "            <input type='submit' value='删除选定链接' name='submit' onClick=""document.myform.Action.value='Del'"">&nbsp;"
    Response.Write "            <input type='submit' value='设为推荐链接' name='submit' onClick=""document.myform.Action.value='SetElite'"">&nbsp;"
    Response.Write "            <input type='submit' value='取消推荐链接' name='submit' onClick=""document.myform.Action.value='CancelElite'"">&nbsp;"
    Response.Write "            <input name='Action' type='hidden' id='Action' value=''>"
    Response.Write "          </td>" & vbCrLf
    Response.Write "        </tr>" & vbCrLf
    Response.Write "      </table>" & vbCrLf
    Response.Write "    </td>" & vbCrLf
    Response.Write "  </form>" & vbCrLf
    Response.Write "  </tr>" & vbCrLf
    Response.Write "</table>" & vbCrLf
    If totalPut > 0 Then
        Response.Write ShowPage(strFileName, totalPut, MaxPerPage, CurrentPage, True, True, "个友情链接", True)
    End If
End Sub


Sub ShowJS_AddModify()
    Response.Write "<script language = 'JavaScript'>" & vbCrLf
    Response.Write "function CheckForm(){" & vbCrLf
    Response.Write "  if(document.myform.SiteName.value==''){" & vbCrLf
    Response.Write "    alert('请输入网站名称！');" & vbCrLf
    Response.Write "    document.myform.SiteName.focus();" & vbCrLf
    Response.Write "    return false;" & vbCrLf
    Response.Write "  }" & vbCrLf
    Response.Write "  if(document.myform.SiteUrl.value=='' || document.myform.SiteUrl.value=='http://'){" & vbCrLf
    Response.Write "    alert('请输入网站地址！');" & vbCrLf
    Response.Write "    document.myform.SiteUrl.focus();" & vbCrLf
    Response.Write "    return false;" & vbCrLf
    Response.Write "  }" & vbCrLf
    Response.Write "}" & vbCrLf
    Response.Write "</script>" & vbCrLf
End Sub


Sub Add()
    Call ShowJS_AddModify
    Response.Write "<form method='post' name='myform' onsubmit='return CheckForm()' action='Admin_FriendSite.asp'>"
    Response.Write "  <table border='0' cellpadding='2' cellspacing='1' align='center' width='100%' class='border'>"
    Response.Write "    <tr class='title'>"
    Response.Write "      <td height='22' colspan='2' align='center'><strong>添加友情链接</strong></td>"
    Response.Write "    </tr>"
    Response.Write "    <tr class='tdbg'>"
    Response.Write "      <td width='150' align='right'><strong>链接所属类别：</strong></td>"
    Response.Write "      <td><select name='KindID' id='KindID'>" & GetFsKind_Option(0) & "</select>"
    Response.Write "      </td>"
    Response.Write "    </tr>"
    Response.Write "    <tr class='tdbg'>"
    Response.Write "      <td width='150' align='right'><strong>网站名称：</strong></td>"
    Response.Write "      <td>"
    Response.Write "        <input type='text' name='SiteName' id='SiteName' size='60' maxlength='50' value=''> <font color='#FF0000'> *</font>"
    Response.Write "      </td>"
    Response.Write "    </tr>"
    Response.Write "    <tr class='tdbg'>"
    Response.Write "      <td width='150' align='right'><strong>网站地址：</strong></td>"
    Response.Write "      <td>"
    Response.Write "        <input type='text' name='SiteUrl' id='SiteUrl' size='80' maxlength='100' value='http://'> <font color='#FF0000'>*</font>"
    Response.Write "      </td>"
    Response.Write "    </tr>"
    Response.Write "    <tr class='tdbg'>"
    Response.Write "      <td width='150' align='right'><strong>网站Logo地址：</strong></td>"
    Response.Write "      <td>"
    Response.Write "        <input type='text' name='LogoUrl' id='LogoUrl' size='80' maxlength='100' value='http://'>"
    Response.Write "      <br /><iframe style='top:2px' ID='uploadPhoto' src='Upload.asp?dialogtype=Flinkpic' frameborder=0 scrolling=no width='360' height='25'></iframe>"
    Response.Write "      </td>"
    Response.Write "    </tr>"
    Response.Write "    <tr class='tdbg'>"
    Response.Write "      <td width='150' align='right'><strong>网站简介：</strong></td>"
    Response.Write "      <td><textarea name='SiteIntro' id='SiteIntro' cols='67' rows='4'></textarea></td>"
    Response.Write "    </tr>"
    Response.Write "    <tr class='tdbg'>"
    Response.Write "      <td width='150' align='right'><strong>录入时间：</strong></td>"
    Response.Write "      <td>"
    Response.Write "        <input type='text' name='UpdateTime' id='UpdateTime' value='" & Now() & "' maxlength='50'> 时间格式为“年-月-日 时:分:秒”，如：<font color='#0000FF'>2003-5-12 12:32:47</font>"
    Response.Write "      </td>"
    Response.Write "    </tr>"
    Response.Write "    <tr class='tdbg'>"
    Response.Write "      <td width='150' align='right'><strong>是否推荐站点：</strong></td>"
    Response.Write "      <td>"
    Response.Write "        <input type='radio' name='Elite' value='yes' checked> 是&nbsp;&nbsp;"
    Response.Write "        <input type='radio' name='Elite' value='no'> 否&nbsp;&nbsp;"
    Response.Write "      </td>"
    Response.Write "    </tr>"
    Response.Write "    <tr class='tdbg'>"
    Response.Write "      <td height='40' colspan='2' align='center'>"
    Response.Write "        <input name='Action' type='hidden' id='Action' value='SaveAdd'>"
    Response.Write "        <input type='submit' value=' 确 定 ' name='submit'>&nbsp;&nbsp;"
    Response.Write "        <input type='reset' value=' 重 填 ' name='reset'>"
    Response.Write "      </td>"
    Response.Write "    </tr>"
    Response.Write "  </table>"
    Response.Write "</form>"
End Sub


Sub Modify()
    Dim ID, rsFriendSite, sqlFriendSite
    ID = PE_CLng(GetUrl("ID"))
    If ID = 0 Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请指定友情链接ID</li>"
        Exit Sub
    End If
    sqlFriendSite = "select * from PW_FriendSite where ID=" & ID
    Set rsFriendSite = Conn.Execute(sqlFriendSite)
    If rsFriendSite.BOF And rsFriendSite.EOF Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>找不到友情链接！</li>"
        rsFriendSite.Close
        Set rsFriendSite = Nothing
        Exit Sub
    End If
    Call ShowJS_AddModify
    Response.Write "<form method='post' name='myform' onsubmit='return CheckForm()' action='Admin_FriendSite.asp'>"
    Response.Write "  <table border='0' cellpadding='2' cellspacing='1' align='center' width='100%' class='border'>"
    Response.Write "    <tr class='title'>"
    Response.Write "      <td height='22' colspan='2' align='center'><strong>修改友情链接</strong></td>"
    Response.Write "    </tr>"
    Response.Write "    <tr class='tdbg'>"
    Response.Write "      <td width='150' align='right'><strong>链接所属类别：</strong></td>"
    Response.Write "      <td><select name='KindID' id='KindID'>" & GetFsKind_Option(rsFriendSite("KindID")) & "</select>"
    Response.Write "      </td>"
    Response.Write "    </tr>"
    Response.Write "    <tr class='tdbg'>"
    Response.Write "      <td width='150' align='right'><strong>网站名称：</strong></td>"
    Response.Write "      <td>"
    Response.Write "        <input type='text' name='SiteName' id='SiteName' size='60' maxlength='50' value='" & rsFriendSite("SiteName") & "'> <font color='#FF0000'> *</font>"
    Response.Write "      </td>"
    Response.Write "    </tr>"
    Response.Write "    <tr class='tdbg'>"
    Response.Write "      <td width='150' align='right'><strong>网站地址：</strong></td>"
    Response.Write "      <td>"
    Response.Write "        <input type='text' name='SiteUrl' id='SiteUrl' size='80' maxlength='100' value='" & rsFriendSite("SiteUrl") & "'> <font color='#FF0000'>*</font>"
    Response.Write "      </td>"
    Response.Write "    </tr>"
    Response.Write "    <tr class='tdbg'>"
    Response.Write "      <td width='150' align='right'><strong>网站Logo地址：</strong></td>"
    Response.Write "      <td>"
    Response.Write "        <input type='text' name='LogoUrl' id='LogoUrl' size='80' maxlength='100' value='" & GetPhotoUrl(rsFriendSite("LogoUrl")) & "'>"
    Response.Write "      <br /><iframe style='top:2px' ID='uploadPhoto' src='Upload.asp?dialogtype=Flinkpic' frameborder=0 scrolling=no width='360' height='25'></iframe>"
    Response.Write "      </td>"
    Response.Write "    </tr>"
    Response.Write "    <tr class='tdbg'>"
    Response.Write "      <td width='150' align='right'><strong>网站简介：</strong></td>"
    Response.Write "      <td><textarea name='SiteIntro' id='SiteIntro' cols='67' rows='4'>" & PE_ConvertBR(rsFriendSite("SiteIntro")) & "</textarea></td>"
    Response.Write "    </tr>"
    Response.Write "    <tr class='tdbg'>"
    Response.Write "      <td width='150' align='right'><strong>录入时间：</strong></td>"
    Response.Write "      <td>"
    Response.Write "        <input type='text' name='UpdateTime' id='UpdateTime' value='" & rsFriendSite("UpdateTime") & "' maxlength='50'> 时间格式为“年-月-日 时:分:秒”，如：<font color='#0000FF'>2003-5-12 12:32:47</font>"
    Response.Write "      </td>"
    Response.Write "    </tr>"
    Response.Write "    <tr class='tdbg'>"
    Response.Write "      <td width='150' align='right'><strong>是否推荐站点：</strong></td>"
    Response.Write "      <td>"
    Response.Write "        <input type='radio' name='Elite' value='yes' " & IsRadioChecked(rsFriendSite("Elite"), True) & "> 是&nbsp;&nbsp;"
    Response.Write "        <input type='radio' name='Elite' value='no' " & IsRadioChecked(rsFriendSite("Elite"), False) & "> 否&nbsp;&nbsp;"
    Response.Write "      </td>"
    Response.Write "    </tr>"
    Response.Write "    </tr>"
    Response.Write "    <tr class='tdbg'>"
    Response.Write "      <td height='40' colspan='2' align='center'>"
    Response.Write "        <input name='ID' type='hidden' id='ID' value='" & rsFriendSite("ID") & "'>"
    Response.Write "        <input name='Action' type='hidden' id='Action' value='SaveModify'>"
    Response.Write "        <input type='submit' value=' 修 改 ' name='submit'>&nbsp;&nbsp;"
    Response.Write "        <input name='Cancel' type='button' id='Cancel' value=' 取 消 ' onClick=""window.location.href='Admin_FriendSite.asp'"" style='cursor:hand;'>"
    Response.Write "      </td>"
    Response.Write "    </tr>"
    Response.Write "  </table>"
    Response.Write "</form>"
    rsFriendSite.Close
    Set rsFriendSite = Nothing
End Sub


Sub SaveFriendSite()
    Dim rsFriendSite, sqlFriendSite
    Dim ID, KindID, LinkType, SiteName, SiteUrl, SiteIntro, LogoUrl,UpdateTime, Elite, Passed
    ID = PE_CLng(GetForm("ID"))
    KindID = PE_CLng(GetForm("KindID"))
    SiteName = GetForm("SiteName")
    SiteUrl = GetForm("SiteUrl")
    SiteIntro = GetForm("SiteIntro")
    LogoUrl = GetForm("LogoUrl")
    UpdateTime = PE_CDate(GetForm("UpdateTime"))
    Elite = GetForm("Elite")

    If SiteName = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>网站名称不能为空！</li>"
    End If
    If SiteUrl = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>网站地址不能为空！</li>"
    End If

    If FoundErr = True Then
        Exit Sub
    End If

    If LogoUrl = "" Or LogoUrl = "http://" Then
        LinkType = 2
    Else
        LinkType = 1
    End If
    SiteName = ReplaceBadChar(SiteName)
    SiteUrl = ReplaceUrlBadChar(SiteUrl)
    LogoUrl = ReplaceUrlBadChar(LogoUrl)
    SiteIntro = PE_HTMLEncode(SiteIntro)
    Elite = CBool(Elite = "yes")
    Set rsFriendSite = Server.CreateObject("adodb.recordset")

    If Action = "SaveAdd" Then
        sqlFriendSite = "select top 1 * from PW_FriendSite where SiteName='" & SiteName & "' and SiteUrl='" & SiteUrl & "'"
        rsFriendSite.Open sqlFriendSite, Conn, 1, 3
        If Not (rsFriendSite.BOF And rsFriendSite.EOF) Then
            FoundErr = True
            ErrMsg = ErrMsg & "<li>你要添加的网站已经存在！</li>"
            rsFriendSite.Close
            Set rsFriendSite = Nothing
            Exit Sub
        End If
        rsFriendSite.addnew
		rsFriendSite("OrderID") = GetNewID("PW_FriendSite", "OrderID")
    ElseIf Action = "SaveModify" Then
        If ID = 0 Then
            FoundErr = True
            ErrMsg = ErrMsg & "<li>不能确定友情链接ID</li>"
            Exit Sub
        End If
        sqlFriendSite = "select * from PW_FriendSite where ID=" & ID
        rsFriendSite.Open sqlFriendSite, Conn, 1, 3
        If rsFriendSite.BOF And rsFriendSite.EOF Then
            FoundErr = True
            ErrMsg = ErrMsg & "<li>找不到指定的友情链接！</li>"
            rsFriendSite.Close
            Set rsFriendSite = Nothing
            Exit Sub
        End If
    End If
    rsFriendSite("KindID") = KindID
    rsFriendSite("LinkType") = LinkType
    rsFriendSite("SiteName") = SiteName
    rsFriendSite("SiteUrl") = SiteUrl
    rsFriendSite("SiteIntro") = SiteIntro
    rsFriendSite("LogoUrl") = SetPhotoUrl(LogoUrl)
    rsFriendSite("UpdateTime") = UpdateTime
    rsFriendSite("Elite") = Elite
    rsFriendSite.Update
    rsFriendSite.Close
    Set rsFriendSite = Nothing
    Call CloseConn
    Response.Redirect "Admin_FriendSite.asp"
End Sub


Sub SetProperty()
    Dim ID, sqlProperty, rsProperty
    ID = Trim(Request("ID"))
    If IsValidID(ID) = False Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请先选定友情链接！</li>"
        Exit Sub
    End If
    If Action = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>参数不足！</li>"
        Exit Sub
    End If

    If InStr(ID, ",") > 0 Then
        sqlProperty = "select * from PW_FriendSite where ID in (" & ID & ")"
    Else
        sqlProperty = "select * from PW_FriendSite where ID=" & ID
    End If
    Set rsProperty = Server.CreateObject("ADODB.Recordset")
    rsProperty.Open sqlProperty, Conn, 1, 3
    Do While Not rsProperty.EOF
        Select Case Action
        Case "SetElite"
            rsProperty("Elite") = True
        Case "CancelElite"
            rsProperty("Elite") = False
        Case "Del"
            rsProperty.Delete
        End Select
        rsProperty.Update
        rsProperty.MoveNext
    Loop
    rsProperty.Close
    Set rsProperty = Nothing
    Call CloseConn
    Response.Redirect ComeUrl
End Sub



Sub FsKind()
    Dim rsFsKind, sqlFsKind
    Response.Write "<br>"
    Response.Write "<table width='100%' border='0' align='center' cellpadding='0' cellspacing='1' class='border'>"
    Response.Write "  <tr class='title' height='22'>"
    Response.Write "    <td width='30' align='center'><strong>ID</strong></td>"
    Response.Write "    <td width='200' align='center'><strong>类别名称</strong></td>"
    Response.Write "    <td align='center'><strong>类别说明</strong></td>"
    Response.Write "    <td width='80' align='center'><strong>包含链接数</strong></td>"
    Response.Write "    <td width='120' align='center'><strong>常规操作</strong></td>"
    Response.Write "  </tr>"

    sqlFsKind = "select * from PW_FsKind order by KindID"
    Set rsFsKind = Conn.Execute(sqlFsKind)
    Do While Not rsFsKind.EOF
        Response.Write "  <tr class='tdbg' onmouseout=""this.className='tdbg'"" onmouseover=""this.className='tdbgmouseover'"">"
        Response.Write "    <td width='30' align='center'>" & rsFsKind("KindID") & "</td>"
        Response.Write "    <td width='200' align='center'>"
        Response.Write "      <a href='Admin_FriendSite.asp?KindID=" & rsFsKind("KindID") & "' title='点击进入管理此类别的友情链接'>" & PE_HTMLEncode(rsFsKind("KindName")) & "</a>"
        Response.Write "    </td>"
        Response.Write "    <td>" & rsFsKind("ReadMe") & "</td>"
        Response.Write "    <td width='80' align='center'>" & GetLinkNum(rsFsKind("KindID")) & "</td>"
        Response.Write "    <td width='120' align='center'>"
        Response.Write "      <a href='Admin_FriendSite.asp?action=ModifyFsKind&KindID=" & rsFsKind("KindID") & "'>修改</a>&nbsp;"
        Response.Write "      <a href='Admin_FriendSite.asp?Action=DelFsKind&KindID=" & rsFsKind("KindID") & "' onClick=""return confirm('确定要删除此类别吗？删除此类别后原属于此类别的友情链接将不属于任何类别。');"">删除</a>&nbsp;"
        Response.Write "      <a href='Admin_FriendSite.asp?Action=ClearFsKind&KindID=" & rsFsKind("KindID") & "' onClick=""return confirm('确定要清空此类别中的友情链接吗？本操作将原属于此类别的友情链接改为不属于任何类别。');"">清空</a>"
        Response.Write "    </td>"
        Response.Write "  </tr>"
        rsFsKind.MoveNext
    Loop
    rsFsKind.Close
    Set rsFsKind = Nothing
    Response.Write "</table>"
End Sub


Sub AddFsKind()
    Response.Write "<form name='myform' method='post' action='Admin_FriendSite.asp'>"
    Response.Write "  <table width='100%' border='0' align='center' cellpadding='2' cellspacing='1' class='border' >"
    Response.Write "    <tr class='title'>"
    Response.Write "      <td height='22' colspan='2' align='center'><strong>添加友情链接类别</strong></td>"
    Response.Write "    </tr>"
    Response.Write "    <tr class='tdbg'>"
    Response.Write "      <td width='350' class='tdbg'><strong>类别名称：</strong></td>"
    Response.Write "      <td class='tdbg'>"
    Response.Write "        <input name='KindName' type='text' id='KindName' size='49' maxlength='30'>"
    Response.Write "      </td>"
    Response.Write "    </tr>"
    Response.Write "    <tr class='tdbg'>"
    Response.Write "      <td width='350' class='tdbg'><strong>类别说明</strong></td>"
    Response.Write "      <td class='tdbg'>"
    Response.Write "        <textarea name='ReadMe' cols='40' rows='5' id='ReadMe'></textarea>"
    Response.Write "      </td>"
    Response.Write "    </tr>"
    Response.Write "    <tr class='tdbg'>"
    Response.Write "      <td colspan='2' align='center' class='tdbg'>"
    Response.Write "        <input name='Action' type='hidden' id='Action' value='SaveAddFsKind'>"
    Response.Write "        <input  type='submit' name='Submit' value=' 添 加 '>&nbsp;&nbsp;"
    Response.Write "        <input name='Cancel' type='button' id='Cancel' value=' 取 消 ' onClick=""window.location.href='Admin_FriendSite.asp'"" style='cursor:hand;'>"
    Response.Write "      </td>"
    Response.Write "    </tr>"
    Response.Write "  </table>"
    Response.Write "</form>"
End Sub


Sub ModifyFsKind()
    If KindID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请指定要修改的类别ID！</li>"
        Exit Sub
    Else
        KindID = PE_CLng(KindID)
    End If
    Dim rsFsKind, sqlFsKind
    sqlFsKind = "Select * from PW_FsKind Where KindID=" & KindID
    Set rsFsKind = Conn.Execute(sqlFsKind)
    If rsFsKind.BOF And rsFsKind.EOF Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>找不到指定的类别！</li>"
        rsFsKind.Close
        Set rsFsKind = Nothing
        Exit Sub
    End If

    Response.Write "<form name='myform' method='post' action='Admin_FriendSite.asp'>"
    Response.Write "  <table width='100%' border='0' align='center' cellpadding='2' cellspacing='1' class='border' >"
    Response.Write "    <tr class='title'>"
    Response.Write "      <td height='22' colspan='2' align='center'><strong>修改友情链接类别</strong></td>"
    Response.Write "    </tr>"
    Response.Write "    <tr class='tdbg'>"
    Response.Write "      <td width='350' class='tdbg'><strong>类别名称：</strong></td>"
    Response.Write "      <td class='tdbg'>"
    Response.Write "        <input name='KindName' type='text' id='KindName' size='49' maxlength='30' value='" & rsFsKind("KindName") & "'>"
    Response.Write "      </td>"
    Response.Write "    </tr>"
    Response.Write "    <tr class='tdbg'>"
    Response.Write "      <td width='350' class='tdbg'><strong>类别说明</strong></td>"
    Response.Write "      <td class='tdbg'>"
    Response.Write "        <textarea name='ReadMe' cols='40' rows='5' id='ReadMe'>" & PE_ConvertBR(rsFsKind("ReadMe")) & "</textarea>"
    Response.Write "      </td>"
    Response.Write "    </tr>"
    Response.Write "    <tr class='tdbg'>"
    Response.Write "      <td colspan='2' align='center' class='tdbg'>"
    Response.Write "        <input name='KindID' type='hidden' id='KindID' value='" & rsFsKind("KindID") & "'>"
    Response.Write "        <input name='Action' type='hidden' id='Action' value='SaveModifyFsKind'>"
    Response.Write "        <input  type='submit' name='Submit' value='保存修改结果'>&nbsp;&nbsp;"
    Response.Write "        <input name='Cancel' type='button' id='Cancel' value=' 取 消 ' onClick=""window.location.href='Admin_FriendSite.asp'"" style='cursor:hand;'>"
    Response.Write "      </td>"
    Response.Write "    </tr>"
    Response.Write "  </table>"
    Response.Write "</form>"

    rsFsKind.Close
    Set rsFsKind = Nothing
End Sub




Sub SaveFsKind()
    Dim KindID, KindName, ReadMe
    Dim rsFsKind, sqlFsKind
    KindID = PE_CLng(GetForm("KindID"))
    KindName = GetForm("KindName")
    ReadMe = GetForm("ReadMe")

    If KindName = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>类别名称不能为空！</li>"
    Else
        If CheckBadChar(KindName) = False Then
            FoundErr = True
            ErrMsg = ErrMsg & "<li>类别名称中含有非法字符！</li>"
        End If
    End If
    If FoundErr = True Then Exit Sub
    
    KindName = PE_HTMLEncode(KindName)
    ReadMe = PE_HTMLEncode(ReadMe)

    Set rsFsKind = Server.CreateObject("Adodb.RecordSet")
    If Action = "SaveAddFsKind" Then
        sqlFsKind = "select top 1 * from PW_FsKind"
        rsFsKind.Open sqlFsKind, Conn, 1, 3
        rsFsKind.addnew
        rsFsKind("KindID") = GetNewID("PW_FsKind", "KindID")
    ElseIf Action = "SaveModifyFsKind" Then
        If KindID = 0 Then
            FoundErr = True
            ErrMsg = ErrMsg & "<li>请指定要修改的" & KindTypeName & "ID！</li>"
            Exit Sub
        Else
            sqlFsKind = "Select * from PW_FsKind Where KindID=" & KindID
            rsFsKind.Open sqlFsKind, Conn, 1, 3
            If rsFsKind.BOF And rsFsKind.EOF Then
                FoundErr = True
                ErrMsg = ErrMsg & "<li>找不到指定的类别！</li>"
                rsFsKind.Close
                Set rsFsKind = Nothing
                Exit Sub
            End If
        End If
    End If
    rsFsKind("KindName") = KindName
    rsFsKind("ReadMe") = ReadMe
    rsFsKind.Update
    rsFsKind.Close
    Set rsFsKind = Nothing

    Call CloseConn
    Response.Redirect "Admin_FriendSite.asp?Action=FsKind "
End Sub

Sub DelFsKind()
    If KindID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请指定要删除的类别ID！</li>"
        Exit Sub
    Else
        KindID = PE_CLng(KindID)
    End If

    Conn.Execute ("delete from PW_FsKind where KindID=" & KindID)
    Conn.Execute ("update PW_FriendSite set KindID=0 where KindID=" & KindID)
    Call CloseConn
    Response.Redirect "Admin_FriendSite.asp?Action=FsKind"
End Sub

Sub ClearFsKind()
    If KindID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请指定要清空的" & KindTypeName & "ID！</li>"
        Exit Sub
    Else
        KindID = PE_CLng(KindID)
    End If
    Conn.Execute ("update PW_FriendSite set KindID=0 where KindID=" & KindID)
    Call WriteSuccessMsg("清空此类别的友情链接成功。", ComeUrl)
	Call Refresh(ComeUrl,1)
End Sub


Sub Order()
    Dim rsFriendSite, sqlFriendSite, iCount, i, j

    Response.Write "<br>"
    Response.Write "<table width='100%' border='0' cellpadding='0' cellspacing='0'>"
    Response.Write "  <tr>"
    Response.Write "  <form name='myform' method='Post' action='Admin_FriendSite.asp' onsubmit='return ConfirmDel();'>"
    Response.Write "    <td>"
    Response.Write "      <table class='border' border='0' cellspacing='1' width='100%' cellpadding='0'>"
    Response.Write "        <tr class='title' height='22'> "
    Response.Write "          <td width='30' align='center'><strong>序号</strong></td>"
    Response.Write "          <td width='80' align='center'><strong>链接分类</strong></td>"
    Response.Write "          <td width='60' align='center'><strong>链接类型</strong></td>"
    Response.Write "          <td align='center'><strong>网站名称</strong></td>"
    Response.Write "          <td width='100' align='center'><strong>网站LOGO</strong></td>"
    Response.Write "          <td width='240' Colspan='2' align='center'><strong>操作</strong></td>"
    Response.Write "        </tr>"

    sqlFriendSite = "select ID,KindID,LinkType,SiteName,SiteUrl,SiteIntro,LogoUrl,Elite,OrderID,UpdateTime from PW_FriendSite order by OrderID asc"
    Set rsFriendSite = Server.CreateObject("ADODB.Recordset")
    rsFriendSite.Open sqlFriendSite, Conn, 1, 1
    iCount = rsFriendSite.RecordCount
    j = 1
    If rsFriendSite.BOF And rsFriendSite.EOF Then
		Response.Write "<tr class='tdbg'><td colspan='20' align='center'><br>没有任何友情链接！<br><br></td></tr>"
    Else
        Do While Not rsFriendSite.EOF
            Response.Write "        <tr class='tdbg' onmouseout=""this.className='tdbg'"" onmouseover=""this.className='tdbgmouseover'"">"
            Response.Write "          <td width='30' align='center'>"
            Response.Write rsFriendSite("OrderID")
            Response.Write "          </td>"
            Response.Write "          <td width='80' align='center'>"
            Response.Write GetKindName(rsFriendSite("KindID"))
            Response.Write "          </td>"
            Response.Write "          <td width='60' align='center'>"
            If rsFriendSite("LinkType") = 1 Then
                Response.Write "            LOGO链接"
            Else
                Response.Write "            文字链接"
            End If
            Response.Write "          </td>"
            Response.Write "          <td><a href='" & rsFriendSite("SiteUrl") & "' target='blank' > "& rsFriendSite("SiteName") & "</a></td>"
            Response.Write "          <td width='100' align='center'>"
            If rsFriendSite("LogoUrl") <> "" And rsFriendSite("LogoUrl") <> "http://" Then
                If LCase(Right(rsFriendSite("LogoUrl"), 3)) = "swf" Then
                    Response.Write "<object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000' codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#versFriendSiteion=5,0,0,0' width='88' height='31'><param name='movie' value='" & rsFriendSite("LogoUrl") & "'><param name='quality' value='high'><embed src='" & rsFriendSite("LogoUrl") & "' pluginspage='http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash' type='application/x-shockwave-flash' width='88' height='31'></embed></object>"
                Else
                    Response.Write "<a href='" & rsFriendSite("SiteUrl") & "' target='_blank' title='" & rsFriendSite("LogoUrl") & "'><img src='" & rsFriendSite("LogoUrl") & "' width='88' height='31' border='0'></a>"
                End If
            Else
                Response.Write "&nbsp;"
            End If
            Response.Write "          </td>"
            Response.Write "<form action='Admin_FriendSite.asp?Action=UpOrder' method='post'>"
            Response.Write "          <td width='120' align='center'>"

            If j > 1 Then
                Response.Write "<select name=MoveNum size=1><option value=0>向上移动</option>"
                For i = 1 To j - 1
                    Response.Write "<option value=" & i & ">" & i & "</option>"
                Next
                Response.Write "</select>"
                Response.Write "<input type=hidden name=iFriendSiteID value=" & rsFriendSite("ID") & ">"
                Response.Write "<input type=hidden name=cOrderID value=" & rsFriendSite("OrderID") & ">&nbsp;<input type=submit name=Submit value=修改>"
            Else
                Response.Write "&nbsp;"
            End If
            Response.Write "</td></form>"
            Response.Write "<form action='Admin_FriendSite.asp?Action=DownOrder' method='post'>"
            Response.Write "  <td width='120' align='center'>"
            If iCount > j Then
                Response.Write "<select name=MoveNum size=1><option value=0>向下移动</option>"
                For i = 1 To iCount - j
                    Response.Write "<option value=" & i & ">" & i & "</option>"
                Next
                Response.Write "</select>"
                Response.Write "<input type=hidden name=iFriendSiteID value=" & rsFriendSite("ID") & ">"
                Response.Write "<input type=hidden name=cOrderID value=" & rsFriendSite("OrderID") & ">&nbsp;<input type=submit name=Submit value=修改>"
            Else
                Response.Write "&nbsp;"
            End If
            Response.Write "</td></form></tr>"
            j = j + 1
            rsFriendSite.MoveNext
        Loop
    End If
    rsFriendSite.Close
    Set rsFriendSite = Nothing
    Response.Write "      </table>"

    Response.Write "    </td>"
    Response.Write "  </form>"
    Response.Write "  </tr>"
    Response.Write "</table>"

    Response.Write "<br>"

End Sub

Sub UpOrder()
    Dim FriendSiteID, sqlOrder, rsOrder, MoveNum, cOrderID, tOrderID, i, rsFriendSite
    FriendSiteID = GetForm("iFriendSiteID")
    cOrderID = GetForm("cOrderID")
    MoveNum = GetForm("MoveNum")
    If FriendSiteID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>参数不足！</li>"
    Else
        FriendSiteID = PE_CLng(FriendSiteID)
    End If
    If cOrderID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>错误参数！</li>"
    Else
        cOrderID = PE_CLng(cOrderID)
    End If
    If MoveNum = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>错误参数！</li>"
    Else
        MoveNum = PE_CLng(MoveNum)
        If MoveNum = 0 Then
            FoundErr = True
            ErrMsg = ErrMsg & "<li>请选择要提升的数字！</li>"
        End If
    End If
    If FoundErr = True Then
        Exit Sub
    End If

    Dim mrs, MaxOrderID
    Set mrs = Conn.Execute("select max(OrderID) from PW_FriendSite")
    MaxOrderID = mrs(0) + 1
    '先将当前项目移至最后
    Conn.Execute ("update PW_FriendSite set OrderID=" & MaxOrderID & " where ID=" & FriendSiteID)
    
    '然后将位于当前项目以上的项目的OrderID依次加一，范围为要提升的数字
    sqlOrder = "select * from PW_FriendSite where OrderID<" & cOrderID & " order by OrderID desc"
    Set rsOrder = Server.CreateObject("adodb.recordset")
    rsOrder.Open sqlOrder, Conn, 1, 3
    If rsOrder.BOF And rsOrder.EOF Then
        Response.Redirect ("Admin_FriendSite.asp?Action=Order")
        Exit Sub        '如果当前项目已经在最上面，则无需移动
    End If
    i = 1
    Do While Not rsOrder.EOF
        tOrderID = rsOrder("OrderID")     '得到要提升位置的OrderID
        Conn.Execute ("update PW_FriendSite set OrderID=OrderID+1 where OrderID=" & tOrderID)
        i = i + 1
        If i > MoveNum Then
            Exit Do
        End If
        rsOrder.MoveNext
    Loop
    rsOrder.Close
    Set rsOrder = Nothing
    
    '然后再将当前项目从最后移到相应位置
    Conn.Execute ("update PW_FriendSite set OrderID=" & tOrderID & " where ID=" & FriendSiteID)

    Response.Redirect ("Admin_FriendSite.asp?Action=Order")
End Sub

Sub DownOrder()
    Dim FriendSiteID, sqlOrder, rsOrder, MoveNum, cOrderID, tOrderID, i, rsFriendSite, PrevID, NextID
    FriendSiteID = GetForm("iFriendSiteID")
    cOrderID = GetForm("cOrderID")
    MoveNum = GetForm("MoveNum")
    If FriendSiteID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>参数不足！</li>"
    Else
        FriendSiteID = PE_CLng(FriendSiteID)
    End If
    If cOrderID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>错误参数！</li>"
    Else
        cOrderID = PE_CLng(cOrderID)
    End If
    If MoveNum = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>错误参数！</li>"
    Else
        MoveNum = PE_CLng(MoveNum)
        If MoveNum = 0 Then
            FoundErr = True
            ErrMsg = ErrMsg & "<li>请选择要提升的数字！</li>"
        End If
    End If
    If FoundErr = True Then
        Exit Sub
    End If

    Dim mrs, MaxOrderID
    Set mrs = Conn.Execute("select max(OrderID) from PW_FriendSite")
    MaxOrderID = mrs(0) + 1
    '先将当前项目移至最后
    Conn.Execute ("update PW_FriendSite set OrderID=" & MaxOrderID & " where ID=" & FriendSiteID)
    
    '然后将位于当前项目以下的项目的OrderID依次减一，范围为要下降的数字
    sqlOrder = "select * from PW_FriendSite where OrderID>" & cOrderID & " order by OrderID"
    Set rsOrder = Server.CreateObject("adodb.recordset")
    rsOrder.Open sqlOrder, Conn, 1, 3
    If rsOrder.BOF And rsOrder.EOF Then
        Exit Sub        '如果当前项目已经在最下面，则无需移动
    End If
    i = 1
    Do While Not rsOrder.EOF
        tOrderID = rsOrder("OrderID")     '得到要提升位置的OrderID
        Conn.Execute ("update PW_FriendSite set OrderID=OrderID-1 where OrderID=" & tOrderID)
        i = i + 1
        If i > MoveNum Then
            Exit Do
        End If
        rsOrder.MoveNext
    Loop
    rsOrder.Close
    Set rsOrder = Nothing
    
    '然后再将当前项目从最后移到相应位置
    Conn.Execute ("update PW_FriendSite set OrderID=" & tOrderID & " where ID=" & FriendSiteID)
    
    Response.Redirect ("Admin_FriendSite.asp?Action=Order")
End Sub



Function GetFsKindList()
    Dim rsFsKind, sqlFsKind, strFsKind, i
    sqlFsKind = "select * from PW_FsKind"
    sqlFsKind = sqlFsKind & " order by KindID"
    Set rsFsKind = Conn.Execute(sqlFsKind)
    If rsFsKind.BOF And rsFsKind.EOF Then
        strFsKind = strFsKind & "没有任何有情连接"
    Else
        i = 1
        strFsKind = "| "
        Do While Not rsFsKind.EOF
            If rsFsKind("KindID") = KindID Then
                strFsKind = strFsKind & "<a href='" & FileName & "&KindID=" & KindID & "'><font color=red>" & rsFsKind("KindName") & "</font></a>"
            Else
                strFsKind = strFsKind & "<a href='" & FileName & "&KindID=" & rsFsKind("KindID") & "'>" & rsFsKind("KindName") & "</a>"
            End If
            strFsKind = strFsKind & " | "
            i = i + 1
            If i Mod 10 = 0 Then
                strFsKind = strFsKind & "<br>"
            End If
            rsFsKind.MoveNext
        Loop
    End If
    rsFsKind.Close
    Set rsFsKind = Nothing
    GetFsKindList = strFsKind
End Function


Function GetFriendSitePath()
    Dim strPath
    strPath = "您现在的位置：&nbsp;<a href='Admin_FriendSite.asp'>友情链接管理</a>&nbsp;&gt;&gt;&nbsp;"
    If KindID <> "" Then
        strPath = strPath & "<a href='" & FileName & "&KindID=" & KindID & "'>" & KindName & "</a>&nbsp;&gt;&gt;&nbsp;"
    End If
	strPath = strPath & "所有友情链接"
    GetFriendSitePath = strPath
End Function


Function GetKindName(iKindID)
    Dim strKindName, rsFsKind, sqlFsKind
    If iKindID > 0 Then
        sqlFsKind = "select KindName from PW_FsKind where KindID=" & iKindID
        Set rsFsKind = Conn.Execute(sqlFsKind)
        If rsFsKind.BOF And rsFsKind.EOF Then
            strKindName = ""
        Else
            strKindName = "<a href='" & FileName & "&KindID=" & iKindID & "'>" & rsFsKind(0) & "</a>"
        End If
        rsFsKind.Close
        Set rsFsKind = Nothing
    End If
    GetKindName = strKindName
End Function


Function GetFsKind_Option(KindID)
    Dim sqlFsKind, rsFsKind, strOption
    strOption = "<option value='0'"
    If KindID = "" Then
        strOption = strOption & " selected"
    End If
    strOption = strOption & ">不属于任何类别</option>"
    sqlFsKind = "select * from PW_FsKind"
    sqlFsKind = sqlFsKind & " order by KindID"
    Set rsFsKind = Conn.Execute(sqlFsKind)
    Do While Not rsFsKind.EOF
        If rsFsKind("KindID") = KindID Then
            strOption = strOption & "<option value='" & rsFsKind("KindID") & "' selected>" & rsFsKind("KindName") & "</option>"
        Else
            strOption = strOption & "<option value='" & rsFsKind("KindID") & "'>" & rsFsKind("KindName") & "</option>"
        End If
        rsFsKind.MoveNext
    Loop
    rsFsKind.Close
    Set rsFsKind = Nothing
    GetFsKind_Option = strOption
End Function


Function GetLinkNum(iKindID)
    Dim rsLinkNum
    Set rsLinkNum = Conn.Execute("select count(ID) from PW_FriendSite where KindID=" & iKindID & "")
    If IsNull(rsLinkNum(0)) Then
        GetLinkNum = 0
    Else
        GetLinkNum = rsLinkNum(0)
    End If
    Set rsLinkNum = Nothing
End Function

%>