<%


'==================================================
'函数名：ReplaceRemoteUrl
'作  用：替换字符串中的远程文件为本地文件并保存远程文件
'参  数：strContent ------ 要替换的字符串
'==================================================
Function ReplaceRemoteUrl(ByVal strContent)
    If IsObjInstalled("Microsoft.XMLHTTP") = False Or ObjInstalled_FSO = False Then
        ReplaceRemoteUrl = strContent
        Exit Function
    End If
    Dim RemoteFiles, RemoteFile, RemoteFileUrl, SaveFilePath, SavePath, SavePath2, SaveFileName, ThumbFileName, SaveFileType, arrSaveFileName, ranNum, dtNow, FileCount, SavedFiles
    Dim temptime, FilesArray, tempi
    If fso.FolderExists(Server.MapPath(InstallDir)) = False Then fso.CreateFolder Server.MapPath(InstallDir)
    SavePath = strInstallDir & UploadDir        '文件保存的本地路径
    If fso.FolderExists(Server.MapPath(SavePath)) = False Then fso.CreateFolder Server.MapPath(SavePath)
    SavePath = SavePath & "/"
    
    FileCount = 0
    SavedFiles = "|"
    tempi = 0
    regEx.Pattern = "((http|https|ftp|rtsp|mms):(\/\/|\\\\){1}([\w\-]+[.]){1,}(net|com|cn|org|cc|tv|[0-9]{1,3})(\S*\/)((\S)+[.]{1}(gif|jpg|jpeg|jpe|bmp|png)))"
    Set RemoteFiles = regEx.Execute(strContent)

    

    For Each RemoteFile In RemoteFiles
        RemoteFileUrl = RemoteFile.value
        If InStr(SavedFiles, "|" & RemoteFileUrl & "|") > 0 Then
            '如果已经保存则不进行处理
        Else
            If FileCount = 0 Then
                Response.Write "<b>正在保存远程文件……请稍候！<font color='red'>在此过程中请勿刷新页面！</font></b> "
                Response.Flush
            End If

            SavedFiles = SavedFiles & RemoteFileUrl & "|"
            dtNow = Now()
            arrSaveFileName = Split(RemoteFileUrl, ".")
            SaveFileType = arrSaveFileName(UBound(arrSaveFileName))
            SavePath2 = Year(dtNow) & Right("0" & Month(dtNow), 2)
            If fso.FolderExists(Server.MapPath(SavePath & SavePath2)) = False Then fso.CreateFolder Server.MapPath(SavePath & SavePath2)
            SavePath2 = SavePath2 & "/"
            SaveFilePath = SavePath & SavePath2
            
            Randomize
            ranNum = Int(900 * Rnd) + 100
            temptime = Year(dtNow) & Right("0" & Month(dtNow), 2) & Right("0" & Day(dtNow), 2) & Right("0" & Hour(dtNow), 2) & Right("0" & Minute(dtNow), 2) & Right("0" & Second(dtNow), 2) & ranNum
            SaveFileName = temptime & "." & SaveFileType
            If SaveRemoteFile(RemoteFileUrl, SaveFilePath & SaveFileName) = True Then
                strContent = Replace(strContent, RemoteFileUrl, InstallDir&UploadDir&"/" & SavePath2 & SaveFileName)

				If UploadFiles = "" Then
					UploadFiles = InstallDir&UploadDir&"/" &SavePath2 & SaveFileName
				Else
					UploadFiles = UploadFiles & "|" & InstallDir&UploadDir&"/" &SavePath2 & SaveFileName
				End If
                If PE_CLng(GetForm("IncludePic")) = 0 Then
                    If FileCount > 0 Then
                        IncludePic = 2
                    Else
                        IncludePic = 1
                    End If
                Else
                    IncludePic = PE_CLng(GetForm("IncludePic"))
                End If

                If InStr(UploadFiles, "|") = 0 Then
                    DefaultPicUrl = UploadFiles
                Else
                    FilesArray = Split(UploadFiles, "|")
                    DefaultPicUrl = FilesArray(0)
                End If
                FileCount = FileCount + 1
            End If
            tempi = tempi + 1
            Response.Write "·"
            Response.Flush
        End If
    Next
    If FileCount > 0 Then Response.Write " <b><font color='blue'>共成功保存了 " & FileCount & " 张远程图片！</font></b><br>"
    ReplaceRemoteUrl = strContent
End Function

'==================================================
'函数名：SaveRemoteFile
'作  用：保存远程的文件到本地
'参  数：LocalFileName ------ 本地文件名
'        RemoteFileUrl ------ 远程文件URL
'返回值：True ----- 保存成功
'       False ----- 保存失败
'==================================================
Function SaveRemoteFile(RemoteFileUrl, LocalFileName)
    On Error Resume Next

    Dim Ads, Retrieval, GetRemoteData
    Set Retrieval = Server.CreateObject("Microsoft.XMLHTTP")
    With Retrieval
        .Open "Get", RemoteFileUrl, False, "", ""
        .Send
        GetRemoteData = .ResponseBody
    End With
    If Err.Number <> 0 Then
        Err.Clear
        Response.Write "<br>" & RemoteFileUrl & " Get Failed"
        SaveRemoteFile = False
        Exit Function
    End If
    Set Retrieval = Nothing
    Set Ads = Server.CreateObject("Adodb.Stream")
    With Ads
        .Type = 1
        .Open
        .Write GetRemoteData
        .SaveToFile Server.MapPath(LocalFileName), 2
        .Cancel
        .Close
    End With
    Set Ads = Nothing
    If Err.Number <> 0 Then
        Err.Clear
        Response.Write "<br>" & LocalFileName & " Save Failed"
        SaveRemoteFile = False
    Else
        SaveRemoteFile = True
    End If
End Function

%>
