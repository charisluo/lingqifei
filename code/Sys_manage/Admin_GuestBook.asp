<!--#include file="Admin_Common.asp"-->
<%
Const NeedCheckComeUrl = True   '是否需要检查外部访问
Const PurviewLevel_Channel = 0   '0--不检查
Const PurviewLevel_Others = "Gbook"   '其他权限


Dim rs, sql, rsGuest, sqlGuest
Dim GuestID, Passed, i

Passed = GetValue("Passed")
GuestID = GetValue("GuestID")
If Passed = "" Then
    Passed = Session("Passed")
End If
Session("Passed") = Passed

If IsValidID(GuestID) = False Then
    GuestID = ""
End If

strFileName = "Admin_GuestBook.asp?Action=" & Action


Response.Write "<html><head><title>留言管理</title>" & vbCrLf
Response.Write "<meta http-equiv='Content-Type' content='text/html; charset=gb2312'>" & vbCrLf
Response.Write "<link href='Admin_Style.css' rel='stylesheet' type='text/css'>" & vbCrLf
Response.Write "</head>" & vbCrLf
Response.Write "<body leftmargin='2' topmargin='0' marginwidth='0' marginheight='0'>" & vbCrLf
Response.Write "<table width='100%' border='0' align='center' cellpadding='2' cellspacing='1' class='border'>" & vbCrLf
Call ShowPageTitle("网 站 留 言 管 理")

Response.Write "  <tr class='tdbg'>" & vbCrLf
Response.Write "    <td width='70' height='30'><strong>管理导航：</strong></td>" & vbCrLf
Response.Write "    <td>"
Response.Write "    <a href='Admin_GuestBook.asp?Passed=All'>网站留言管理</a>&nbsp;|&nbsp;"
Response.Write "    <a href='Admin_GuestBook.asp?Passed=False'>网站留言审核</a>&nbsp;"
Response.Write "    </td>" & vbCrLf
Response.Write "  </tr>" & vbCrLf
If Action = "" Then
    Response.Write "<form name='form' method='Post' action='Admin_GuestBook.asp'><tr class='tdbg'>"
    Response.Write "      <td width='70' height='30' ><strong>留言选项：</strong></td><td>"
    Response.Write "  <input name='Passed' type='radio' value='All' onclick='submit();'"
    If Passed = "All" Then Response.Write " checked"
    Response.Write ">所有留言&nbsp;&nbsp;&nbsp;&nbsp;<input name='Passed' type='radio' value='False' onclick='submit();'"
    If Passed = "False" Then Response.Write " checked"
    Response.Write ">未审核的留言&nbsp;&nbsp;&nbsp;&nbsp;<input name='Passed' type='radio' value='True' onclick='submit();'"
    If Passed = "True" Then Response.Write " checked"
    Response.Write ">已审核的留言</td></tr></form>" & vbCrLf
End If
Response.Write "</table>" & vbCrLf


Select Case Action
Case "Modify"
    Call Modify
Case "Show"
    Call Show
Case "SaveModify"
    Call SaveModify
Case "AdminReply"
    Call AdminReply
Case "SaveAdminReply"
    Call SaveAdminReply
Case "Del", "SetPassed", "CancelPassed"
    Call SetProperty
Case Else
    Call Main
End Select


If FoundErr = True Then
    Call WriteErrMsg(ErrMsg, ComeUrl)
End If
Response.Write "</body></html>"
Call CloseConn


Sub Main()
    
    Call ShowJS_Main("留言")
    Response.Write "<br><table width='100%' border='0' align='center' cellpadding='0' cellspacing='0'>"
    Response.Write "  <tr>"
    Response.Write "    <td height='22'>" & GetManagePath() & "</td>"
    Response.Write "  </tr>"
    Response.Write "</table>"
    Response.Write "<table width='100%' border='0' cellpadding='0' cellspacing='0'><tr>"
    Response.Write "  <form name='myform' method='Post' action='Admin_GuestBook.asp' onsubmit='return ConfirmDel();'>"
    Response.Write "  <td><table class='border' border='0' cellspacing='1' width='100%' cellpadding='0'>"
    Response.Write "     <tr class='title'>"
    Response.Write "    <td width='30' height='22' align='center'><strong>选中</strong></td>"
    Response.Write "    <td width='85' height='22' align='center'><strong>留言人</strong></td>"
    Response.Write "    <td height='22' align='center'><strong>留言主题</strong></td>"
    Response.Write "    <td width='120' height='22' align='center'><strong>留言时间</strong></td>"
    Response.Write "    <td width='30' height='22' align='center'><strong>审核</strong></td>"
    Response.Write "    <td width='328' height='22' align='center'><strong>操作</strong></td>"
    Response.Write "  </tr>"

    sqlGuest = " select G.* from PW_GuestBook G where 1=1"
    If Passed = "True" Then
        sqlGuest = sqlGuest & " and GuestIsPassed=1"
    ElseIf Passed = "False" Then
        sqlGuest = sqlGuest & " and GuestIsPassed=0"
    End If
    sqlGuest = sqlGuest & " order by G.GuestId desc"
    Set rsGuest = Server.CreateObject("adodb.recordset")
    rsGuest.Open sqlGuest, Conn, 1, 1
    If rsGuest.BOF And rsGuest.EOF Then
        Response.Write "<tr class='tdbg'><td colspan='20' align='center'><br>没有任何留言！<br><br></td></tr>"
    Else
        totalPut = rsGuest.RecordCount
        If CurrentPage < 1 Then
            CurrentPage = 1
        End If
        If (CurrentPage - 1) * MaxPerPage > totalPut Then
            If (totalPut Mod MaxPerPage) = 0 Then
                CurrentPage = totalPut \ MaxPerPage
            Else
                CurrentPage = totalPut \ MaxPerPage + 1
            End If
        End If
        If CurrentPage > 1 Then
            If (CurrentPage - 1) * MaxPerPage < totalPut Then
                rsGuest.Move (CurrentPage - 1) * MaxPerPage
            Else
                CurrentPage = 1
            End If
        End If

        Dim GuestNum
        GuestNum = 0
        Do While Not rsGuest.EOF
            Response.Write "    <tr class='tdbg' onmouseout=""this.className='tdbg'"" onmouseover=""this.className='tdbgmouseover'"">"
            Response.Write "      <td align='center'><input name='GuestID' type='checkbox' onclick='unselectall()' value='" & rsGuest("GuestID") & "'></td>"
            Response.Write "      <td align='center'><div style='cursor:hand'>" & rsGuest("GuestName") & "</div></td>"
            Response.Write "      <td><a href='Admin_GuestBook.asp?Action=AdminReply&GuestID=" & rsGuest("GuestID") & "'>"& rsGuest("GuestTitle") &"</a></td>"
			Response.write "	  </td>"
            Response.Write "      <td align='center'>"
            If rsGuest("GuestDatetime") <> "" Then
                Response.Write "(" & TransformTime(FormatDateTime(rsGuest("GuestDatetime"), 0)) & ")"
            End If
            Response.Write "</td>"
            Response.Write "      <td align='center'>"
            If rsGuest("GuestIsPassed") = True Then
                Response.Write "<b>√</b>"
            Else
                Response.Write "<font color=red><b>×</b></font>"
            End If
            Response.Write "      </td>"
            Response.Write "      <td width='328' align='center'>"
			Response.Write "      <a href='Admin_GuestBook.asp?Action=Modify&GuestID=" & rsGuest("GuestID") & "'>修改</a>"
			Response.Write "      <a href='Admin_GuestBook.asp?Action=Del&GuestID=" & rsGuest("GuestID") & "' onClick=""return confirm('确定要删除此回复吗？');"">删除</a>"
			If rsGuest("GuestIsPassed") = False Then
				Response.Write "      <a href='Admin_GuestBook.asp?Action=SetPassed&GuestID=" & rsGuest("GuestID") & "'>通过审核</a>"
			Else
				Response.Write "      <a href='Admin_GuestBook.asp?Action=CancelPassed&GuestID=" & rsGuest("GuestID") & "'>取消审核</a>"
			End If
			Response.Write "      <a href='Admin_GuestBook.asp?Action=AdminReply&GuestID=" & rsGuest("GuestID") & "'>回复</a>"
            Response.Write "      </td>"
            Response.Write "    </tr>"

            GuestNum = GuestNum + 1
            If GuestNum >= MaxPerPage Then Exit Do
            rsGuest.MoveNext
        Loop
    End If
    rsGuest.Close
    Set rsGuest = Nothing
    Response.Write "</table>"
    Response.Write "<table width='100%' border='0' cellpadding='0' cellspacing='0'>"
    Response.Write "  <tr>"
    Response.Write "    <td width='130' height='30'><input name='chkAll' type='checkbox' id='chkAll' onclick='CheckAll(this.form)' value='checkbox'>选中所有的留言</td><td>"
    Response.Write "<input type='submit' value='删除选定的留言' name='submit' onClick=""document.myform.Action.value='Del'"" >&nbsp;&nbsp;"
    Response.Write "<input name='submit1' type='submit' id='submit1' onClick=""document.myform.Action.value='SetPassed'"" value='审核通过选定的留言' >&nbsp;&nbsp;"
    Response.Write "<input name='submit2' type='submit' id='submit2' onClick=""document.myform.Action.value='CancelPassed'"" value='取消审核选定的留言' >&nbsp;&nbsp;"
    Response.Write "<input name='Action' type='hidden' id='Action' value=''>"
    Response.Write "  </td></tr>"
    Response.Write "</table>"
    Response.Write "</td>"
    Response.Write "</form></tr></table>"
    If totalPut > 0 Then
        Response.Write ShowPage(strFileName, totalPut, MaxPerPage, CurrentPage, True, True, "条留言", True)
    End If
End Sub


Function GetManagePath()
    Dim strPath
    strPath = "您现在的位置：网站留言管理&nbsp;&gt;&gt;&nbsp;"
	If Passed = "True" Then
		strPath = strPath & "所有<font color=green>已审核</font>的留言"
	ElseIf Passed = "False" Then
		strPath = strPath & "所有<font color=blue>未审核</font>的留言"
	Else
		strPath = strPath & "所有留言"
	End If
    GetManagePath = strPath
End Function


Sub SetProperty()
    Dim sqlProperty, rsProperty
    If GuestID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请指定留言ID</li>"
    End If
    If Action = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>参数不足！</li>"
    End If
    If FoundErr = True Then
        Exit Sub
    End If
    If InStr(GuestID, ",") > 0 Then
        sqlProperty = "select * from PW_GuestBook where GuestID in (" & GuestID & ")"
    Else
        sqlProperty = "select * from PW_GuestBook where GuestID=" & GuestID
    End If
    Set rsProperty = Server.CreateObject("ADODB.Recordset")
    rsProperty.Open sqlProperty, Conn, 1, 3
    Do While Not rsProperty.EOF
        Select Case Action
        Case "SetPassed"
			rsProperty("GuestIsPassed") = True
        Case "CancelPassed"
			rsProperty("GuestIsPassed") = False
        Case "Del"
			rsProperty.Delete
        End Select
        rsProperty.Update
        rsProperty.MoveNext
    Loop
    rsProperty.Close
    Set rsProperty = Nothing
    Call CloseConn
    If FoundErr = True Then
        Exit Sub
    End If
    Response.Redirect ComeUrl
End Sub

Sub ShowJS_Guest()
    Response.Write "<script language = 'JavaScript'>" & vbCrLf
    Response.Write "function CheckForm()" & vbCrLf
    Response.Write "{" & vbCrLf
    Response.Write "  if(document.myform.GuestName.value==''){" & vbCrLf
    Response.Write "    alert('姓名不能为空！');" & vbCrLf
    Response.Write "    document.myform.GuestName.focus();" & vbCrLf
    Response.Write "    return(false) ;" & vbCrLf
    Response.Write "  }" & vbCrLf
    Response.Write "  if(document.myform.GuestTitle.value==''){" & vbCrLf
    Response.Write "    alert('留言主题不能为空！');" & vbCrLf
    Response.Write "    document.myform.GuestTitle.focus();" & vbCrLf
    Response.Write "    return(false);" & vbCrLf
    Response.Write "  }" & vbCrLf
    Response.Write "  if(document.myform.GuestContent.value.==''){" & vbCrLf
    Response.Write "    alert('留言内容不能为空！');" & vbCrLf
    Response.Write "    document.myform.GuestContent.focus();" & vbCrLf
    Response.Write "    return(false);" & vbCrLf
    Response.Write "  }" & vbCrLf
    Response.Write "}" & vbCrLf

    Response.Write "</script>" & vbCrLf
End Sub

Sub Modify()
    If GuestID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请指定要修改的留言ID！</li>"
        Exit Sub
    Else
        GuestID = PE_CLng(GuestID)
    End If
    sql = "select * from PW_GuestBook where GuestID=" & GuestID
    Set rs = Server.CreateObject("adodb.recordset")
    rs.Open sql, Conn, 1, 1
    If rs.BOF And rs.EOF Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>找不到指定的留言！</li>"
        rs.Close
        Set rs = Nothing
        Exit Sub
    End If
    Call ShowJS_Guest
    Response.Write "<br><table width='100%' border='0' align='center' cellpadding='0' cellspacing='0' class='border'>"
    Response.Write "  <tr class='title'>"
    Response.Write "    <td height='22' align='center'><b>修改留言</b></td>"
    Response.Write "  </tr>"
    Response.Write "  <tr align='center'>"
    Response.Write "    <td><table width='100%' border='0' cellpadding='2' cellspacing='0'>"
    Response.Write "        <form name='myform' method='post' action='Admin_GuestBook.asp' onSubmit='return CheckForm()'>" & vbCrLf
	Response.Write "          <tr class='tdbg'>" & vbCrLf
	Response.Write "            <td width='30%' align='right'>姓 &nbsp;名： </td>" & vbCrLf
	Response.Write "            <td width='70%'>" & vbCrLf
	Response.Write "              <input type='text' name='GuestName' maxlength='14' size='20' value='" & rs("GuestName") & "'>"
	Response.Write "              <font color=red>*</font>" & vbCrLf
	Response.Write "            </td>" & vbCrLf
	Response.Write "          </tr>" & vbCrLf
	Response.Write "          <tr class='tdbg'>" & vbCrLf
	Response.Write "            <td align='right'>Oicq： </td>" & vbCrLf
	Response.Write "            <td>" & vbCrLf
	Response.Write "              <input type='text' name='GuestTel' maxlength='15' size='20' value='" & rs("GuestTel") & "'>"
	Response.Write "            </td>" & vbCrLf
	Response.Write "          </tr>" & vbCrLf
	Response.Write "          <tr class='tdbg'>" & vbCrLf
	Response.Write "            <td align='right'>QQ： </td>" & vbCrLf
	Response.Write "            <td>" & vbCrLf
	Response.Write "              <input type='text' name='GuestOicq' maxlength='15' size='20' value='" & rs("GuestOicq") & "'>"
	Response.Write "            </td>" & vbCrLf
	Response.Write "          </tr>" & vbCrLf
	Response.Write "          <tr class='tdbg'>" & vbCrLf
	Response.Write "            <td align='right'>Email： </td>" & vbCrLf
	Response.Write "            <td>" & vbCrLf
	Response.Write "              <input type='text' name='GuestEmail' maxlength='15' size='20' value='" & rs("GuestEmail") & "'>"
	Response.Write "            </td>" & vbCrLf
	Response.Write "          </tr>" & vbCrLf
    Response.Write "          <tr class='tdbg'>" & vbCrLf
    Response.Write "            <td align='right'>留言主题： </td>" & vbCrLf
    Response.Write "            <td>" & vbCrLf
    Response.Write "              <input type='text' name='GuestTitle' size='37' maxlength='21' value='" & rs("GuestTitle") & "'>" & vbCrLf
    Response.Write "              <font color=red>*</font>" & vbCrLf
    Response.Write "            </td>" & vbCrLf
    Response.Write "          </tr>" & vbCrLf
    Response.Write "          <tr class='tdbg'>" & vbCrLf
    Response.Write "            <td valign='middle' align='right'>留言内容：</td>" & vbCrLf
    Response.Write "            <td valign='top'>" & vbCrLf
    Response.Write "              <textarea name='GuestContent' cols='60' rows='6' >" & PE_HtmlDecode(rs("GuestContent")) & "</textarea>" & vbCrLf
    Response.Write "            </td>" & vbCrLf
    Response.Write "          </tr>" & vbCrLf
    Response.Write "          <tr class='tdbg'>" & vbCrLf
    Response.Write "            <td colspan='2' align='center'  height='40'>" & vbCrLf
    Response.Write "              <input type='hidden' name='GuestID'  value='" & GuestID & "'>"
    Response.Write "              <input name='Action' type='hidden' id='Action' value='SaveModify'>"
    Response.Write "              <input name='Save' type='submit' value='保存修改结果' style='cursor:hand;'>&nbsp;"
    Response.Write "              <input name='Cancel' type='button' id='Cancel' value=' 取 消 ' onClick=""window.location.href='Admin_GuestBook.asp';"" style='cursor:hand;'>"
    Response.Write "            </td>" & vbCrLf
    Response.Write "          </tr>" & vbCrLf
    Response.Write "        </form>" & vbCrLf
    Response.Write "      </table>" & vbCrLf
    Response.Write "    </td>" & vbCrLf
    Response.Write "  </tr>" & vbCrLf
    Response.Write "</table>" & vbCrLf
    
    rs.Close
    Set rs = Nothing
End Sub


Sub SaveModify()
	Dim GuestName, GuestSex, GuestTel, GuestOicq, GuestEmail
	Dim GuestTitle, GuestContent, GuestIsPassed
	
	GuestName = PE_HTMLEncode(GetForm("GuestName"))
	GuestSex = GetForm("GuestSex")
	GuestTel = PE_HTMLEncode(GetForm("GuestTel"))
	GuestOicq = PE_HTMLEncode(GetForm("GuestOicq"))
	GuestEmail = PE_HTMLEncode(GetForm("GuestEmail"))
    GuestTitle = PE_HTMLEncode(GetForm("GuestTitle"))
    GuestContent = PE_HTMLEncode(FilterJS(GetForm("GuestContent")))

    If GuestName = "" Or GuestTitle = "" Or GuestContent = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>留言保存失败！</li><li>请将必要的信息填写完整！</li>"
        Exit Sub
    End If
    If GuestID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请指定要编辑的留言ID！</li>"
        Exit Sub
    Else
        Set rsGuest = Server.CreateObject("adodb.recordset")
        sql = "select * from PW_GuestBook where GuestID=" & GuestID
        rsGuest.Open sql, Conn, 1, 3
        rsGuest("GuestName") = GuestName
        rsGuest("GuestSex") = GuestSex
        rsGuest("GuestTel") = GuestTel
        rsGuest("GuestOicq") = GuestOicq
        rsGuest("GuestEmail") = GuestEmail
        rsGuest("GuestTitle") = GuestTitle
        rsGuest("GuestContent") = GuestContent
        rsGuest.Update
    End If
    Call CloseConn
    Response.Redirect "Admin_GuestBook.asp"
End Sub

Sub AdminReply()
    Dim GuestReply
    If GuestID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请指定要回复的留言ID！</li>"
        Exit Sub
    Else
        GuestID = PE_CLng(GuestID)
    End If
    sql = "select * from PW_GuestBook where GuestID=" & GuestID
    Set rsGuest = Server.CreateObject("adodb.recordset")
    rsGuest.Open sql, Conn, 1, 1
    If rsGuest.BOF And rsGuest.EOF Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>找不到指定的留言！</li>"
        rsGuest.Close
        Set rsGuest = Nothing
        Exit Sub
    End If
    GuestReply = rsGuest("GuestReply")
    Response.Write "<br><table width='100%' border='0' align='center' cellpadding='0' cellspacing='0' class='border'>"
    Response.Write "  <tr class='title'>"
    Response.Write "    <td height='22' align='center'><b>留言内容</b></td>"
    Response.Write "  </tr>"
    Response.Write "  <tr align='center'>"
    Response.Write "    <td><table width='100%' border='0' cellpadding='2' cellspacing='0'>"
	Response.Write "          <tr class='tdbg'>" & vbCrLf
	Response.Write "            <td width='30%' align='right'>姓 &nbsp;名： </td>" & vbCrLf
	Response.Write "            <td width='70%'>" & rsGuest("GuestName") &"</td>"& vbCrLf
	Response.Write "          </tr>" & vbCrLf
	Response.Write "          <tr class='tdbg'>" & vbCrLf
	Response.Write "            <td align='right'>Oicq： </td>" & vbCrLf
	Response.Write "            <td>" & rsGuest("GuestTel") &"</td>" & vbCrLf
	Response.Write "          </tr>" & vbCrLf
	Response.Write "          <tr class='tdbg'>" & vbCrLf
	Response.Write "            <td align='right'>QQ： </td>" & vbCrLf
	Response.Write "            <td>" & rsGuest("GuestOicq") &"</td>" & vbCrLf
	Response.Write "          </tr>" & vbCrLf
	Response.Write "          <tr class='tdbg'>" & vbCrLf
	Response.Write "            <td align='right'>Email： </td>" & vbCrLf
	Response.Write "            <td>" & rsGuest("GuestEmail") &"</td>" & vbCrLf
	Response.Write "          </tr>" & vbCrLf
    Response.Write "          <tr class='tdbg'>" & vbCrLf
    Response.Write "            <td align='right'>留言主题： </td>" & vbCrLf
	Response.Write "            <td>" & rsGuest("GuestTitle") &"</td>" & vbCrLf
    Response.Write "          </tr>" & vbCrLf
    Response.Write "          <tr class='tdbg'>" & vbCrLf
    Response.Write "            <td valign='middle' align='right'>留言内容：</td>" & vbCrLf
 	Response.Write "            <td>" & rsGuest("GuestContent") &"</td>" & vbCrLf
    Response.Write "          </tr>" & vbCrLf
    Response.Write "      </table>" & vbCrLf
    Response.Write "    </td>" & vbCrLf
    Response.Write "  </tr>" & vbCrLf
    Response.Write "</table>" & vbCrLf
    Response.Write "<br><br><table width='100%' border='0' align='center' cellpadding='0' cellspacing='0' class='border'>"
    Response.Write "  <tr class='title'>"
    Response.Write "    <td height='22' align='center'><b>回复留言</b></td>"
    Response.Write "  </tr>"
    Response.Write "  <tr align='center'>"
    Response.Write "    <td><table width='100%' border='0' cellpadding='2' cellspacing='0'>"
    Response.Write "        <form name='myform' method='post' action='Admin_GuestBook.asp' onSubmit='return CheckForm()'>" & vbCrLf
    Response.Write "          <tr class='tdbg'>" & vbCrLf
    Response.Write "            <td valign='middle' align='right'>回复内容：</td>" & vbCrLf
    Response.Write "            <td valign='top'>" & vbCrLf
    Response.Write "              <textarea name='GuestReply' id='GuestReply' cols='60' rows='6' >" & PE_HtmlDecode(rsGuest("GuestReply")) & "</textarea>" & vbCrLf
    Response.Write "            </td>" & vbCrLf
    Response.Write "          </tr>" & vbCrLf
    Response.Write "          <tr class='tdbg'>" & vbCrLf
    Response.Write "            <td colspan='2' align='center'  height='40'>" & vbCrLf
    Response.Write "              <input type='hidden' name='GuestID'  value='" & GuestID & "'>"
    Response.Write "              <input name='Action' type='hidden' id='Action' value='SaveAdminReply'>"
    Response.Write "              <input name='Save' type='submit' value='确定回复' style='cursor:hand;'>&nbsp;"
    Response.Write "              <input name='Cancel' type='button' id='Cancel' value=' 取 消 ' onClick=""window.location.href='Admin_GuestBook.asp';"" style='cursor:hand;'>"
    Response.Write "            </td>" & vbCrLf
    Response.Write "          </tr>" & vbCrLf
    Response.Write "        </form>" & vbCrLf
    Response.Write "      </table>" & vbCrLf
    Response.Write "    </td>" & vbCrLf
    Response.Write "  </tr>" & vbCrLf
    Response.Write "</table>" & vbCrLf
	rsGuest.close
	set rsGuest=Nothing
End Sub


Sub SaveAdminReply()
    If GuestID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>不能确定留言ID</li>"
        Exit Sub
    Else
        GuestID = PE_CLng(GuestID)
    End If
	Dim GuestReply
    GuestReply = PE_HTMLEncode(FilterJS(GetForm("GuestReply")))
    Set rs = Server.CreateObject("adodb.recordset")
    sql = "select * from PW_GuestBook where GuestID=" & GuestID
    rs.Open sql, Conn, 1, 3
    If rs.BOF And rs.EOF Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>找不到指定的留言！</li>"
        rs.Close
        Set rs = Nothing
        Exit Sub
    End If
    rs("GuestReply") = GuestReply
    rs("GuestReplyDatetime") = Now()
    rs.Update
    rs.Close
    Set rs = Nothing
    Call CloseConn
    Response.Redirect "Admin_GuestBook.asp"
End Sub




'=================================================
'函数名：TransformTime()
'作  用：格式化时间
'参  数：时间
'=================================================
Function TransformTime(GuestDatetime)
    If Not IsDate(GuestDatetime) Then Exit Function
    Dim thour, tminute, tday, nowday, dnt, dayshow, pshow
    thour = Hour(GuestDatetime)
    tminute = Minute(GuestDatetime)
    tday = DateValue(GuestDatetime)
    nowday = DateValue(Now)
    If thour < 10 Then
        thour = "0" & thour
    End If
    If tminute < 10 Then
        tminute = "0" & tminute
    End If
    dnt = DateDiff("d", tday, nowday)
    If dnt > 2 Then
       dayshow = Year(GuestDatetime)
       If (Month(GuestDatetime) < 10) Then
           dayshow = dayshow & "-0" & Month(GuestDatetime)
       Else
           dayshow = dayshow & "-" & Month(GuestDatetime)
       End If
       If (Day(GuestDatetime) < 10) Then
           dayshow = dayshow & "-0" & Day(GuestDatetime)
       Else
           dayshow = dayshow & "-" & Day(GuestDatetime)
       End If
       TransformTime = dayshow
       Exit Function
    ElseIf dnt = 0 Then
       dayshow = "今天 "
    ElseIf dnt = 1 Then
       dayshow = "昨天 "
    ElseIf dnt = 2 Then
       dayshow = "前天 "
    End If
    TransformTime = dayshow & pshow & thour & ":" & tminute
End Function

%>