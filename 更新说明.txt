2016-06-08

    1、修改【classlist(channelID,classID,sytleName,)】
        增加{$stylename}可以设置当前栏目样式
        ./include/PW_Common_Front.asp

2015-10-15
    1、增加通用标签，{$NetAddress} 网址 
           /Sys_Start.asp 
          ../include/PW_Common_Front.asp

2015-0-22
    1、修改 include/PW_product.asp 48行开始，主要是前台通过分类来调用 

    If InStr(arrClassID, ",") >= 0 Then 
        strSql = strSql & " and P.ClassID in (" & FilterArrNull(arrClassID, ",") & ") " 
    Else 


2015-03-17
    1、 include/PW_article.asp中增加调用标签 外链接“{$linkurl}” 

2014-12-17
    1、增加产品/文章中标签{$defaultpicurl} 

2014-11-14
    1、修改自定义字段里增加“文本编辑器”的选项时字数不够，修改字段为ＴＥＸＴ类型
    2、修改{$showclass}和【classs(ichnnle,icalss)】排序不一样的问题
           关联文件：include/PW_Common_Front.asp 

2014-11-12 
    2、在“产品”自定义字段里增加“文本编辑器”的选项
    Sys_manage/Admin_product.asp 
     


2014-07-11 
    1、增加通用标签，{$NetAddress} 网址 

2014-07-10 
    1、在文章内容调用页面增加时间标签调用，{$year}、{$month}、{$day} 
    2、在自定义字段里增加“文本编辑器”的选项 
        include/PW_product.asp 
        include/PW_article.asp 
        include/PW_Common_Manage.asp 
        sys_mananage/Admin_Feild.asp
    3、关于添加自定义字段报错,
    “
            Microsoft VBScript 运行时错误 错误 '800a0009'
            下标越界: '[number: 2]'
    ”
    解决办法： 
    include/PW_photo.php 
Dim rsField, ArrField, iField 
    Set rsField = Conn.Execute("select FieldName,LabelName ,FieldType  from PW_Field where ChannelID=" & iChannelID & "") 
    If Not (rsField.BOF And rsField.EOF) Then 
        ArrField = rsField.getrows(-1) 
    End If 
    Set rsField = Nothing 



2014-07-03 
    1、修改显示招聘详细plus/job.asp，标签调用出错问题，调用模板显示未转换成汉字 

2014-06-09 
    1、修改了首页中调用产品列表信息无法使用子分类显示信息， 
    2、修改include/PW_product.asp页面中函数，增加频道参数 
         原:    GetSqlStr(arrClassID, IncludeChild, IsHot, IsElite, OrderType, IsPicUrl)
         现:    GetSqlStr(ChannelID,arrClassID, IncludeChild, IsHot, IsElite, OrderType, IsPicUrl)