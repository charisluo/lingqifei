<%
Dim ProductID, ProductName, ProductUrl
Dim rsProduct

Class Product

Private strPrice, strPrice_Member, strPrice_Original ''原始零售价Price_Original 当前零售价 Price,会员零售价 Price_Member

'定义其它全局的变量
Private rsClass, NoPrice, NoPrice_Member, NoPrice_Original

Public Sub Init()
    FoundErr = False
    ErrMsg = ""
	PrevChannelID = ChannelID

    NoPrice = "0"
    NoPrice_Member = "—"
    NoPrice_Original = "—"
    strPrice = "现价："
    strPrice_Member = "会员价："
    strPrice_Original = "原&nbsp;&nbsp;价："
    '*****************************
	Call GetChannel(ChannelID)
	strNavPath = strNavPath & "&nbsp;" & strNavLink & "&nbsp;<a href='"
	If UseCreateHTML > 0 Then
		strNavPath = strNavPath & ChannelUrl & "/Index.html"
	Else
		strNavPath = strNavPath & ChannelUrl_ASPFile & "/Index.asp"
	End If
	strNavPath = strNavPath & "'>" & ChannelName & "</a>"
	strPageTitle = ChannelName&"_"&SiteName
End Sub

Private Function GetSqlStr(arrClassID, IncludeChild, IsHot, IsElite, OrderType, IsPicUrl)
    Dim strSql, IDOrder
    If IsValidID(arrClassID) = False Then
        arrClassID = 0
    Else
        arrClassID = ReplaceLabelBadChar(arrClassID)
    End If	
	strSql = " from PW_Product P left join PW_Class C on P.ClassID=C.ClassID"
    strSql = strSql & " where P.Deleted=" & PE_False & ""

    If InStr(arrClassID, ",") > 0 Then
        strSql = strSql & " and P.ClassID in (" & FilterArrNull(arrClassID, ",") & ") "
    Else
        arrClassID = PE_CLng(arrClassID)
        If arrClassID > 0 Then
            If IncludeChild = True Then
                Dim trs
                Set trs = Conn.Execute("select arrChildID from PW_Class where ChannelID=" & ChannelID & " and ClassID=" & arrClassID & "")
                If trs.BOF And trs.EOF Then
                    arrClassID = 0
                Else
                    If IsNull(trs(0)) Or Trim(trs(0)) = "" Then
                        arrClassID = 0
                    Else
                        arrClassID = trs(0)
                    End If
                End If
                Set trs = Nothing
            End If
            If InStr(arrClassID, ",") > 0 Then
                strSql = strSql & " and P.ClassID in (" & arrClassID & ") "
            Else
                If PE_CLng(arrClassID) > 0 Then strSql = strSql & " and P.ClassID=" & PE_CLng(arrClassID)
            End If
        End If
    End If
    If IsHot = True Then
        strSql = strSql & " and P.IsHot=" & PE_True & ""
    End If
    If IsElite = True Then
        strSql = strSql & " And P.IsElite=" & PE_True & ""
    End If

    If IsPicUrl = True Then
        strSql = strSql & " and P.ProductThumb<>'' "
    End If
	if  Keyword<>"" then
		strSql = strSql & " and P.ProductName like'%"& Keyword &"%' "
	End if
    strSql = strSql & " order by P.OnTop " & PE_OrderType & ","
    Select Case PE_CLng(OrderType)
    Case 1, 2

    Case 3
        strSql = strSql & "P.UpdateTime desc,"
    Case 4
        strSql = strSql & "P.UpdateTime asc,"
    Case 5
        strSql = strSql & "P.Hits desc,"
    Case 6
        strSql = strSql & "P.Hits asc,"
    Case Else

    End Select
    If OrderType = 2 Then
        IDOrder = "asc"
    Else
        IDOrder = "desc"
    End If
    strSql = strSql & "P.ProductID " & IDOrder
    GetSqlStr = strSql
End Function


Public Function GetCustomFromTemplate(strValue)   '得到自定义列表的版面设计的HTML代码
    Dim strCustom, strParameter
    strCustom = strValue
    regEx.Pattern = "【productlist\((.*?)\)】([\s\S]*?)【\/productlist】"
    Set Matches = regEx.Execute(strCustom)
    For Each Match In Matches
        strParameter = Replace(Match.SubMatches(0), Chr(34), " ")
        strCustom = PE_Replace(strCustom, Match.Value, GetCustomFromLabel(strParameter, Match.SubMatches(1)))
    Next
    GetCustomFromTemplate = strCustom
End Function


'【ProductList(ChannelID,arrClassID,IncludeChild,ProductNum,IsHot,IsElite,OrderType,UsePage,TitleLen,ContentLen,IsPicUrl)】【/ProductList】
Private Function GetCustomFromLabel(strTemp, strList)
    Dim arrTemp
    Dim strProductThumb, arrPicTemp
    Dim iChannelID,arrClassID, IncludeChild, ProductNum, IsHot, IsElite, OrderType, UsePage, TitleLen, ContentLen
    Dim iCols, iColsHtml, iRows, iRowsHtml, iNumber
    Dim IncludePic    
    If strTemp = "" Or strList = "" Then GetCustomFromLabel = "": Exit Function

    iCols = 1: iRows = 1: iColsHtml = "": iRowsHtml = ""
    regEx.Pattern = "【(cols|rows)=(\d{1,2})\s*(?:\||｜)(.+?)】"
    Set Matches = regEx.Execute(strList)
    For Each Match In Matches
        If LCase(Match.SubMatches(0)) = "cols" Then
            If Match.SubMatches(1) > 1 Then iCols = Match.SubMatches(1)
            iColsHtml = Match.SubMatches(2)
        ElseIf LCase(Match.SubMatches(0)) = "rows" Then
            If Match.SubMatches(1) > 1 Then iRows = Match.SubMatches(1)
            iRowsHtml = Match.SubMatches(2)
        End If
        strList = regEx.Replace(strList, "")
    Next
    
    arrTemp = Split(strTemp, ",")
    If UBound(arrTemp) <> 10 and UBound(arrTemp) <> 9  Then
        GetCustomFromLabel = "自定义列表标签：【productlist(参数列表)】列表内容【/productlist】的参数个数不对。请检查模板中的此标签。"
        Exit Function
    End If
	
    Select Case Trim(arrTemp(0))
    Case "channelid"
        iChannelID = ChannelID
    Case Else
        iChannelID = PE_CLng(arrTemp(0))
    End Select
	
    Select Case Trim(arrTemp(1))
    Case "rsclass_arrchildid"
        If IsObject(rsClass) Then
            arrClassID = rsClass("arrChildID")
        Else
            arrClassID = arrChildID
        End If
    Case "arrchildid"
        arrClassID = arrChildID
    Case "classid"
        arrClassID = ClassID
    Case Else
        arrClassID = arrTemp(1)
    End Select
    arrClassID = Replace(Trim(arrClassID), "|", ",")

    IncludeChild = PE_CBool(arrTemp(2))
    
	ProductNum = PE_CLng(arrTemp(3))
    IsHot = PE_CBool(arrTemp(4))
    IsElite = PE_CBool(arrTemp(5))
	
	OrderType = PE_CLng(arrTemp(6))
    UsePage = PE_CBool(arrTemp(7))
    TitleLen = PE_CLng(arrTemp(8))
    ContentLen = PE_CLng(arrTemp(9))
    If UBound(arrTemp) = 10  then
        IncludePic = PE_CBool(arrTemp(10))
    Else
        IncludePic = False	    
    End If        
    If iChannelID <> PrevChannelID Or ChannelID = 0 Then
        Call GetChannel(iChannelID)
		PrevChannelID = iChannelID
    End If
	Dim rsField, ArrField, iField
    Set rsField = Conn.Execute("select FieldName,LabelName,FieldType from PW_Field where ChannelID=" & iChannelID & "")
    If Not (rsField.BOF And rsField.EOF) Then
        ArrField = rsField.getrows(-1)
    End If
    Set rsField = Nothing

    Dim sqlCustom, rsCustom, iCount, strCustomList
    iCount = 0
    sqlCustom = ""
    strCustomList = ""
    
    If ProductNum > 0 Then
        sqlCustom = "select top " & ProductNum & " "
    Else
        sqlCustom = "select "
    End If
    If ContentLen > 0 Then
        sqlCustom = sqlCustom & "P.ProductExplain,"
    End If
    If IsArray(ArrField) Then
        For iField = 0 To UBound(ArrField, 2)
            sqlCustom = sqlCustom & "P." & ArrField(0, iField) & ","
        Next
    End If
    sqlCustom = sqlCustom & "P.ProductID,P.ClassID,P.ProductName,P.UpdateTime,P.ProductThumb,P.ProductIntro,P.Hits,P.InfoPurview"
    sqlCustom = sqlCustom & ",P.IsHot,P.IsElite,P.OnTop,P.ProductModel,P.ProductStandard,P.ProducerName,P.TrademarkName"
    sqlCustom = sqlCustom & ",P.Unit,P.Price,Price_Original,P.Price_Member,C.ClassName"
    sqlCustom = sqlCustom & GetSqlStr(arrClassID, IncludeChild, IsHot, IsElite, OrderType, IncludePic)
    Set rsCustom = Server.CreateObject("ADODB.Recordset")
    rsCustom.Open sqlCustom, Conn, 1, 1
    If rsCustom.BOF And rsCustom.EOF Then
        totalPut = 0
		 strCustomList = GetInfoList_StrNoItem(arrClassID, IsHot, IsElite)
        rsCustom.Close
        Set rsCustom = Nothing
        GetCustomFromLabel = strCustomList
        Exit Function
    End If

    If UsePage = True Then
        totalPut = rsCustom.RecordCount
        If CurrentPage < 1 Then
            CurrentPage = 1
        End If
        If (CurrentPage - 1) * MaxPerPage > totalPut Then
            If (totalPut Mod MaxPerPage) = 0 Then
                CurrentPage = totalPut \ MaxPerPage
            Else
                CurrentPage = totalPut \ MaxPerPage + 1
            End If
        End If
        If CurrentPage > 1 Then
            If (CurrentPage - 1) * MaxPerPage < totalPut Then
                iMod = 0
                rsCustom.Move (CurrentPage - 1) * MaxPerPage - iMod
            Else
                CurrentPage = 1
            End If
        End If
    End If
    Do While Not rsCustom.EOF
        strTemp = strList

        If UsePage = True Then
            iNumber = (CurrentPage - 1) * MaxPerPage + iCount + 1
        Else
            iNumber = iCount + 1
        End If

        strTemp = PE_Replace(strTemp, "{$autoid}", iNumber)
        strTemp = PE_Replace(strTemp, "{$classid}", rsCustom("ClassID"))
        strTemp = PE_Replace(strTemp, "{$classname}", rsCustom("ClassName"))
        strTemp = PE_Replace(strTemp, "{$classurl}", GetClassUrl(rsCustom("ClassID")))

        strTemp = PE_Replace(strTemp, "{$productid}", rsCustom("ProductID"))
		strTemp = PE_Replace(strTemp, "{$producturl}", GetProductUrl(rsCustom("UpdateTime"), rsCustom("ProductID"),rsCustom("InfoPurview")))
        strTemp = PE_Replace(strTemp, "{$updatedate}", FormatDateTime(rsCustom("UpdateTime"), 2))
        strTemp = PE_Replace(strTemp, "{$updatetime}", rsCustom("updatetime"))
        strTemp = PE_Replace(strTemp, "{$producername}", rsCustom("ProducerName"))
        strTemp = PE_Replace(strTemp, "{$trademarkname}", rsCustom("TrademarkName"))
        strTemp = PE_Replace(strTemp, "{$productmodel}", rsCustom("ProductModel"))
        strTemp = PE_Replace(strTemp, "{$productstandard}", rsCustom("ProductStandard"))
        strTemp = PE_Replace(strTemp, "{$hits}", rsCustom("Hits"))
        strTemp = PE_Replace(strTemp, "{$unit}", rsCustom("Unit"))

		if InStr(strTemp, "{$property}") >0 then
			Dim PropertyStr
			PropertyStr = ""
			if rsCustom("OnTop") = True then
				PropertyStr = PropertyStr&"<font color=""green""> 顶</font>"
			end if
			if rsCustom("Elite") = True then
				PropertyStr = PropertyStr&"<font color=""blue""> 荐</font>"
			end if
			if rsCustom("Hits") > HitsOfHot then
				PropertyStr = PropertyStr&"<font color=""red""> 荐</font>"
			end if
			strTemp = PE_Replace(strTemp, "{$property}", PropertyStr)	
		end if
        
        If TitleLen > 0 Then
            strTemp = PE_Replace(strTemp, "{$productname}", GetSubStr(rsCustom("ProductName"), TitleLen, false))
        Else
            strTemp = PE_Replace(strTemp, "{$productname}", rsCustom("ProductName"))
        End If
        strTemp = PE_Replace(strTemp, "{$productintro}", rsCustom("ProductIntro"))
        If ContentLen > 0 Then
            If InStr(strTemp, "{$productexplain}") > 0 Then strTemp = PE_Replace(strTemp, "{$productexplain}", Left(nohtml(rsCustom("ProductExplain")), ContentLen))
        Elseif ContentLen=-1 then
            strTemp = PE_Replace(strTemp, "{$productexplain}", GetContent(rsCustom("ProductExplain")))
        Else
            strTemp = PE_Replace(strTemp, "{$productexplain}", "")
        End If

        '替换首页图片
        regEx.Pattern = "\{\$productthumb\((.*?)\)\}"
        Set Matches = regEx.Execute(strTemp)
        For Each Match In Matches
            arrPicTemp = Split(Match.SubMatches(0), ",")
            strProductThumb = GetProductThumb(Trim(rsCustom("ProductThumb")), PE_CLng(arrPicTemp(0)), PE_CLng(arrPicTemp(1)))
            strTemp = Replace(strTemp, Match.Value, strProductThumb)
        Next

        If InStr(strTemp, "{$price}") > 0 Then strTemp = PE_Replace(strTemp, "{$price}", GetPrice_NoSymbol(rsCustom("Price")))
        If InStr(strTemp, "{$price_original}") > 0 Then strTemp = PE_Replace(strTemp, "{$price_original}", GetPrice_Original_NoSymbol(rsCustom("Price_Original")))
        If InStr(strTemp, "{$price_member}") > 0 Then strTemp = PE_Replace(strTemp, "{$price_member}", GetPrice_Member_NoSymbol(rsCustom("Price_Member")))
		
        If IsArray(ArrField) Then
            For iField = 0 To UBound(ArrField, 2)
                Select Case ArrField(2, iField)
				'新增加10为编辑器选项
                Case 8,9,10
                    strTemp = PE_Replace(strTemp, ArrField(1, iField), PE_HTMLDecode(rsCustom(Trim(ArrField(0, iField)))))
                Case 4
                    If PE_HTMLDecode(rsCustom(Trim(ArrField(0, iField))))="" or IsNull(PE_HTMLDecode(rsCustom(Trim(ArrField(0, iField))))) or PE_HTMLDecode(rsCustom(Trim(ArrField(0, iField))))="http://" Then
                        strTemp = PE_Replace(strTemp, ArrField(1, iField), "")	
                    Else 
                        strTemp = PE_Replace(strTemp, ArrField(1, iField), "<img  class='fieldImg' src='" &PE_HTMLDecode(rsCustom(Trim(ArrField(0, iField))))&"' border=0>")	
                    End If
                Case Else
                    strTemp = PE_Replace(strTemp, ArrField(1, iField), PE_HTMLEncode(rsCustom(Trim(ArrField(0, iField)))))				
                End Select 
           Next
        End If

        strCustomList = strCustomList & strTemp
        rsCustom.MoveNext
        iCount = iCount + 1
        If iCols > 1 And iCount Mod iCols = 0 Then strCustomList = strCustomList & iColsHtml
        If iRows > 1 And iCount Mod iCols * iRows = 0 Then strCustomList = strCustomList & iRowsHtml
        If UsePage = True And iCount >= MaxPerPage Then Exit Do
    Loop
    rsCustom.Close
    Set rsCustom = Nothing
    
    GetCustomFromLabel = strCustomList
End Function



'【relateproduct(参数列表)】循环内容【/relateproduct】
Public Function GetCorrelativeFromTemplate(strValue)  
    Dim strCorrelative, strParameter
    strCorrelative = strValue
    regEx.Pattern = "【relateproduct\((.*?)\)】([\s\S]*?)【\/relateproduct】"
    Set Matches = regEx.Execute(strCorrelative)
    For Each Match In Matches
        strParameter = Replace(Match.SubMatches(0), Chr(34), " ")
        strCorrelative = PE_Replace(strCorrelative, Match.Value, GetCorrelative(strParameter, Match.SubMatches(1)))
    Next
    GetCorrelativeFromTemplate = strCorrelative
End Function

'=================================================
'函数名：GetCorrelative
'作  用：显示相关图片 之中参数(iChannelID,arrClassID, ProductNum, TitleLen, OrderType)
'=================================================
private Function GetCorrelative(strTemp, strList)
    Dim arrTemp
	Dim iChannelID,arrClassID,ProductNum,TitleLen,OrderType
    Dim rsCorrelative, sqlCorrelative, strCorrelative, iCols, iTemp
    Dim strKey, arrKey, i
	strCorrelative=""
    arrTemp = Split(strTemp, ",")
    If UBound(arrTemp) <> 4 Then
        GetCorrelative = "相关产品标签：【relateproduct(参数列表)】列表内容【/relateproduct】的参数个数不对。请检查模板中的此标签。"
        Exit Function
    End If
    iChannelID = Replace(iChannelID,"|",",")
    Select Case iChannelID
    Case "channelid"
        iChannelID = ChannelID
    Case else
        If IsValidID(iChannelID) = False Then
            iChannelID = 0
        End If  
    End Select	
    Select Case Trim(arrTemp(1))
    Case "arrchildid"
        arrClassID = arrChildID
    Case "classid"
        arrClassID = ClassID
    Case Else
        arrClassID = arrTemp(1)
    End Select
    arrClassID = Replace(arrClassID,"|",",")	
	ProductNum = PE_CLng(arrTemp(2))
    TitleLen = PE_CLng(arrTemp(3))
	OrderType = PE_CLng(arrTemp(4))
    If IsValidID(arrClassID) = False Then
        arrClassID = 0
    End If  
    If ProductNum > 0 And ProductNum <= 20 Then
        sqlCorrelative = "select top " & ProductNum
    Else
        sqlCorrelative = "Select Top 5 "
    End If
    strKey = Mid(rsProduct("Keyword"), 2, Len(rsProduct("Keyword")) - 2)
    If InStr(strKey, "|") > 1 Then
        arrKey = Split(strKey, "|")
        MaxNum = UBound(arrKey)   
        strKey = "((P.Keyword like '%|" & Replace(Replace(arrKey(0), "［", ""), "］", "") & "|%')"
        For i = 1 To MaxNum
            strKey = strKey & " or (P.Keyword like '%|" & Replace(Replace(arrKey(i), "［", ""), "］", "") & "|%')"
        Next
        strKey = strKey & ")"
    Else
        strKey = "(P.Keyword like '%|" & strKey & "|%')"
    End If
    sqlCorrelative = sqlCorrelative & " P.ProductID,P.ClassID,P.ProductName,P.UpdateTime,P.Hits,P.InfoPurview,C.ClassName from PW_Product P left join PW_Class C on P.ClassID=C.ClassID where 1=1"
    If InStr(iChannelID, ",") > 0 Then
        sqlCorrelative = sqlCorrelative & " and P.ChannelID in (" & FilterArrNull(iChannelID, ",") & ")"
    Else
        If PE_CLng(iChannelID) > 0 Then sqlCorrelative = sqlCorrelative & " and P.ChannelID=" & PE_CLng(iChannelID)
    End If	
    If arrClassID <> "0" Then
        If InStr(arrClassID, ",") > 0 Then
            sqlCorrelative = sqlCorrelative & " and P.ClassID in (" & FilterArrNull(arrClassID, ",") & ")"
        Else
            If PE_CLng(arrClassID) > 0 Then sqlCorrelative = sqlCorrelative & " and P.ClassID=" & PE_CLng(arrClassID)
        End If
    End If
    sqlCorrelative = sqlCorrelative & " and P.Deleted=" & PE_False & ""
    sqlCorrelative = sqlCorrelative & " and " & strKey & " and P.ProductID<>" & ProductID & " Order by "
    Select Case PE_CLng(OrderType)
    Case 1
        sqlCorrelative = sqlCorrelative & "P.ProductID desc"
    Case 2
        sqlCorrelative = sqlCorrelative & "P.ProductID asc"
    Case 3
        sqlCorrelative = sqlCorrelative & "P.UpdateTime desc"
    Case 4
        sqlCorrelative = sqlCorrelative & "P.UpdateTime asc"
    Case Else
        sqlCorrelative = sqlCorrelative & "P.ProductID desc"
    End Select
    Set rsCorrelative = Conn.Execute(sqlCorrelative)
    If TitleLen < 0 Or TitleLen > 255 Then TitleLen = 50
    If rsCorrelative.BOF And rsCorrelative.EOF Then
        strCorrelative = "没有相关信息！"
    Else
        Do While Not rsCorrelative.EOF
			strTemp = strList
			strTemp = PE_Replace(strTemp, "{$classid}", rsCorrelative("ClassID"))
			strTemp = PE_Replace(strTemp, "{$classname}", rsCorrelative("ClassName"))
			strTemp = PE_Replace(strTemp, "{$classurl}", GetClassUrl(rsCorrelative("ClassID")))
			If TitleLen > 0 Then
				strTemp = PE_Replace(strTemp, "{$productname}", GetSubStr(rsCorrelative("ProductName"), TitleLen, false))
			Else
				strTemp = PE_Replace(strTemp, "{$productname}", "")
			End If
			strTemp = PE_Replace(strTemp, "{$productnameall}", rsCorrelative("ProductName"))
			strTemp = PE_Replace(strTemp, "{$producturl}", GetProductUrl(rsCorrelative("UpdateTime"), rsCorrelative("ProductID"),rsCorrelative("InfoPurview")))
			strTemp = PE_Replace(strTemp, "{$updatetime}", rsCorrelative("UpdateTime"))
        strCorrelative = strCorrelative & strTemp
        rsCorrelative.MoveNext
    Loop
	End if
    rsCorrelative.Close
    Set rsCorrelative = Nothing
    
    GetCorrelative = strCorrelative
End function


'=================================================
'函数名：GetPrevProduct
'作  用：显示上一个产品
'参  数：TitleLen   ----标题最多字符数，一个汉字=两个英文字符
'=================================================
Private Function GetPrevProduct(TitleLen)
    Dim rsPrev, sqlPrev, strPrev
    strPrev = Replace("<li>下一{$ItemUnit}： ", "{$ItemUnit}", ChannelItemUnit & ChannelShortName)
    sqlPrev = "Select Top 1 ProductID,ProductName,UpdateTime,InfoPurview from PW_Product Where ChannelID=" & ChannelID & " and Deleted=" & PE_False & " and ClassID=" & rsProduct("ClassID") & " and ProductID<" & rsProduct("ProductID") & " order by ProductID DESC"
    Set rsPrev = Conn.Execute(sqlPrev)
    If TitleLen < 0 Or TitleLen > 255 Then TitleLen = 50
    If rsPrev.EOF Then
        strPrev = strPrev & "没有了"
    Else
        strPrev = strPrev & "<a href='" & GetProductUrl(rsPrev("UpdateTime"), rsPrev("ProductID"),rsPrev("InfoPurview")) &"'"
        strPrev = strPrev & " title='" & rsPrev("ProductName") &"'>" & GetSubStr(rsPrev("ProductName"), TitleLen, True) & "</a>"
    End If
    rsPrev.Close
    Set rsPrev = Nothing
    strPrev = strPrev & "</li>"
    GetPrevProduct = strPrev
End Function


'=================================================
'函数名：GetNextProduct
'作  用：显示下一个产品
'参  数：TitleLen   ----标题最多字符数，一个汉字=两个英文字符
'=================================================
Private Function GetNextProduct(TitleLen)
    Dim rsNext, sqlNext, strNext
    strNext = Replace("<li>上一{$ItemUnit}： ", "{$ItemUnit}", ChannelItemUnit & ChannelShortName)
    sqlNext = "Select Top 1 ProductID,ProductName,UpdateTime,InfoPurview from PW_Product Where ChannelID=" & ChannelID & " and Deleted=" & PE_False & " and ClassID=" & rsProduct("ClassID") & " and ProductID>" & rsProduct("ProductID") & " order by ProductID ASC"
    Set rsNext = Conn.Execute(sqlNext)
    If TitleLen < 0 Or TitleLen > 255 Then TitleLen = 50
    If rsNext.EOF Then
        strNext = strNext & "没有了"
    Else
        strNext = strNext & "<a href='" & GetProductUrl(rsNext("UpdateTime"), rsNext("ProductID"),rsNext("InfoPurview")) &"'"
        strNext = strNext & " title='" & rsNext("ProductName") &"'>" & GetSubStr(rsNext("ProductName"), TitleLen, True) & "</a>"
    End If
    rsNext.Close
    Set rsNext = Nothing
    strNext = strNext & "</li>"
    GetNextProduct = strNext
End Function


Private Function GetPrice_NoSymbol(tPrice)
    If tPrice > 0 Then
        GetPrice_NoSymbol = "￥" & FormatNumber(tPrice, 2, vbTrue, vbFalse, vbFalse)
    Else
        GetPrice_NoSymbol = NoPrice
    End If
End Function

Private Function GetPrice_Member_NoSymbol(tPrice_Member)
    If tPrice_Member > 0 Then
        GetPrice_Member_NoSymbol = "￥" & FormatNumber(tPrice_Member, 2, vbTrue, vbFalse, vbFalse)
    Else
        GetPrice_Member_NoSymbol = NoPrice_Member
    End If
End Function

Private Function GetPrice_Original_NoSymbol(tPrice_Original)
    If tPrice_Original > 0 Then
        GetPrice_Original_NoSymbol = "￥" & FormatNumber(tPrice_Original, 2, vbTrue, vbFalse, vbFalse)
    Else
        GetPrice_Original_NoSymbol = NoPrice_Original
    End If
End Function

Private Function GetHits()
    If UseCreateHTML > 0 Then
        GetHits = "<script language='javascript' src='" & ChannelUrl_ASPFile & "/GetHits.asp?ProductID=" & ProductID & "'></script>"
    Else
        GetHits = rsProduct("Hits")
    End If
End Function

Private Function GetProductThumb(ProductThumb, iWidth, iHeight)
    Dim strProductThumb, FileType, strPicUrl
    
    If ProductThumb = "" Then
        strProductThumb = strProductThumb & "<img src=""" & strPicUrl & strInstallDir & "images/nopic.gif"" "
        If iWidth > 0 Then strProductThumb = strProductThumb & " width=""" & iWidth & """"
        If iHeight > 0 Then strProductThumb = strProductThumb & " height=""" & iHeight & """"
        strProductThumb = strProductThumb & " border=""0"">"
    Else
		strPicUrl = GetContent(ProductThumb)
        FileType = LCase(Mid(ProductThumb, InStrRev(ProductThumb, ".") + 1))
        If FileType = "swf" Then
            strProductThumb = strProductThumb & "<object classid=""clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"" codebase=""http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=5,0,0,0"" "
            If iWidth > 0 Then strProductThumb = strProductThumb & " width=""" & iWidth & """"
            If iHeight > 0 Then strProductThumb = strProductThumb & " height=""" & iHeight & """"""
            strProductThumb = strProductThumb & "><param name=""movie"" value=""" & strPicUrl & """><param name=""quality"" value=""high""><embed src=""" & strPicUrl & """ pluginspage=""http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash"" type=""application/x-shockwave-flash"" "
            If iWidth > 0 Then strProductThumb = strProductThumb & " width=""" & iWidth & """"
            If iHeight > 0 Then strProductThumb = strProductThumb & " height=""" & iHeight & """"
            strProductThumb = strProductThumb & "></embed></object>"
        ElseIf FileType = "gif" Or FileType = "jpg" Or FileType = "jpeg" Or FileType = "jpe" Or FileType = "bmp" Or FileType = "png" Then
            strProductThumb = strProductThumb & "<img class=""pic_pro"" src="""& strPicUrl & """ "
            If iWidth > 0 Then strProductThumb = strProductThumb & " width=""" & iWidth & """"
            If iHeight > 0 Then strProductThumb = strProductThumb & " height=""" & iHeight & """"
            strProductThumb = strProductThumb & " border=""0"">"
        Else
            strProductThumb = strProductThumb & "<img src=""" & strInstallDir & "images/nopic.gif"" "
            If iWidth > 0 Then strProductThumb = strProductThumb & " width=""" & iWidth & """"
            If iHeight > 0 Then strProductThumb = strProductThumb & " height=""" & iHeight & """"
            strProductThumb = strProductThumb & " border=""0"">"
        End If
    End If
    GetProductThumb = strProductThumb
End Function

Public Sub GetHtml_Index()
    Call GetChannel(ChannelID)
	strHtml = GetTemplate(tempindex)
    ClassID = 0
    Call ReplaceCommonLabel
    strHtml = PE_Replace(strHtml, "{$pagetitle}", strPageTitle)
    strHtml = PE_Replace(strHtml, "{$location}", strNavPath)
	strHtml = PE_Replace(strHtml, "{$classtitle}", ChannelName)
    strHtml = GetCustomFromTemplate(strHtml)
    If ChannelID <> PrevChannelID Then
        Call GetChannel(ChannelID)
        PrevChannelID = ChannelID
    End If
    If UseCreateHTML > 0 Then
        If InStr(strHtml, "{$showpage}") > 0 Then strHtml = Replace(strHtml, "{$showpage}", ShowPage_Html(ChannelUrl & "/", 0, ".html", strFileName, totalPut, MaxPerPage, CurrentPage, True, True, ChannelItemUnit & ChannelShortName))
        If InStr(strHtml, "{$showpage_en}") > 0 Then strHtml = Replace(strHtml, "{$showpage_en}", ShowPage_en_Html(ChannelUrl & "/", 0, ".html", strFileName, totalPut, MaxPerPage, CurrentPage, True, True, ChannelItemUnit & ChannelShortName))
    Else
        If InStr(strHtml, "{$showpage}") > 0 Then strHtml = Replace(strHtml, "{$showpage}", ShowPage(strFileName, totalPut, MaxPerPage, CurrentPage, True, True, ChannelItemUnit & ChannelShortName, False))
        If InStr(strHtml, "{$showpage_en}") > 0 Then strHtml = Replace(strHtml, "{$ShowPage_en}", ShowPage_en(strFileName, totalPut, MaxPerPage, CurrentPage, True, True, ChannelItemUnit & ChannelShortName, False))
    End If
End Sub

Public Sub GetHtml_Class()
	If list_template="" then
		list_template = templist
	End if
	strHtml = GetTemplate(list_template)
    strHtml = PE_Replace(strHtml, "{$classid}", ClassID)
    strHtml = PE_Replace(strHtml, "{$meta_keywords}", Meta_Keywords_Class)
    strHtml = PE_Replace(strHtml, "{$meta_description}", Meta_Description_Class)
    Call ReplaceCommonLabel
    strHtml = PE_Replace(strHtml, "{$classid}", ClassID)
    strHtml = PE_Replace(strHtml, "{$pagetitle}", strPageTitle)
    strHtml = PE_Replace(strHtml, "{$location}", strNavPath)
	strHtml = PE_Replace(strHtml, "{$classtitle}", ClassName)
    strHtml = GetCustomFromTemplate(strHtml)
    If ChannelID <> PrevChannelID Then
        Call GetChannel(ChannelID)
        PrevChannelID = ChannelID
    End If
    Dim strPath
    strPath = ChannelUrl & "/"

    strHtml = PE_Replace(strHtml, "{$classname}", ClassName)
    strHtml = PE_Replace(strHtml, "{$classpicurl}", ClassPicUrl)
    strHtml = PE_Replace(strHtml, "{$classurl}", GetClassUrl(ClassID))
	IF UseCreateHTML > 0 then
		If InStr(strHtml, "{$showpage}") > 0 Then strHtml = PE_Replace(strHtml, "{$showpage}", ShowPage_Html(strPath, ClassID, ".html", "", totalPut, MaxPerPage, CurrentPage, True, True, ChannelItemUnit & ChannelShortName))
        If InStr(strHtml, "{$showpage_en}") > 0 Then strHtml = PE_Replace(strHtml, "{$showpage_en}", ShowPage_en_Html(strPath, ClassID, ".html", "", totalPut, MaxPerPage, CurrentPage, True, True, ChannelItemUnit & ChannelShortName))
	else
		If InStr(strHtml, "{$showpage}") > 0 Then strHtml = PE_Replace(strHtml, "{$showpage}", ShowPage(strFileName, totalPut, MaxPerPage, CurrentPage, True, True, ChannelItemUnit & ChannelShortName, False))
        If InStr(strHtml, "{$showpage_en}") > 0 Then strHtml = PE_Replace(strHtml, "{$showpage_en}", ShowPage_en(strFileName, totalPut, MaxPerPage, CurrentPage, True, True, ChannelItemUnit & ChannelShortName, False))	
	end IF
End Sub

Public Sub GetHtml_Product()
    strHtml = GetCustomFromTemplate(strHtml)  '必须先解析自定义列表标签
    If PrevChannelID <> ChannelID Then
        Call GetChannel(ChannelID)
    End If
    strHtml = PE_Replace(strHtml, "{$productid}", ProductID)
    strHtml = PE_Replace(strHtml, "{$classid}", ClassID)
    strHtml = PE_Replace(strHtml, "{$pagetitle}", ProductName & "_" & SiteName)
    strHtml = PE_Replace(strHtml, "{$location}", strNavPath & "&nbsp;" & strNavLink & "&nbsp;详细内容")
    strHtml = PE_Replace(strHtml, "{$meta_keywords}", GetKeywords(",", rsProduct("Keyword")))
    strHtml = PE_Replace(strHtml, "{$meta_description}", rsProduct("productintro"))
   Call ReplaceCommonLabel   '解析通用标签，包含自定义标签
    strHtml = GetCustomFromTemplate(strHtml)  '必须先解析自定义列表标签
    If PrevChannelID <> ChannelID Then
        Call GetChannel(ChannelID)
    End If
    If InStr(strHtml, "{$MY_") > 0 Then
        Dim rsField
        Set rsField = Conn.Execute("select * from PW_Field where ChannelID=" & ChannelID & "")
        Do While Not rsField.EOF
            If rsField("FieldType") = 8 Or rsField("FieldType") = 9 Or rsField("FieldType") = 10 Then
                strHtml = PE_Replace(strHtml, rsField("LabelName"), PE_HTMLDecode(rsProduct(Trim(rsField("FieldName")))))
            Else
                strHtml = PE_Replace(strHtml, rsField("LabelName"), PE_HTMLEncode(rsProduct(Trim(rsField("FieldName")))))		
            End If	
            rsField.MoveNext
        Loop
        Set rsField = Nothing
    End If
    strHtml = PE_Replace(strHtml, "{$classid}", ClassID)
    strHtml = PE_Replace(strHtml, "{$classname}", ClassName)
	strHtml = PE_Replace(strHtml, "{$classurl}", GetClassUrl(ClassID))
	
    strHtml = PE_Replace(strHtml, "{$productname}", ProductName)
    strHtml = PE_Replace(strHtml, "{$productnum}", rsProduct("ProductNum"))
    strHtml = PE_Replace(strHtml, "{$productmodel}", rsProduct("ProductModel")) 
    strHtml = PE_Replace(strHtml, "{$productstandard}", rsProduct("ProductStandard"))
	strHtml = PE_Replace(strHtml, "{$producername}", rsProduct("ProducerName"))
	strHtml = PE_Replace(strHtml, "{$trademarkname}", rsProduct("TrademarkName"))
    strHtml = PE_Replace(strHtml, "{$unit}", rsProduct("Unit"))
	strHtml = PE_Replace(strHtml, "{$price_original}", GetPrice_Original_NoSymbol(rsProduct("Price_Original")))
    If rsProduct("price") > 0 Then
        strHtml = PE_Replace(strHtml, "{$price}", GetPrice_NoSymbol(rsProduct("price")))
    Else
        strHtml = PE_Replace(strHtml, "{$price}", GetPrice_Original_NoSymbol(rsProduct("Price_Original")))
    End If
    If rsProduct("Price_Member") > 0 Then
        strHtml = PE_Replace(strHtml, "{$price_member}", GetPrice_Member_NoSymbol(rsProduct("Price_Member")))
    Else
		If rsProduct("price") > 0 then
        	strHtml = PE_Replace(strHtml, "{$price_member}", GetPrice_NoSymbol(rsProduct("price")))
		else
        	strHtml = PE_Replace(strHtml, "{$price_member}", GetPrice_Original_NoSymbol(rsProduct("Price_Original")))
		end if
    End If
    strHtml = PE_Replace(strHtml, "{$hits}", GetHits())
	strHtml = PE_Replace(strHtml, "{$updatedate}", FormatDateTime(rsProduct("UpdateTime"), 2))
    strHtml = PE_Replace(strHtml, "{$updatetime}", rsProduct("UpdateTime"))
	
	If InStr(strHtml, "{$month}") > 0 Then strHtml = PE_Replace(strHtml, "{$month}", getMonth(rsProduct("UpdateTime")))	
	If InStr(strHtml, "{$day}") > 0 Then strHtml = PE_Replace(strHtml, "{$day}", getDay(rsProduct("UpdateTime")))	
	If InStr(strHtml, "{$year}") > 0 Then strHtml = PE_Replace(strHtml, "{$year}", getYear(rsProduct("UpdateTime")))
	
    strHtml = PE_Replace(strHtml, "{$productintro}", PE_HTMLEncode(rsProduct("ProductIntro")))
    strHtml = PE_Replace(strHtml, "{$productexplain}", ReplaceKeyLink(GetContent(rsProduct("ProductExplain"))))
	strHtml = PE_Replace(strHtml, "{$defaultpicurl}", rsProduct("ProductThumb"))
	
    '替换产品缩略图
	dim arrTemp,strProductThumb
    regEx.Pattern = "\{\$productthumb\((.*?)\)\}"
    Set Matches = regEx.Execute(strHtml)
    For Each Match In Matches
        arrTemp = Split(Match.SubMatches(0), ",")
        strProductThumb = GetProductThumb(Trim(rsProduct("ProductThumb")), PE_CLng(arrTemp(0)), PE_CLng(arrTemp(1)))
        strHtml = Replace(strHtml, Match.Value, strProductThumb)
    Next
	'''替换相关软件
	strHtml = GetCorrelativeFromTemplate(strHtml)
	
	'''替换上一条
	Dim strParameter, strTemp
	regEx.Pattern = "\{\$prevproduct\((.*?)\)\}"
	Set Matches = regEx.Execute(strHtml)
	For Each Match In Matches
		strParameter = PE_Clng(Match.SubMatches(0))
		if strParameter<=0 and strParameter>255 then
			strParameter = 50
		end if
		strTemp = GetPrevProduct(strParameter)
		strHtml = PE_Replace(strHtml, Match.Value, strTemp)
	Next
	'''替换下一条
	regEx.Pattern = "\{\$nextproduct\((.*?)\)\}"
	Set Matches = regEx.Execute(strHtml)
	For Each Match In Matches
		strParameter = PE_Clng(Match.SubMatches(0))
		if strParameter<=0 and strParameter>255 then
			strParameter = 50
		end if
		strTemp = GetNextProduct(strParameter)
		strHtml = PE_Replace(strHtml, Match.Value, strTemp)
	Next
End Sub
End Class


%>