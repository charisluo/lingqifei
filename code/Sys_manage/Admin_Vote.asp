<!--#include file="Admin_Common.asp"-->
<!--#include file="../Include/PW_FSO.asp"-->
<%
Const NeedCheckComeUrl = True   '是否需要检查外部访问
Const PurviewLevel_Channel = 0   '0--不检查
Const PurviewLevel_Others = "Vote"   '其他权限

strFileName = "Admin_Vote.asp?Action=" & Action & ""
Dim ItemName, ID
ItemName = "调查"
ID = GetValue("ID")
If IsValidID(ID) = False Then
    ID = ""
End If

Response.Write "<html><head><title>调查管理</title>" & vbCrLf
Response.Write "<meta http-equiv='Content-Type' content='text/html; charset=gb2312'>" & vbCrLf
Response.Write "<link href='Admin_Style.css' rel='stylesheet' type='text/css'>" & vbCrLf
Response.Write "</head>" & vbCrLf
Response.Write "<body leftmargin='2' topmargin='0' marginwidth='0' marginheight='0'>" & vbCrLf
Response.Write "<table width='100%' border='0' align='center' cellpadding='2' cellspacing='1' class='border'>" & vbCrLf
Call ShowPageTitle("网 站 调 查 管 理")
Response.Write "  <tr class='tdbg'>" & vbCrLf
Response.Write "    <td width='70' height='30'><strong>管理导航：</strong></td>" & vbCrLf
Response.Write "    <td><a href='Admin_Vote.asp'>调查管理首页</a>&nbsp;|&nbsp;<a href='Admin_Vote.asp?Action=Add'>添加新调查</a>"
Response.Write "    </td>" & vbCrLf
Response.Write "  </tr>" & vbCrLf
Response.Write "</table><br>" & vbCrLf
Select Case Action
Case "Add"
    Call Add
Case "Modify"
    Call Modify
Case "SaveAdd", "SaveModify"
    Call SaveVote
Case "SetNew", "CancelNew", "Del"
    Call SetProperty
Case Else
    Call main
End Select
If FoundErr = True Then
    Call WriteErrMsg(ErrMsg, ComeUrl)
End If
Response.Write "</body></html>"
Call CloseConn



Sub main()
    Dim rs, sql
    Call ShowJS_Main(ItemName)
    Response.Write "<table width='100%' border='0' align='center' cellpadding='0' cellspacing='0'>"& vbCrLf
    Response.Write "    <td height='22'>"& vbCrLf
    Call ShowManagePath("")
    Response.Write "  </td></tr>"& vbCrLf
    Response.Write "</table>"& vbCrLf
    Response.Write "<table width='100%' border='0' cellpadding='0' cellspacing='0'><tr>"& vbCrLf
    Response.Write "  <form name='myform' method='Post' action='Admin_Vote.asp' onsubmit='return ConfirmDel();'>"& vbCrLf
    Response.Write "  <td><table class='border' border='0' cellspacing='1' width='100%' cellpadding='0'>"& vbCrLf
    Response.Write "  <tr class='title'>"& vbCrLf
    Response.Write "    <td width='30' height='22' align='center'><strong>选中</strong></td>"& vbCrLf
    Response.Write "    <td width='30' height='22' align='center'><strong>ID</strong></td>"& vbCrLf
    Response.Write "    <td height='22' align='center'><strong>主题</strong></td>"& vbCrLf
    Response.Write "    <td width='60' height='22' align='center'><strong>调查状态</strong></td>"& vbCrLf
    Response.Write "    <td width='60' height='22' align='center'><strong>调查类型</strong></td>"& vbCrLf
    Response.Write "    <td width='120' height='22' align='center'><strong>发布时间</strong></td>"& vbCrLf
    Response.Write "    <td width='80' height='22' align='center'><strong>操作</strong></td>"& vbCrLf
    Response.Write "  </tr>"& vbCrLf

    sql = "select * from PW_Vote order by IsSelected,ID desc"
    Set rs = Conn.Execute(sql)
    If rs.BOF And rs.EOF Then
        Response.Write "<tr class='tdbg'><td colspan='20' align='center'><br>没有任何调查！<br><br></td></tr>"& vbCrLf
    Else
        Do While Not rs.EOF
            Response.Write "    <tr class='tdbg' onmouseout=""this.className='tdbg'"" onmouseover=""this.className='tdbgmouseover'"">"& vbCrLf
            Response.Write "      <td width='30' align='center'><input name='ID' type='checkbox' onclick='unselectall()' value='" & rs("ID") & "'></td>"& vbCrLf
            Response.Write "      <td width='30' align='center'>" & rs("ID") & "</td>"& vbCrLf
			Response.Write "      <td><a href='Admin_Vote.asp?Action=Modify&ID=" & rs("ID") & "'>" & rs("Title") & "</a></td>"& vbCrLf
            Response.Write "      <td width='60' align='center'>"& vbCrLf
			If rs("IsSelected") = True Then
                Response.Write "<font color=green>启用</font>"
            Else
                Response.Write "<font color=red>停止</font>"
            End If
            Response.Write "      </td>"& vbCrLf
            Response.Write "      <td width='60' align='center'>"& vbCrLf
            If rs("VoteType") = 0 Then
                Response.Write "单选"
            ElseIf rs("VoteType") = 1 Then
                Response.Write "多选"
            End If
            Response.Write "      </td>"& vbCrLf
            Response.Write "      <td align='center'>" & rs("VoteTime") & "</td>"& vbCrLf
            Response.Write "      <td width='80' align='center'>"& vbCrLf
			Response.Write "      <a href='Admin_Vote.asp?Action=Modify&ID=" & rs("ID") & "'>修改</a>&nbsp;"
            Response.Write "      <a href='Admin_Vote.asp?Action=Del&ID=" & rs("ID") & "' onClick=""return confirm('确定要删除此调查吗？');"">删除</a>&nbsp;"
            Response.Write "      </td>"& vbCrLf
            Response.Write "    </tr>"& vbCrLf
            rs.MoveNext
        Loop
    End If
    rs.Close
    Set rs = Nothing
    Response.Write "</table>"& vbCrLf
    Response.Write "<table width='100%' border='0' cellpadding='0' cellspacing='0'>"& vbCrLf
    Response.Write "  <tr>"& vbCrLf
    Response.Write "    <td width='130' height='30'><input name='chkAll' type='checkbox' id='chkAll' onclick='CheckAll(this.form)' value='checkbox'>选中所有的调查</td><td>"& vbCrLf
    Response.Write "<input type='submit' value='删除选定的调查' name='submit' onClick=""document.myform.Action.value='Del'"">&nbsp;&nbsp;"
	Response.Write "<input type='submit' value='启用调查' name='submit1' onClick=""document.myform.Action.value='SetNew'"">&nbsp;&nbsp;"
	Response.Write "<input type='submit' value='停止调查' name='submit2' onClick=""document.myform.Action.value='CancelNew'"">&nbsp;&nbsp;"
    Response.Write "<input name='Action' type='hidden' id='Action' value=''>"
    Response.Write "  </td></tr>"& vbCrLf
    Response.Write "</table>"& vbCrLf
    Response.Write "</td>"& vbCrLf
    Response.Write "</form></tr></table>"& vbCrLf
	Response.Write "<br><b>说明：</b><br>&nbsp;&nbsp;&nbsp;&nbsp;只有将调查设为最新调查后才会在前台显示"
End Sub


Sub ShowJS_AddModify()
    Response.Write "<script language = 'JavaScript'>" & vbCrLf
    Response.Write "function CheckForm(){" & vbCrLf
    Response.Write "  if (document.myform.Title.value==''){" & vbCrLf
    Response.Write "     alert('调查主题不能为空！');" & vbCrLf
    Response.Write "     document.myform.Title.focus();" & vbCrLf
    Response.Write "     return false;" & vbCrLf
    Response.Write "  }" & vbCrLf
    Response.Write "  return true;  " & vbCrLf
    Response.Write "}" & vbCrLf
    Response.Write "function SetVoteNum(type){" & vbCrLf
    Response.Write "  if (type=='1'){" & vbCrLf
    Response.Write "     document.getElementById('VoteNum').style.display='';" & vbCrLf
    Response.Write "     document.getElementById('VoteNumtips').style.display='';" & vbCrLf	
    Response.Write "  }" & vbCrLf
    Response.Write "  else{" & vbCrLf
    Response.Write "     document.getElementById('VoteNum').style.display='None';" & vbCrLf
    Response.Write "     document.getElementById('VoteNumtips').style.display='None';" & vbCrLf	
    Response.Write "  }" & vbCrLf	
    Response.Write "}" & vbCrLf
    Response.Write "</script>" & vbCrLf		
End Sub
Sub Addtr(TempNum)
    Response.Write "<script language = 'JavaScript'>" & vbCrLf
    Response.Write "  var count="&TempNum&";" & vbCrLf	
    Response.Write "  function AddRow(){" & vbCrLf
    Response.Write "   if (count>15){" & vbCrLf
    Response.Write "   alert('最多只能添加15项')" & vbCrLf	
    Response.Write "   }" & vbCrLf
    Response.Write "   else{" & vbCrLf			
    Response.Write "   var tr = document.createElement('tr');" & vbCrLf
    Response.Write "   var td1 = document.createElement('td');" & vbCrLf
    Response.Write "   var td2 = document.createElement('td');" & vbCrLf	
    Response.Write "   var td3 = document.createElement('td');" & vbCrLf	
    Response.Write "   var td4 = document.createElement('td');" & vbCrLf			
    Response.Write "   var input1 = document.createElement('input');" & vbCrLf	
    Response.Write "   var input2 = document.createElement('input');" & vbCrLf			
    Response.Write "   var Items = document.createTextNode('选项'+count+'：')" & vbCrLf
    Response.Write "   var Nums = document.createTextNode('票数：');" & vbCrLf
    Response.Write "   tr.setAttribute('class','tdbg');" & vbCrLf	
    Response.Write "   td1.setAttribute('width','20%');" & vbCrLf	
    Response.Write "   td1.setAttribute('align','right');" & vbCrLf		
    Response.Write "   td2.setAttribute('width','35%');" & vbCrLf			
    Response.Write "   td3.setAttribute('width','10%');" & vbCrLf	
    Response.Write "   td3.setAttribute('align','right');" & vbCrLf					
    Response.Write "   td4.setAttribute('width','80');" & vbCrLf			
    Response.Write "   input1.setAttribute('type','text');" & vbCrLf
    Response.Write "   input1.setAttribute('name','select'+count);" & vbCrLf
    Response.Write "   input1.setAttribute('size','36');" & vbCrLf		
    Response.Write "   input2.setAttribute('type','text');" & vbCrLf
    Response.Write "   input2.setAttribute('name','answer'+count);" & vbCrLf
    Response.Write "   input2.setAttribute('size','10');" & vbCrLf		
    Response.Write "   td1.appendChild(Items);" & vbCrLf	
    Response.Write "   td2.appendChild(input1);" & vbCrLf			
    Response.Write "   td3.appendChild(Nums);" & vbCrLf		
    Response.Write "   td4.appendChild(input2);" & vbCrLf				
    Response.Write "   tr.appendChild(td1);" & vbCrLf
    Response.Write "   tr.appendChild(td2);" & vbCrLf
    Response.Write "   tr.appendChild(td3);" & vbCrLf	
    Response.Write "   tr.appendChild(td4);" & vbCrLf	
    Response.Write "   var AddRow = document.getElementById('AddRow');" & vbCrLf
    Response.Write "   AddRow.appendChild(tr);" & vbCrLf
    Response.Write "   count++;" & vbCrLf	
    Response.Write "  }" & vbCrLf
    Response.Write "  }" & vbCrLf	
    Response.Write " </script>" & vbCrLf
End Sub


Sub Add()
    Dim i
    Call ShowJS_AddModify
    Response.Write "<form method='POST' name='myform' onSubmit='return CheckForm();' action='Admin_Vote.asp' target='_self'>"& vbCrLf
    Response.Write "  <table width='100%' border='0' align='center' cellpadding='2' cellspacing='1' class='border'>"& vbCrLf
    Response.Write "    <tr class='title'>"& vbCrLf
    Response.Write "      <td height='22' class='title' colspan=4 align=center><b>添 加 调 查</b></td>"& vbCrLf
    Response.Write "    </tr>"& vbCrLf
    Response.Write "    <tr class='tdbg'>"& vbCrLf
    Response.Write "      <td align='right'>调查主题：</td>"& vbCrLf
    Response.Write "      <td colspan='3'>"& vbCrLf
    Response.Write "        <input type='text' maxlength='255' name='Title' size='40' />"& vbCrLf
    Response.Write "      </td>"& vbCrLf
    Response.Write "    </tr>"& vbCrLf
    For i = 1 To 8
        Response.Write "    <tr class='tdbg'>"& vbCrLf
        Response.Write "      <td width='20%' align='right'>选项" & i & "：</td>"& vbCrLf
        Response.Write "      <td width='35%'>"& vbCrLf
        Response.Write "        <input type='text' name='select" & i & "' size='36'>"& vbCrLf
        Response.Write "      </td>"& vbCrLf
        Response.Write "      <td width='10%' align='right'>票数：</td>"& vbCrLf
        Response.Write "      <td width='35%' width='80'>"& vbCrLf
        Response.Write "        <input type='text' name='answer" & i & "' size='10'>"& vbCrLf
        Response.Write "      </td>"& vbCrLf
        Response.Write "    </tr>"& vbCrLf

    Next
    Call Addtr(i)		
    Response.Write "    <tbody id='AddRow' class='tdbg'>"& vbCrLf
    Response.Write "</tbody>"	& vbCrLf
    Response.Write "    <tr class='tdbg'><td align='right'><a onclick='AddRow()' style=""cursor:hand;color:#f00;"">>>继续添加</a></td><td colspan='5'></td></tr>"& vbCrLf		
    Response.Write "    <tr class='tdbg'>"& vbCrLf
    Response.Write "      <td align='right'>调查类型：</td>"& vbCrLf
    Response.Write "      <td colspan='5'>"& vbCrLf
    Response.Write "        <select onchange=""SetVoteNum(this.options[this.selectedIndex].value)""  name='VoteType' id='VoteType'>"
    Response.Write "          <option value='0' selected>单选</option>"& vbCrLf
    Response.Write "          <option value='1'>多选</option>"& vbCrLf
    Response.Write "        </select>"& vbCrLf
    Response.Write "     <input name='VoteNum'  style='display:none' type='text' value='0' onclick=""this.value=''"" title='设置可选票数,0为不限制' id='VoteNum' size='2' maxlength='2'><font id='VoteNumtips'  color='#FF0000' style='display:none'> （最大可选票数，0为不限制）</font></td>"
    Response.Write "    </tr>"& vbCrLf
    Response.Write "    <tr class='tdbg'>"& vbCrLf
    Response.Write "      <td align='right'>发布时间：</td>"& vbCrLf
    Response.Write "      <td colspan='3'>"& vbCrLf
    Response.Write "        <input name='VoteTime' type='text' id='VoteTime' value='" & Now() & "' size='20' maxlength='20'>"& vbCrLf
    Response.Write "      </td>"& vbCrLf
    Response.Write "    </tr>"& vbCrLf
    Response.Write "    <tr class='tdbg'>"& vbCrLf
    Response.Write "      <td align='right'>&nbsp;</td>"& vbCrLf
    Response.Write "      <td colspan='3'>"& vbCrLf
    Response.Write "        <input name='IsSelected' type='checkbox' id='IsSelected' value='yes' checked>"
    Response.Write "        启用本调查</td>"& vbCrLf
    Response.Write "    </tr>"& vbCrLf
    Response.Write "    <tr class='tdbg'>"& vbCrLf
    Response.Write "      <td colspan=4 align=center>"& vbCrLf
    Response.Write "        <input name='Action' type='hidden' id='Action' value='SaveAdd'>"
    Response.Write "        <input name='Submit' type='submit' id='Submit' value=' 添 加 '>"
    Response.Write "        &nbsp;"
    Response.Write "        <input  name='Reset' type='reset' id='Reset' value=' 清 除 '>"
    Response.Write "      </td>"& vbCrLf
    Response.Write "    </tr>"& vbCrLf
    Response.Write "  </table>"& vbCrLf
    Response.Write "</form>"& vbCrLf
End Sub


Sub Modify()
    Dim rs, sql
    Dim i
    Dim TempNum
    TempNum=1	
    If ID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请指定要修改的调查ID！</li>"
        Exit Sub
    Else
        ID = PE_CLng(ID)
    End If
    sql = "select * from PW_Vote where ID=" & ID
    Set rs = Conn.Execute(sql)
    If rs.BOF And rs.EOF Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>找不到指定的调查！</li>"
        rs.Close
        Set rs = Nothing
        Exit Sub
    End If

    Call ShowJS_AddModify

    Response.Write "<form method='POST' name='myform' onSubmit='return CheckForm();' action='Admin_Vote.asp' target='_self'>"& vbCrLf
    Response.Write "  <table width='100%' border='0' align='center' cellpadding='2' cellspacing='1' class='border'>"& vbCrLf
    Response.Write "    <tr class='title'>"& vbCrLf
    Response.Write "      <td height='22' class='title' colspan=4 align=center><b>修 改 调 查</b></td>"& vbCrLf
    Response.Write "    </tr>"& vbCrLf
    Response.Write "    <tr class='tdbg'>"& vbCrLf
    Response.Write "      <td align='right'>调查主题：</td>"& vbCrLf
    Response.Write "      <td colspan='3'>"& vbCrLf
    Response.Write "        <input type='text' maxlength='255' name='Title' size='40' value='" & rs("Title") & "' />"
    Response.Write "      </td>"& vbCrLf
    Response.Write "    </tr>"& vbCrLf
    For i = 1 To 15
        If rs("select" & i) <>"" then
            Response.Write "    <tr class='tdbg'>"& vbCrLf
            Response.Write "      <td width='20%' align='right'>选项" & i & "：</td>"& vbCrLf
            Response.Write "      <td width='35%' >"& vbCrLf
            Response.Write "        <input type='text' name='select" & i & "' value='" & rs("select" & i) & "' size='36'>"& vbCrLf
            Response.Write "      </td>"& vbCrLf
            Response.Write "      <td width='10%' align='right'>票数：</td>"& vbCrLf
            Response.Write "      <td width='35%'>"& vbCrLf
            Response.Write "        <input type='text' name='answer" & i & "' value='" & rs("answer" & i) & "' size='10'>"
            Response.Write "      </td>"& vbCrLf
            Response.Write "    </tr>"& vbCrLf
            TempNum = TempNum + 1			
        End IF	
    Next
    Call Addtr(TempNum)		
    Response.Write "    <tbody id='AddRow' class='tdbg'>"& vbCrLf
    Response.Write "</tbody>"& vbCrLf	
    Response.Write "    <tr class='tdbg'><td align='right' cols=5><a onclick='AddRow()' style=""cursor:hand;color:#f00;"">>>继续添加</a></td><td colspan='5'></td></tr>"	& vbCrLf	
    Response.Write "    <tr class='tdbg'>"& vbCrLf
    Response.Write "      <td align='right'>调查类型：</td>"& vbCrLf
    Response.Write "      <td colspan='3'>"& vbCrLf
    Response.Write "        <select name='VoteType' onchange=""SetVoteNum(this.options[this.selectedIndex].value)""   id='VoteType'>"
    Response.Write "          <option value='0' "
    If rs("VoteType") = 0 Then Response.Write " selected"
    Response.Write "          >单选</option>"
    Response.Write "          <option value='1' "
    If rs("VoteType") = 1 Then Response.Write " selected"
    Response.Write "          >多选</option>"
    Response.Write "        </select>"
    Response.Write "     <input name='VoteNum' "
    If rs("VoteType") = "Single" Then Response.Write " style='display:none'"
    Response.Write "  type='text' value='"& rs("VoteNum")&"' onclick=""this.value=''"" title='设置可选票数,0为不限制' id='VoteNum' size='2' maxlength='2'><font id='VoteNumtips'  color='#FF0000' "
    If rs("VoteType") = "Single" Then Response.Write "  style='display:none'"
    Response.Write "	  > （最大可选票数，0为不限制）</font></td>"
    Response.Write "    </tr>"	& vbCrLf
    Response.Write "    <tr class='tdbg'>"& vbCrLf
    Response.Write "      <td align='right'>发布时间：</td>"& vbCrLf
    Response.Write "      <td colspan='3'>"& vbCrLf
    Response.Write "        <input name='VoteTime' type='text' id='VoteTime' value='" & rs("VoteTime") & "' size='20' maxlength='20'>"& vbCrLf
    Response.Write "      </td>"& vbCrLf
    Response.Write "    </tr>"& vbCrLf
    Response.Write "    <tr class='tdbg'>"& vbCrLf
    Response.Write "      <td align='right'>&nbsp;</td>"& vbCrLf
    Response.Write "      <td colspan='3'>"& vbCrLf
    Response.Write "        <input name='IsSelected' type='checkbox' id='IsSelected' value='yes' "
    If rs("IsSelected") = True Then Response.Write " checked"
    Response.Write "        >启用本调查</td>"& vbCrLf
    Response.Write "    </tr>"& vbCrLf
    Response.Write "    <tr class='tdbg'>"& vbCrLf
    Response.Write "      <td height='40' colspan=4 align=center>"& vbCrLf
    Response.Write "        <input name='ID' type='hidden' id='ID' value='" & ID & "'>"
    Response.Write "        <input name='Action' type='hidden' id='Action' value='SaveModify'>"
    Response.Write "        <input name='Submit' type='submit' id='Submit' value=' 保 存 '>"
    Response.Write "      </td>"& vbCrLf
    Response.Write "    </tr>"& vbCrLf
    Response.Write "  </table>"& vbCrLf
    Response.Write "</form>"& vbCrLf
    rs.Close
    Set rs = Nothing
End Sub


Sub SaveVote()
    Dim Title, VoteTime, VoteType, IsSelected, VoteNum
    Dim rs, sql
    Dim i
    Title = GetForm("Title")
    VoteTime = PE_CDate(GetForm("VoteTime"))
    VoteType = PE_Clng(GetForm("VoteType"))
    IsSelected = GetForm("IsSelected")
    VoteNum = PE_Clng(GetForm("VoteNum"))	
    If Title = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>调查主题不能为空！</li>"
    End If
    If FoundErr = True Then
        Exit Sub
    End If

    If IsSelected = "yes" Then
        IsSelected = True
    Else
        IsSelected = False
    End If

    Set rs = Server.CreateObject("adodb.recordset")
    If Action = "SaveAdd" Then
        sql = "select top 1 * from PW_Vote"
        rs.Open sql, Conn, 1, 3
        rs.addnew
    ElseIf Action = "SaveModify" Then
        If ID = "" Then
            FoundErr = True
            ErrMsg = ErrMsg & "<li>不能确定调查ID</li>"
            Exit Sub
        Else
            sql = "select * from PW_Vote where ID=" & PE_CLng(ID)
            Set rs = Server.CreateObject("adodb.recordset")
            rs.Open sql, Conn, 1, 3
            If rs.BOF And rs.EOF Then
                FoundErr = True
                ErrMsg = ErrMsg & "<li>找不到指定的调查！</li>"
                rs.Close
                Set rs = Nothing
                Exit Sub
            End If
        End If
    End If
    rs("Title") = Title
    Dim j
    j=1
    For i = 1 To 15
        If GetForm("select" & i)<>"" Then 
            rs("select" & j) = GetForm("select" & i)
            If GetForm("answer" & i) = "" Then
                rs("answer" & j) = 0
            Else
                rs("answer" & j) = PE_CLng(GetForm("answer" & i))
            End If
            j=j+1
        End If	
    Next
    For i = j To 15
        rs("select" & i) = ""	
        rs("answer" & i) = 0	
    Next
    rs("VoteTime") = VoteTime
    rs("VoteNum") = VoteNum	
    rs("VoteType") = VoteType
    rs("IsSelected") = IsSelected
    rs.Update
    rs.Close
    Set rs = Nothing
    Call CloseConn
    Response.Redirect "admin_Vote.asp"
End Sub

Sub SetProperty()
    Dim sqlProperty, rsProperty
    If ID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请指定调查ID</li>"
    End If
    If FoundErr = True Then
        Exit Sub
    End If
    If InStr(ID, ",") > 0 Then
        sqlProperty = "select * from PW_Vote where ID in (" & ID & ")"
    Else
        sqlProperty = "select * from PW_Vote where ID=" & ID
    End If
    Set rsProperty = Server.CreateObject("ADODB.Recordset")
    rsProperty.Open sqlProperty, Conn, 1, 3
    Do While Not rsProperty.EOF
        Select Case Action
        Case "SetNew"
            rsProperty("IsSelected") = True
        Case "CancelNew"
            rsProperty("IsSelected") = False
        Case "Del"
            rsProperty.Delete
        End Select
        rsProperty.Update
        rsProperty.MoveNext
    Loop
    rsProperty.Close
    Set rsProperty = Nothing
    Call CloseConn
    Response.Redirect ComeUrl
End Sub
%>