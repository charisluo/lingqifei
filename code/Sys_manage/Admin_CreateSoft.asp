<!--#include file="Admin_createcommon.asp"-->
<%
Dim PW_Content

Set PW_Content = New Soft
PW_Content.Init
tmpPageTitle = strPageTitle    '保存页面标题到临时变量中，以做为栏目及内容页循环生成时初始值
tmpNavPath = strNavPath
SoftID = Trim(Request("SoftID"))
Select Case Action
Case "CreateSoft"
    Call CreateSoft
Case "CreateSoft2"
    If AutoCreateType > 0 Then
        IsAutoCreate = True
        Call CreateSoft
        If ClassID > 0 Then
            ClassID = ParentPath & "," & ClassID
            Call CreateClass
        End If
        '在生成首页前，要将栏目ID置为0
        ClassID = 0
        arrChildID = 0
        Call CreateIndex

        Call CreateSiteIndex     '生成网站首页
    End If
Case "CreateClass"
    Call CreateClass
Case "CreateIndex"
    Call CreateIndex
Case Else
    FoundErr = True
    ErrMsg = ErrMsg & "<li>参数错误！</li>"
End Select

If FoundErr = True Then
    Call WriteErrMsg(ErrMsg, ComeUrl)
ELse
	Call ShowProcess
End If

Response.Write "</body></html>"
Set PW_Content = Nothing
Call CloseConn


Sub CreateSoft()
    'On Error Resume Next
    Dim sql, strFields, SoftPath
	If IsAutoCreate = False Then
		Response.Write "<b>正在生成" & ChannelShortName & "页面……请稍候！<font color='red'>在此过程中请勿刷新此页面！！！</font></b><br>"
		Response.Flush
    End If
    
    sql = "select * from PW_Soft where Deleted=" & PE_False & " and ChannelID=" & ChannelID
    Select Case CreateType
    Case 1 '选定的下载
        If IsValidID(SoftID) = False Then
            FoundErr = True
            ErrMsg = ErrMsg & "<li>请正确指定要生成的" & ChannelShortName & "ID</li>"
            Exit Sub
        End If
        If InStr(SoftID, ",") > 0 Then
            sql = sql & " and SoftID in (" & SoftID & ")"
        Else
            sql = sql & " and SoftID=" & SoftID
        End If
        strUrlParameter = "&SoftID=" & SoftID
    Case 2 '选定的栏目
        ClassID = PE_CLng(GetValue("ClassID"))
        If ClassID = 0 Then
            FoundErr = True
            ErrMsg = ErrMsg & "<li>请指定要生成的栏目ID</li>"
            Exit Sub
        End If
        Call GetClass
        If FoundErr = True Then Exit Sub
        If InStr(arrChildID, ",") > 0 Then
            sql = sql & " and ClassID in (" & arrChildID & ")"
        Else
            sql = sql & " and ClassID=" & ClassID
        End If
    Case 3 '所有下载
        
    Case 4 '最新的下载
        Dim TopNew
        TopNew = PE_CLng(GetValue("TopNew"))
        If TopNew <= 0 Then
            FoundErr = True
            ErrMsg = ErrMsg & "<li>请指定有效的数目！"
            Exit Sub
        End If
        sql = "select top " & TopNew & " * from PW_Soft where Deleted=" & PE_False & " and ChannelID=" & ChannelID
        strUrlParameter = "&TopNew=" & TopNew
    Case 5 '指定更新时间
        Dim BeginDate, EndDate
        BeginDate = GetValue("BeginDate")
        EndDate = GetValue("EndDate")
        If Not (IsDate(BeginDate) And IsDate(EndDate)) Then
            FoundErr = True
            ErrMsg = ErrMsg & "<li>请输入有效的日期！</li>"
            Exit Sub
        End If
        sql = sql & " and UpdateTime between #" & BeginDate & "# and #" & EndDate & "#"
        strUrlParameter = "&BeginDate=" & BeginDate & "&EndDate=" & EndDate
    Case 6 '指定ID范围
        Dim BeginID, EndID
        BeginID = GetValue("BeginID")
        EndID = GetValue("EndID")
        If Not (IsNumeric(BeginID) And IsNumeric(EndID)) Then
            FoundErr = True
            ErrMsg = ErrMsg & "<li>请输入数字！</li>"
            Exit Sub
        End If
        sql = sql & " and SoftID between " & BeginID & " and " & EndID
        strUrlParameter = "&BeginID=" & BeginID & "&EndID=" & EndID
    Case 9 '所有未生成的软件
        sql = "select top " & MaxPerPage_Create & " * from PW_Soft where Deleted=" & PE_False & " and ChannelID=" & ChannelID
		sql = sql & " and (CreateTime is null or CreateTime<=UpdateTime)"
    Case Else
        Response.Write "参数错误！"
        Exit Sub
    End Select
    Set rsSoft = Server.CreateObject("ADODB.Recordset")
    rsSoft.Open sql, Conn, 1, 1
    If rsSoft.Bof And rsSoft.EOF Then
        TotalCreate = 0
		iTotalPage = 0
        rsSoft.Close
        Set rsSoft = Nothing
        Exit Sub
    Else
        If CreateType = 9 Then
			TotalCreate = PE_Clng(Conn.Execute("select count(*) from PE_Soft where Deleted=" & PE_False & " and Status=3 and ChannelID=" & ChannelID & " and (CreateTime is null or CreateTime<=UpdateTime)")(0))
		Else
			TotalCreate = rsSoft.RecordCount
		End If
    End If
    
    strFileName = ChannelUrl_ASPFile & "/detial.asp"

    Call MoveRecord(rsSoft)
    Call ShowTotalCreate(ChannelItemUnit & ChannelShortName)

    Do While Not rsSoft.EOF
        FoundErr = False
        strPageTitle = tmpPageTitle
        strNavPath = tmpNavPath
        ClassID = rsSoft("ClassID")
        If ChannelID <> PrevChannelID Then
            Call GetChannel(ChannelID)
            PrevChannelID = ChannelID
        End If
        Call GetClass
		If temparticle <> article_template And article_template<>"" then
			strTemplate = GetTemplate(article_template) '分类的内容页
		Else
			strTemplate = GetTemplate(temparticle)'频道的内容页
		end If
        SoftID = rsSoft("SoftID")
        CurrentPage = 1
        
        SoftPath = ChannelUrl&"/"
        If CreateMultiFolder(SoftPath) = False Then
            Response.Write "请检查服务器。系统不能创建生成文件所需要的文件夹，"
            Exit Sub
        End If
        tmpFileName = SoftPath & GetItemFileName(rsSoft("UpdateTime"), SoftID)
            
        SoftName = Replace(Replace(Replace(Replace(rsSoft("SoftName") & "", "&nbsp;", " "), "&quot;", Chr(34)), "&gt;", ">"), "&lt;", "<")

        strHTML = strTemplate
        Call PW_Content.GetHtml_Soft
		Call GetModelFront_Html()
        Call WriteToFile(tmpFileName, strHTML)

        iCount = iCount + 1
        Response.Write "<li>成功生成第 <font color='red'><b>" & iCount & " </b></font> " & ChannelItemUnit & ChannelShortName & " 生成的ID </b><FONT color='Red'>" & SoftID & "</FONT> 地址 <a href='" & tmpFileName & "' target='_blank'>" & tmpFileName & "</a></li><br>" & vbCrLf
        Response.Flush
        '生成内容结束，更新内容的生成时间
        Conn.Execute ("update PW_Soft set CreateTime=" & PE_Now & " where SoftID=" & SoftID)

        If Response.IsClientConnected = False Then Exit Do
        If iCount Mod MaxPerPage_Create = 0 Then Exit Do
        rsSoft.MoveNext
    Loop
    rsSoft.Close
    Set rsSoft = Nothing
End Sub


%>