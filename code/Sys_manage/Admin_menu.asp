<!--#include file="Admin_Common.asp"-->
<%

Const NeedCheckComeUrl = True   '是否需要检查外部访问

Const PurviewLevel_Channel = 0   '0--不检查,1检查
Const PurviewLevel_Others = ""   '其他权限

Dim MenuID, rsMenu, sqlMenu

MenuID = GetValue("MenuID")
If IsValidID(MenuID) = False Then
    MenuID = ""
End If


Response.Write "<html><head><title>导航目录管理</title>" & vbCrLf
Response.Write "<meta http-equiv='Content-Type' content='text/html; charset=gb2312'>" & vbCrLf
Response.Write "<link href='Admin_Style.css' rel='stylesheet' type='text/css'>" & vbCrLf
Response.Write "</head>" & vbCrLf
Response.Write "<body leftmargin='2' topmargin='0' marginwidth='0' marginheight='0'>" & vbCrLf
Response.Write "<table width='100%' border='0' align='center' cellpadding='2' cellspacing='1' class='border'>" & vbCrLf
call ShowPageTitle("导 航 目 录 管 理")

Response.Write "<table width='100%' border='0' align='center' cellpadding='2' cellspacing='1' class='border'>" & vbCrLf
Response.Write "  <tr class='tdbg'>" & vbCrLf
Response.Write "    <td width='70' height='30'><strong>管理导航：</strong></td>" & vbCrLf
Response.Write "    <td>"
Response.Write "    <a href='Admin_menu.asp'>导航目录管理首页</a>&nbsp;|&nbsp;"
Response.Write "    <a href='Admin_menu.asp?Action=Add'>添加导航目录</a>&nbsp;"
Response.Write "    </td>" & vbCrLf
Response.Write "  </tr>" & vbCrLf
Response.Write "  </table>" & vbCrLf

Select Case Action
	Case "Add"
		Call Add
	Case "Modify"
		Call Modify
	Case "SaveAdd","SaveModify"
		Call SaveMenu
	Case "Del"
		Call Del
	Case Else
		Call main
End Select

If FoundErr = True Then
    Call WriteErrMsg(ErrMsg, ComeUrl)
End If
Response.Write "</body></html>"
Call CloseConn

Sub main
    Dim rsMenu, sqlMenu
    Call ShowJS_Main("栏目导航")
    Response.Write "<br>" & vbCrLf
    Response.Write "<table width='100%' border='0' cellpadding='0' cellspacing='0'>" & vbCrLf
    Response.Write "  <tr>" & vbCrLf
    Response.Write "  <form name='myform' method='Post' action='Admin_Menu.asp' onsubmit='return ConfirmDel();'>" & vbCrLf
    Response.Write "    <td>" & vbCrLf
    Response.Write "      <table class='border' border='0' cellspacing='1' width='100%' cellpadding='0'>" & vbCrLf
    Response.Write "        <tr class='title' height='22'> " & vbCrLf
    Response.Write "          <td width='30' align='center'><strong>选中</strong></td>" & vbCrLf
    Response.Write "          <td width='150' align='center'><strong>导航名称</strong></td>" & vbCrLf
    Response.Write "          <td align='center'><strong>链接地址</strong></td>" & vbCrLf
    Response.Write "          <td width='60' align='center'><strong>排序号</strong></td>" & vbCrLf
    Response.Write "          <td width='150' align='center'><strong>操作</strong></td>" & vbCrLf
    Response.Write "        </tr>" & vbCrLf
    sqlMenu = "select * from PW_Menu order by MenuOrder asc"
	Set rsMenu = Server.CreateObject("ADODB.Recordset")
    rsMenu.Open sqlMenu, Conn, 1, 1
    If rsMenu.BOF And rsMenu.EOF Then
		Response.Write "<tr class='tdbg'><td colspan='20' align='center'><br>没有任何导航栏目！<br><br></td></tr>"
    Else
        Do While Not rsMenu.EOF
            Response.Write "        <tr class='tdbg' onmouseout=""this.className='tdbg'"" onmouseover=""this.className='tdbgmouseover'"">" & vbCrLf
            Response.Write "          <td align='center'>"
            Response.Write "            <input name='MenuID' type='checkbox' onclick='unselectall()' id='MenuID' value='" & rsMenu("MenuID") & "'>"
            Response.Write "          </td>" & vbCrLf
            Response.Write "          <td>&nbsp;<a href='Admin_Menu.asp?Action=Modify&MenuID=" & rsMenu("MenuID") & "'>"& rsMenu("MenuName") & "</a></td>"
            Response.Write "          <td>&nbsp;"& rsMenu("MenuUrl") & "</td>"
            Response.Write "          <td align='center'>&nbsp;"& rsMenu("MenuOrder") & "</td>"
            Response.Write "          <td align='center'>"
            Response.Write "            <a href='Admin_Menu.asp?Action=Modify&MenuID=" & rsMenu("MenuID") & "'>修改</a>"
            Response.Write "            <a href='Admin_Menu.asp?Action=Del&MenuID=" & rsMenu("MenuID") & "' onclick=""return confirm('确定要删除此导航栏目吗？');"">删除</a>"
            Response.Write "          </td>" & vbCrLf
            Response.Write "        </tr>" & vbCrLf
            rsMenu.MoveNext
        Loop
    End If
    rsMenu.Close
    Set rsMenu = Nothing
    Response.Write "      </table>" & vbCrLf
    Response.Write "      <table width='100%' border='0' cellpadding='0' cellspacing='0'>" & vbCrLf
    Response.Write "        <tr>" & vbCrLf
    Response.Write "          <td width='160' height='30'>"
    Response.Write "            <input name='chkAll' type='checkbox' id='chkAll' onclick='CheckAll(this.form)' value='checkbox'>选中本页所有友情链接"
    Response.Write "          </td>" & vbCrLf
    Response.Write "          <td>"
    Response.Write "            <input type='submit' value='删除选定链接' name='submit' onClick=""document.myform.Action.value='Del'"">&nbsp;"
    Response.Write "            <input name='Action' type='hidden' id='Action' value=''>"
    Response.Write "          </td>" & vbCrLf
    Response.Write "        </tr>" & vbCrLf
    Response.Write "      </table>" & vbCrLf
    Response.Write "    </td>" & vbCrLf
    Response.Write "  </form>" & vbCrLf
    Response.Write "  </tr>" & vbCrLf
    Response.Write "</table>" & vbCrLf
End Sub



Sub ShowJS_AddModify()
    Response.Write "<script language = 'JavaScript'>" & vbCrLf
    Response.Write "function CheckForm(){" & vbCrLf
    Response.Write "  if(document.myform.MenuName.value==''){" & vbCrLf
    Response.Write "    alert('请输入导航名称！');" & vbCrLf
    Response.Write "    document.myform.MenuName.focus();" & vbCrLf
    Response.Write "    return false;" & vbCrLf
    Response.Write "  }" & vbCrLf
    Response.Write "  if(document.myform.MenuUrl.value=='' || document.myform.MenuUrl.value=='http://'){" & vbCrLf
    Response.Write "    alert('请输入导航地址！');" & vbCrLf
    Response.Write "    document.myform.MenuUrl.focus();" & vbCrLf
    Response.Write "    return false;" & vbCrLf
    Response.Write "  }" & vbCrLf
    Response.Write "}" & vbCrLf
    Response.Write "</script>" & vbCrLf
End Sub

Sub Add
    Call ShowJS_AddModify
    Response.Write "<br><table width='100%'><tr><td align='left'>您现在的位置：<a href='Admin_menu.asp'>导航目录管理</a>&nbsp;&gt;&gt;&nbsp;添加导航目录</td></tr></table>"& vbCrLf
    Response.Write "<form method='post' name='myform' onsubmit='return CheckForm()' action='Admin_menu.asp'>"
    Response.Write "  <table border='0' cellpadding='2' cellspacing='1' align='center' width='100%' class='border'>"& vbCrLf
    Response.Write "    <tr class='title'>"& vbCrLf
    Response.Write "      <td height='22' colspan='2' align='center'><strong>添加导航目录</strong></td>"& vbCrLf
    Response.Write "    </tr>"& vbCrLf
    Response.Write "    <tr class='tdbg'>"& vbCrLf
    Response.Write "      <td width='150' align='right'><strong>导航名称：</strong></td>"& vbCrLf
    Response.Write "      <td align='left'>&nbsp;<input type='text' name='MenuName' id='MenuName' size='30' maxlength='50' value=''> <font color='#FF0000'> *</font></td>"& vbCrLf
    Response.Write "    </tr>"& vbCrLf	
    Response.Write "    <tr class='tdbg'>"& vbCrLf
    Response.Write "      <td align='right'><strong>链接地址：</strong></td>"& vbCrLf
    Response.Write "      <td align='left'>&nbsp;<input type='text' name='MenuUrl' id='MenuUrl' size='50' maxlength='100' value=''> <font color='#FF0000'>*</font></td>"& vbCrLf
    Response.Write "    </tr>"& vbCrLf	
    Response.Write "    <tr class='tdbg'>"& vbCrLf
    Response.Write "      <td align='right'><strong>排序号：</strong></td>"& vbCrLf
    Response.Write "      <td align='left'>&nbsp;<input type='text' name='MenuOrder' id='MenuOrder' size='3' maxlength='2' value=''> <font color='#FF0000'>*</font></td>"& vbCrLf
    Response.Write "    </tr>"& vbCrLf	
    Response.Write "    <tr class='tdbg'>"& vbCrLf
    Response.Write "      <td align='right'><strong>说明：</strong></td>"& vbCrLf
    Response.Write "      <td align='left'>&nbsp;<textarea name='MenuIntro' cols='50' rows='2'></textarea></td>"& vbCrLf
    Response.Write "    </tr>"& vbCrLf	
    Response.Write "    <tr class='tdbg'>"
    Response.Write "      <td height='40' colspan='2' align='center'>"
    Response.Write "        <input name='Action' type='hidden' id='Action' value='SaveAdd'>"
    Response.Write "        <input type='submit' value=' 确 定 ' name='submit'>&nbsp;&nbsp;"
    Response.Write "        <input type='reset' value=' 重 填 ' name='reset'>"
    Response.Write "      </td>"
    Response.Write "    </tr>"
    Response.Write "  </table>"
    Response.Write "</form>"
End Sub



Sub Modify
    If MenuID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请指定要修改的导航ID！</li>"
        Exit Sub
    Else
        MenuID = PE_CLng(MenuID)
    End If
    sqlMenu = "Select * from PW_Menu Where MenuID=" & MenuID
    Set rsMenu = Conn.Execute(sqlMenu)
	If rsMenu.BOF And rsMenu.EOF Then
		FoundErr = True
		ErrMsg = ErrMsg & "<li>找不到指定的类别！</li>"
		rsMenu.Close
		Set rsMenu = Nothing
		Exit Sub
   End If
   Call ShowJS_AddModify
    Response.Write "<br><table width='100%'><tr><td align='left'>您现在的位置：<a href='Admin_menu.asp'>导航目录管理</a>&nbsp;&gt;&gt;&nbsp;添加导航目录</td></tr></table>"& vbCrLf
    Response.Write "<form method='post' name='myform' onsubmit='return CheckForm()' action='Admin_menu.asp'>"
    Response.Write "  <table border='0' cellpadding='2' cellspacing='1' align='center' width='100%' class='border'>"& vbCrLf
    Response.Write "    <tr class='title'>"& vbCrLf
    Response.Write "      <td height='22' colspan='2' align='center'><strong>添加导航目录</strong></td>"& vbCrLf
    Response.Write "    </tr>"& vbCrLf
    Response.Write "    <tr class='tdbg'>"& vbCrLf
    Response.Write "      <td width='150' align='right'><strong>导航名称：</strong></td>"& vbCrLf
    Response.Write "      <td align='left'>&nbsp;<input type='text' name='MenuName' id='MenuName' size='30' maxlength='50' value='" & rsMenu("MenuName") & "'> <font color='#FF0000'> *</font></td>"& vbCrLf
    Response.Write "    </tr>"& vbCrLf	
    Response.Write "    <tr class='tdbg'>"& vbCrLf
    Response.Write "      <td align='right'><strong>链接地址：</strong></td>"& vbCrLf
    Response.Write "      <td align='left'>&nbsp;<input type='text' name='MenuUrl' id='MenuUrl' size='50' maxlength='100' value='" & rsMenu("MenuUrl") & "'> <font color='#FF0000'>*</font></td>"& vbCrLf
    Response.Write "    </tr>"& vbCrLf	
    Response.Write "    <tr class='tdbg'>"& vbCrLf
    Response.Write "      <td align='right'><strong>排序号：</strong></td>"& vbCrLf
    Response.Write "      <td align='left'>&nbsp;<input type='text' name='MenuOrder' id='MenuOrder' size='3' maxlength='2' value='" & rsMenu("MenuOrder") & "'> <font color='#FF0000'>*</font></td>"& vbCrLf
    Response.Write "    </tr>"& vbCrLf	
    Response.Write "    <tr class='tdbg'>"& vbCrLf
    Response.Write "      <td align='right'><strong>说明：</strong></td>"& vbCrLf
    Response.Write "      <td align='left'>&nbsp;<textarea name='MenuIntro' cols='50' rows='2'>" & rsMenu("MenuIntro") & "</textarea></td>"& vbCrLf
    Response.Write "    </tr>"& vbCrLf	
    Response.Write "    <tr class='tdbg'>"
    Response.Write "      <td height='40' colspan='2' align='center'>"
    Response.Write "        <input name='Action' type='hidden' id='Action' value='SaveModify'><input name='MenuID' type='hidden' id='MenuID' value='"& MenuID &"'>"
    Response.Write "        <input type='submit' value=' 修 改 ' name='submit'>&nbsp;&nbsp;"
    Response.Write "        <input type='reset' value=' 重 填 ' name='reset'>"
    Response.Write "      </td>"
    Response.Write "    </tr>"
    Response.Write "  </table>"
    Response.Write "</form>"
	rsMenu.Close
    Set rsMenu = Nothing
End Sub

Sub SaveMenu
	Dim MenuName, MenuUrl, MenuOrder, MenuIntro
    MenuName = GetForm("MenuName")
    MenuUrl = GetForm("MenuUrl")
    MenuOrder = PE_Clng(GetForm("MenuOrder"))
    MenuIntro = GetForm("MenuIntro")
    If MenuName = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>导航名称不能为空！</li>"
    End If
    If MenuUrl = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>链接地址不能为空！</li>"
    End If
    If MenuOrder = 0  Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>排序号不能为空！</li>"
    End If
	
    Set rsMenu = Server.CreateObject("adodb.recordset")

    If Action = "SaveAdd" Then
        sqlMenu = "select top 1 * from PW_Menu where MenuName='" & MenuName & "' and MenuUrl='" & MenuUrl & "'"
        rsMenu.Open sqlMenu, Conn, 1, 3
        If Not (rsMenu.BOF And rsMenu.EOF) Then
            FoundErr = True
            ErrMsg = ErrMsg & "<li>你要添加的导航已经存在！</li>"
            rsMenu.Close
            Set rsMenu = Nothing
            Exit Sub
        End If
        rsMenu.addnew
		rsMenu("MenuID") = GetNewID("PW_Menu", "MenuID")
    ElseIf Action = "SaveModify" Then
        If MenuID = "" Then
            FoundErr = True
            ErrMsg = ErrMsg & "<li>不能确定栏目导航ID</li>"
            Exit Sub
        End If
        sqlMenu = "select * from PW_Menu where MenuID=" & MenuID
        rsMenu.Open sqlMenu, Conn, 1, 3
        If rsMenu.BOF And rsMenu.EOF Then
            FoundErr = True
            ErrMsg = ErrMsg & "<li>找不到指定的栏目导航！</li>"
            rsMenu.Close
            Set rsMenu = Nothing
            Exit Sub
        End If
    End If
    rsMenu("MenuName") = MenuName
    rsMenu("MenuUrl") = MenuUrl
    rsMenu("MenuOrder") = MenuOrder
    rsMenu("MenuIntro") = MenuIntro
    rsMenu.Update
    rsMenu.Close
    Set rsMenu = Nothing
    Call CloseConn
    Response.Redirect "Admin_Menu.asp"

End Sub

Sub Del
	Dim sqlDel
	
    If MenuID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请指定要删除的导航ID！</li>"
        Exit Sub
    Else
        MenuID = PE_CLng(MenuID)
    End If
    If InStr(MenuID, ",") > 0 Then
        sqlDel = "Delete * from PW_Menu where MenuID in (" & MenuID & ")"
    Else
        sqlDel = "Delete * from PW_Menu where MenuID=" & MenuID
    End If
	Conn.execute sqlDel
	Response.Redirect ComeUrl
End Sub


%>