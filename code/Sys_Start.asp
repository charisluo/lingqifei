<%@language="vbscript" codepage="936" %>
<%
Option Explicit
Response.Buffer = True
Dim BeginTime
BeginTime = Timer
%>
<!--#include file="Conn.asp"-->
<!--#include file="Config.asp"-->
<!--#include file="Include/PW_Common_All.asp"-->
<!--#include file="Include/PW_Common_Security.asp"-->
<%
Dim UserTrueIP,ScriptName
Dim Site_Sn
Dim strInstallDir
Dim Template_Dir

'网站配置相关的变量
Dim SiteName, CompanyName,Address,NetAddress,contact,Tel,Fax,Phone,Email,PostCode,QQ,CopyRight,Meta_Keywords,Meta_Description,Sitestate,Closemsg,ADDir,DaysOfNew,HitsOfHot,Modules,UploadDir,MaxFileSize,UpFileType,Site_Template,Site_TemplateArr,templateFile_Url
Dim ObjInstalled_FSO, fso, hf
'用户相关的变量
Dim UserLogined, UserID, UserName, UserPassword, GroupID, LoginTimes, RegTime,LastLoginTime, LastLoginIP, TrueName, Sex, Sexstr, U_Address, U_Phone,U_Fax, U_Email, U_ZipCode
Dim GroupName, GroupIntro
'生成HTML
DIm UseCreateHTML,AutoCreateType,HTMLDIR
'分页时所用变量
Dim FileName, strFileName, MaxPerPage, CurrentPage, totalPut

'搜索用变量
Dim SearchType, strField, Keyword

Dim Action, FoundErr, ErrMsg, ComeUrl
'XML相关的变量
Dim XmlDoc, XMLDOM, Node


'正则表达式相关的变量
Dim regEx, Match, Match2, Matches, Matches2
Set regEx = New RegExp
regEx.IgnoreCase = True
regEx.Global = True
regEx.MultiLine = True
ScriptName = Trim(Request.ServerVariables("SCRIPT_NAME"))
UserTrueIP = Request.ServerVariables("HTTP_X_FORWARDED_FOR")
If UserTrueIP = "" Then UserTrueIP = Request.ServerVariables("REMOTE_ADDR")
UserTrueIP = ReplaceBadChar(UserTrueIP)
If EnableStopInjection = True Then
    If Request.QueryString <> "" Then Call StopInjection(Request.QueryString)
    If Request.Cookies <> "" Then Call StopInjection(Request.Cookies)
    If LCase(Mid(ScriptName, InStrRev(ScriptName, "/") + 1)) <> "upfile.asp" Then
        Call StopInjection2(Request.Form)
    End If
End If
FoundErr = False
ErrMsg = ""
Call OpenConn
Call GetSiteConfig
Call InitVar

Sub InitVar()
	strInstallDir = InstallDir
	Template_Dir = InStallDir& "template/"&TemplateDir&"/"
    If GetValue("page") <> "" Then
        CurrentPage = PE_CLng(GetValue("page"))
    Else
        CurrentPage = 1
    End If
    MaxPerPage = PE_CLng(GetValue("MaxPerPage"))
    If MaxPerPage <= 0 Then MaxPerPage = 20
    SearchType = PE_CLng(GetValue("SearchType"))
    strField = GetValue("Field")
    Keyword = ReplaceBadChar(GetValue("keyword"))
    ObjInstalled_FSO = IsObjInstalled(objName_FSO)
    If ObjInstalled_FSO = True Then
        Set fso = Server.CreateObject(objName_FSO)
    Else
        Response.Write "<li>FSO组件不可用，各种与FSO相关的功能都将出错！</li>"
    End If
        
    ComeUrl = FilterJs(GetValue("ComeUrl"))
    If ComeUrl = "" Then
        ComeUrl = FilterJs(Trim(Request.ServerVariables("HTTP_REFERER")))
    End If
    Action = ReplaceBadChar(GetValue("Action"))
    FoundErr = False
    ErrMsg = ""

    Site_Sn = Replace(Replace(LCase(Request.ServerVariables("SERVER_NAME") & InstallDir), "/", ""), ".", "")
	UserLogined = CheckUserLogined()
    
    Set XmlDoc = CreateObject("Microsoft.XMLDOM")
    XmlDoc.async = False

End Sub

Sub StopInjection(Values)
    Dim FoundInjection
    regEx.Pattern = "'|;|#|([\s\b+()]+(select|update|insert|delete|declare|@|exec|dbcc|alter|drop|create|backup|if|else|end|and|or|add|set|open|close|use|begin|retun|as|go|exists)[\s\b+]*)"
    Dim sItem, sValue
    For Each sItem In Values
        sValue = Values(sItem)
        If regEx.Test(sValue) Then
            FoundInjection = True
            Response.Write "很抱歉，由于您提交的内容中含有危险的SQL注入代码，致使本次操作无效！ "
            Response.Write "<br>字段名：" & sItem
            Response.Write "<br>字段值：" & sValue
            Response.Write "<br>关键字："
            Set Matches = regEx.Execute(sValue)
            For Each Match In Matches
                Response.Write FilterJS(Match.value)
            Next
            Response.Write "<br><br>如果您是正常提交仍出现上面的提示，请联系站长修改Config.asp文件的第2行，暂时禁用掉防SQL注入功能，操作完成后再打开。"
            
        End If
    Next
    If FoundInjection = True Then
        Response.End
    End If
End Sub

Sub StopInjection2(Values)
    Dim FoundInjection
    regEx.Pattern = "[';#()][\s+()]*(select|update|insert|delete|declare|@|exec|dbcc|alter|drop|create|backup|if|else|end|and|or|add|set|open|close|use|begin|retun|as|go|exists)[\s+]*"
    Dim sItem, sValue
    For Each sItem In Values
        sValue = Values(sItem)
        If regEx.Test(sValue) Then
            FoundInjection = True
            Response.Write "很抱歉，由于您提交的内容中含有危险的SQL注入代码，致使本次操作无效！ "
            Response.Write "<br>字段名：" & sItem
            Response.Write "<br>字段值：" & sValue
            Response.Write "<br>关键字："
            Set Matches = regEx.Execute(sValue)
            For Each Match In Matches
                Response.Write FilterJS(Match.value)
            Next
            Response.Write "<br><br>如果您是正常提交仍出现上面的提示，请联系站长，暂时禁用掉防SQL注入功能，操作完成后再打开。"
            
        End If
    Next
    If FoundInjection = True Then
        Response.End
    End If
End Sub

Sub GetSiteConfig()
    On Error Resume Next
    Dim rsConfig
    Set rsConfig = Conn.Execute("select * from PW_config")
    If rsConfig.BOF And rsConfig.EOF Then
        rsConfig.Close
        Set rsConfig = Nothing
        Response.Write "网站配置数据丢失！系统无法正常运行！"
        Response.End
        Exit Sub
    End If
	''''''替换系统变量
    SiteName = rsConfig("SiteName")
	CompanyName = rsConfig("CompanyName")
	Address = rsConfig("Address")
	NetAddress = rsConfig("NetAddress")
	contact = rsConfig("contact")
	Tel = rsConfig("Tel")
	Fax = rsConfig("Fax")
	Phone = rsConfig("Phone")
	Email = rsConfig("Email")
	PostCode = rsConfig("PostCode")
	QQ = rsConfig("QQ")
	CopyRight = rsConfig("CopyRight")
	Meta_Keywords = rsConfig("Meta_Keywords")
	Meta_Description = rsConfig("Meta_Description")
	Sitestate = rsConfig("Sitestate")
	Closemsg = rsConfig("Closemsg")
	UseCreateHTML = PE_Clng(rsConfig("UseCreateHTML"))
	AutoCreateType = rsConfig("AutoCreateType")
	HTMLDIR = rsConfig("htmlDir")
	ADDir = rsConfig("ADDir")
	HitsOfHot = rsConfig("HitsOfHot")
	DaysOfNew = rsConfig("DaysOfNew")
	Modules = rsConfig("Modules")
	UploadDir = rsConfig("UploadDir")
	MaxFileSize = rsConfig("MaxFileSize")
	UpFileType = rsConfig("UpFileType")
	Site_Template = rsConfig("Site_Template")
	Site_TemplateArr = Split(trim(Site_Template),",")
    rsConfig.Close
    Set rsConfig = Nothing
End Sub
%>